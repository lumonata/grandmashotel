<?php
    /*
    Title: Custom
    Preview: template.png
    Author: Cham Freaks
    Author Url: http://www.lumonata.com
    Description: Custom template for jasmine elizabeth
    */

    header('Access-Control-Allow-Origin: *');
    require_once 'functions.php';



    //-- SET TEMPLATE
    if (isset($actions->action['is_use_ajax']) && $actions->action['is_use_ajax']['func_name'][0]) {
        $main_content = set_content();

        $template_in_use = TEMPLATE_PATH.'/ajax.html';
        set_template($template_in_use);
        add_block('mainblock', 'mBlock');
        add_variable('content_area', $main_content);
    } elseif (is_home()) {
        $template_in_use = TEMPLATE_PATH.'/index.html';
        set_template($template_in_use);
        add_block('mainblock', 'mBlock');
        add_variable('google_analytic', get_meta_data('g_analytic', 'global_setting'));
        add_variable('navigation', get_first_navigation());
        add_variable('slider_top_home', get_first_slider_top());
        add_variable('slider_top_home_mobile', get_first_slider_mobile_top());
        add_variable('label_book', get_first_labelbook_top());
        add_variable('content_area_home', get_first_home_content());
        add_variable('gmaps', get_gmaps_n_book_block());
        add_variable('modal_book_now', get_modal_book_now());
        add_variable('modal_career', get_modal_career());
        $address = get_destination_address();
        foreach ($address as $key => $value) {
          if ($key == "seminyak") {
              add_variable('a_seminyak', get_additional_field($value, 'address', 'destinations'));
              add_variable('p_seminyak', get_additional_field($value, 'phone_number', 'destinations'));
          } elseif ($key == "legian") {
            add_variable('a_legian', get_additional_field($value, 'address', 'destinations'));
            add_variable('p_legian', get_additional_field($value, 'phone_number', 'destinations'));
          } else {
            add_variable('a_airport', get_additional_field($value, 'address', 'destinations'));
            add_variable('p_airport', get_additional_field($value, 'phone_number', 'destinations'));
          }
        }
    } elseif (is_special_offers()) {
        $template_in_use = TEMPLATE_PATH.'/index.html';
        set_template($template_in_use);
        add_block('mainblock', 'mBlock');
        add_variable('google_analytic', get_meta_data('g_analytic', 'global_setting'));
        add_variable('navigation', get_first_navigation());
        add_variable('special_label', get_special_offers_label());
        add_variable('special_label_mobile', get_special_offers_label_mobile());
        add_variable('content_area_home', get_first_special_content());
        add_variable('modal_book_now', get_modal_book_now());
        add_variable('modal_career', get_modal_career());
        add_actions('meta_title', 'GrandMas - Special Offers');
        $address = get_destination_address();
        foreach ($address as $key => $value) {
          if ($key == "seminyak") {
              add_variable('a_seminyak', get_additional_field($value, 'address', 'destinations'));
              add_variable('p_seminyak', get_additional_field($value, 'phone_number', 'destinations'));
          } elseif ($key == "legian") {
            add_variable('a_legian', get_additional_field($value, 'address', 'destinations'));
            add_variable('p_legian', get_additional_field($value, 'phone_number', 'destinations'));
          } else {
            add_variable('a_airport', get_additional_field($value, 'address', 'destinations'));
            add_variable('p_airport', get_additional_field($value, 'phone_number', 'destinations'));
          }
        }
    } else {
        global $db;
        $appname = trim(get_appname());

        $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND lsef=%s';
        $q = $db->prepare_query($s, 'destinations', $appname);
        $r = $db->do_query($q);
        $d = $db->fetch_array($r);
        $n = $db->num_rows($r);

        $post_id  = $d['larticle_id'];
        $post_sef = $d['lsef'];
        $sef      = get_uri_sef();

        add_variable('google_analytic', get_g_analytic($post_id));
        add_variable('pixel_script', get_pixel_script($post_id));
        add_variable('modal_book_now', get_modal_book_now());
        add_variable('modal_career', get_modal_career());
        add_variable('for_special_offers','bg-position-offers');

        $main_content = set_content($n, $d);

        if ($main_content == 404) {
            $template_in_use = TEMPLATE_PATH.'/template/page-404.html';
            set_template($template_in_use);
            add_block('mainblock', 'mBlock');
            add_variable('navigation', get_second_navigation($post_sef, '', $post_id));
            add_actions('include_css', 'get_custom_css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
            add_actions('include_js', 'get_custom_javascript', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js');
            add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/datepicker-custom.js');
            add_variable('message', 'Sorry, the page you are looking cannot be found');
            add_variable('instagram', get_instagram_photo());
        } elseif ($main_content == 4042) {
            header('location:http://'.SITE_URL);
        } else {
            if ($sef != 'gallery' && $sef != 'location' && $sef != 'book' ) {
                $template_in_use = TEMPLATE_PATH.'/destination.html';
                set_template($template_in_use);
                add_block('mainblock', 'mBlock');
            } elseif ($sef == 'gallery') {
                $template_in_use = TEMPLATE_PATH.'/gallery.html';
                set_template($template_in_use);
                add_block('mainblock', 'mBlock');
            } elseif ($sef == 'location') {
                $template_in_use = TEMPLATE_PATH.'/location.html';
                set_template($template_in_use);
                add_block('mainblock', 'mBlock');
            }
        }
    }



    add_variable('include_css', attemp_actions('include_css'));
    add_variable('include_js', attemp_actions('include_js'));

    add_variable('address', get_meta_data('address', 'global_setting'));
    add_variable('city', get_meta_data('city', 'global_setting'));
    add_variable('phone_number', get_meta_data('phone_number', 'global_setting'));
    add_variable('email_address', get_meta_data('email_address', 'global_setting'));

    add_variable('facebook_account', get_meta_data('facebook_account', 'global_setting'));
    add_variable('instagram_account', get_meta_data('instagram_account', 'global_setting'));
    add_variable('twitter_account', get_meta_data('twitter_account', 'global_setting'));

    if (is_special_offers()) {
        add_variable('meta_title', attemp_actions('meta_title'));
        add_variable('meta_keywords', run_actions('meta_keywords'));
        add_variable('meta_title', run_actions('meta_title'));
    } else {
        add_variable('meta_description', run_actions('meta_description'));
        add_variable('meta_keywords', run_actions('meta_keywords'));
        add_variable('meta_title', run_actions('meta_title'));
    }


    add_variable('web_title', web_title());
    add_variable('tagline', web_tagline());

    add_variable('og_url', run_actions('og_url'));
    add_variable('og_title', run_actions('og_title'));
    add_variable('og_image', run_actions('og_image'));
    add_variable('og_site_name', run_actions('og_site_name'));
    add_variable('og_description', run_actions('og_description'));

    add_variable('year', date('Y'));

    add_variable('site_url', 'http://'.SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);

    parse_template('mainblock', 'mBlock');
    print_template();
