$(document).ready(function() {

  var $attribute = $('[data-smart-affix]');
  var a = $attribute.prevObject["0"].URL;
  var c = a.split('#');
  var d = c[1];

  // var sm = $('.container-text-image[data-affix-after="' + d + '"]');
  if (d != '') {
    $('html, body').animate({
      scrollTop: $('#nav').offset().top - 100
    }, 1000);
  }



  $('.expand').on('click', function(event) {
    event.preventDefault();
    /* Act on the event */

    if ($(this).next().hasClass('desc')) {
      $(this).next().removeClass('desc');
    } else {
      $(this).next().addClass('desc');
    }
    // $('.container-data').isotope('layout');
  });

  $('.filter-button-group').on('click', 'li', function() {

    var filterValue = $(this).attr('data-filter');
    // console.log(filterValue);
    $('.container-data').isotope({
      filter: filterValue
    });
  });
});
