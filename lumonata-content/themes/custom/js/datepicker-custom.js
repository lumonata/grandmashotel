$(document).ready(function() {
  //DATEPICKER

  var check_out_date = '';
  var check_in_date = '';
  // var promo_code  = '';
  var link;
  var adults       = $('select[name=number_adults]').val();
  var child        = $('select[name=number_children]').val();
  var infants      = $('select[name=number_infants]').val();
  var destinations = $('select[name=destinations]').val();


  $(".check_in_date").datepicker({
    minDate: 0,
    dateFormat: "dd-mm-yy",
    defaultDate: new Date(),
    onSelect: function(selectedDate) {
      $(".check_out_date").datepicker("option", "minDate", selectedDate);
      check_in_date = $(this).val();
      checkStatus();
    },
    beforeShow: function() {
      if (screen.width < 561) {
        $(this).datepicker("option", "numberOfMonths", 1);
      } else {
        $(this).datepicker("option", "numberOfMonths", 2);
      };
    }
  });



  $(".check_out_date").datepicker({
    defaultDate: "+1d",
    minDate: 0,
    dateFormat: "dd-mm-yy",
    onSelect: function(selectedDate) {
      $(".check_in_date").datepicker("option", "maxDate", selectedDate);
      check_out_date = $(this).val();
      checkStatus();
    },
    beforeShow: function() {
      if (screen.width < 561) {
        $(this).datepicker("option", "numberOfMonths", 1);
      } else {
        $(this).datepicker("option", "numberOfMonths", 2);
      };
    }
  });

  $('select[name=destinations]').on('change', function(event) {
    event.preventDefault();
    /* Act on the event */
    destinations = $(this).val();
    checkStatus();
  });
  //
  $('select[name=number_adults]').on('change', function(event) {
    event.preventDefault();
    /* Act on the event */
    adults = $(this).val();
    checkStatus();
  });
  //
  $('select[name=number_children]').on('change', function(event) {
    event.preventDefault();
    child = $(this).val();
    checkStatus();
  });

  $('select[name=number_infants]').on('change', function(event) {
    event.preventDefault();
    infants = $(this).val();
    checkStatus();
  });
  // //
  // $('input[name=promo_code]').on('change', function(event) {
  //   event.preventDefault();
  //   promo_code = $(this).val().toUpperCase();
  //   checkStatus();
  // });


  function checkStatus(){
    if (check_out_date != '' && check_in_date != '' && check_out_date != 'undefined' && check_in_date != 'undefined') {
      $('button[type=submit]').removeAttr('disabled');
      $('button[type=submit]').css('background-color', 'rgb(174, 208, 0)');  
    }

    link = 'https://app-apac.thebookingbutton.com/properties/'+destinations;
    console.log(link);
    $('.submit-form').attr('action', link);
    // console.log(destinations);
    // if (child != '' && promo_code == '') {
    //     console.log('ada child, promo code ndak ada');
    //     // link = 'https://app-apac.thebookingbutton.com/properties/'+destinations+'?locale=en&check_in_date='+check_in_date+'&check_out_date='+check_out_date+'&number_adults='+adults+'&number_children='+child;
    //     link = 'https://app-apac.thebookingbutton.com/properties/'+destinations;
    //     console.log(link);
    //     $('.submit-form').attr('action', link);
    // } else if (child != '' && promo_code != '') {
    //     console.log('ada child, promo code ada');
    //     link = 'https://app-apac.thebookingbutton.com/properties/'+destinations;
    //     console.log(link);
    //     $('.submit-form').attr('action', link);
    // } else {
    //     console.log('tidakada child, tidak promo code');
    //     // link = 'https://app-apac.thebookingbutton.com/properties/'+destinations+'?locale=en&check_in_date='+check_in_date+'&check_out_date='+check_out_date+'&number_adults='+adults;
    //     // console.log(link);
    //     link = 'https://app-apac.thebookingbutton.com/properties/'+destinations;
    //     console.log(link);
    //     $('.submit-form').attr('action', link);
    // }
  }


});
