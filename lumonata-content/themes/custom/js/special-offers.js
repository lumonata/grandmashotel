$(document).ready(function() {

  // var $attribute = $('[data-smart-affix]');
  // var a = $attribute.prevObject["0"].URL;
  // var c = a.split('#');
  // var d = c[1];
  //
  // var sm = $('.container-text-image[data-affix-after="' + d + '"]');
  // if (d != '') {
  //   $('html, body').animate({
  //     scrollTop:  $('.container-text-image[data-affix-after="' + d + '"]').top();
  //   }, 1000);
  // }

  var $grid = $('.container-data').imagesLoaded(function() {
    // init Isotope after all images have loaded
    $grid.isotope({
      itemSelector: '.item',
    });
  });

  $('.expand').on('click', function(event) {
    event.preventDefault();
    /* Act on the event */

    if ($(this).next().hasClass('desc')) {
      $(this).next().removeClass('desc');
    } else {
      $(this).next().addClass('desc');
    }
    $('.container-data').isotope('layout');
  });
});
