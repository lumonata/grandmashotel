var locations = function() {
  var json = null;
  var url = $('input[name=url]').val();
  console.log(url);
  $.ajax({
    url: url + '/ajax-function/',
    type: 'POST',
    async: false,
    data: {
      pkey: 'view-locations'
    },
    success: function(result) {
      // console.log(result);
      json = JSON.parse(result);
      // console.log(json);
    }
  });
  return json;
}();
//
// console.log(locations[1]['longitude']);
// console.log(locations[1]['latitude']);

var map;
var gmarkers = [];
var imarkers = [];
var globalClose = [];
var image = [
  ['images/disc-attraction.svg'],
  ['images/disc-restaurant.svg'],
  ['images/disc-shop.svg'],
  ['images/grandmas-disc-maps.svg']
];
var hoverImage = ['images/disc-att-trans.svg', 'images/disc-res-trans.svg', 'images/disc-shop-trans.svg'];



function initMap() {

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 14,
    center: {
      lat: parseFloat(locations[1]['latitude']),
      lng: parseFloat(locations[1]['longitude'])
    },
    scrollwheel: false
  });

  var noPoi = [{
    featureType: "poi.business",
    elementType: "labels",
    stylers: [{
      visibility: "off"
    }]
  }];

  map.setOptions({
    styles: noPoi
  });


  setMarkers(map);
}


// console.log(String(locations[0]['loc_type']));


function setMarkers(map) {
  var infowindow = new google.maps.InfoWindow({
    maxWidth: 250
  });
  var marker, i;
  var closeMarker;

  for (i = 0; i < locations.length; i++) {
    var imgIcon = locations[i]['icon'];
    // console.log(imgIcon);
    var marker = new google.maps.Marker({
      position: {
        lat: parseFloat(locations[i]['latitude']),
        lng: parseFloat(locations[i]['longitude'])
      },
      category: locations[i]['loc_type'],
      map: map,
      icon: {
        url: imgIcon,
        scaledSize: new google.maps.Size(40, 40),
        labelOrigin: new google.maps.Point(20, 18)
      },
      title: locations[i]['title'],
      label: {
        text: locations[i]['get_off'] ? locations[i]['get_off'] + '%' : ' ',
        color: '#777777',
        fontSize: '12px',
        fontFamily: 'Montserrat',
        fontWeight: 'bold'
      }
    });

    // console.log(locations[i]['loc_type'] );
    gmarkers.push(marker);
    imarkers.push(i);

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      // console.log(i);
      // console.log(marker);
      return function() {

        var contentString = '<div id="gmaps-content">' +
          '<img src="' + locations[i]['img'] + '" class="img-responsive" alt="">' +
          '<div class="title">' +
          locations[i]['title'] +
          '</div>' +
          '<div class="category">' +
          locations[i]['loc_type'] +
          '</div>' +
          '<div class="discount">' +
          setOff(locations[i]['get_off']) +
          '</div>' +
          '</div>';

          $('.partner').css('background', 'white');
          $('.partner > .title-partner').css('color', '#777777');
          $('#'+i).css('background','rgb(174, 208, 0)');
          $('#'+i).children('.title-partner').css('color', 'white');

          // 
          // console.log($('.list-partner').height());
          // console.log($('.partner').height());
          // console.log($('.list-partner #'+i).position().top);
          // $('.list-partner').stop().animate({
          //   scrollTop : ($(window).scrollTop() - $('.list-partner #'+i).position().top) + 300
          // }, 2000);

          //  var b = $('#'+i).offset();
          //
          // console.log(b);

        // close content
        closeWindow();
        infowindow.setContent(contentString);
        infowindow.open(map, marker);
        closeMarker = infowindow;
        globalClose.push(closeMarker);
      }
    })(marker, i));
  }

}



append_data();

function append_data(type) {
  $('.list-partner').empty();
  // console.log( locations.length);
  var number = 1;
  for (var i = 0; i < locations.length; i++) {
    // console.log(i);
    // console.log(locations[i]['loc_type']);
    if (type == undefined && locations[i]['loc_type'] != "Hotel") {
      $('.list-partner').append("<div id='" + i + "' class='partner'><div class='title-partner'>" + number + ". " + locations[i]['title'] + "</div><div class='discount-partner'>" + locations[i]['get_off'] + "% OFF</div></div>");
      number++;
      // console.log(locations[i]['loc_type']);
    } else if (type == locations[i]['loc_type'] && locations[i]['loc_type'] != "Hotel") {
      $('.list-partner').append("<div id='" + i + "' class='partner " + i + "'><div class='title-partner'>" + number + ". " + locations[i]['title'] + "</div><div class='discount-partner'>" + locations[i]['get_off'] + "% OFF</div></div>");
      number++;
    }

  }

}


$(document).on('click', '.partner', function(event) {
  event.preventDefault();
  /* Act on the event */

  $('.partner').css('background', 'white');
  $('.partner > .title-partner').css('color', '#777777');
  $(this).css('background','rgb(174, 208, 0)');
  $(this).children('.title-partner').css('color', 'white');


  var value = $(this).attr('id');
  var closedSelectetd = [];
  var infowindow = new google.maps.InfoWindow({
    maxWidth: 250
  });

  // close content
  closeWindow();

  for (i = 0; i < locations.length; i++) {
    marker = gmarkers[i];
    // console.log(i);
    // If is same category or category not picked
    if (i == value || marker.category == 'Hotel') {
      marker.setVisible(true);

      if (i == value) {
        //
        // map.setCenter(new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']));
        // marker.setPosition(new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']));

        var contentStringSelected = '<div id="gmaps-content">' +
          '<img src="' + locations[i]['img'] + '" class="img-responsive" alt="">' +
          '<div class="title">' +
          locations[i]['title'] +
          '</div>' +
          '<div class="category">' +
          locations[i]['loc_type'] +
          '</div>' +
          '<div class="discount">' +
          setOff(locations[i]['get_off']) +
          '</div>' +
          '</div>';

        infowindow.setContent(contentStringSelected);
        infowindow.open(map, marker);

        closedSelectetd = infowindow;
        globalClose.push(closedSelectetd);
      }
    } else {
      marker.setVisible(false);
    }
  }


});



$('.all-restaurant').on('click', function(event) {
  event.preventDefault();
  // console.log('restaurant');
  filterMarkers('Restaurants');
  append_data('Restaurants');
});

$('.all-location').on('click', function(event) {
  event.preventDefault();
  // console.log('location');
  filterMarkers('');
  append_data();
});

$('.all-attraction').on('click', function(event) {
  event.preventDefault();
  // console.log('attraction');
  filterMarkers('Attractions');
  append_data('Attractions');
});

$('.all-shops').on('click', function(event) {
  event.preventDefault();
  // console.log('shops');
  filterMarkers('Shops');
  append_data('Shops');
});

function closeWindow() {
  var closeTheWindows;

  console.log(globalClose);

  if (globalClose.length != 0) {
    for (var i = 0; i < globalClose.length; i++) {
      closeTheWindows = globalClose[i];
      closeTheWindows.close();
    }
  }
}

function filterMarkers(category) {
  //close info windows
  closeWindow();

  for (i = 0; i < locations.length; i++) {
    marker = gmarkers[i];
    // If is same category or category not picked
    if (marker.category == category || category.length === 0 || marker.category == 'Hotel') {
      marker.setVisible(true);
    }
    // Categories don't match
    else {
      marker.setVisible(false);
    }
  }
}


function setImageClick(beach) {
  switch (beach) {
    case 'Attractions':
      return hoverImage[0];
      break;
    case 'Restaurants':
      return hoverImage[1];
      break;
    case 'Shops':
      return hoverImage[2];
      break;
  }
}

function setOff(beach) {
  if (beach != '') {
    // console.log(beach);
    return 'Get Off ' + beach + '%';
  } else {
    return '';
  }
}
