$(document).ready(function() {

  var bg = function() {
    var json = null;
    var urls = $('input[name=site_url]').val();
    console.log(urls);
    $.ajax({
      url: urls + '/ajax-function/',
      type: 'POST',
      async: false,
      data: {
        pkey: 'bg-home-slider'
      },
      success: function(result) {

        json = JSON.parse(result);
        // console.log(json);
      }
    });
    return json;
  }();

  console.log(bg[0]['url']);

  $(".bg-home-top-slider").vegas({
    autoplay: true,
    slides: [{
        src: bg[0]['url'] ? bg[0]['url']  : ''
      },
      {
        src: bg[1]['url'] ? bg[1]['url']  : ''
      },
      {
        src: bg[2]['url'] ? bg[2]['url']  : ''
      },
      {
        src: bg[3]['url'] ? bg[3]['url']  : ''
      }
    ]
  });


});
