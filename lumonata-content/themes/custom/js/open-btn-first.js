$(document).ready(function() {
  $('#scroll-header').on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    $('html, body').animate({
      scrollTop: $('#content').offset().top
    }, 1000)
  });

  $('.open-btn').on('click', function(e) {
    e.preventDefault();
    /* Act on the event */
    $('.navigation').css('height', '100vh');
    // $('body').css('overflow', 'hidden');
  });

  $('.close-btn').on('click', function(e) {
    e.preventDefault();
    /* Act on the event */
    $('.navigation').css('height', '0vh');
    // $('body').css('overflow', 'initial');
  });

  var hash = window.location.hash;
  // console.log(hash);
  if (hash !== '') {
    goToByScroll(hash);
  }

  function goToByScroll(id) {

    id = id.replace("#", "");
    console.log(id);
    $('html, body').animate({
      scrollTop: $('#'+id).offset().top - 80
    }, 1000)
  }
});
