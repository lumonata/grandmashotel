$(document).ready(function() {
  $('#scroll-header').on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    $('html, body').animate({
      scrollTop: $('#content').offset().top - 85
    }, 1000)
  });

  $('.open-btn-second').on('click', function(e) {
    e.preventDefault();
    /* Act on the event */
    $('.nav-fluid').css('height', '100%');
    // $('body').css('overflow', 'hidden');
  });

  $('.close-btn-second').on('click', function(e) {
    e.preventDefault();
    /* Act on the event */
    $('.nav-fluid').css('height', '0vh');
    // $('body').css('overflow', 'initial');
  });
});
