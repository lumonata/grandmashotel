$(document).ready(function() {


  $('.list-of-facilities').slick({
    centerMode: true,
    centerPadding: '0',
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    // autoplay: true,
    // autoplaySpeed: 3000,
    // infinite: true,
    responsive: [{
        breakpoint: 1199,
        settings: {
          arrows: true,
          centerMode: false,
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 600,
        settings: {
          arrows: false,
          centerMode: true,
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 321,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 1,
        }
      }
    ]
  });

  $('.sub-list').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    // centerMode: true,
    centerPadding: '0',
    // dots: true,
    arrows: true,
    // autoplay: true,
    // autoplaySpeed: 2000,
    responsive: [{
        breakpoint: 1199,
        settings: {
          centerMode: false,
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 780,
        settings: {
          centerMode: true,
          slidesToShow: 1,
          // dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          centerMode: true,
          slidesToShow: 1,
          // dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          centerMode: true,
          slidesToShow: 1,
          // dots: true
        }
      },
      {
        breakpoint: 321,
        settings: {
          centerMode: false,
          slidesToShow: 1,
          // dots: true
        }
      }
    ]
  });

  var images = $('.list-of-facilities .item').length;
  if (images == 3) {
    $(".why .slick-slide").css('opacity', 1);
    $(".why .slick-center").css('opacity', 1);

    $(".why .sub-item.slick-slide").css('opacity', 1);
    $(".why .sub-item.slick-center").css('opacity', 1);
  }

  // $('.list-of-facilities').slick('reinit');

  // if ($(window).width() < 1440) {
  //
  //   $(".why .slick-center").css('opacity', 1);
  //
  //   $('.why .slick-arrow').on('click', function(event) {
  //     event.preventDefault();
  //     console.log('asdas');
  //     /* Act on the event */
  //     $(".why .slick-slide").css('opacity', 0.5);
  //     $(".why .sub-item.slick-slide").css('opacity', 1);
  //     $(".why .slick-center").css('opacity', 1);
  //   });
  //
  // } else {
  //   $(".why .slick-center").prev().css('opacity', 1);
  //   $(".why .slick-center").next().css('opacity', 1);
  //   $(".why .slick-center").css('opacity', 1);
  //
  //   $('.why .slick-arrow').on('click', function(event) {
  //     event.preventDefault();
  //     console.log('asdas');
  //     /* Act on the event */
  //     $(".why .slick-slide").css('opacity', 0.5);
  //     $(".why .sub-item.slick-slide").css('opacity', 1);
  //     $(".why .slick-center").prev().css('opacity', 1);
  //     $(".why .slick-center").next().css('opacity', 1);
  //     $(".why .slick-center").css('opacity', 1);
  //   });
  // }

});
