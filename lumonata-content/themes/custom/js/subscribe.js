$(document).ready(function() {

  // $(function() {
  //   $('#modal-send-subscribe').on('show.bs.modal', function() {
  //     var myModal = $(this);
  //     clearTimeout(myModal.data('hideInterval'));
  //     myModal.data('hideInterval', setTimeout(function() {
  //       myModal.modal('hide');
  //     }, 5000));
  //   });
  // });

  $('#send_subscribe').on('click', function(event) {
    event.preventDefault();
    /* Act on the event */

    $('#modal-send-subscribe .modal-body p').empty();
    $('#modal-send-subscribe .modal-header .modal-title').empty();

    console.log('tes');
    var email = $('input[name=email-subscribed]').val();
    var url = $('input[name=site_url]').val();

    console.log(email);
    console.log(url);

    if (email != undefined && email != '') {

      $.ajax({
        url: url + '/ajax-function',
        type: 'POST',
        data: {
          pkey: 'subscribe',
          email: email,
        },
        success: function(result) {
          console.log(result);
          json = JSON.parse(result);
          $('#modal-send-subscribe .modal-header .modal-title').append(json['title']);
          $('#modal-send-subscribe .modal-body p').append(json['data']);
          $('#modal-send-subscribe').modal('show');
        }
      }).fail(function() {
        $('#modal-send-subscribe .modal-header .modal-title').append('Error');
        $('#modal-send-subscribe .modal-body p').append('Check Your Internet Connection');
        $('#modal-send-subscribe').modal('show');
      });

    } else {
      $('#modal-send-subscribe .modal-header .modal-title').append('Error');
      $('#modal-send-subscribe .modal-body p').append('Please Fill The Email');
      $('#modal-send-subscribe').modal('show');
    }
  });
});
