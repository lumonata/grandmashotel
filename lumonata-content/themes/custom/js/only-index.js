$(document).ready(function() {
  $('.content-slide').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    fade: true,
    cssEase: 'linear',
    adaptiveHeight: true,
    zIndex: 2000
  });

  $('.content-slide-mobile').slick({
    infinite: true,
    speed: 300,
    fade: true,
    cssEase: 'linear',
    slidesToShow: 1,
    adaptiveHeight: true
  });
});
