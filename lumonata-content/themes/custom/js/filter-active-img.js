$(document).ready(function() {
  $('.filter-button-group').on('click', 'li', function() {

    $('.filter-button-group li').removeClass('active');
    $(this).addClass('active');


    var filterValue = $(this).attr('data-filter');
    // console.log(filterValue);
    $('.container-data').isotope({
      filter: filterValue
    });
  });
});
