$(document).ready(function() {
  var $grid = $('.data-gallery').imagesLoaded(function() {
    // init Isotope after all images have loaded
    $grid.isotope({
      itemSelector: '.item',
    });
  });

  $('.filter-button-group').on('click', 'li', function() {

    var filterValue = $(this).attr('data-filter');
    // console.log(filterValue);
    $('.data-gallery').isotope({
      filter: filterValue
    });
  });

});
