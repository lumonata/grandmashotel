<?php

/*
| -----------------------------------------------------------------------------
| Set All Page Content
| -----------------------------------------------------------------------------
*/
function set_content($num, $data)
{
    global $db;


    if (is_ajax_function()) {
        return set_ajax_page();
    } elseif (is_home()==false) {
        if ($num > 0) {
            if (get_uri_count()==1) {
                $sef = get_uri_sef();
                return destination_homepage($data, $sef);
            } elseif (get_uri_count()==2) {
                $sef = get_uri_sef();
                $a = array('about','rooms','dining','spa','activities','gallery','special-offers','location');
                if (in_array($sef, $a)) {
                    return destination_page($data, $sef);
                } else {
                    return 404;
                }
            } else {
                return 404;
            }
        }
        return 4042;
    }
}


function get_destination_address($type="destinations")
{
    global $db;
    $data = array();
    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
    $q = $db->prepare_query($s, $type); // legian
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $data[$d['lsef']] = $d['larticle_id'];
        }
    }
    return $data;
}

/*
| -----------------------------------------------------------------------------
| Add Meta Data Value
| -----------------------------------------------------------------------------
*/
function add_meta_data_value($mtitle='', $mkey='', $mdesc='')
{
    global $actions;

    if (isset($actions->action['meta_title'])) {
        unset($actions->action['meta_title']);
    }

    if (isset($actions->action['meta_keywords'])) {
        unset($actions->action['meta_keywords']);
    }

    if (isset($actions->action['meta_description'])) {
        unset($actions->action['meta_description']);
    }

    add_actions('meta_title', $mtitle);
    add_actions('meta_keywords', $mkey);
    add_actions('meta_description', $mdesc);
}

/*
| -----------------------------------------------------------------------------
| Show Error Page
| -----------------------------------------------------------------------------
*/
function show_404($post, $sef)
{
    echo "string";
    exit;
    add_meta_data_value('Page Not Found - '.trim(web_title()));

    $post_id  = $post['larticle_id'];
    $post_sef = $post['lsef'];

    $message   = (empty($mess) ? 'Sorry, the page you are looking cannot be found.' : $mess);
    $exist_app = array('legian','seminyak','airport');
    $app_name  = get_appname();

    if (in_array($app_name, $exist_app)) {
        set_template(TEMPLATE_PATH.'/template/page-404.html', 'error_template');
        add_block('mainblock', 'et', 'error_template');
        add_variable('navigation', get_second_navigation($post_sef, $sef, $post_id));
        add_variable('instagram', get_instagram_photo());
        add_variable('message', $message);

        add_variable('url', 'http://' . site_url() . '/'.$app_name);

        parse_template('mainblock', 'et');
        return return_template('error_template');
    } else {
        header('location:http://'.SITE_URL);
    }
}

/*
| -----------------------------------------------------------------------------
|
| MENUS
|
| -----------------------------------------------------------------------------
*/

/*
| -----------------------------------------------------------------------------
| Get First Home Content
| -----------------------------------------------------------------------------
*/
function get_first_home_content()
{
    set_template(TEMPLATE_PATH . '/template/part/first/home_template.html', 'first_fhomepage_template');
    add_block('home_content_block', 'hcbblock', 'first_fhomepage_template');
    add_variable('site_url', 'http://' . SITE_URL);

    add_variable('brief_seminyak', get_meta_data('fhomepage_brief_seminyak', 'global_setting'));
    add_variable('brief_legian', get_meta_data('fhomepage_brief_legian', 'global_setting'));
    add_variable('brief_airport', get_meta_data('fhomepage_brief_airport', 'global_setting'));

    add_variable('fhome_bg_image', get_fhomepage_top_background());
    add_variable('seminyak_image', get_first_image_seminyak());
    add_variable('legian_image', get_first_image_legian());
    add_variable('airport_image', get_first_image_airport());
    add_variable('instagram', get_instagram_photo());

    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/font-awesome.min.css');
    add_actions('include_css', 'get_custom_css', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css');
    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/slick-theme.css');
    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/js/vegas/vegas.min.css');
    add_actions('include_css', 'get_custom_css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    add_actions('include_js', 'get_custom_javascript', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js');
    add_actions('include_js', 'get_custom_javascript', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/gmaps.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/open-btn-first.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/vegas/vegas.min.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/slider_bg_home.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/only-index.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/datepicker-custom.js');
    add_actions('include_js', 'get_custom_javascript', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAs3p7ay3WO0GNQ-U9qVL3MofRyva1HEak&callback=initMap');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/subscribe.js');

    parse_template('home_content_block', 'hcbblock');

    return return_template('first_fhomepage_template');
}


/*
| -----------------------------------------------------------------------------
| Get Data Locations
| -----------------------------------------------------------------------------
*/
function get_location_datas()
{
    global $db;

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
    $q = $db->prepare_query($s, 'locations');
    $r = $db->do_query($q);

    $f = array();

    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $title      = $d['larticle_title'];
            $loc_type   = get_additional_field($d['larticle_id'], 'loc_type', 'locations');
            $longitude  = get_additional_field($d['larticle_id'], 'longitude', 'locations');
            $latitude   = get_additional_field($d['larticle_id'], 'latitude', 'locations');
            $get_off    = get_additional_field($d['larticle_id'], 'get_off', 'locations');
            $img        = get_additional_field($d['larticle_id'], 'locations_image', 'locations');
            if (!empty($img)) {
                $url_img    = 'http://' . site_url() . '/lumonata-plugins/locations/background/'.$img;
            } else {
                $url_img    = 'http://' . site_url() . '/lumonata-plugins/locations/images/official.jpg';
            }


            if ($loc_type == 'Attractions') {
                $icon = 'http://' . site_url() . '/lumonata-plugins/locations/images/disc-attraction.svg';
            } elseif ($loc_type == 'Shops') {
                $icon = 'http://' . site_url() . '/lumonata-plugins/locations/images/disc-shop.svg';
            } elseif ($loc_type == 'Restaurants') {
                $icon = 'http://' . site_url() . '/lumonata-plugins/locations/images/disc-restaurant.svg';
            } elseif ($loc_type == 'Hotel') {
                $icon = 'http://' . site_url() . '/lumonata-plugins/locations/images/grandmas-disc-maps.svg';
            }

            $f[] = array(
            'title' => $title,
            'loc_type' => $loc_type,
            'longitude' => $longitude,
            'latitude' => $latitude,
            'get_off' => $get_off,
            'img'  => $url_img,
            'icon'  => $icon
          );
        }
    }
    return $f;
}

/*
| -----------------------------------------------------------------------------
| Get Data Background Slider
| -----------------------------------------------------------------------------
*/
function get_bg_slider()
{
    global $db;

    $s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name LIKE %s';
    $q = $db->prepare_query($s, '%fhome_bg_image%');
    $r = $db->do_query($q);

    $f = array();

    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            // print_r($d['lmeta_value']);
          $f[] = array(
            'name' => $d['lmeta_name'],
            'value' => $d['lmeta_value'],
            'url' => 'http://' . site_url() . '/lumonata-plugins/global/background/'.$d['lmeta_value']
          );
        }
    }

    // exit;
    return $f;
}

/*
| -----------------------------------------------------------------------------
| Get Second Home Content
| -----------------------------------------------------------------------------
*/
function get_template_one_content($post_sef, $sef)
{
    set_template(TEMPLATE_PATH . '/template/part/second/template_one.html', 'themes_one_template');
    add_block('template_one_block', 'toblock', 'themes_one_template');
    add_variable('site_url', SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);


    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND larticle_type=%s';
    $q = $db->prepare_query($s, $post_sef, $sef);
    $r = $db->do_query($q);

    $number = 0;
    $list   = '';

    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $title    = $d['larticle_title'];
            $img      = get_additional_field($d['larticle_id'], 'main_image', $sef);
            $link     = get_additional_field($d['larticle_id'], 'website_link', $sef);
            $content  = $d['larticle_content'];

            $links = '<div class="link">
              <a href="'.$link.'" target="_blank">Read More</a>
            </div>';

            if (($number % 2) == 1) {
                $list .= '<div class="image-content mobile">
                <img src="http://' . site_url() . '/lumonata-plugins/additional/background/'.$img.'" class="full-cover" alt="">
              </div>

              <div class="text-content container-table">
                <div class="container-table-cell">
                  <div class="title">
                    '.$title.'
                  </div>
                  <div class="line">&#151;</div>
                  <div class="description">
                    '.$content.'
                  </div>
                  '.($link ? $links : "").'
                </div>
              </div>
              <div class="image-content desktop">
                <img src="http://' . site_url() . '/lumonata-plugins/additional/background/'.$img.'" class="full-cover" alt="">
              </div>';
            }
            if (($number % 2) == 0) {
                $list .= '<div class="image-content">
                <img src="http://' . site_url() . '/lumonata-plugins/additional/background/'.$img.'" class="full-cover" alt="">
              </div>
              <div class="text-content container-table">
                <div class="container-table-cell">
                  <div class="title">
                    '.$title.'
                  </div>
                  <div class="line">&#151;</div>
                  <div class="description">
                    '.$content.'
                  </div>
                  '.($link ? $links : "").'
                </div>
              </div>';
            }

            $number++;
        }
    }

    add_variable('content_area_home', $list);
    add_variable('destinations_page', $post_sef);
    add_variable('front_image', get_first_image_destination($post_sef));
    add_variable('brief_text', get_first_brief_destination($post_sef));


    parse_template('template_one_block', 'toblock');

    return return_template('themes_one_template');
}


function get_template_rooms($post_sef, $sef)
{
    set_template(TEMPLATE_PATH . '/template/part/second/template_one.html', 'themes_one_template');
    add_block('template_one_block', 'toblock', 'themes_one_template');
    add_variable('site_url', SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);


    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND larticle_type=%s ORDER BY larticle_id DESC';
    $q = $db->prepare_query($s, $post_sef, $sef);
    $r = $db->do_query($q);

    $number = 0;
    $list   = '';

    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $title    = $d['larticle_title'];
            $img      = get_additional_field($d['larticle_id'], 'main_image', $sef);
            $link     = get_additional_field($d['larticle_id'], 'website_link', $sef);
            $content_list  = get_additional_list_rooms($d['larticle_id']);

            if (($number % 2) == 1) {
                $list .= '<div class="image-content mobile">
                <img src="http://' . site_url() . '/lumonata-plugins/additional/background/'.$img.'" class="full-cover" alt="">
              </div>

              <div class="text-content container-table">
                <div class="container-table-cell">
                  <div class="title">
                    '.$title.'
                  </div>
                  <div class="line">&#151;</div>
                  <div class="description">
                    '.$content_list.'
                  </div>
                </div>
              </div>
              <div class="image-content desktop">
                <img src="http://' . site_url() . '/lumonata-plugins/additional/background/'.$img.'" class="full-cover" alt="">
              </div>';
            }
            if (($number % 2) == 0) {
                $list .= '<div class="image-content">
                <img src="http://' . site_url() . '/lumonata-plugins/additional/background/'.$img.'" class="full-cover" alt="">
              </div>
              <div class="text-content container-table">
                <div class="container-table-cell">
                  <div class="title">
                    '.$title.'
                  </div>
                  <div class="line">&#151;</div>
                  <div class="description">
                      '.$content_list.'
                  </div>
                </div>
              </div>';
            }

            $number++;
        }
    }


    add_variable('content_area_home', $list);
    add_variable('destinations_page', $post_sef);
    add_variable('front_image', get_first_image_destination($post_sef));
    add_variable('brief_text', get_first_brief_destination($post_sef));

    parse_template('template_one_block', 'toblock');

    return return_template('themes_one_template');
}

function get_template_special_offers($post_sef, $sef, $post_id='')
{
    set_template(TEMPLATE_PATH . '/template/part/second/special_offers.html', 'special_offers_template');
    add_block('special_offers_destination_data_block', 'soddblock', 'special_offers_template');
    add_block('special_offers_destination_block', 'sodblock', 'special_offers_template');

    add_variable('site_url', SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);
    add_variable('plugin_url', PLUGINS_PATH);


    add_variable('fhome_bg_image', get_destination_page_background($post_id, $sef));

    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND larticle_type=%s ORDER BY lorder ASC';
    $q = $db->prepare_query($s, $post_sef, 'special-offers');
    $r = $db->do_query($q);
    $number = 1;
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $link = get_additional_field($d['larticle_id'], 'website_link', 'special-offers');

            $link_html ='<div class="btn-book">
              <a href="'.$link.'" class="btn button">Book Now</a>
            </div>';

            $promocode = get_additional_field($d['larticle_id'], 'promo_code', 'special-offers');
            $pro = '<strong>Promocode : </strong> <span class="text-uppercase">'.$promocode.'</span>';
            add_variable('title', $d['larticle_title']);
            add_variable('content', $d['larticle_content']);
            add_variable('content_brief', get_additional_field($d['larticle_id'], 'brief', 'special-offers'));
            add_variable('tag', get_additional_field($d['larticle_id'], 'destination', 'special-offers'));
            add_variable('image_special_offers', get_additional_field($d['larticle_id'], 'main_image', 'special-offers'));
            add_variable('promo_code', $promocode ? $pro : '');
            add_variable('website_link', $link ? $link_html : '');
            add_variable('additional_list_category', get_additional_list($d['larticle_id']));
            add_variable('price_offers_net', get_additional_field($d['larticle_id'], 'price_offers_net', 'special-offers'));
            add_variable('number', $number);
            $number++;
            parse_template('special_offers_destination_data_block', 'soddblock', true);
        }
    }
    parse_template('special_offers_destination_block', 'sodblock', false);

    return return_template('special_offers_template');
}

function get_template_about($post_sef, $sef, $post_id='')
{
    set_template(TEMPLATE_PATH . '/template/part/second/about.html', 'about_template');
    add_block('about_block', 'ablock', 'about_template');

    add_variable('site_url', SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);
    add_variable('plugin_url', PLUGINS_PATH);

    add_variable('fhome_bg_image', get_destination_page_background($post_id, $sef));
    add_variable('brief_text_top', get_brief_text_top($post_sef));
    add_variable('brief_img_top', get_brief_img_top($post_sef));
    add_variable('description_text_middle', get_description_text_middle($post_sef));
    add_variable('brief_text_bottom', get_brief_text_bottom($post_sef));
    add_variable('brief_img_bottom', get_brief_img_bottom($post_sef));
    add_variable('fhome_bg_image', get_destination_page_background($post_id, $sef));

    parse_template('about_block', 'ablock', false);

    return return_template('about_template');
}

function get_brief_img_top($post_sef)
{
    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND lapp_name=%s';
    $q = $db->prepare_query($s, $post_sef, 'about-us'); // legian
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $a = get_additional_field($d['larticle_id'], 'brief_image_top', 'about-us');
        }
    }
    return 'http://' . site_url() . '/lumonata-plugins/additional/background/' . $a;
}

function get_brief_img_bottom($post_sef)
{
    global $db;
    $b = '';
    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND lapp_name=%s';
    $q = $db->prepare_query($s, $post_sef, 'about-us'); // legian
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $b = get_additional_field($d['larticle_id'], 'brief_image_bottom', 'about-us');
        }
    }

    return 'http://' . site_url() . '/lumonata-plugins/additional/background/' . $b;
}

function get_description_text_middle($post_sef)
{
    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND lapp_name=%s';
    $q = $db->prepare_query($s, $post_sef, 'about-us');
    $r = $db->do_query($q);
    $a ='';
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $a = $d['larticle_content'];
        }
    }


    return $a;
}

function get_brief_text_top($post_sef)
{
    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND lapp_name=%s';
    $q = $db->prepare_query($s, $post_sef, 'about-us'); // legian
    $r = $db->do_query($q);

    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $a = get_additional_field($d['larticle_id'], 'brief_top_text', 'about-us');
        }
    }

    return $a;
}

function get_brief_text_bottom($post_sef)
{
    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND lapp_name=%s';
    $q = $db->prepare_query($s, $post_sef, 'about-us');
    $r = $db->do_query($q);
    $a = '';
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $a = get_additional_field($d['larticle_id'], 'brief_bottom_text', 'about-us');
        }
    }

    return $a;
}


/*
| -----------------------------------------------------------------------------
| Get Second Home Content
| -----------------------------------------------------------------------------
*/
function get_second_home_content($post_sef)
{
    set_template(TEMPLATE_PATH . '/template/part/second/home_template.html', 'second_shomepage_template');
    add_block('second_home_content_block', 'shcblock', 'second_shomepage_template');
    add_variable('site_url', SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);

    add_variable('destinations_page', $post_sef);
    add_variable('front_image', get_first_image_destination($post_sef));
    add_variable('brief_text', get_first_brief_destination($post_sef));

    parse_template('second_home_content_block', 'shcblock');

    return return_template('second_shomepage_template');
}

/*
| -----------------------------------------------------------------------------
| Get First Special Content
| -----------------------------------------------------------------------------
*/
function get_first_special_content()
{
    set_template(TEMPLATE_PATH . '/template/part/first/special_offers.html', 'first_special_offers_template');
    add_block('special_offers_data_block', 'spodblock', 'first_special_offers_template');
    add_block('special_offers_block', 'spoblock', 'first_special_offers_template');


    add_variable('site_url', SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);
    add_variable('plugin_url', PLUGINS_PATH);

    add_variable('bg-home-top', 'bg-home-top');
    add_variable('fhome_bg_image', get_fhomepage_top_background_special_offers());
    add_variable('list_category_destination', get_destination_list_data_top());

    global $db;

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY lorder ASC';
    $q = $db->prepare_query($s, 'special-offers');
    $r = $db->do_query($q);
    $number = 1;
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $link = get_additional_field($d['larticle_id'], 'website_link', 'special-offers');
            $link_html ='<div class="btn-book">
              <a href="'.$link.'" class="btn button">Book Now</a>
            </div>';
            $promocode = get_additional_field($d['larticle_id'], 'promo_code', 'special-offers');
            $pro = '<strong>Promocode : </strong> <span class="text-uppercase">'.$promocode.'</span>';
            add_variable('title', $d['larticle_title']);
            add_variable('content', $d['larticle_content']);
            add_variable('content_brief', get_additional_field($d['larticle_id'], 'brief', 'special-offers'));
            add_variable('tag', get_additional_field($d['larticle_id'], 'destination', 'special-offers'));
            add_variable('image_special_offers', get_additional_field($d['larticle_id'], 'main_image', 'special-offers'));
            add_variable('promo_code', $promocode ? $pro : '');
            add_variable('url', $link ? $link_html : '');
            add_variable('price_offers_net', get_additional_field($d['larticle_id'], 'price_offers_net', 'special-offers'));
            add_variable('additional_list_category', get_additional_list($d['larticle_id']));
            add_variable('number', $number.'link');
            $number++;
            parse_template('special_offers_data_block', 'spodblock', true);
        }
    }

    add_variable('instagram', get_instagram_photo());
    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/font-awesome.min.css');
    add_actions('include_css', 'get_custom_css', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css');
    add_actions('include_css', 'get_custom_css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/slick-theme.css');

    add_actions('include_js', 'get_custom_javascript', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js');
    add_actions('include_js', 'get_custom_javascript', 'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js');
    add_actions('include_js', 'get_custom_javascript', 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.js');
    add_actions('include_js', 'get_custom_javascript', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/open-btn-first.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/filter-active-img.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/datepicker-custom.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/special-offers.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/subscribe.js');

    parse_template('special_offers_block', 'spoblock', false);

    return return_template('first_special_offers_template');
}

function get_additional_list($larticle_id)
{
    $list = get_additional_field($larticle_id, 'additional_list_category', 'special-offers');
    $content = '';
    if (!empty($list)) {
        $list = json_decode($list, true);
        $content .= '<ul>';
        foreach ($list as $key) {
            $content .= '<li>'.$key['name'].'</li>';
        }
        $content .= '</ul>';
    }

    return $content;
}

function get_additional_list_rooms($larticle_id)
{
    $list = get_additional_field($larticle_id, 'additional_list_category', 'rooms');
    $content = '';
    if (!empty($list)) {
        $list = json_decode($list, true);

        foreach ($list as $key) {
            $content .= '<li>'.$key['name'].'</li>';
        }
    }

    return $content;
}


/*
| -----------------------------------------------------------------------------
| Get Gallery
| -----------------------------------------------------------------------------
*/
function get_template_gallery($post_sef, $sef)
{
    set_template(TEMPLATE_PATH . '/template/part/gallery_content.html', 'gallery_template');
    add_block('gallery_list_block', 'glblock', 'gallery_template');
    add_block('list_category_block', 'lcatblock', 'gallery_template');
    add_block('gallery_content_block', 'gcblock', 'gallery_template');
    add_variable('site_url', SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);

    $part = cek_url();
    add_variable('sef', $part[0]);

    $list    = get_post_by_type_list($post_sef, $sef);
    foreach ($list as $obj) {
        $gallery   = get_post_gallery_image($obj['larticle_id'], 'post_gallery_image', $sef);

        if (!empty($gallery)) {
            $count = count($gallery);
            foreach ($gallery as $img) {
                $gal[] = array(
                       'large'=>$img['large_url'],
                       'medium'=>$img['medium_url'],
                       'thumb'=>$img['thumb_url'],
                       'cat'=>$obj['lsef']
                   );
                $cat[] = array(
                  'cat'=>$obj['lsef'],
                  'title'=>$obj['larticle_title']
                );
            }
        }
    }


    if (!empty($cat)) {
        $unique = array();
        foreach ($cat as $list) {
            if (!in_array($list, $unique)) {
                array_push($unique, $list);
            }
        }
    }

    if (!empty($unique)) {
        foreach ($unique as $category) {
            add_variable('category_top', $category['cat']);
            add_variable('title', $category['title']);

            parse_template('list_category_block', 'lcatblock', true);
        }
    }

    if (!empty($gal)) {
        // shuffle($gal);
        foreach ($gal as $img) {
            add_variable('large_url', 'http://'.site_url().$img['large']);
            add_variable('medium_url', 'http://'.site_url().$img['medium']);
            add_variable('thumb_url', 'http://'.site_url().$img['thumb']);
            add_variable('category_bottom', $img['cat']);

            parse_template('gallery_list_block', 'glblock', true);
        }
    }


    parse_template('gallery_content_block', 'gcblock');

    return return_template('gallery_template');
}

/*
| -----------------------------------------------------------------------------
| Get Location
| -----------------------------------------------------------------------------
*/
function get_template_location()
{
    set_template(TEMPLATE_PATH . '/template/part/location_content.html', 'location_template');
    add_block('location_content_block', 'lcblock', 'location_template');
    add_variable('themes_url', TEMPLATE_URL);
    add_variable('site_url', 'http://'. SITE_URL);
    $part = cek_url();
    add_variable('sef', $part[0]);
    parse_template('location_content_block', 'lcblock');

    return return_template('location_template');
}



/*
| -----------------------------------------------------------------------------
| Show Homepage Destination
| -----------------------------------------------------------------------------
*/
function destination_homepage($post, $sef)
{
    $post_id  = $post['larticle_id'];
    $post_sef = $post['lsef'];

    $mtitle = get_meta_title($post_id, 'destinations');
    $mkey   = get_meta_keywords($post_id, 'destinations');
    $mdesc  = get_meta_description($post_id, 'destinations');
    $mtitle = (empty($mtitle) ? $post['larticle_title'].' - '.trim(web_title()) : $mtitle);

    add_meta_data_value($mtitle, $mkey, $mdesc);

    add_variable('navigation', get_second_navigation($post_sef, $sef, $post_id));
    add_variable('label_book', get_first_labelbook_top($post_id));
    add_variable('gmaps_nbook', get_gmaps_n_book_block($post_id));
    add_variable('why_stay', get_footer_slider($post_id, $sef));

    add_variable('content_area_home', get_second_home_content($post_sef));
    add_variable('fhome_bg_image', get_destinations_background($post_id));
    add_variable('slider_top', get_first_slider_destination_top($post_sef));
    add_variable('slider_top_mobile', get_first_slider_destination_top_mobile($post_sef));

    // add_variable('label_info', get_label_info($post_sef, $post_id));
    add_variable('title', $post_sef);
    add_variable('address_label', get_additional_field($post_id, 'address', 'destinations'));
    add_variable('phone_number_label', get_additional_field($post_id, 'phone_number', 'destinations'));
    add_variable('instagram', get_instagram_photo());

    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/font-awesome.min.css');
    add_actions('include_css', 'get_custom_css', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css');
    add_actions('include_css', 'get_custom_css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/slick-theme.css');
    add_actions('include_js', 'get_custom_javascript', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js');
    add_actions('include_js', 'get_custom_javascript', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/gmaps.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/open-btn-second.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/only-index.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/custom-expect-home.js');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/datepicker-custom.js');
    add_actions('include_js', 'get_custom_javascript', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAs3p7ay3WO0GNQ-U9qVL3MofRyva1HEak&callback=initMap');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/subscribe.js');


    parse_template('home_content_block', 'hcbblock');

    return return_template('first_fhomepage_template');
}


/*
| -----------------------------------------------------------------------------
| Show Destination Page
| -----------------------------------------------------------------------------
*/
function destination_page($post, $sef)
{
    $post_id  = $post['larticle_id'];
    $post_sef = $post['lsef'];

    $mtitle = get_meta_title($post_id, 'destinations');
    $mkey   = get_meta_keywords($post_id, 'destinations');
    $mdesc  = get_meta_description($post_id, 'destinations');
    $mtitle = (empty($mtitle) ? $post['larticle_title'].' - '.trim(web_title()) : $mtitle);

    $template_one   = array('dining','spa','activities');
    $rooms          = array('rooms');
    $about          = array('about');
    $special_offers = array('special-offers');
    $gallery        = array('gallery');
    $part           = cek_url();


    if (in_array($sef, $template_one)) {
        if (($sef != 'spa' && $part[0] != 'legian')) {
            add_variable('content_area_home', get_template_one_content($post_sef, $sef));
        }
    } elseif (in_array($sef, $about)) {
        add_variable('content_area_home', get_template_about($post_sef, $sef, $post_id));
    } elseif (in_array($sef, $rooms)) {
        add_variable('content_area_home', get_template_rooms($post_sef, $sef, $post_id));
    } elseif (in_array($sef, $special_offers)) {
        add_variable('content_area_home', get_template_special_offers($post_sef, $sef, $post_id));
        add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/special-offers-details.js');
    } elseif (in_array($sef, $gallery)) {
        add_variable('content_area_home', get_template_gallery($post_sef, $sef));
    }

    add_meta_data_value($mtitle, $mkey, $mdesc);

    add_variable('fhome_bg_image', get_destination_page_background($post_id, $sef));
    add_variable('navigation', get_second_navigation($post_sef, $sef, $post_id));

    if ($sef != 'gallery') {
        add_variable('why_stay', get_footer_slider($post_id, $sef));
    }

    add_variable('label_book', get_first_labelbook_top($post_id));
    if ($sef != 'location') {
        add_variable('book_now', get_book_now_block($post_id));
    } else {
        add_variable('book_now', get_template_location($post_sef, $sef));
    }

    if ($sef == 'spa' && $part[0] == 'legian') {
        add_variable('grand_label', get_grand_label_spa($post_sef, $sef));
        add_variable('grand_label_mobile', get_grand_label_mobile_spa($post_sef, $sef));
    } else {
        add_variable('grand_label', get_grand_label($post_sef, $sef));
        add_variable('grand_label_mobile', get_grand_label_mobile($post_sef, $sef));
    }

    // add_variable('label_info', get_label_info($post_sef, $post_id));
    add_variable('title', $post_sef);
    add_variable('address_label', get_additional_field($post_id, 'address', 'destinations'));
    add_variable('phone_number_label', get_additional_field($post_id, 'phone_number', 'destinations'));
    // add_variable('instagram', get_instagram_photo());


    $btn = array('seminyak','airport');
    if ($sef == 'spa' && in_array($part[0], $btn)) {
        add_variable('label_spa', get_button_reservation_spa());
    } elseif ($sef == 'spa' && $part[0] == 'legian') {
        add_variable('label_spa', get_button_reservation_spa_1());
        add_variable('only_legian_spa', get_spa_only_legian());
    }

    if ($sef != 'gallery') {
        add_actions('include_css', 'get_custom_css', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css');
        add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/slick-theme.css');
        add_actions('include_js', 'get_custom_javascript', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js');
        add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/open-btn-second.js');
        add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/only-index.js');
        add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/custom-expect-home.js');
        if ($sef == 'location') {
            add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/gmaps.js');
            add_actions('include_js', 'get_custom_javascript', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAs3p7ay3WO0GNQ-U9qVL3MofRyva1HEak&callback=initMap');
        }
    } else {
        add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/gallery.js');
        add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/open-btn-second.js');
        add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/filter-active-img.js');
        add_actions('include_js', 'get_custom_javascript', 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.js');
        add_actions('include_js', 'get_custom_javascript', 'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js');
        add_actions('include_css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');
        add_actions('include_js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js');
    }

    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/datepicker-custom.js');
    add_actions('include_js', 'get_custom_javascript', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js');
    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/font-awesome.min.css');
    add_actions('include_css', 'get_custom_css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    add_actions('include_js', 'get_custom_javascript', 'http://' . TEMPLATE_URL . '/js/subscribe.js');

    parse_template('home_content_block', 'hcbblock');

    return return_template('first_fhomepage_template');
}


function get_template_booking($post_sef, $sef, $post_id='')
{
    set_template(TEMPLATE_PATH . '/template/part/second/booking_template.html', 'booking_template');
    add_block('booking_template_block', 'btblock', 'booking_template');

    add_variable('site_url', SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);
    add_variable('plugin_url', PLUGINS_PATH);

    $api = "0f5361e1548ea8a0eb36c4ffca50d37c0"; // change this value with hotel apikey
        $max_adult = 3; // max adult in search form
        $max_child = 2; // max child in search form
        $max_pax = 9; // max adult in search form

        //Valid String POST / GET / REQUEST
        function inputSecurity($validate=null)
        {
            if ($validate == null) {
                foreach ($_REQUEST as $key => $val) {
                    if (is_string($val)) {
                        $_REQUEST[$key] = htmlentities($val);
                    } elseif (is_array($val)) {
                        $_REQUEST[$key] = inputSecurity($val);
                    }
                }
                foreach ($_GET as $key => $val) {
                    if (is_string($val)) {
                        $_GET[$key] = htmlentities($val, ENT_QUOTES, 'UTF-8');
                    } elseif (is_array($val)) {
                        $_GET[$key] = inputSecurity($val);
                    }
                }
                foreach ($_POST as $key => $val) {
                    if (is_string($val)) {
                        $_POST[$key] = htmlentities($val, ENT_QUOTES, 'UTF-8');
                    } elseif (is_array($val)) {
                        $_POST[$key] = inputSecurity($val);
                    }
                }
            } else {
                foreach ($validate as $key => $val) {
                    if (is_string($val)) {
                        $validate[$key] = htmlentities($val, ENT_QUOTES, 'UTF-8');
                    } elseif (is_array($val)) {
                        $validate[$key] = inputSecurity($val);
                    }
                    return $validate;
                }
            }
        }
    inputSecurity();

    if (!isset($_POST['cd']) || $_POST['cd'] == '') {
        if (isset($_GET['fy'])) {
            $fy=$_GET['fy'];
            $fm=$_GET['fm'];
            $fd=$_GET['fd'];
            $fromdate = strtotime("$fy-$fm-$fd");
        } else {
            $fromdate = time();
            $fy=date('Y', strtotime($fromdate));
            $fm=date('m', strtotime($fromdate));
            $fd=date('d', strtotime($fromdate));
        }

        $now = time();
        if ($now <= $fromdate) {
            $bsk = strtotime("$fy-$fm-$fd +1 days");
            $cd = date("j", $fromdate);
            $cm = date("n", $fromdate);
            $cy = date("Y", $fromdate);

            $cod = date("j", $bsk);
            $com = date("n", $bsk);
            $coy = date("Y", $bsk);
        } else {
            $now = strtotime("tomorrow");
            $bsk = strtotime("+ 2 days");
            $cd = date("j", $now);
            $cm = date("n", $now);
            $cy = date("Y", $now);

            $cod = date("j", $bsk);
            $com = date("n", $bsk);
            $coy = date("Y", $bsk);
        }
    }
    $cd = isset($_REQUEST['cd'])?$_REQUEST['cd']:$cd; //check in day
        $cm = isset($_REQUEST['cm'])?$_REQUEST['cm']:$cm; //check in month
        $cy = isset($_REQUEST['cy'])?$_REQUEST['cy']:$cy; //check in year
        $cod = isset($_REQUEST['cod'])?$_REQUEST['cod']:$cod; //check out day
        $com = isset($_REQUEST['com'])?$_REQUEST['com']:$com; //check out month
        $coy = isset($_REQUEST['coy'])?$_REQUEST['coy']:$coy; //check out year
        $pax = isset($_REQUEST['pax'])?$_REQUEST['pax']:1; //  pax
        $adult = isset($_REQUEST['adult'])?$_REQUEST['adult']:2; //adults
        $child = isset($_REQUEST['child'])?$_REQUEST['child']:0; //childs
        $nite = isset($_REQUEST['nite'])?$_REQUEST['nite']:1; // per night
        $cbs = isset($_REQUEST['cbs'])?$_REQUEST['cbs']:0;
    $promo = isset($_REQUEST['promo'])?$_REQUEST['promo']: '';
    $utc = isset($_REQUEST['utc'])?$_REQUEST['utc']:'';
    $hid = isset($_REQUEST['hid'])?$_REQUEST['hid']:'';
    $url = isset($_REQUEST['url'])?$_REQUEST['url']:''; // url

    add_variable('cd', $cd);
    add_variable('cm', $cm);
    add_variable('cy', $cy);
    add_variable('cod', $cod);
    add_variable('com', $com);
    add_variable('coy', $coy);
    add_variable('pax', $pax);
    add_variable('adult', $adult);
    add_variable('child', $child);
    add_variable('nite', $nite);
    add_variable('cbs', $cbs);
    add_variable('promo', $promo);
    add_variable('utc', $utc);
    add_variable('hid', $hid);
    // add_variable('url',$url);
    add_variable('api', $api);

    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/font-awesome.min.css');
    add_actions('include_css', 'get_custom_css', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css');
    add_actions('include_css', 'get_custom_css', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    add_actions('include_css', 'get_custom_css', 'http://' . TEMPLATE_URL . '/css/slick-theme.css');

    add_actions('include_js', 'get_custom_javascript', 'http://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js');
    //
    // add_variable('fhome_bg_image', get_destination_page_background($post_id, $sef));
    // add_variable('brief_text_top', get_brief_text_top($post_sef));
    // add_variable('brief_img_top', get_brief_img_top($post_sef));
    // add_variable('description_text_middle', get_description_text_middle($post_sef));
    // add_variable('brief_text_bottom', get_brief_text_bottom($post_sef));
    // add_variable('brief_img_bottom', get_brief_img_bottom($post_sef));
    // add_variable('fhome_bg_image', get_destination_page_background($post_id, $sef));

    parse_template('booking_template_block', 'btblock', false);

    return return_template('booking_template');
}

/*
| -----------------------------------------------------------------------------
| Get Reservation Spa
| -----------------------------------------------------------------------------
*/
function get_button_reservation_spa()
{
    set_template(TEMPLATE_PATH . '/template/part/btn_spa.html', 'btn_spa_template');
    add_block('btn_spa_block', 'bsblock', 'btn_spa_template');

    parse_template('btn_spa_block', 'bsblock');

    return return_template('btn_spa_template');
}

function get_button_reservation_spa_1()
{
    set_template(TEMPLATE_PATH . '/template/part/btn_spa_1.html', 'btn_spa1_template');
    add_block('btn_spa1_block', 'bs1block', 'btn_spa1_template');

    parse_template('btn_spa1_block', 'bs1block');

    return return_template('btn_spa1_template');
}


/*
| -----------------------------------------------------------------------------
| Modal Book Now
| -----------------------------------------------------------------------------
*/
function get_modal_book_now()
{
    set_template(TEMPLATE_PATH . '/template/part/modal-book-now.html', 'modal_book_now_template');
    add_block('modal_book_now_block', 'mbnblock', 'modal_book_now_template');
    $part = cek_url();

    if ($part[0] == 'legian') {
        add_variable('legian', 'selected');
    } elseif ($part[0] == 'seminyak') {
        add_variable('seminyak', 'selected');
    } elseif ($part[0] == 'airport') {
        add_variable('airport', 'selected');
    }

    add_variable('themes_url', TEMPLATE_URL);

    parse_template('modal_book_now_block', 'mbnblock');

    return return_template('modal_book_now_template');
}

/*
| -----------------------------------------------------------------------------
| Modal Career
| -----------------------------------------------------------------------------
*/
function get_modal_career()
{
    set_template(TEMPLATE_PATH . '/template/part/modal_career.html', 'modal_career_template');
    add_block('modal_career_block', 'mcblock', 'modal_career_template');

    add_variable('themes_url', TEMPLATE_URL);

    parse_template('modal_career_block', 'mcblock');

    return return_template('modal_career_template');
}

/*
| -----------------------------------------------------------------------------
| REHAT SPA Only Legian
| -----------------------------------------------------------------------------
*/
function get_spa_only_legian()
{
    set_template(TEMPLATE_PATH . '/template/part/only_legian_spa.html', 'modal_spa_legian_template');
    add_block('loop_list_spa_block', 'llsblock', 'modal_spa_legian_template');
    add_block('spa_legain_block', 'slblock', 'modal_spa_legian_template');

    $additional_description = get_meta_data('post_type_setting', 'spa', '167');
    $json= json_decode($additional_description, true);
    $desc = $json['additional_description'];

    add_variable('description', nl2br($desc));
    add_variable('themes_url', TEMPLATE_URL);

    global $db;

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
    $q = $db->prepare_query($s, 'spa');
    $r = $db->do_query($q);
    $number = 1;
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            add_variable('title', $d['larticle_title']);
            add_variable('content', $d['larticle_content']);
            add_variable('price_spa', get_additional_field($d['larticle_id'], 'price_spa', 'spa'));
            add_variable('time_spa', get_additional_field($d['larticle_id'], 'time_spa', 'spa'));
            add_variable('number', $number.'link');
            $number++;
            parse_template('loop_list_spa_block', 'llsblock', true);
        }
    }



    parse_template('spa_legain_block', 'slblock');

    return return_template('modal_spa_legian_template');
}


/*
| -----------------------------------------------------------------------------
| Special Offers Label Block
| -----------------------------------------------------------------------------
*/
function get_special_offers_label()
{
    set_template(TEMPLATE_PATH . '/template/part/first/special_offers_label.html', 'special_offers_label_template');
    add_block('special_offers_label_block', 'solblock', 'special_offers_label_template');

    add_variable('themes_url', TEMPLATE_URL);

    parse_template('special_offers_label_block', 'solblock');

    return return_template('special_offers_label_template');
}

function get_special_offers_label_mobile()
{
    set_template(TEMPLATE_PATH . '/template/part/first/special_offers_label_mobile.html', 'special_offers_label_template_mobile');
    add_block('special_offers_label_mobile_block', 'solmblock', 'special_offers_label_template_mobile');

    add_variable('themes_url', TEMPLATE_URL);

    parse_template('special_offers_label_mobile_block', 'solmblock');

    return return_template('special_offers_label_template_mobile');
}


/*
| -----------------------------------------------------------------------------
| Get First Navigation
| -----------------------------------------------------------------------------
*/
function get_first_navigation()
{
    set_template(TEMPLATE_PATH . '/template/part/first/navigation.html', 'first_nav_template');
    add_block('navigation_block', 'fntblock', 'first_nav_template');
    add_variable('list_destination_page', get_destination_link());
    add_variable('themes_url', TEMPLATE_URL);
    add_variable('book_link', get_meta_data('link_booknow_desktop', 'global_setting'));
    add_variable('url', 'http://'. SITE_URL);

    parse_template('navigation_block', 'fntblock');

    return return_template('first_nav_template');
}

/*
| -----------------------------------------------------------------------------
| Get First TOP Slider
| -----------------------------------------------------------------------------
*/
function get_first_slider_top()
{
    set_template(TEMPLATE_PATH . '/template/part/first/slider_home.html', 'first_slider_template');
    add_block('slider_home_loop', 'shlblock', 'first_slider_template');
    add_block('slider_home_block', 'shbblock', 'first_slider_template');

    global $db;

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY larticle_id DESC LIMIT 3';
    $q = $db->prepare_query($s, 'special-offers');
    $r = $db->do_query($q);
    $number = 1;
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            add_variable('title', $d['larticle_title']);
            add_variable('content', get_additional_field($d['larticle_id'], 'brief', 'special-offers'));
            add_variable('number', $number.'link');
            $number++;
            parse_template('slider_home_loop', 'shlblock', true);
        }
    }

    parse_template('slider_home_block', 'shbblock', false);

    return return_template('first_slider_template');
}

/*
| -----------------------------------------------------------------------------
| Get First TOP Slider Mobile
| -----------------------------------------------------------------------------
*/
function get_first_slider_mobile_top()
{
    set_template(TEMPLATE_PATH . '/template/part/first/slider_home_mobile.html', 'first_slider_mobile_template');
    add_block('slider_home_mobile_loop', 'shmlblock', 'first_slider_mobile_template');
    add_block('slider_home_mobile_block', 'shmbblock', 'first_slider_mobile_template');


    global $db;

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY larticle_id DESC LIMIT 3';
    $q = $db->prepare_query($s, 'special-offers');
    $r = $db->do_query($q);

    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            add_variable('title', $d['larticle_title']);
            add_variable('content', get_additional_field($d['larticle_id'], 'brief', 'special-offers'));
            parse_template('slider_home_mobile_loop', 'shmlblock', true);
        }
    }

    parse_template('slider_home_mobile_block', 'shmbblock');

    return return_template('first_slider_mobile_template');
}

/*
| -----------------------------------------------------------------------------
| Get First Label Book
| -----------------------------------------------------------------------------
*/
function get_first_labelbook_top($post_id='')
{
    set_template(TEMPLATE_PATH . '/template/part/label_book.html', 'first_labelbook_template');
    add_block('label_book_block', 'lbblock', 'first_labelbook_template');

    $sef  = get_uri_sef();
    $url  = cek_url();
    $home = array('special-offers','');
    $fhp  = array('legian','seminyak','airport');

    if (get_uri_count($url) == 1) {
        if (in_array($sef, $home)) {
            // add_variable('book_now_link', get_meta_data('link_booknow_mobile', 'global_setting'));
            add_variable('special-offers', 'special-offers');
        } elseif (in_array($sef, $fhp)) {
            if (!empty($post_id)) {
                // add_variable('book_now_link', get_book_now_link($post_id));
                add_variable('special-offers', 'http://' . site_url() . '/'.$sef.'/special-offers');
            }
        }
    } elseif (get_uri_count($url) == 2) {
        if (!empty($post_id)) {
            // add_variable('book_now_link', get_book_now_link($post_id));
            add_variable('special-offers', 'http://' . site_url() . '/'.$url[0].'/special-offers');
        }
    }



    add_variable('themes_url', TEMPLATE_URL);

    parse_template('label_book_block', 'lbblock');

    return return_template('first_labelbook_template');
}


function get_gmaps_n_book_block($post_id='')
{
    set_template(TEMPLATE_PATH . '/template/part/gmaps_n_book.html', 'gmaps_n_book_template');
    add_block('gmaps_n_book_block', 'gnblock', 'gmaps_n_book_template');

    add_variable('themes_url', TEMPLATE_URL);
    add_variable('site_url', 'http://'. SITE_URL);

    $sef  = get_uri_sef();
    $url  = cek_url();
    $home = array('special-offers','');
    $fhp  = array('legian','seminyak','airport');

    add_variable('sef', $url[0]);
    add_variable('link_book', get_book($sef, $url, $home, $fhp, $post_id));

    parse_template('gmaps_n_book_block', 'gnblock');

    return return_template('gmaps_n_book_template');
}

function get_book_now_block($post_id="")
{
    set_template(TEMPLATE_PATH . '/template/part/book_now.html', 'book_now_template');
    add_block('book_now_block', 'bnblock', 'book_now_template');

    $sef  = get_uri_sef();
    $url  = cek_url();
    $home = array('special-offers','');
    $fhp  = array('legian','seminyak','airport');


    add_variable('link_book', get_book($sef, $url, $home, $fhp, $post_id));
    add_variable('themes_url', TEMPLATE_URL);
    add_variable('sef', $url[0]);

    parse_template('book_now_block', 'bnblock');

    return return_template('book_now_template');
}

function get_book($sef, $url, $home, $fhp, $post_id)
{
    if (get_uri_count($url) == 1) {
        if (in_array($sef, $home)) {
            return get_meta_data('link_booknow_mobile', 'global_setting');
        } elseif (in_array($sef, $fhp)) {
            if (!empty($post_id)) {
                // return get_book_now_link($post_id);
            }
        }
    } elseif (get_uri_count($url) == 2) {
        if (!empty($post_id)) {
            // return get_book_now_link($post_id);
        }
    }
}

// function get_label_info($post_sef, $post_id)
// {
//     set_template(TEMPLATE_PATH . '/template/part/label_info.html', 'label_info_template');
//     add_block('label_info_block', 'litblock', 'label_info_template');
//
//     add_variable('title', $post_sef);
//     add_variable('address_label', get_additional_field($post_id, 'address', 'destinations'));
//     add_variable('phone_number_label', get_additional_field($post_id, 'phone_number', 'destinations'));
//     add_variable('themes_url', TEMPLATE_URL);
//
//     parse_template('label_info_block', 'litblock');
//
//     return return_template('label_info_template');
// }


/*
| -----------------------------------------------------------------------------
| Get All Image First Home Content
| -----------------------------------------------------------------------------
*/
function get_first_image_seminyak()
{
    $img = get_meta_data('fhome_seminyak_image', 'global_setting');

    if (!empty($img)) {
        return 'http://' . site_url() . '/lumonata-plugins/global/background/' . $img;
    }
}
function get_first_image_legian()
{
    $img = get_meta_data('fhome_legian_image', 'global_setting');

    if (!empty($img)) {
        return 'http://' . site_url() . '/lumonata-plugins/global/background/' . $img;
    }
}
function get_first_image_airport()
{
    $img = get_meta_data('fhome_airport_image', 'global_setting');

    if (!empty($img)) {
        return 'http://' . site_url() . '/lumonata-plugins/global/background/' . $img;
    }
}

/*
| -----------------------------------------------------------------------------
| Get Homepage TOP Background
| -----------------------------------------------------------------------------
*/
function get_fhomepage_top_background()
{
    $img = get_meta_data('fhome_bg_image', 'global_setting');

    if (!empty($img)) {
        return "http://". site_url() . "/lumonata-plugins/global/background/". $img;
    }
}


/*
| -----------------------------------------------------------------------------
| Get Homepage TOP Background Special Offers
| -----------------------------------------------------------------------------
*/
function get_fhomepage_top_background_special_offers()
{
    $img = get_meta_data('fhome_bg_special_offers', 'global_setting');
    // print_r($img);
    // exit;
    if (!empty($img)) {
        return "http://". site_url() . "/lumonata-plugins/global/background/". $img;
    }
}


/*
| -----------------------------------------------------------------------------
| Get Background All Home Pages Destination
| -----------------------------------------------------------------------------
*/
function get_destinations_background($post_id)
{
    $img = get_additional_field($post_id, 'background_image', 'destinations');

    if (!empty($img)) {
        return "http://". site_url() . "/lumonata-plugins/destinations/background/". $img;
    }
}

/*
| -----------------------------------------------------------------------------
| Get Background All Pages Next Destination
| -----------------------------------------------------------------------------
*/
function get_destination_page_background($post_id, $sef)
{
    if ($sef == 'about') {
        $sef = 'about-us';
    }

    $img = get_post_type_setting($post_id, $sef);

    $front_image = $img['background'];

    if (!empty($img)) {
        return "http://". site_url() . "/lumonata-plugins/custom-post/background/". $front_image;
    }
}

/*
| -----------------------------------------------------------------------------
| Get First Top Slider Destination Desktop and Mobile
| -----------------------------------------------------------------------------
*/
function get_first_slider_destination_top($post_sef)
{
    set_template(TEMPLATE_PATH . '/template/part/second/slider_home.html', 'slider_detail_template');
    add_block('slider_home_detail_loop', 'shdlblock', 'slider_detail_template');
    add_block('slider_home_detail_block', 'shdblock', 'slider_detail_template');

    $part = cek_url();

    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND larticle_type=%s';
    $q = $db->prepare_query($s, $post_sef, 'special-offers'); // legian
    $r = $db->do_query($q);

    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            add_variable('title', $d['larticle_title']);
            add_variable('content', get_additional_field($d['larticle_id'], 'brief', 'special-offers'));
            add_variable('url', 'http://' . site_url() . '/'.$part[0].'/special-offers');
            parse_template('slider_home_detail_loop', 'shdlblock', true);
        }
    }

    parse_template('slider_home_detail_block', 'shdblock', false);

    return return_template('slider_detail_template');
}


function get_first_slider_destination_top_mobile($post_sef)
{
    set_template(TEMPLATE_PATH . '/template/part/second/slider_home_mobile.html', 'slider_detail_mobile_template');
    add_block('slider_home_detail_loop_mobile', 'shdlmblock', 'slider_detail_mobile_template');
    add_block('slider_home_detail_mobile_block', 'shdmblock', 'slider_detail_mobile_template');

    $part = cek_url();

    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id WHERE lvalue=%s AND larticle_type=%s';
    $q = $db->prepare_query($s, $post_sef, 'special-offers'); // legian
    $r = $db->do_query($q);

    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            add_variable('title', $d['larticle_title']);
            add_variable('content', get_additional_field($d['larticle_id'], 'brief', 'special-offers'));
            add_variable('url', 'http://' . site_url() . '/'.$part[0].'/special-offers');
            parse_template('slider_home_detail_loop_mobile', 'shdlmblock', true);
        }
    }

    parse_template('slider_home_detail_mobile_block', 'shdmblock', false);

    return return_template('slider_detail_mobile_template');
}

/*
| -----------------------------------------------------------------------------
| Get Grand Label Desktop and Mobile
| -----------------------------------------------------------------------------
*/
function get_grand_label($post_sef, $sef)
{
    if ($sef == 'special-offers') {
        $sef = 'Special Offers';
    }
    set_template(TEMPLATE_PATH . '/template/part/second/grand_label.html', 'grand_label_template');
    add_block('grand_label_block', 'shdblock', 'grand_label_template');
    add_variable('title_destination', $post_sef);
    add_variable('title_page', $sef);

    add_variable('themes_url', TEMPLATE_URL);

    parse_template('grand_label_block', 'shdblock', false);

    return return_template('grand_label_template');
}

function get_grand_label_mobile($post_sef, $sef)
{
    if ($sef == 'special-offers') {
        $sef = 'Special Offers';
    }
    set_template(TEMPLATE_PATH . '/template/part/second/grand_label_mobile.html', 'grand_label_mobile_template');
    add_block('grand_label_mobile_block', 'glmblock', 'grand_label_mobile_template');
    add_variable('title_destination', $post_sef);
    add_variable('title_page', $sef);

    add_variable('themes_url', TEMPLATE_URL);

    parse_template('grand_label_mobile_block', 'glmblock', false);

    return return_template('grand_label_mobile_template');
}

function get_grand_label_spa()
{
    set_template(TEMPLATE_PATH . '/template/part/second/grand_label_spa.html', 'grand_label_template');
    add_block('grand_label_block', 'shdblock', 'grand_label_template');


    parse_template('grand_label_block', 'shdblock', false);

    return return_template('grand_label_template');
}

function get_grand_label_mobile_spa()
{
    set_template(TEMPLATE_PATH . '/template/part/second/grand_label_mobile_spa.html', 'grand_label_mobile_template');
    add_block('grand_label_mobile_block', 'glmblock', 'grand_label_mobile_template');

    add_variable('themes_url', TEMPLATE_URL);

    parse_template('grand_label_mobile_block', 'glmblock', false);

    return return_template('grand_label_mobile_template');
}

function get_footer_slider($post_id, $sef)
{
    set_template(TEMPLATE_PATH . '/template/part/why-stay-with-us.html', 'stay_with_us_template');
    add_block('why_stay_with_us', 'wswublock', 'stay_with_us_template');
    add_variable('themes_url', TEMPLATE_URL);
    $part = cek_url();

    add_variable('sef', $part[0]);

    $footer = footer_list($part[0]);
    add_variable('footer_list', $footer);


    parse_template('why_stay_with_us', 'wswublock', false);

    return return_template('stay_with_us_template');
}

function footer_list($part)
{
    global $db;

    $s = 'SELECT * FROM lumonata_rules WHERE lrule=%s AND lgroup=%s';
    $q = $db->prepare_query($s, 'categories', 'featured');
    $r = $db->do_query($q);

    $content ='';
    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $validate = validation_having_categories($part, $d['lsef']);
            $sub = get_sub_item($part, $d['lsef']);
            if ($validate > 1) {
                $content .= '<div class="item">
                                <div class="sub-list">
                                '.$sub.'
                                </div>
                            </div>';
            } elseif ($validate == 1) {
                $content .= '<div class="item">
                              '.$sub.'
                              </div>';
            }
        }
    }
    return $content;
}

function validation_having_categories($sef, $lsef)
{
    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a
  INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
  INNER JOIN lumonata_rule_relationship AS c ON b.lapp_id = c.lapp_id
  INNER JOIN lumonata_rules AS d ON c.lrule_id = d.lrule_id
  WHERE b.lvalue=%s AND a.larticle_type=%s AND d.lsef=%s';
    $q = $db->prepare_query($s, $sef, 'featured', $lsef);
    $r = $db->do_query($q);
    return $db->num_rows($r);
}

function get_sub_item($sef, $lsef)
{
    global $db;

    $s = 'SELECT * FROM lumonata_articles AS a
    INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
    INNER JOIN lumonata_rule_relationship AS c ON b.lapp_id = c.lapp_id
    INNER JOIN lumonata_rules AS d ON c.lrule_id = d.lrule_id
    WHERE b.lvalue=%s AND a.larticle_type=%s AND d.lsef=%s';
    $q = $db->prepare_query($s, $sef, 'featured', $lsef);
    $r = $db->do_query($q);
    $list ='';



    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $img    = get_additional_field($d['larticle_id'], 'main_image', 'featured');
            $link   = get_additional_field($d['larticle_id'], 'website_link', 'featured');
            $bg  = 'http://' . site_url() . '/lumonata-plugins/additional/background/' . $img;
            $title  = $d['larticle_title'];
            $content = $d['larticle_content'];
            $list .= '<div class="sub-item sub-items">
              <a href="#">
                <a href="'.$link.'"><img src="'.$bg.'" alt=""></a>
              </a>
              <div class="container-desc">
                <div class="category">
                  <a href="'.$link.'">'.$lsef.'</a>
                </div>
                <div class="name">
                <a href="'.$link.'">'.$title.'</a>
                </div>
                <div class="link">
                    <a href=="'.$link.'">See Detail</a>
                  </div>
              </div>
            </div>';
        }
    }

    return $list;
}


/*
| -----------------------------------------------------------------------------
| Get Instagram Photo
| -----------------------------------------------------------------------------
*/
function get_instagram_photo()
{
    $access_token = get_meta_data("ig_token", 'global_setting');
    $photo_count  = 10;

    $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
    $json_link.="access_token={$access_token}&count={$photo_count}";

    $json = file_get_contents($json_link);
    $obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
    $list = '<section class="instagram">
      <div class="container">
        <div class="title">
          WHAT’S NEW IN
        </div>
        <div class="subtitle">
          GRANDMAS HOTELS
        </div>
        <div class="line">
          <img src="http://'.site_url().'/lumonata-content/themes/custom/images/line.svg" alt="">
        </div>
        <div class="insta-photo">
          <ul class="list-unstyled list-inline">';

    foreach ($obj['data'] as $post) {
        $pic_src = $post['images']['standard_resolution']['url'];
        $link_src = $post['link'];
        $list .='<li><a href="#"><img src="'.$pic_src.'" class="img-responsive" alt=""></a></li>';
    }

    $list .='</ul></div>
          </div>
        </section>';
    return $list;
}

/*
| -----------------------------------------------------------------------------
| GET DESTINATION DATA LIST TOP
| -----------------------------------------------------------------------------
*/

function get_destination_list_data_top()
{
    global $db;

    $result = '';

    $s = 'SELECT larticle_id, larticle_title, larticle_content, lsef
		  FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY larticle_title DESC';
    $q = $db->prepare_query($s, 'destinations', 'publish');
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        while ($data = $db->fetch_array($r)) {
            $result .= '<li data-filter=".'.$data['lsef'].'">'.$data['larticle_title'].'</li>';
        }
    }
    return $result;
}


/*
| -----------------------------------------------------------------------------
| GET FIRST IMAGE DESTINATION
| -----------------------------------------------------------------------------
*/
function get_first_image_destination($post_sef)
{
    if ($post_sef == 'seminyak') {
        $img = get_meta_data('fhome_seminyak_image', 'global_setting');
    } elseif ($post_sef == 'legian') {
        $img = get_meta_data('fhome_legian_image', 'global_setting');
    } else {
        $img = get_meta_data('fhome_airport_image', 'global_setting');
    }

    if (!empty($img)) {
        return 'http://' . site_url() . '/lumonata-plugins/global/background/' . $img;
    }
}

/*
| -----------------------------------------------------------------------------
| Get FIrst Brief NAVIGATIOn
| -----------------------------------------------------------------------------
*/
function get_first_brief_destination($post_sef)
{
    if ($post_sef == 'seminyak') {
        $brief = get_meta_data('fhomepage_brief_seminyak', 'global_setting');
    } elseif ($post_sef == 'legian') {
        $brief = get_meta_data('fhomepage_brief_legian', 'global_setting');
    } else {
        $brief = get_meta_data('fhomepage_brief_airport', 'global_setting');
    }

    return $brief;
}

/*
| -----------------------------------------------------------------------------
| Get Second Navigation
| -----------------------------------------------------------------------------
*/
function get_second_navigation($post_sef, $sef='', $post_id='')
{
    set_template(TEMPLATE_PATH . '/template/part/second/navigation.html', 'second_nav_template');
    add_block('navigation_second_block', 'sfntblock', 'second_nav_template');

    add_variable('destinations_page', $post_sef);
    add_variable('site_url', SITE_URL);
    add_variable('themes_url', TEMPLATE_URL);

    $sef  = get_uri_sef();
    $url  = cek_url();
    $home = array('special-offers','');
    $fhp  = array('legian','seminyak','airport');

    add_variable('link_book', get_book($sef, $url, $home, $fhp, $post_id));
    add_variable('list_destinations', get_list_desti($sef, $url, $home, $fhp, $post_id));
    add_variable('url', 'http://'.SITE_URL.'/'.$url[0]);

    if ($url[0] == 'legian') {
        add_variable('logo_nav', '<a href="http://'.SITE_URL.'/'.$url[0].'"><img src="http://'.TEMPLATE_URL.'/images/logo-legian.svg" alt=""></a>');
    } elseif ($url[0] == 'seminyak') {
        add_variable('logo_nav', '<a href="http://'.SITE_URL.'/'.$url[0].'"><img src="http://'.TEMPLATE_URL.'/images/logo-seminyak.svg" alt=""></a>');
    } elseif ($url[0] == 'airport') {
        add_variable('logo_nav', '<a href="http://'.SITE_URL.'/'.$url[0].'"><img src="http://'.TEMPLATE_URL.'/images/logo-airport.svg" alt=""></a>');
    }

    if ($sef == 'about') {
        add_variable('about', 'active');
    } elseif ($sef == 'rooms') {
        add_variable('rooms', 'active');
    } elseif ($sef == 'dining') {
        add_variable('dining', 'active');
    } elseif ($sef == 'spa') {
        add_variable('spa', 'active');
    } elseif ($sef == 'activities') {
        add_variable('activities', 'active');
    } elseif ($sef == 'gallery') {
        add_variable('gallery', 'active');
    } elseif ($sef == 'special-offers') {
        add_variable('offers', 'active');
    }


    parse_template('navigation_second_block', 'sfntblock');

    return return_template('second_nav_template');
}

function get_list_desti($sef, $url, $home, $fhp, $post_id)
{
    $list = '';
    if (get_uri_count($url) == 1) {
        if (in_array($sef, $fhp)) {
            if (!empty($post_id && $sef == 'legian')) {
                $list .= '<option value="http://'.site_url() .'/seminyak">Seminyak</option>
              <option value="http://'.site_url() .'/legian" selected>Legian</option>
              <option value="http://'.site_url() .'/airport">Airport</option>';
                return $list;
            } elseif (!empty($post_id && $sef == 'airport')) {
                $list .= '<option value="http://'.site_url() .'"/seminyak">Seminyak</option>
            <option value="http://'.site_url() .'/legian">Legian</option>
            <option value="http://'.site_url() .'/airport" selected>Airport</option>';
                return $list;
            } else {
                $list .= '<option value="http://'.site_url() .'/seminyak" selected>Seminyak</option>
            <option value="http://'.site_url() .'/legian" >Legian</option>
            <option value="http://'.site_url() .'/airport">Airport</option>';
                return $list;
            }
        }
    } elseif (get_uri_count($url) == 2) {
        if (in_array($url[0], $fhp)) {
            if (!empty($post_id && $url[0] == 'legian')) {
                $list .= '<option value="http://'.site_url() .'/seminyak">Seminyak</option>
            <option value="http://'.site_url() .'/legian" selected>Legian</option>
            <option value="http://'.site_url() .'/airport">Airport</option>';
                return $list;
            } elseif (!empty($post_id && $url[0] == 'airport')) {
                $list .= '<option value="http://'.site_url() .'/seminyak">Seminyak</option>
          <option value="http://'.site_url() .'/legian">Legian</option>
          <option value="http://'.site_url() .'/airport" selected>Airport</option>';
                return $list;
            } else {
                $list .= '<option value="http://'.site_url() .'/seminyak" selected>Seminyak</option>
          <option value="http://'.site_url() .'/legian">Legian</option>
          <option value="http://'.site_url() .'/airport">Airport</option>';
                return $list;
            }
        }
    }

    return $list;
}



/*
| -----------------------------------------------------------------------------
| Show Destination Page Detail
| -----------------------------------------------------------------------------
*/
function destination_page_detail($post, $dest, $type, $sef)
{
    $data = get_post_detail_by_type($dest, $type, $sef);

    $post_id = $post['larticle_id'];
    $dest    = $post['lsef'];
    $list    = get_post_by_type_list($dest, $sef);
    $setting = get_post_type_setting($post_id, $sef);

    //-- Get meta data
    $mtitle = get_meta_title($post_id);
    $mkey   = get_meta_keywords($post_id);
    $mdesc  = get_meta_description($post_id);
    $mtitle = (empty($mtitle) ? $setting['title'].' | '.$post['larticle_title'].' - '.trim(web_title()) : $mtitle);

    add_meta_data_value($mtitle, $mkey, $mdesc);

    parse_template('homeblock', 'hblock', false);
    return return_template('home_template');

    // return show_404();
}

function get_destination_link()
{
    global $db;

    $result = '';

    $s = 'SELECT larticle_id, larticle_title, larticle_content, lsef
		  FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY larticle_title DESC';
    $q = $db->prepare_query($s, 'destinations', 'publish');
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        while ($d=$db->fetch_array($r)) {
            $result .= '
            <li>
            	<a href="http://'.site_url().'/'.$d['lsef'].'">'.$d['larticle_title'].'</a>
            </li>';
        }
    }

    return $result;
}


function get_header_link($title)
{
    global $db;

    $result = '';

    $s = 'SELECT larticle_id, larticle_title, larticle_content, lsef
		  FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s';
    $q = $db->prepare_query($s, 'destinations', 'publish');
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        while ($d=$db->fetch_array($r)) {
            if ($title == $d['larticle_title']) {
                $result .= '
            <li class="active">
            	<a href="http://'.SITE_URL.'/'.$d['lsef'].'">'.$d['larticle_title'].'</a>
            </li>';
            } else {
                $result .= '
            <li>
            	<a href="http://'.SITE_URL.'/'.$d['lsef'].'">'.$d['larticle_title'].'</a>
            </li>';
            }
        }
    }

    return $result;
}


function is_special_offers()
{
    $part = cek_url();

    if ($part[0]=='special-offers') {
        return true;
    } else {
        return false;
    }
}

function is_destination_page()
{
    $part = cek_url();
    $a = array('legian','airport','seminyak');

    if (in_array($part[0], $a)) {
        return true;
    } else {
        return false;
    }
}



function curl_func($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $r = curl_exec($ch);
    curl_close($ch);
    $d = json_decode($r, true);
    if ($d === null && json_last_error() !== JSON_ERROR_NONE) {
        return array();
    } else {
        return $d;
    }
}

function get_location_data()
{
    global $db;

    $type = (isset($_POST['type']) ? strtolower($_POST['type']) : 'all');
    $list = '';

    $s = 'SELECT * FROM lumonata_articles AS a
		  INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
		  WHERE a.larticle_type=%s AND a.larticle_status=%s AND b.lkey=%s
		  ORDER BY (b.lvalue = %s) DESC, a.lorder';
    $q = $db->prepare_query($s, 'locations', 'publish', 'loc_corporate', $_POST['defsef']);
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    $res = array();

    if ($n > 0) {
        while ($d=$db->fetch_array($r)) {
            if ($type!='all') {
                if ($type==strtolower(get_additional_field($d['larticle_id'], 'loc_type', 'locations'))) {
                    $res[] = array(
                        'id'=>$d['larticle_id'],
                        'dest'=>$d['lvalue'],
                        'title'=>$d['larticle_title'],
                        'content'=>$d['larticle_content']
                    );
                }
            } else {
                $res[] = array(
                    'id'=>$d['larticle_id'],
                    'dest'=>$d['lvalue'],
                    'title'=>$d['larticle_title'],
                    'content'=>$d['larticle_content']
                );
            }
        }
    }

    return $res;
}

function get_approx_time($origin, $dest)
{
    $durl  = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$origin.'&destinations='.$dest.'&mode=driving&key=AIzaSyCKEd6RcImm2D_BKo2byZPmVUZLZ2Ht4Qs';
    $dtime = curl_func($durl);

    if (!empty($dtime) && $dtime['status']=='OK') {
        foreach ($dtime['rows'] as $el) {
            foreach ($el['elements'] as $obj) {
                if ($obj['status']=='OK') {
                    return array('text'=>$obj['distance']['text'], 'value'=>$obj['distance']['value']);
                    break;
                }
            }
        }
    }

    return array('text'=>'', 'value'=>time());
}

function sort_location_data($loc_data, $origin)
{
    $sort_loc_data = array();

    foreach ($loc_data as $dt) {
        $origin_coor = $origin['lat'].','.$origin['long'];
        $locate_coor = get_additional_field($dt['id'], 'loc_cordinate', 'locations');

        $dist = get_approx_time($origin_coor, $locate_coor);
        $sort_loc_data[$dist['value']] = array('data'=>$dt,'distance'=>$dist['text']);
    }

    ksort($sort_loc_data);
    return $sort_loc_data;
}

function validate_form_data()
{
    $error = array();

    if (empty($_POST['c_number'])) {
        $error['c-number'] = 'Input your confirmation number';
    }

    if (empty($_POST['hotel_name'])) {
        $error['hotel-name'] = 'Select your hotel name';
    }

    if (empty($_POST['arrival'])) {
        $error['arrival'] = 'Input your date of arrival';
    }

    if (empty($_POST['departure'])) {
        $error['departure'] = 'Input your date of departure';
    }

    if (empty($_POST['num_adults'])) {
        $error['num-adults'] = 'Input number of adults';
    }

    // if(empty($_POST['num_children']))
    // {
    // 	$error['num-children'] = 'Input number of children';
    // }

    if (empty($_POST['num_rooms'])) {
        $error['num-rooms'] = 'Input number of rooms';
    }

    if (empty($_POST['room_type'])) {
        $error['room-type'] = 'Input room type';
    }

    if (empty($_POST['total_rate_and_currency'])) {
        $error['total-rate-and-currency'] = 'Input total rate & currency';
    }

    if (empty($_POST['provider'])) {
        $error['provider'] = 'Input name of competing provider';
    }

    if (empty($_POST['total_rate'])) {
        $error['total-rate'] = 'Input total rate';
    }

    if (empty($_POST['currency_rate'])) {
        $error['currency-rate'] = 'Input currency rate';
    }

    if (empty($_POST['givenname'])) {
        $error['givenname'] = 'Input your first/given name';
    }

    if (empty($_POST['surename'])) {
        $error['surename'] = 'Input your family/surename';
    }

    if (empty($_POST['email_addr'])) {
        $error['email-addr'] = 'Input your valid email';
    } elseif (!isEmailAddress($_POST['email_addr'])) {
        $error['email-addr'] = 'Input your valid email';
    }

    if (empty($_POST['phone_number'])) {
        $error['phone-number'] = 'Input your mobile/phone number';
    }

    return $error;
}

function article_content()
{
    $dest    = get_appname();
    $list    = get_post_by_type_list($dest, 'articles');
    $result  = '';

    if (empty($list)) {
        return $result;
    }

    $result .= '
	<div class="article-list clearfix">';
    foreach ($list as $obj) {
        $main_image = get_additional_field($obj['larticle_id'], 'main_image', 'articles');
        $pdf_file   = get_additional_field($obj['larticle_id'], 'pdf_file', 'articles');
        $background = 'http://'.SITE_URL.'/lumonata-plugins/additional/background/'.$main_image;
        $pdf_link   = 'http://'.SITE_URL.'/lumonata-plugins/additional/pdf/'.$pdf_file;

        $result .= '
			<div id="'.$obj['lsef'].'" class="box b-lazy" data-src="'.$background.'">
	        	<div class="b-overlay">
	                <h3><a href="'.$background.'" class="fancybox" rel="articles-box">'.$obj['larticle_title'].'</a></h3>
	                <a href="'.$pdf_link.'" target="_blank" class="view-pdf">View PDF</a>
	            </div>
	        </div>';
    }

    $result .= '
	</div>';

    return $result;
}

function awards_content()
{
    $dest    = get_appname();
    $awards  = get_post_by_type_list($dest, 'awards');
    $result  = '';

    if (empty($awards)) {
        return $result;
    }

    $result = '
	<div class="awards-box clearfix">';
    foreach ($awards as $obj) {
        $gallery   = get_additional_field($obj['larticle_id'], 'main_image', 'awards');
        $preview   = '';

        if (!empty($gallery)) {
            $img_url  = 'http://'.SITE_URL.'/lumonata-plugins/additional/background/'.$gallery;
            $preview .= '<img class="item b-lazy" data-src="'.$img_url.'" src="'.$img_url.'">';
        }

        $result .= '
            <div id="'.$obj['lsef'].'" class="box">
                <div class="preview '.(empty($preview) ? 'hidden' : '').'">'.$preview.'</div>
                <div class="a-title">
                	<h3>'.$obj['larticle_title'].'</h3>
                </div>
            </div>';
    }
    $result .= '
    </div>';

    return $result;
}

function get_subpage($id, $type)
{
    global $db;

    $result = '';

    $s = "SELECT lapp_id FROM lumonata_additional_fields WHERE lkey=%s AND lapp_name=%s AND lvalue=%s";
    $q = $db->prepare_query($s, 'parent', $type, $id);
    $r = $db->do_query($q);

    while ($d = $db->fetch_array($r)) {
        $dd = fetch_artciles_by_id($d['lapp_id']);
        $result .= '
		<h2>'.$dd['larticle_title'].'</h2>
        <div class="desc">'.$dd['larticle_content'].'</div>';

        $q2 = $db->prepare_query($s, 'parent', $type, $d['lapp_id']);
        $r2 = $db->do_query($q2);

        if ($db->num_rows($r2) > 0) {
            $indx  = 0;
            $hlist = '';
            $hcont = '';
            while ($d2 = $db->fetch_array($r2)) {
                $ddd = fetch_artciles_by_id($d2['lapp_id']);
                $hlist .= '
				<li class="'.($indx==0 ? 'selected' : '').'">
            		<a data-id="'.$ddd['lsef'].'">'.$ddd['larticle_title'].'</a>
            	</li>';

                $hcont .= '
	            <div data-id-content="'.$ddd['lsef'].'" class="box">
	                <a class="mobile-subpage-link">'.$ddd['larticle_title'].'</a>
	                <div class="subpage-tab-inner-content">
					    <div class="the-subpage">
		                    <div class="the-subpage-block">
		                        <div class="title">
		                            <h3>'.$ddd['larticle_title'].'</h3>
		                        </div>
		                    </div>
	                        <div class="inner">
	                        	<div class="inner-desc">
	                        		'.$ddd['larticle_content'].'
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	            </div>';

                $indx++;
            }

            $result .= '
            <div class="section-wrapp">
                <div class="subpage-tab">
					<div class="subpage-tab-head">
			            <ul>'.$hlist.'</ul>
			        </div>
        			<div class="subpage-tab-content">
        				'.$hcont.'
        			</div>
                </div>
            </div>';
        }
    }

    return $result;
}

function return_post_description($data)
{
    $result = $data;

    if (!empty($data)) {
        preg_match_all('/\[([a-zA-Z0-9-_]+?)\]/', $data, $output);

        foreach ($output[1] as $obj) {
            $result = str_replace('['.$obj.']', run_shortcode($obj), $data);
        }
    }

    return $result;
}

function run_shortcode($actions)
{
    if (function_exists($actions)) {
        return call_user_func_array($actions, array());
    } else {
        return $actions;
    }
}

function get_google_adwords_conversation($appname)
{
    if ($appname=='ubud') {
        return '
		<script type="text/javascript">
			(function(a, e, c, f, g, h, b, d) {
			    var
			        k = {
			            ak: "863739964",
			            cl: "HW04CLjbtm0QvMDumwM",
			            autoreplace: "+62361977888"
			        };
			    a[c] = a[c] || function() {
			            (a[c].q = a[c].q || []).push(arguments)
			        };
			    a[g] ||
			        (a[g] = k.ak);
			    b = e.createElement(h);
			    b.async = 1;
			    b.src = "//www.gstatic.com/wcm/loader.js";
			    d = e.getElementsByTagName(h)[0];
			    d.parentNode.insertBefore(b, d);
			    a[f] = function(b, d, e) {
			        a[c](2, b, k, d, null, new Date, e)
			    };
			    a[f]()
			})(window, document, "_googWcmImpl", "_googWcmGet", "_googWcmAk", "script");
		</script>';
    } elseif ($appname=='sanur') {
        return '
		<script type="text/javascript">
			(function(a, e, c, f, g, h, b, d) {
			    var
			        k = {
			            ak: "863739964",
			            cl: "41R1COamsm0QvMDumwM",
			            autoreplace: "+623618497800"
			        };
			    a[c] = a[c] || function() {
			            (a[c].q = a[c].q || []).push(arguments)
			        };
			    a[g] ||
			        (a[g] = k.ak);
			    b = e.createElement(h);
			    b.async = 1;
			    b.src = "//www.gstatic.com/wcm/loader.js";
			    d = e.getElementsByTagName(h)[0];
			    d.parentNode.insertBefore(b, d);
			    a[f] = function(b, d, e) {
			        a[c](2, b, k, d, null, new Date, e)
			    };
			    a[f]()
			})(window, document, "_googWcmImpl", "_googWcmGet", "_googWcmAk", "script");
	    </script>';
    }
}

function set_ajax_page()
{
    if (isset($_POST['pkey']) and $_POST['pkey']=='view-locations') {
        $asd = get_location_datas();
        echo json_encode($asd);
    }

    if (isset($_POST['pkey']) and $_POST['pkey'] == 'subscribe') {
        $api        = get_meta_data('api_mailchimp', 'global_setting');
        $list_id     = get_meta_data('list_id_mailchimp', 'global_setting');
        $MailChimp  = new MailChimp($api);
        $MailChimp->verify_ssl = false;

        $data = '';

        $result = $MailChimp->post("lists/$list_id/members", [
                'email_address' => $_POST['email'],
                'status'        => 'subscribed',
            ]);

        if ($result['status'] == 'subscribed') {
            $data['title'] = 'Success';
            $data['data']   = 'Thank you for your subscibe.';
        } else {
            $data['title'] = 'Sorry';
            $data['data']   = 'Your email has subscribed on our list';
        }

        echo json_encode($data);
    }

    if (isset($_POST['pkey']) and $_POST['pkey']=='bg-home-slider') {
        $data = get_bg_slider();
        echo json_encode($data);
    }
    exit;
}
