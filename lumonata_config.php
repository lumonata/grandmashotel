<?php

	//- Define the database setting here
	define('HOSTNAME','localhost');
	define('DBUSER','root');
	define('DBPASSWORD','');
	define('DBNAME','grandmas');		
	define('ERR_DEBUG',true);

	if(!defined('ROOT_PATH'))
	{
		define('ROOT_PATH',dirname(__FILE__));
	}

	require_once(ROOT_PATH.'/lumonata-functions/error_handler.php');
	require_once(ROOT_PATH.'/lumonata-classes/db.php');

?>
