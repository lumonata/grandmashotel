<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home Page Example</title>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="ichronoz/css/custom.css">
</head>
<body>
  <section class="wrapper-header bg-book">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="logo">
            <img src="ichronoz/img/grandmas logo.svg" alt="">
          </div>
        </div>
        <div class="col-lg12 col-md-12 col-sm-12 col-xs-12">
          <?php include "reservation_form_horizontal_multyproperty.php" ?>
        </div>
      </div>
    </div>
  </section>

</body>
</html>
