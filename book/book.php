<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home Page Example</title>
	<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
	<!-- iChronoz css -->
		<?php $version = '2.0.5' ?>
		<link rel="stylesheet" type="text/css" href="<?php echo 'ichronoz/css/ichronoz.css?'.$version ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo 'ichronoz/css/font-awesome.css?'.$version ?> " />
	<!-- End iChronoz css -->

	<!-- iChronoz js -->
		<!-- just comment this line if jquery exist before -->
		<script type="text/javascript" src="<?php echo 'ichronoz/js/jquery.min.js?'.$version ?>"></script>

		<script type="text/javascript" src="<?php echo 'ichronoz/js/angular.min.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/angular-modal-service.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/ui-bootstrap.min.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/angular-ui-router.min.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/angular-idle.min.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/angular-sanitize.min.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/angular-animate.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/angular-filter.min.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/app.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/book.js?'.$version ?> "></script>
		<script type="text/javascript" src="<?php echo 'ichronoz/js/calendar.js?'.$version ?> "></script>
		<?php
		$api = "0f5361e1548ea8a0eb36c4ffca50d37c0"; // change this value with hotel apikey
		$max_adult = 3; // max adult in search form
		$max_child = 2; // max child in search form
		$max_pax = 9; // max adult in search form

		//Valid String POST / GET / REQUEST
		function inputSecurity($validate=null) {
		    if ($validate == null) {
		        foreach ($_REQUEST as $key => $val) {
		            if (is_string($val)) {
		                $_REQUEST[$key] = htmlentities($val);
		            } else if (is_array($val)) {
		                $_REQUEST[$key] = inputSecurity($val);
		            }
		        }
		        foreach ($_GET as $key => $val) {
		            if (is_string($val)) {
		                $_GET[$key] = htmlentities($val, ENT_QUOTES, 'UTF-8');
		            } else if (is_array($val)) {
		                $_GET[$key] = inputSecurity($val);
		            }
		        }
		        foreach ($_POST as $key => $val) {
		            if (is_string($val)) {
		                $_POST[$key] = htmlentities($val, ENT_QUOTES, 'UTF-8');
		            } else if (is_array($val)) {
		                $_POST[$key] = inputSecurity($val);
		            }
		        }
		    } else {
		        foreach ($validate as $key => $val) {
		            if (is_string($val)) {
		                $validate[$key] = htmlentities($val, ENT_QUOTES, 'UTF-8');
		            } else if (is_array($val)) {
		                $validate[$key] = inputSecurity($val);
		            }
		            return $validate;
		        }
		    }
		}
		inputSecurity();

		if(!isset($_POST['cd']) || $_POST['cd'] == ''){
			if(isset($_GET['fy'])){
				$fy=$_GET['fy'];
				$fm=$_GET['fm'];
				$fd=$_GET['fd'];
				$fromdate = strtotime("$fy-$fm-$fd");
			}else{
				$fromdate = time();
				$fy=date('Y', strtotime($fromdate));
				$fm=date('m', strtotime($fromdate));
				$fd=date('d', strtotime($fromdate));
			}

			$now = time();
			if($now <= $fromdate){
				$bsk = strtotime("$fy-$fm-$fd +1 days");
				$cd = date("j", $fromdate );
				$cm = date("n", $fromdate );
				$cy = date("Y", $fromdate );

				$cod = date("j", $bsk);
				$com = date("n", $bsk);
				$coy = date("Y", $bsk);
			}else{
				$now = strtotime("tomorrow");
				$bsk = strtotime("+ 2 days");
				$cd = date("j", $now);
				$cm = date("n", $now);
				$cy = date("Y", $now);

				$cod = date("j", $bsk);
				$com = date("n", $bsk);
				$coy = date("Y", $bsk);
			}
		}
		$cd = isset($_REQUEST['cd'])?$_REQUEST['cd']:$cd; //check in day
		$cm = isset($_REQUEST['cm'])?$_REQUEST['cm']:$cm; //check in month
		$cy = isset($_REQUEST['cy'])?$_REQUEST['cy']:$cy; //check in year
		$cod = isset($_REQUEST['cod'])?$_REQUEST['cod']:$cod; //check out day
		$com = isset($_REQUEST['com'])?$_REQUEST['com']:$com; //check out month
		$coy = isset($_REQUEST['coy'])?$_REQUEST['coy']:$coy; //check out year
		$pax = isset($_REQUEST['pax'])?$_REQUEST['pax']:1; //  pax
		$adult = isset($_REQUEST['adult'])?$_REQUEST['adult']:2; //adults
		$child = isset($_REQUEST['child'])?$_REQUEST['child']:0; //childs
		$nite = isset($_REQUEST['nite'])?$_REQUEST['nite']:1; // per night
		$cbs = isset($_REQUEST['cbs'])?$_REQUEST['cbs']:0;
		$promo = isset($_REQUEST['promo'])?$_REQUEST['promo']: '';
		$utc = isset($_REQUEST['utc'])?$_REQUEST['utc']:'';
		$hid = isset($_REQUEST['hid'])?$_REQUEST['hid']:'';
		$url = isset($_REQUEST['url'])?$_REQUEST['url']:''; // url
		?>
		<script type="text/javascript" charset="utf-8">
			var objek = new Array('<?php echo $hid."', '".$cd."', '".$cm."', '".$cy."', '".$cod."', '".$com."', '".$coy."', '".$pax."', '".$adult."', '".$child."', '".$url."', '".$api."', '".$utc."', '".$nite."', '".$cbs."', '".$promo."', '".$max_adult."', '".$max_child."', '".$max_pax ?>');
			var tags = objek;
			console.log(tags);
		</script>

		<link rel="stylesheet" href="<?php echo 'ichronoz/js/venobox/venobox.css?'.$version ?>" type="text/css" media="screen" />
		<script type="text/javascript" src="<?php echo 'ichronoz/js/venobox/venobox.min.js?'.$version ?>"></script>
	<!-- End iChronoz js -->

</head>
<body>

	<div class="row">
		<div class="col-md-12">
			<!-- iChronoz Content -->
				<div class="chr_container">
					<div ng-app="ichronoz" class="chr_row chronoz">
						<div ui-view></div>
						<section data-ng-controller="sessionExpired">
						</section>
					</div>
				</div>
			<!-- End iChronoz Content -->
		</div>
	</div>

	</div>

</body>
</html>
