
<link rel="stylesheet" href="ichronoz/css/ichronoz.css" />
<link rel="stylesheet" href="ichronoz/css/datepicker.css" />
<link rel="stylesheet" href="ichronoz/css/font-awesome.css" />

<?php
$domain = 'http://lumonatalabs.com/grandmashotel/book/'; // change this value with posting page. example. http://ichronoz.com
$actionParsing = $domain.'/book.php';
$amendUrl = $domain.'/ichronoz/amend.html';
?>

<div class="chronoz">
  <form name="searcForm" id="searcForm" action="<?php echo $actionParsing; ?>" method="POST" target="_self" class="form-reserve">
    <div class="">
      <div class="chr_row chr_title">
        <div class="chr_col-md-12 chr_col-sx-12 chr_text-center">
          <div class="chr_ptop10 chr_pb15">
              <span >SIMPLY BOOK HERE</span>
          </div>
        </div>
        <div class="chr_col-md-2">
          <span>Property: </span>
					<select name="hid" class="chr_form-control" id="hid" >
            <option value="272">Grandmas Plus Hotel Seminyak</option>
            <option value="273">Grandmas Plus Hotel Legian</option>
            <option value="274">Grandmas Plus Hotel Airport</option>
					</select>
				</div>
				<div class="chr_col-lg-4 chr_col-md-12 chr_col-sm-12 chr_col-xs-12 form-group has-feedback">
					<div class="chr_row">
						<div class="chr_col-md-6 chr_col-sm-6 chr_col-xs-12 form-group has-feedback">
              <span><span>Check-in: &nbsp;</span><span id="cday"></span></span>
              <div class='chr_input-group date' id='datetimepicker1' style="background: none;width: auto;height: auto;">
                <input id="check_in" type='text' placeholder="Check-in" class="chr_form-control datepickerbutton" style="cursor: pointer;" />
	            </div>
						</div>
						<div class="chr_col-md-6 chr_col-sm-6 chr_col-xs-12">
              <span>Check-out: &nbsp;</span>
              <div class='chr_input-group date' id='datetimepicker2' style="background: none;width: auto;height: auto;">
                <input id="check_out" type='text' placeholder="Check-out" class="chr_form-control datepickerbutton" style="cursor: pointer;" />
	            </div>
            </div>
					</div>
				</div>

                <div class="chr_col-lg-2 chr_col-md-6 chr_col-sm-6 chr_col-xs-12 form-group has-feedback">
                	<div class="chr_row">
                		<div class="chr_col-md-6 chr_col-sm-6 chr_col-xs-6">
                			<span>Adult:</span>
							<span class=''>
			                    <select class="chr_form-control" id="adult" name="adult">
			                        <option>1</option>
			                        <option selected="selected">2</option>
			                        <option>3</option>
			                    </select>
			                      <!-- <i class="glyphicon glyphicon-user chr_form-control-feedback"></i> -->
			                </span>
                		</div>
                		<!-- <div class="chr_col-md-4 chr_col-sm-4 chr_col-xs-4">
                			<span>Child:</span>
							<span class=''>
			                    <select class="chr_form-control" id="adult" name="adult">
			                        <option selected="selected">0</option>
			                        <option>1</option>
			                        <option>2</option>
			                    </select>
			                </span>
                		</div> -->
                		<div class="chr_col-md-6 chr_col-sm-6 chr_col-xs-6">
                			<span>Rooms:</span>
							<span class=''>
			                    <select class="chr_form-control" id="pax" name="pax">
			                        <option>1</option>
			                        <option>2</option>
			                        <option>3</option>
			                        <option>4</option>
			                        <option>5</option>
			                        <option>6</option>
			                        <option>7</option>
			                        <option>8</option>
			                        <option>9</option>
			                    </select>
			                </span>

							          <input type="hidden" id="cd" name="cd">
		                    <input type="hidden" id="cm" name="cm">
		                    <input type="hidden" id="cy" name="cy">
		                    <input type="hidden" id="cod" name="cod">
					              <input type="hidden" id="com" name="com">
					              <input type="hidden" id="coy" name="coy">
							          <input type="hidden" name="child" id="child" value="0">
							          <input type="hidden" name="nite" id="nite">
					              <input type="hidden" name="url" value="">
					              <input type="hidden" id="cbs" name="cbs" value="0">
                		</div>
                	</div>
				</div>
				<div class="chr_col-lg-2 chr_col-md-3 chr_col-sm-6 chr_col-xs-12 form-group has-feedback">
					<div class="chr_row">
						<div class="chr_col-md-12 chr_col-sm-12 chr_col-xs-12">
							<span>Promo Code:</span>
			                <input id="utc" name="utc" type='text' class="chr_form-control" placeholder="Promo Code" min="1" />

						</div>
					</div>
				</div>
				<div class="chr_col-lg-2 chr_col-md-3 chr_col-sm-12 chr_col-xs-12 chr_chr_text-center chr_ptop10">
					<div>
						<input type="submit" name="availcheck" class="chr_btn chr_btn-book chr_w100" onClick="return chrCheckCond(this.form)" value="BOOK">
					</div>
					<div class="chr_size80 chr_text-right">
						<a href="<?php echo $amendUrl; ?>" class="chr_amend-button chr_btn">Amend / Cancel</a>
					</div>
        		</div>
			</div>
        </div>
    </form>
</div>
<!-- just comment this jquery if exist -->
<script type="text/javascript" src="ichronoz/js/jquery.min.js"></script>
<script type="text/javascript" src="ichronoz/js/moment.js"></script>
<script type="text/javascript" src="ichronoz/js/datepicker.js"></script>

<script type="text/javascript" src="ichronoz/js/hbook_v2.js"></script>
