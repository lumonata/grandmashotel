
(function() {
	var path = 'ichronoz/view';
	var app = angular.module('ichronoz', ['ui.rCalendar','ngSanitize', 'ngIdle', 'view-directives','angularModalService','ui.bootstrap', 'ui.router','angular.filter'], function($httpProvider){	
		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
	});
  	
  	app.config(function($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			// HOME STATES AND NESTED VIEWS
			.state('book', {
				url: '/',
				templateUrl: path+'/room-list.html',
				controller: 'MainController',
			})
			// nested list with custom controller
			.state('reserve', {
				url: '^/reservation/:id',
				views:{
					'@': {
					  templateUrl: path+'/reservation.html',
					  controller: 'ReserveController'
					}
				  },
				
			})
			.state('confirm', {
				url: '^/confirm',
				views:{
					'@': {
					  templateUrl: path+'/confirmation.html',
					  controller: 'ConfirmController'
					}
				  },
				
			})
			.state('wait', {
				url: '^/wait',
				views:{
					'@': {
					  templateUrl: path+'/wait.html',
					  controller: 'WaitController'
					}
				  },
				
			})
			.state('thanks', {
				url: '^/thanks',
				views:{
					'@': {
					  templateUrl: path+'/thanks.html',
					  controller: 'ThanksController'
					}
				  },
				
			})
			.state('thanksApi', {
				url: '^/thanksApi',
				views:{
					'@': {
					  templateUrl: path+'/thanksApi.html',
					  controller: 'ThanksApiController'
					}
				  },
				
			});
	});
  
  	app.filter('currencys', ['$filter', '$locale', function($filter, $locale) {
			return function (num, cur) {
				var sym = $locale.NUMBER_FORMATS.CURRENCY_SYM;
				if(cur == "IDR")
					return $filter('currency')(num, ' ', 0);
				else
					return $filter('currency')(num, ' ');
			};
		}
	]);
  
  	app.filter('split', function() {
	  return function(input, delimiter) {
		delimiter = delimiter || ','

		return input.split(delimiter);
	  }
	});

	app.filter('splitToArray', function() {
	  return function(input, delimiter, splitIndex) {
            // do some bounds checking here to ensure it has that index
            return input.split(delimiter)[splitIndex];
        }
    });
  
  	app.filter('each', function() {
	  return function(input, total) {
		total = parseInt(total);
		for (var i=0; i<total; i++)
		  input.push(i);
		return input;
	  };
	});
  
  	//search form service
  	app.factory("FormService", function() {
	  var service = {};
	  service.hid = ''; service.fd = '';  service.fm = '';  service.fy = '';
	  service.td = '';  service.tm = '';  service.ty = '';  service.pax = '';
	  service.adu = ''; service.chi = ''; service.url = ''; service.api = ''; service.utc = '';

	  service.updateValue = function(hid, fd, fm, fy, td, tm, ty, pax, adu, chi, url, api, utc){
		this.hid = hid; this.fd = fd;   this.fm = fm;
		this.fy = fy;   this.td = td;   this.tm = tm;
		this.ty = ty;   this.pax = pax; this.adu = adu;
		this.chi = chi; this.url = url; this.api = api; this.utc = utc;
	  }
	  
	  return service;
	});
  
  	//service for Rooms
  	app.factory('RoomService', ['$modal', function ($modal) { 
		var prod = '';
		return {
			getReserve: function() {
				return prod;
			},
			setReserve: function(products) {
				prod = products;
			},
			//getPersentase
			persen: function(oldRate, newRate) {
				persentase = ((oldRate - newRate) / oldRate) *100;
				persentase = persentase.toFixed(2);
				return persentase;
			},
			converter: function(product,ModalService) {
				$modal.open({
		          templateUrl: path+'/currencyConverter.html',
		          windowClass: 'modal-danger',
		          controller: 'ConverterController',
		          resolve: {
			         product: function () {
			           return product;
			         }
			       },
			      windowClass: 'chronoz-modal chronoz chr-bs',
		          backdrop:'static',
		        });
			}
		};
	}]);
  
  	//service for additional service
  	app.factory('AddService', [function () { 
		var srvc = '';
		return {
			getReserve: function() {
				return srvc;
			},
			setReserve: function(add) {
				srvc = add;
			}
		};
	}]);
  
	//service for Info
	app.factory('InfoService', [function () { 
		var srvc = '';
		return {
			getReserve: function() {
				return srvc;
			},
			setReserve: function(add) {
				srvc = add;
			}
		};
	}]);
  
  	app.factory('OptService', [function () { 
		var srvc = '';
		return {
			getReserve: function() {
				return srvc;
			},
			setReserve: function(opt) {
				srvc = opt;
			}
		};
	}]);
  
  	//service for Room detail
  	app.factory('ReserveService', [function () { 
		var prod = '';
		return {
			getReserve: function() {
				return prod;
			},
			setReserve: function(products) {
				prod = products;
			}
		};
	}]);

  	// Refreshing Page
  	app.factory('Refreshing', ['$state','$stateParams', function ($state, $stateParams) { 
		var prod = '';
		return {
			pageRefresh: function() {
				alert("refreshing page");
				
				return 'refresh';
			},
			
		};
	}]);
    
    app.config(function(IdleProvider, KeepaliveProvider, $provide) {
		IdleProvider.idle(5);
		IdleProvider.timeout(60*10);
		KeepaliveProvider.interval(5); 
    });

	app.run(['Idle','$rootScope', function(Idle, $rootScope) {
		$rootScope.paymentUrl = '';
		Idle.watch();
	}]);
  	
  	app.run(function($rootScope) {
        $rootScope.globalPajak = function(srvc, govTax, total_value) {
            srvcCharge = (total_value * srvc) / 100;
            ppn = ( (total_value + srvcCharge) * govTax) / 100;
            pajak = srvcCharge + ppn;
            return pajak;
        };
        $rootScope.onePajak = function(srvc, govTax, total_value, type) {
            srvcCharge = (total_value * srvc) / 100;
            ppn = ( (total_value + srvcCharge) * govTax) / 100;
            if(type == 'srvc_charge')
            	return srvcCharge;
            else if(type == 'gov_tax')
            	return ppn;
        };

        $rootScope.depositGuarantee = function(option, value, deposit, total) {
            if(option == 0){ // for day
            	guarantee = value * deposit;
            	return guarantee;
            }
            else if(option == 1){ // for %
            	guarantee = (total * deposit) / 100;
            	return guarantee;
            }
        };
    });

  	app.controller("WaitController",["$timeout","$state","$location","$window","$rootScope",function(a,b,c,d,e){console.log("wait.."),console.log(e.paymentUrl),""==e.paymentUrl&&(d.location.href=d.document.location.origin),d.location.href=e.paymentUrl}]),app.controller("ThanksController",["$timeout","$location","$window",function(a,b,c){a(function(){c.location.href=c.document.location.origin},3e4)}]),app.controller("ThanksApiController",["$timeout","$location","$window",function(a,b,c){a(function(){c.location.href=c.document.location.origin},3e4)}]),app.controller("sessionExpired",["$scope","Idle","Keepalive","$modal",function(a,b,c,d){function e(){a.warning&&(a.warning.close(),a.warning=null),a.timedout&&(a.timedout.close(),a.timedout=null)}a.started=!1,a.$on("IdleStart",function(){}),a.$on("IdleEnd",function(){e()}),a.$on("IdleTimeout",function(){e(),a.timedout=d.open({templateUrl:path+"/timedout-dialog.html",windowClass:"modal-danger",controller:"ModalInstanceCtrl",backdrop:"static"})})}]),app.controller("ModalInstanceCtrl",["$scope","$modalInstance","$state","$stateParams","Idle","$window",function(a,b,c,d,e,f){a.ok=function(){f.location.reload(),c.go("book"),e.watch(),b.dismiss("cancel")}}]),app.controller("ModalPopupController",["$scope","$modalInstance","$state","$stateParams","Idle","$window","product",function(a,b,c,d,e,f,g){a.product=g,a.close=function(){b.dismiss("cancel")}}]),app.controller("MainController",["$modal","$filter","$window","$http","$rootScope","$scope","FormService","ModalService","RoomService","AddService","InfoService","$state","$stateParams","$timeout",function(a,b,c,d,e,f,g,h,j,k,l,m,n,o){f.havePromoCode=!1,f.roomTypeImage="",f.defaultDay=1,f.defaultNights=2,f.defaultYears=4,f.datePriorToday="Check-in Date selected is prior Today's date. Please select next Today's date...",f.chrGetDay=function(a){return f.weekday=new Array(7),f.weekday[0]="Sunday",f.weekday[1]="Monday",f.weekday[2]="Tuesday",f.weekday[3]="Wednesday",f.weekday[4]="Thursday",f.weekday[5]="Friday",f.weekday[6]="Saturday",f.weekday[a]},f.chrGetDay2=function(a){return f.weekday=new Array(7),f.weekday[0]="Sun",f.weekday[1]="Mon",f.weekday[2]="Tue",f.weekday[3]="Wed",f.weekday[4]="Thu",f.weekday[5]="Fri",f.weekday[6]="Sat",f.weekday[a]},f.chrGetMonth=function(a){return f.month=new Array,f.month[0]="Jan",f.month[1]="Feb",f.month[2]="Mar",f.month[3]="Apr",f.month[4]="May",f.month[5]="Jun",f.month[6]="Jul",f.month[7]="Aug",f.month[8]="Sep",f.month[9]="Oct",f.month[10]="Nov",f.month[11]="Dec",f.month[a]},f.getDay=function(a,b,c,d){f.n=new Date,f.d=new Date(c,b-1,a,f.n.getHours(),f.n.getMinutes(),f.n.getSeconds(),f.n.getMilliseconds()),f.dayCheckin=f.chrGetDay(f.d.getDay()),f.monthCheckin=f.chrGetMonth(f.d.getMonth()),f.yearCheckin=f.d.getFullYear(),f.cday=f.dayCheckin,f.cdays=f.dayCheckin+", "+("0"+f.d.getDate()).slice(-2)+" "+f.monthCheckin+" "+f.yearCheckin,e.checkinDate=f.d,f.f=new Date(c,b-1,a,f.n.getHours(),f.n.getMinutes(),f.n.getSeconds(),f.n.getMilliseconds()),f.f.setDate(f.f.getDate()+parseInt(d)),f.dayCheckout=f.chrGetDay2(f.f.getDay()),f.monthCheckout=f.chrGetMonth(f.f.getMonth()),f.yearCheckout=f.f.getFullYear(),f.td=f.f.getDate(),f.tm=f.f.getMonth()+1,f.ty=f.f.getFullYear(),f.coday=f.dayCheckout+", "+("0"+f.f.getDate()).slice(-2)+" "+f.monthCheckout+" "+f.yearCheckout},f.chrCheckout=function(){f.getDay(f.fd,f.fm,f.fy,f.nite)},f.Math=c.Math,f.myMessage=!0,f.products=[],f.add=[],f.info=[],f.convertCur=function(a,b){f.myLoading=!0;var c="https://query.yahooapis.com/v1/public/yql";if("USD"==a)var g='select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20("'+a+b+'")';else var g='select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20("'+b+a+'")';var h=c+"?q="+g+"&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";d.get(h).success(function(b){"IDR"==a?e.exchange=1/b.query.results.rate.Rate:e.exchange=1*b.query.results.rate.Rate,console.log(e.exchange),f.myLoading=!1}).error(function(a){console.log("result failed")})},f.getLastMinuteDeal=function(a,c,d){f.skg=new Date,f.dateNow=b("date")(f.skg,"d/M/yyyy"),f.searchNow=a+"/"+c+"/"+d,f.dateNow==f.searchNow?e.lastMinuteDeal=!0:e.lastMinuteDeal=!1},f.addForm=function(){if(f.n=new Date,f.d=new Date(f.fy,f.fm-1,f.fd,f.n.getHours(),f.n.getMinutes(),f.n.getSeconds(),f.n.getMilliseconds()),f.cbs=0,f.n>f.d)return alert("Check-in Date selected is prior Today's date. Please select next Today's date..."),f.fd=e.fd,f.fm=e.fm,f.fy=e.fy,f.getDay(f.fd,f.fm,f.fy,f.nite),!1;f.myLoading=!0,f.getLastMinuteDeal(f.fd,f.fm,f.fy),g.updateValue(f.hid,f.fd,f.fm,f.fy,f.td,f.tm,f.ty,f.pax,f.adu,f.chi,f.url,f.api,f.utc);var a=e.apiUrl+"/availability/list?";d.jsonp(a,{params:{id:f.hid,fd:f.fd,fm:f.fm,fy:f.fy,td:f.td,tm:f.tm,ty:f.ty,pax:f.pax,adu:f.adu,chi:f.chi,url:f.url,api:f.api,utc:f.utc,callback:"JSON_CALLBACK"}}).success(function(a){""==a||"gagal"==a.message?(f.myMessage=!1,f.hname=a.hname,f.hphone=a.hphone,f.gagal=a,f.showDescriptionNoResult=!1,f.loadEvents(a.calendarData)):(f.myMessage=!0,f.myMessage=!0,j.setReserve(a.data),k.setReserve(a.add),l.setReserve(a.info),f.products=a.data,f.add=a.add,f.info=a.info,e.taxes=parseInt(a.govTax)+parseInt(a.srvcTax),e.govTax=parseInt(a.govTax),e.srvcTax=parseInt(a.srvcTax),a.info.cur!=a.info.localCur?f.convertCur(a.info.cur,a.info.localCur):e.exchange=0),f.myLoading=!1}).error(function(a,b){f.myMessage=!1,f.myLoading=!1,c.location.href=c.document.location.origin})},f.customResult=null,f.showCustom=function(b,c){f.showCustoms=a.open({templateUrl:path+"/custom.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return b},lastMinuteDeal:function(){return c}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},f.showTerm=function(b,c){f.showCustoms=a.open({templateUrl:path+"/term.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return b},lastMinuteDeal:function(){return c}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},f.currencyConverter=function(a){j.converter(a,h)},f.getPersen=function(a,b){return j.persen(a,b)},f.step=1,f.isStep=function(a){return f.step===a},f.setStep=function(a){f.step=a},f.reservationEmail=function(){e.url=f.url,e.api=f.api,e.utc=f.utc;var a=e.apiUrl+"/response/reservation";d.jsonp(a,{params:{fd:f.fd,fm:f.fm,fy:f.fy,nite:f.nite,td:f.td,tm:f.tm,ty:f.ty,pax:f.pax,adu:f.adu,chi:f.chi,email:f.noEmail,hid:e.hid,fn:f.fn,ln:e.ln,url:e.url,api:e.api,utc:e.utc,callback:"JSON_CALLBACK"}}).success(function(a){m.go("thanks")}).error(function(a){m.transitionTo("book")})},f.cekBookingEmail="",f.cekBookingCode="",f.bookingStatus="",f.cekBooking=function(){e.url=f.url,e.api=f.api,e.utc=f.utc;var a=e.apiUrl+"/response/cekBooking";d.jsonp(a,{params:{email:f.cekBookingEmail,code:f.cekBookingCode,hid:e.hid,url:e.url,api:e.api,utc:e.utc,callback:"JSON_CALLBACK"}}).success(function(a){null!=a?f.bookingStatus=a:""==f.cekBookingCode?f.bookingStatus="Please Check your email address or code":f.bookingStatus="Result not found"}).error(function(a){m.transitionTo("book")})},f.isShowDetail=!0,f.cekRoomTypeImage=function(a,b){return f.roomTypeImage==a||1!=b||(f.roomTypeImage=a,!1)},f.min=function(a){return b("min")(b("map")(a,"rate"))},f.getValueArray=function(a,c,d){return b("splitToArray")(a,c,d)},f.changeMode=function(a){f.mode=a},f.today=function(){f.currentDate=new Date},f.isToday=function(){var a=new Date,b=new Date(f.currentDate);return a.setHours(0,0,0,0),b.setHours(0,0,0,0),a.getTime()===b.getTime()},f.loadEvents=function(a){f.eventSource=a},f.onEventSelected=function(a){console.log("All good!")},f.toOptions=function(a,b){result=[];for(var c=b;c<=a;c++)result.push(c);return result},f.formInit=function(a){for(f.myLoading=!0,f.hid=a[0],f.fd=a[1],f.fm=a[2],f.fy=a[3],f.td=a[4],f.tm=a[5],f.ty=a[6],f.pax=a[7],f.adu=a[8],f.chi=a[9],f.url=a[10],f.api=a[11],f.utc=a[12],f.nite=a[13],f.cbs=a[14],f.promo=a[15],e.max_adu_opt=f.toOptions(a[16],1),e.max_chi_opt=f.toOptions(a[17],0),e.max_pax_opt=f.toOptions(a[18],1),e.fd=f.fd,e.fm=f.fm,e.fy=f.fy,f.getDay(f.fd,f.fm,f.fy,f.nite),g.updateValue(a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12]),e.hid=f.hid,e.url=f.url,e.api=f.api,e.utc=f.utc,f.d=new Date,f.y=f.d.getFullYear(),f.yearOpt=[],i=f.y;i<=f.y+(f.defaultYears-1);i++)f.yearOpt.push(i);f.getLastMinuteDeal(f.fd,f.fm,f.fy),e.apiUrl="https://api.ichronoz.net";var b=e.apiUrl+"/availability/list?";d.jsonp(b,{params:{id:f.hid,fd:f.fd,fm:f.fm,fy:f.fy,td:f.td,tm:f.tm,ty:f.ty,pax:f.pax,adu:f.adu,chi:f.chi,url:f.url,api:f.api,utc:f.utc,promo:f.promo,callback:"JSON_CALLBACK"}}).success(function(a,b){""==a||"gagal"==a.message?(f.showDescriptionNoResult=!1,f.myMessage=!1,f.hname=a.hname,f.hphone=a.hphone,f.gagal=a,f.loadEvents(a.calendarData)):(f.myMessage=!0,j.setReserve(a.data),k.setReserve(a.add),l.setReserve(a.info),f.products=a.data,f.add=a.add,f.info=a.info,f.orderByField=[f.info.sorting],1==f.info.promoListType?f.reverseSort=!1:f.reverseSort=!0,e.taxes=parseInt(a.govTax)+parseInt(a.srvcTax),e.govTax=parseInt(a.govTax),e.srvcTax=parseInt(a.srvcTax),a.info.cur!=a.info.localCur?f.convertCur(a.info.cur,a.info.localCur):e.exchange=0,1==f.cbs&&(f.myLoading=!1,$(".venobox").venobox(),$(".venobox_cancel").venobox({framewidth:"100%",titleattr:"data-title",numeratio:!0,infinigall:!0,overlayclose:!1}),$("#fram_cancel").attr("href",a.info.url),$("#fram_cancel")[0].click(),o(function(){$(".vbox-close, .vbox-overlay").trigger("click"),m.go("book")},9e5))),f.myLoading=!1}).error(function(a,b){f.myMessage=!1,f.myLoading=!1,alert("We apologize for any inconvenience - This Page is Under Maintenance...")})},f.formInit(c.tags),f.$on("msgid",function(a,b){e.fd=f.fd=b[0].fd,e.fm=f.fm=b[0].fm+1,e.fy=f.fy=b[0].fy,f.chrCheckout(),f.showDescriptionNoResult=!0})}]),app.controller("ReserveController",["$modal","$scope","$rootScope","ReserveService","RoomService","AddService","OptService","InfoService","ModalService","$state","$stateParams","FormService",function(a,b,c,d,e,f,g,h,i,j,k,l){b.exchange=c.exchange,""==e.getReserve()&&j.transitionTo("book"),b.myLoading=!0,b.myMessage=!1,c.indexnya=k.id,b.plus=0,b.products=e.getReserve(),b.add=f.getReserve(),b.info=h.getReserve(),b.taxes=c.taxes,b.govTax=c.govTax,b.srvcTax=c.srvcTax,b.product=b.products[k.id],b.adult=parseInt(l.adu),b.child=parseInt(l.chi),b.room="Villa"==b.product.accType?parseInt(b.product.rooms):parseInt(l.pax),c.extraBedQty=b.product.jumlah,b.minQty=c.extraBedQty,b.changeQty=function(){b.product.adult>b.product.maxPax?(b.product.jumlah=parseInt(b.product.jumlah)+parseInt(b.product.roomPerVilla),b.product.jumlah>b.product.maxExtrabed&&(b.product.jumlah=b.product.maxExtrabed),b.minQty=b.product.jumlah):b.product.jumlah>0&&(b.product.jumlah=b.product.jumlah-b.product.roomPerVilla,b.minQty=b.minQty-b.product.roomPerVilla),b.product.jumlah<b.minQty&&(b.product.jumlah=b.minQty)},b.changeQtyExtrabed=function(){b.product.jumlah<b.minQty&&(b.product.jumlah=b.minQty)},b.optional={childBreakfast:{status:"1"==b.product.breakfast,qty:0,subtotalCB:0},adultBreakfast:{status:"1"==b.product.breakfast,qty:0,subtotalAB:0},adultExtrabed:{qty:0,adultPax:0}},b.plusUnit=function(){b.plus=1},b.currencyConverter=function(a){e.converter(a,i)},b.getTotalBeforeTax=function(){var a=0;return angular.forEach(b.product.services,function(b){b.qty&&(a+=b.qty*b.rate)}),a+=1!=b.product.breakfast&&b.product.adult>0&&b.optional.adultBreakfast.status?b.product.adult*b.product.breakfastRate*b.product.rooms:0,a+=b.product.jumlah>0?b.product.jumlah*b.product.extrabedRate*b.product.rooms:0,a+=b.product.rooms*b.product.rate},b.getTotal=function(){var a=0;return angular.forEach(b.product.services,function(b){b.qty&&(a+=b.qty*b.rate)}),a+=1!=b.product.breakfast&&b.product.adult>0&&b.optional.adultBreakfast.status?b.product.adult*b.product.breakfastRate*b.product.rooms:0,a+=b.product.jumlah>0?b.product.jumlah*b.product.extrabedRate*b.product.rooms:0,a+=b.product.rooms*b.product.rate,0==b.product.taxIncluded&&(a+=c.globalPajak(parseInt(b.product.srvcTax),parseInt(b.product.govTax),a)),a},b.taxInclude=function(a){return includeTax=c.globalPajak(parseInt(b.product.srvcTax),parseInt(b.product.govTax),a),0==b.product.taxIncluded?total=a+includeTax:total=a,total},b.saveAll=function(a){b.loadImage=!0,0==b.optional.adultBreakfast.status?b.breakfastRate=0:b.breakfastRate=b.product.breakfastRate,b.optional={childBreakfast:{status:b.optional.childBreakfast.status,qty:b.product.child,item:b.breakfastRate*b.product.rooms,subtotalCB:b.product.child*b.breakfastRate*b.product.rooms},adultBreakfast:{status:b.optional.adultBreakfast.status,qty:b.product.adult*parseInt(b.product.roomPerVilla),item:b.breakfastRate*b.product.rooms,subtotalAB:b.product.adult*b.breakfastRate*b.product.rooms},adultExtrabed:{qty:b.product.jumlah,item:b.product.extrabedRate*b.product.rooms,subtotalExtra:b.product.jumlah*b.product.extrabedRate*b.product.rooms}},j.go("confirm"),f.setReserve(b.product.services),g.setReserve(b.optional),d.setReserve(b.product),l.adu=b.product.adult,l.chi=b.product.child,l.pax=b.product.rooms;b.loadImage=!1},b.getPersen=function(a,b){return e.persen(a,b)},b.myLoading=!1,b.customResult=null,b.showCustom=function(c,d){b.showCustoms=a.open({templateUrl:path+"/custom.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return c},lastMinuteDeal:function(){return d}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},b.showTerm=function(c,d){b.showCustoms=a.open({templateUrl:path+"/term.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return c},lastMinuteDeal:function(){return d}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})}}]),app.controller("ConfirmController",["$modal","$http","$scope","$rootScope","RoomService","ReserveService","AddService","OptService","InfoService","$state","$stateParams","$window","ModalService","$timeout",function(a,b,c,d,e,f,g,h,i,j,k,l,m,n){if(c.paymentDesc="",c.exchange=d.exchange,""==e.getReserve())j.transitionTo("book");else{c.payment=!0,c.tabActive="newGuest",c.myLoading=!1,c.indexnya=d.indexnya,c.product=f.getReserve(),c.add=g.getReserve(),c.optional=h.getReserve(),c.info=i.getReserve(),c.taxes=d.taxes,c.govTax=d.govTax,c.srvcTax=d.srvcTax,c.additional=!1,1==c.product.breakfast&&0==c.optional.adultBreakfast.status&&(c.optional.adultBreakfast.qty=0,c.optional.adultBreakfast.subtotalAB=0),c.extrabeds=d.extraBedQty,c.optional.adultExtrabed.qty<1&&(c.optional.adultExtrabed.qty=0,c.optional.adultExtrabed.subtotalExtra=0),c.getTotalBeforeTax=function(){var a=0;return angular.forEach(c.add,function(b){b.qty&&(a+=b.qty*b.rate)}),a+=1!=c.product.breakfast&&c.optional.adultBreakfast.qty>0?c.optional.adultBreakfast.subtotalAB:0,a+=c.optional.adultExtrabed.qty>0?c.optional.adultExtrabed.subtotalExtra:0,a+=c.product.rooms*c.product.rate},c.getTotal=function(){var a=0;return angular.forEach(c.add,function(b){b.qty&&(a+=b.qty*b.rate)}),a+=1!=c.product.breakfast&&c.optional.adultBreakfast.qty>0?c.optional.adultBreakfast.subtotalAB:0,a+=c.optional.adultExtrabed.qty>0?c.optional.adultExtrabed.subtotalExtra:0,a+=c.product.rooms*c.product.rate,0==c.product.taxIncluded&&(a+=d.globalPajak(c.product.srvcTax,c.product.govTax,a)),a},c.taxInclude=function(a){return includeTax=d.globalPajak(parseInt(c.product.srvcTax),parseInt(c.product.govTax),a),0==c.product.taxIncluded?total=a+includeTax:total=a,total};c.service=[],c.add.forEach(function(a){c.service.push(JSON.stringify(a))}),c.booking=[{arriveDetail:"",comment:"",mailingList:!0,arriveBy:"",cTitle:"",cFname:"",cLname:"",cPhone:"",cMobile:"",cAddress:"",cEmail:"",cCity:"",cState:"",cZipcode:"",cCountry:"",arriveBy:"",arriveDetail:"",roomId:"",roomName:"",arriveDate:"",departureDate:"",night:"",rooms:"",paxs:"",promoType:"",roomSubtotal:"",payment:!0,aggrement:!1,reviews:[]}],c.getGuest=function(a){c.myLoading=!0;var e=d.apiUrl+"/availability/guest/10";b.jsonp(e,{params:{email:a,url:c.url,api:c.api,utc:c.utc,callback:"JSON_CALLBACK"}}).success(function(a){c.myMessage="Returning Guest",c.booking.payment=!0,c.booking.cTitle=a.guest.gtitle,c.booking.cFname=a.guest.gfirst_name,c.booking.cLname=a.guest.glast_name,c.booking.cPhone=a.guest.gphone,c.booking.cMobile=a.guest.gmobile,c.booking.cEmail=a.guest.gemail,c.booking.cAddress=a.guest.gaddress,c.booking.cCity=a.guest.gcity,c.booking.cState=a.guest.gstate,c.booking.cZipcode=a.guest.gzipcode,c.booking.cCountry=a.guest.gcountry,c.booking.cPassport=a.guest.gpassport,c.booking.cNationality=a.guest.gnationality,c.myLoading=!1}).error(function(a){c.myMessage="New Guest",c.booking.cTitle="",c.booking.cFname="",c.booking.cLname="",c.booking.cPhone="",c.booking.cMobile="",c.booking.cAddress="",c.booking.cCity="",c.booking.cState="",c.booking.cZipcode="",c.booking.cCountry="",c.myLoading=!1})},c.submitForm=function(a){a?(c.booking.payment=!1,c.bookNow()):c.booking.payment=!0},c.bookNow=function(){c.loadImage=!0,c.addServices={items:[]},c.roomRates={items:[]},c.addServices.items.push({id:c.product.id,name:c.product.name+" - "+c.product.typeName+" "+c.product.rooms+" Room x "+c.product.night+" Night",price:c.taxInclude(c.product.rooms*c.product.rate),qty:1,amount:c.taxInclude(c.product.rooms*c.product.rate),type:1}),1!=c.product.breakfast&&0!=c.optional.adultBreakfast.status&&c.addServices.items.push({id:c.product.id,name:"Breakfast for "+c.product.adult*parseInt(c.product.roomPerVilla)*c.product.rooms+" Adult x "+c.product.night+" Night",price:c.taxInclude(c.optional.adultBreakfast.subtotalAB),qty:1,amount:c.taxInclude(c.optional.adultBreakfast.subtotalAB),type:1}),c.extrabeds>0&&c.addServices.items.push({id:c.product.id,name:"Extrabed for "+c.optional.adultExtrabed.qty+" Bed x "+c.product.night+" Night",price:c.taxInclude(c.optional.adultExtrabed.subtotalExtra),qty:1,amount:c.taxInclude(c.optional.adultExtrabed.subtotalExtra),type:1});for(var a in c.add)c.addServices.items.push({id:c.add[a].idAdd,name:c.add[a].name,price:c.taxInclude(c.add[a].qty*c.add[a].rate),qty:c.add[a].qty,amount:c.taxInclude(c.add[a].qty*c.add[a].rate),type:0});for(var a in c.product.allotments)c.roomRates.items.push({date:c.product.allotments[a].dates,oldRate:c.taxInclude(c.product.allotments[a].oldRate),rate:c.taxInclude(c.product.allotments[a].rate)});c.rateTax=c.taxInclude(c.product.rate),c.optional.adultBreakfast.subtotalAB=c.taxInclude(c.optional.adultBreakfast.subtotalAB),c.optional.adultExtrabed.subtotalExtra=c.taxInclude(c.optional.adultExtrabed.subtotalExtra),c.product.desc=c.product.desc+" "+c.product.surchargeDescription+" "+c.product.specialDescription,c.totalGuarantee=d.depositGuarantee(c.product.guaranteeOpt,c.taxInclude(c.product.rate/c.product.night*c.product.rooms),c.product.guarantee,c.getTotal());var f=d.apiUrl+"/availability/create/posts",g={hid:c.product.hid,roomId:c.product.id,roomName:c.product.name,promoType:c.product.promoType,rate:c.rateTax,night:c.product.night,roomDesc:c.product.desc,rooms:c.product.rooms,promoDate:c.product.promoDate,cutOff:c.product.cutOfDay,lastMinDeal:c.product.lastMinute,condition:c.product.condition,govTax:c.product.govTax,srvcTax:c.product.srvcTax,arrivalDate:c.product.fromdate,departureDate:c.product.todate,paymentOption:c.booking.paymentOption,total:c.getTotal(),roomRate:c.roomRates,currency:c.product.cur,adult:c.product.adult,child:c.product.child,breakfastInclude:c.product.breakfast,roomPerVilla:c.product.roomPerVilla,exchangeRate:d.exchange,surchargeDescription:c.product.surchargeDescription,totalGuarantee:c.totalGuarantee,totalRest:c.getTotal()-c.totalGuarantee,breakfast:c.optional.adultBreakfast.qty,breakfastCharge:c.optional.adultBreakfast.subtotalAB,extrabed:c.optional.adultExtrabed.qty,extrabedCharge:c.optional.adultExtrabed.subtotalExtra,addSrvc:c.addServices,cTitle:c.booking.cTitle,cFname:c.booking.cFname,cLname:c.booking.cLname,cPhone:c.booking.cPhone,cMobile:c.booking.cMobile,cAddress:c.booking.cAddress,cCity:c.booking.cCity,cState:c.booking.cState,cZipcode:c.booking.cZipcode,cCountry:c.booking.cCountry,cEmail:c.booking.cEmail,cNationality:c.booking.cNationality,cPassport:c.booking.cPassport,arriveBy:c.booking.arriveBy,arriveDetail:c.booking.arriveDetail,smook:c.booking.smookOption,comment:c.booking.comment,mailingList:c.booking.mailingList,beddingPreff:c.booking.beddingPreff,id:1,url:d.url,api:d.api,utc:d.utc,model:"posts",callback:"JSON_CALLBACK"};b({method:"POST",url:f,data:$.param(g),headers:{"Content-Type":"application/x-www-form-urlencoded"}}).success(function(a){null!=a?l.location.href=a:l.location.href=l.document.location.origin,e.setReserve(""),c.loadImage=!1}).error(function(a){console.log(a)})},c.getPersen=function(a,b){return e.persen(a,b)},c.customResult=null,c.showCustom=function(b,d){c.showCustoms=a.open({templateUrl:path+"/custom.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return b},lastMinuteDeal:function(){return d}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},c.showTerm=function(b,d){c.showCustoms=a.open({templateUrl:path+"/term.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return b},lastMinuteDeal:function(){return d}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},c.showTerms=function(b,d){c.showCustomss=a.open({templateUrl:path+"/terms.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return b},lastMinuteDeal:function(){return d}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})}}}]),app.controller("ConverterController",["$http","$scope","$modalInstance","$state","$stateParams","Idle","$window","product",function(a,b,c,d,e,f,g,h){b.myLoading=!1,b.product=h,b.display=!0,b.close=function(){c.dismiss("cancel")},b.code="USD",b.convertCur=function(c,d){b.myLoading=!0;var e="https://query.yahooapis.com/v1/public/yql";if("IDR"==b.product.cur)var f='select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20("'+b.code+c+'")';else var f='select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20("'+c+b.code+'")';var g=e+"?q="+f+"&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";a.get(g).success(function(a){"IDR"==b.product.cur?b.convertVal=d/a.query.results.rate.Rate:b.convertVal=d*a.query.results.rate.Rate,b.myLoading=!1}).error(function(a){console.log("result failed")})},b.convertCur(h.cur,h.rate)}]);
  
})();
