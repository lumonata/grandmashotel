(function(){
	var path = 'ichronoz/view';

	var app = angular.module('view-directives',[]);
	
	app.directive('ngEnter', function () {
		return function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				if(event.which === 13) {
					scope.$apply(function (){
						scope.$eval(attrs.ngEnter);
					});

					event.preventDefault();
				}
			});
		};
	});
	
	app.directive("searchForm", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/search-form.html'
		};
	  });
		
	app.directive("roomItems", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/room-items.html'
		};
	  });
	
	app.directive("roomItemsRecommend", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/room-items-recommend.html'
		};
	  });
	  
	app.directive("roomItems2", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/room-items2.html'
		};
	  });
	
	app.directive("roomItems3", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/room-items3.html'
		};
	  });
 	  
    app.directive("notResult", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/not-result.html'
		};
	  });
    app.directive("cekBooking", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/cek-booking.html'
		};
	  });
	  
	app.directive("bookingList", function() {
		return {
		  restrict: "E",
		  templateUrl: path+'/booking-list.html',
		  controller: function() {
			this.list = 1;

			this.isSet = function(checkList) {
			  return this.list === checkList;
			};

			this.setList = function(activeList) {
			  this.list = activeList;
			};
		  },
		  controllerAs: "list"
		};
	  });

	app.directive("roomList", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/room-list.html'
		};
	  });
	  
	app.directive("reservation", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/reservation.html'
		};
	  });
	 	  
})();
