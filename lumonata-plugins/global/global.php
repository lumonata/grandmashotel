<?php
/*
    Plugin Name: Global Setting
    Plugin URL: http://lumonata.com/
    Description: -
    Author: Ngurah Rai
    Author URL: http://lumonata.com/
    Version: 1.0.1

*/

add_privileges('administrator', 'global', 'insert');
add_privileges('administrator', 'global', 'update');
add_privileges('administrator', 'global', 'delete');

add_actions('tab_admin_aditional', 'view_global_setting', array('global'=>'Global'));

function view_global_setting()
{
    $alert = '';
    if (isset($_POST['save_changes'])) {
        $update = manage_global_setting();

        if ($update) {
            $alert = '<div class="alert_green_form">Your global setting has been updated.</div>';
        } else {
            $alert = 'div class="alert_green_form">Your global setting failed to update.</div>';
        }
    }

    $seminyak_img     = set_fhome_seminyak_image();
    $legian_img       = set_fhome_legian_image();
    $airport_img      = set_fhome_airport_image();
    $homepage_img     = set_fhome_top_image();
    $homepage_img_1   = set_fhome_top_image_1();
    $homepage_img_2   = set_fhome_top_image_2();
    $homepage_img_3   = set_fhome_top_image_3();
    $homepage_so      = set_fhome_special_offers();

    $brief_seminyak   = get_meta_data('fhomepage_brief_seminyak', 'global_setting');
    $brief_legian     = get_meta_data('fhomepage_brief_legian', 'global_setting');
    $brief_airport    = get_meta_data('fhomepage_brief_airport', 'global_setting');


    $address          = get_meta_data('address', 'global_setting');
    $city             = get_meta_data('city', 'global_setting');
    $phone_number     = get_meta_data('phone_number', 'global_setting');
    $email_address    = get_meta_data('email_address', 'global_setting');

    $fb               = get_meta_data('facebook_account', 'global_setting');
    $instagram        = get_meta_data('instagram_account', 'global_setting');
    $twitter          = get_meta_data('twitter_account', 'global_setting');

    $api_mailchimp     = get_meta_data('api_mailchimp', 'global_setting');
    $list_id_mailchimp = get_meta_data('list_id_mailchimp', 'global_setting');

    $booknow_mobile   = get_meta_data('link_booknow_mobile', 'global_setting');
    $booknow          = get_meta_data('link_booknow_desktop', 'global_setting');

    $smtp_email_addr  = get_meta_data('smtp_email_address', 'global_setting');
    $smtp_email_port  = get_meta_data('smtp_email_port', 'global_setting');
    $smtp_email_pwd   = get_meta_data('smtp_email_pwd', 'global_setting');
    $r_public_key     = get_meta_data('r_public_key', 'global_setting');
    $r_secret_key     = get_meta_data('r_secret_key', 'global_setting');

    // $ig_client_id     = get_meta_data('ig_client_id', 'global_setting');
    // $ig_client_secret = get_meta_data('ig_client_secret', 'global_setting');
    $ig_token         = get_meta_data('ig_token', 'global_setting');
    $g_analytic       = get_meta_data('g_analytic', 'global_setting');

    set_template(PLUGINS_PATH.'/global/form.html', 'add_settings_template');
    add_block('settings_form', 'stf', 'add_settings_template');

    add_actions('section_title', 'Global Setting');
    add_variable('admin_tab', setting_tabs());
    add_variable('alert', $alert);

    add_variable('address', $address);
    add_variable('city', $city);
    add_variable('phone_number', $phone_number);
    add_variable('email_address', $email_address);

    add_variable('facebook_account', $fb);
    add_variable('instagram_account', $instagram);
    add_variable('twitter_account', $twitter);

    add_variable('link_booknow_desktop', $booknow);
    add_variable('link_booknow_mobile', $booknow_mobile);

    add_variable('api_mailchimp', $api_mailchimp);
    add_variable('list_id_mailchimp', $list_id_mailchimp);

    add_variable('fhomepage_brief_seminyak', $brief_seminyak);
    add_variable('fhomepage_brief_legian', $brief_legian);
    add_variable('fhomepage_brief_airport', $brief_airport);

    add_variable('smtp_email_port', $smtp_email_port);
    add_variable('smtp_email_address', $smtp_email_addr);
    add_variable('smtp_email_pwd', $smtp_email_pwd);
    add_variable('r_public_key', $r_public_key);
    add_variable('r_secret_key', $r_secret_key);
    // add_variable('ig_client_id', $ig_client_id);
    // add_variable('ig_client_secret', $ig_client_secret);
    add_variable('ig_token', $ig_token);
    add_variable('g_analytic', $g_analytic);

        /*First Home Page Image*/
    add_variable('fhome_bg_image', $homepage_img);
    add_variable('fhome_bg_image_1', $homepage_img_1);
    add_variable('fhome_bg_image_2', $homepage_img_2);
    add_variable('fhome_bg_image_3', $homepage_img_3);
    add_variable('fhome_bg_special_offers', $homepage_so);
    add_variable('fhome_seminyak_image', $seminyak_img);
    add_variable('fhome_legian_image', $legian_img);
    add_variable('fhome_airport_image', $airport_img);

    add_variable('site_url', SITE_URL);
    add_variable('admin_url', SITE_URL . '/lumonata-admin');
    add_variable('ajax_url', SITE_URL . '/global-setting-ajax/');
    add_variable('plugin_url', SITE_URL.'/lumonata-plugins/global');

    parse_template('settings_form', 'stf', false);
    return return_template('add_settings_template');
}

function is_num_global_setting($key)
{
    global $db;

    $s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name=%s AND lapp_name=%s';
    $q = $db->prepare_query($s, $key, 'global_setting');
    $n = count_rows($q);
    if ($n > 0) {
        return true;
    }
    return false;
}

function manage_global_setting()
{
    global $db;

    foreach ($_POST as $key=>$val) {
        $not_include = array('save_changes');

        if (!in_array($key, $not_include)) {
            update_meta_data($key, $val, 'global_setting');
        }
    }

    return true;
}

function setting_tabs()
{
    global $actions;

    $tabs = run_actions('tab_admin');
    $tabs = $actions->action['tab_admin']['args'][0][0];

    if (is_contributor() || is_author()) {
        $tabs=$tabs[0];
    } else {
        $tabs=$tabs;
    }

    $tabb='';
    $tab_keys = array_keys($tabs);
    if (empty($_GET['tab'])) {
        $the_tab = $tab_keys[0];
    } else {
        $the_tab = $_GET['tab'];
    }

    $tabs = set_tabs($tabs, $the_tab);
    return $tabs;
}

function upload_app_image()
{
    global $db;

    $result = array();

    $app_img      = get_meta_data($_POST['meta_name'], $_POST['app_name']);
    $result_image = empty($app_img) ? '' : $app_img;

    //-- CHECK FOLDER EXIST
    if (!file_exists(PLUGINS_PATH . '/global/background/')) {
        mkdir(PLUGINS_PATH . '/global/background/');
    }

    if (!file_exists(PLUGINS_PATH . '/global/background/thumbnail/')) {
        mkdir(PLUGINS_PATH . '/global/background/thumbnail/');
    }

    $file_name   = $_FILES['file_name']['name'];
    $file_size   = $_FILES['file_name']['size'];
    $file_type   = $_FILES['file_name']['type'];
    $file_source = $_FILES['file_name']['tmp_name'];

    if (is_allow_file_size($file_size)) {
        if (is_allow_file_type($file_type, 'image')) {
            $fix_file_name = file_name_filter(character_filter($file_name)) . '-' . time();
            $file_ext      = file_name_filter($file_name, true);
            $file_name_ori = $fix_file_name . $file_ext;

            $destination       = PLUGINS_PATH . '/global/background/' . $file_name_ori;
            $destination_thumb = PLUGINS_PATH . '/global/background/thumbnail/' . $file_name_ori;

            $is_croped = upload_crop($file_source, $destination_thumb, $file_type, thumbnail_image_width(), thumbnail_image_height());
            $is_upload = upload($file_source, $destination);

            if ($is_upload && $is_croped) {
                if (update_meta_data($_POST['meta_name'], $file_name_ori, $_POST['app_name'])) {
                    if (file_exists(PLUGINS_PATH . '/global/background/' . $result_image) && !empty($result_image)) {
                        unlink(PLUGINS_PATH . '/global/background/' . $result_image);
                    }

                    if (file_exists(PLUGINS_PATH . '/global/background/thumbnail/' . $result_image)  && !empty($result_image)) {
                        unlink(PLUGINS_PATH . '/global/background/thumbnail/' . $result_image);
                    }

                    $result = array( 'filename' => $file_name_ori );
                }
            }
        }
    }

    return $result;
}

function delete_app_images($file_name = '')
{
    $app_img = get_meta_data($_POST['meta_name'], $_POST['app_name']);

    if ($app_img == $file_name) {
        if (file_exists(PLUGINS_PATH . '/global/background/thumbnail/' . $app_img)) {
            unlink(PLUGINS_PATH . '/global/background/thumbnail/' . $app_img);
        }

        if (file_exists(PLUGINS_PATH . '/global/background/' . $app_img)) {
            unlink(PLUGINS_PATH . '/global/background/' . $app_img);
        }

        $app_img = '';
    }

    $new_app_name = empty($app_img) ? '' : $app_img;
    if (update_meta_data($_POST['meta_name'], $new_app_name, $_POST['app_name'])) {
        return true;
    } else {
        return false;
    }
}

function execute_global_ajax_request()
{
    global $db;

    if ($_POST['ajax_key'] == 'add-app-image') {
        $data = upload_app_image();

        if (empty($data)) {
            $res['result'] = 'failed';
        } else {
            $res['result'] = 'success';
            $res['data']   = $data;
        }

        echo json_encode($res);
    }

    if ($_POST['ajax_key'] == 'delete-app-images') {
        if (delete_app_images($_POST['file_name'])) {
            echo '{"result":"success"}';
        } else {
            echo '{"result":"failed"}';
        }
    }
    exit;
}



function set_fhome_seminyak_image()
{
    $img = get_meta_data('fhome_seminyak_image', 'global_setting');

    if (!empty($img)) {
        return '
      <div class="box">
          <img src="http://' . SITE_URL . '/lumonata-plugins/global/background/thumbnail/' . $img . '" />
          <div class="ioverlay" style="display: none;">
              <a data-bg-name="' . $img . '" class="seminyak-delete-header">
                  <img src="http://' . SITE_URL . '/lumonata-plugins/global/images/delete.png">
              </a>
          </div>
      </div>';
    }
}

function set_fhome_legian_image()
{
    $img = get_meta_data('fhome_legian_image', 'global_setting');

    if (!empty($img)) {
        return '
      <div class="box">
          <img src="http://' . SITE_URL . '/lumonata-plugins/global/background/thumbnail/' . $img . '" />
          <div class="ioverlay" style="display: none;">
              <a data-bg-name="' . $img . '" class="legian-delete-header">
                  <img src="http://' . SITE_URL . '/lumonata-plugins/global/images/delete.png">
              </a>
          </div>
      </div>';
    }
}

function set_fhome_airport_image()
{
    $img = get_meta_data('fhome_airport_image', 'global_setting');

    if (!empty($img)) {
        return '
      <div class="box">
          <img src="http://' . SITE_URL . '/lumonata-plugins/global/background/thumbnail/' . $img . '" />
          <div class="ioverlay" style="display: none;">
              <a data-bg-name="' . $img . '" class="airport-delete-header">
                  <img src="http://' . SITE_URL . '/lumonata-plugins/global/images/delete.png">
              </a>
          </div>
      </div>';
    }
}

function set_fhome_top_image()
{
    $img = get_meta_data('fhome_bg_image', 'global_setting');

    if (!empty($img)) {
        return '
      <div class="box">
          <img src="http://' . SITE_URL . '/lumonata-plugins/global/background/thumbnail/' . $img . '" />
          <div class="ioverlay" style="display: none;">
              <a data-bg-name="' . $img . '" class="fhome-bg-delete-header">
                  <img src="http://' . SITE_URL . '/lumonata-plugins/global/images/delete.png">
              </a>
          </div>
      </div>';
    }
}
function set_fhome_top_image_1()
{
    $img = get_meta_data('fhome_bg_image_1', 'global_setting');

    if (!empty($img)) {
        return '
      <div class="box">
          <img src="http://' . SITE_URL . '/lumonata-plugins/global/background/thumbnail/' . $img . '" />
          <div class="ioverlay" style="display: none;">
              <a data-bg-name="' . $img . '" class="fhome-bg-delete-header">
                  <img src="http://' . SITE_URL . '/lumonata-plugins/global/images/delete.png">
              </a>
          </div>
      </div>';
    }
}

function set_fhome_top_image_2()
{
    $img = get_meta_data('fhome_bg_image_2', 'global_setting');

    if (!empty($img)) {
        return '
      <div class="box">
          <img src="http://' . SITE_URL . '/lumonata-plugins/global/background/thumbnail/' . $img . '" />
          <div class="ioverlay" style="display: none;">
              <a data-bg-name="' . $img . '" class="fhome-bg-delete-header">
                  <img src="http://' . SITE_URL . '/lumonata-plugins/global/images/delete.png">
              </a>
          </div>
      </div>';
    }
}
function set_fhome_top_image_3()
{
    $img = get_meta_data('fhome_bg_image_3', 'global_setting');

    if (!empty($img)) {
        return '
      <div class="box">
          <img src="http://' . SITE_URL . '/lumonata-plugins/global/background/thumbnail/' . $img . '" />
          <div class="ioverlay" style="display: none;">
              <a data-bg-name="' . $img . '" class="fhome-bg-delete-header">
                  <img src="http://' . SITE_URL . '/lumonata-plugins/global/images/delete.png">
              </a>
          </div>
      </div>';
    }
}

function set_fhome_special_offers()
{
    $img = get_meta_data('fhome_bg_special_offers', 'global_setting');

    if (!empty($img)) {
        return '
    <div class="box">
        <img src="http://' . SITE_URL . '/lumonata-plugins/global/background/thumbnail/' . $img . '" />
        <div class="ioverlay" style="display: none;">
            <a data-bg-name="' . $img . '" class="special-offers-delete-header">
                <img src="http://' . SITE_URL . '/lumonata-plugins/global/images/delete.png">
            </a>
        </div>
    </div>';
    }
}

add_actions('global-setting-ajax_page', 'execute_global_ajax_request');
