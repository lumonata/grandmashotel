<?php

session_start();

require_once(__DIR__.'../../../lumonata_config.php');
require_once(__DIR__.'../../../lumonata-functions/settings.php');
require_once(__DIR__.'../../../lumonata-admin/includes/instagram-api-php/src/Instagram.php');

$app_id     = get_meta_data('ig_client_id','global_setting');
$app_secret = get_meta_data('ig_client_secret','global_setting');

if(!empty($app_id) && !empty($app_secret))
{
	// initialize class
	$instagram = new MetzWeb\Instagram\Instagram(array(
	    'apiKey' => $app_id,
	    'apiSecret' => $app_secret,
	    'apiCallback' => 'http://'.site_url().'/lumonata-plugins/global/ig-callback.php'
	));

	$loginUrl = $instagram->getLoginUrl(array('basic','public_content'));
	header('Location:'.$loginUrl);
}
else
{
	header('Location:http://'.site_url().'/lumonata-admin/?state=global_settings&tab=global');
}

?>