jQuery(document).ready(function(){
    var site_url     = jQuery('[name=site_url]').val();
    var ajax_url     = jQuery('[name=ajax_url]').val();
    var app_name     = jQuery('[name=app_name]').val();

    //-- Main TOP Image Function
    jQuery('[name=fhome_seminyak_image]').change(function(e){
        jQuery.each(e.currentTarget.files, function(i, el){
            var data = new FormData();
                data.append('ajax_key', 'add-app-image');
                data.append('meta_name', 'fhome_seminyak_image');
                data.append('app_name', app_name);
                data.append('file_name', el);
              // console.log(data);
            var xhrObject = new XMLHttpRequest();

            xhrObject.open('POST', 'http://'+ajax_url);
            xhrObject.onreadystatechange = function(){
                if(xhrObject.readyState==4 && xhrObject.status==200){
                    var ee = JSON.parse(xhrObject.responseText);
                    if(ee.result=='success'){
                        var obj =
                        '<div class="box">'+
                            '<img src="http://'+site_url+'/lumonata-plugins/global/background/thumbnail/'+ee.data.filename+'" />'+
                            '<div class="ioverlay" style="display: none;">'+
                                '<a data-bg-name="'+ee.data.filename+'" class="seminyak-delete-header">'+
                                    '<img src="http://'+site_url+'/lumonata-plugins/global/images/delete.png">'+
                                '</a>'+
                            '</div>'+
                        '</div>';

                        jQuery('.seminyak-image-wrapp').html(obj);
                    }else{
                        alert('Failed to save main image')
                    }
                }
            }
            xhrObject.send(data);
        });
    });

    jQuery('.seminyak-delete-header').live('click',function(){
        var sel = jQuery(this).parent().parent();
        var url = 'http://'+ajax_url;
        var prm = new Object;
            prm.ajax_key  = 'delete-app-images';
            prm.app_name  = app_name;
            prm.meta_name = 'fhome_seminyak_image';
            prm.file_name = jQuery(this).attr('data-bg-name');

        jQuery.post(url, prm, function(e){
            if(e.result=='success')
            {
                sel.fadeOut(200,function(){ jQuery(this).remove(); });
            }
        },'json');
    });

    jQuery('.seminyak-image-wrapp .box').live('mouseover',function(){ jQuery(this).find('.ioverlay').show(); });

    //-- Legian Image
    jQuery('[name=fhome_legian_image]').change(function(e){
        jQuery.each(e.currentTarget.files, function(i, el){
            var data = new FormData();
                data.append('ajax_key', 'add-app-image');
                data.append('meta_name', 'fhome_legian_image');
                data.append('app_name', app_name);
                data.append('file_name', el);

            var xhrObject = new XMLHttpRequest();

            xhrObject.open('POST', 'http://'+ajax_url);
            xhrObject.onreadystatechange = function(){
                if(xhrObject.readyState==4 && xhrObject.status==200){
                    var ee = JSON.parse(xhrObject.responseText);
                    if(ee.result=='success'){
                        var obj =
                        '<div class="box">'+
                            '<img src="http://'+site_url+'/lumonata-plugins/global/background/thumbnail/'+ee.data.filename+'" />'+
                            '<div class="ioverlay" style="display: none;">'+
                                '<a data-bg-name="'+ee.data.filename+'" class="legian-delete-header">'+
                                    '<img src="http://'+site_url+'/lumonata-plugins/global/images/delete.png">'+
                                '</a>'+
                            '</div>'+
                        '</div>';

                        jQuery('.legian-image-wrapp').html(obj);
                    }else{
                        alert('Failed to save main image')
                    }
                }
            }
            xhrObject.send(data);
        });
    });

    jQuery('.legian-delete-header').live('click',function(){
        var sel = jQuery(this).parent().parent();
        var url = 'http://'+ajax_url;
        var prm = new Object;
            prm.ajax_key  = 'delete-app-images';
            prm.app_name  = app_name;
            prm.meta_name = 'fhome_legian_image';
            prm.file_name = jQuery(this).attr('data-bg-name');

        jQuery.post(url, prm, function(e){
            if(e.result=='success')
            {
                sel.fadeOut(200,function(){ jQuery(this).remove(); });
            }
        },'json');
    });

    jQuery('.legian-image-wrapp .box').live('mouseover',function(){ jQuery(this).find('.ioverlay').show(); });


    //-- Airport Image
    jQuery('[name=fhome_airport_image]').change(function(e){
        jQuery.each(e.currentTarget.files, function(i, el){
            var data = new FormData();
                data.append('ajax_key', 'add-app-image');
                data.append('meta_name', 'fhome_airport_image');
                data.append('app_name', app_name);
                data.append('file_name', el);

            var xhrObject = new XMLHttpRequest();

            xhrObject.open('POST', 'http://'+ajax_url);
            xhrObject.onreadystatechange = function(){
                if(xhrObject.readyState==4 && xhrObject.status==200){
                    var ee = JSON.parse(xhrObject.responseText);
                    if(ee.result=='success'){
                        var obj =
                        '<div class="box">'+
                            '<img src="http://'+site_url+'/lumonata-plugins/global/background/thumbnail/'+ee.data.filename+'" />'+
                            '<div class="ioverlay" style="display: none;">'+
                                '<a data-bg-name="'+ee.data.filename+'" class="airport-delete-header">'+
                                    '<img src="http://'+site_url+'/lumonata-plugins/global/images/delete.png">'+
                                '</a>'+
                            '</div>'+
                        '</div>';

                        jQuery('.airport-image-wrapp').html(obj);
                    }else{
                        alert('Failed to save main image')
                    }
                }
            }
            xhrObject.send(data);
        });
    });

    jQuery('.airport-delete-header').live('click',function(){
        var sel = jQuery(this).parent().parent();
        var url = 'http://'+ajax_url;
        var prm = new Object;
            prm.ajax_key  = 'delete-app-images';
            prm.app_name  = app_name;
            prm.meta_name = 'fhome_airport_image';
            prm.file_name = jQuery(this).attr('data-bg-name');

        jQuery.post(url, prm, function(e){
            if(e.result=='success')
            {
                sel.fadeOut(200,function(){ jQuery(this).remove(); });
            }
        },'json');
    });

    jQuery('.airport-image-wrapp .box').live('mouseover',function(){ jQuery(this).find('.ioverlay').show(); });


    //-- Sepecial Offers Image
    jQuery('[name=fhome_bg_special_offers]').change(function(e){
        jQuery.each(e.currentTarget.files, function(i, el){
            var data = new FormData();
                data.append('ajax_key', 'add-app-image');
                data.append('meta_name', 'fhome_bg_special_offers');
                data.append('app_name', app_name);
                data.append('file_name', el);

            var xhrObject = new XMLHttpRequest();

            xhrObject.open('POST', 'http://'+ajax_url);
            xhrObject.onreadystatechange = function(){
                if(xhrObject.readyState==4 && xhrObject.status==200){
                    var ee = JSON.parse(xhrObject.responseText);
                    if(ee.result=='success'){
                        var obj =
                        '<div class="box">'+
                            '<img src="http://'+site_url+'/lumonata-plugins/global/background/thumbnail/'+ee.data.filename+'" />'+
                            '<div class="ioverlay" style="display: none;">'+
                                '<a data-bg-name="'+ee.data.filename+'" class="special-offers-delete-header">'+
                                    '<img src="http://'+site_url+'/lumonata-plugins/global/images/delete.png">'+
                                '</a>'+
                            '</div>'+
                        '</div>';

                        jQuery('.fhome-special-offers-image-wrapp').html(obj);
                    }else{
                        alert('Failed to save special offers image')
                    }
                }
            }
            xhrObject.send(data);
        });
    });

    jQuery('.special-offers-delete-header').live('click',function(){
        var sel = jQuery(this).parent().parent();
        var url = 'http://'+ajax_url;
        var prm = new Object;
            prm.ajax_key  = 'delete-app-images';
            prm.app_name  = app_name;
            prm.meta_name = 'fhome_bg_special_offers';
            prm.file_name = jQuery(this).attr('data-bg-name');

        jQuery.post(url, prm, function(e){
            if(e.result=='success')
            {
                sel.fadeOut(200,function(){ jQuery(this).remove(); });
            }
        },'json');
    });

    jQuery('.fhome-special-offers-image-wrapp .box').live('mouseover',function(){ jQuery(this).find('.ioverlay').show(); });

    //-- Home Page Top Image
    //-- Slider 0
    jQuery('[name=fhome_bg_image]').change(function(e){
        jQuery.each(e.currentTarget.files, function(i, el){
            var data = new FormData();
                data.append('ajax_key', 'add-app-image');
                data.append('meta_name', 'fhome_bg_image');
                data.append('app_name', app_name);
                data.append('file_name', el);

            var xhrObject = new XMLHttpRequest();

            xhrObject.open('POST', 'http://'+ajax_url);
            xhrObject.onreadystatechange = function(){
                if(xhrObject.readyState==4 && xhrObject.status==200){
                    var ee = JSON.parse(xhrObject.responseText);
                    if(ee.result=='success'){
                        var obj =
                        '<div class="box">'+
                            '<img src="http://'+site_url+'/lumonata-plugins/global/background/thumbnail/'+ee.data.filename+'" />'+
                            '<div class="ioverlay" style="display: none;">'+
                                '<a data-bg-name="'+ee.data.filename+'" class="fhome-bg-delete-header">'+
                                    '<img src="http://'+site_url+'/lumonata-plugins/global/images/delete.png">'+
                                '</a>'+
                            '</div>'+
                        '</div>';

                        jQuery('.fhome-image-wrapp').html(obj);
                    }else{
                        alert('Failed to save main image')
                    }
                }
            }
            xhrObject.send(data);
        });
    });

    jQuery('.fhome-bg-delete-header').live('click',function(){
        var sel = jQuery(this).parent().parent();
        var url = 'http://'+ajax_url;
        var prm = new Object;
            prm.ajax_key  = 'delete-app-images';
            prm.app_name  = app_name;
            prm.meta_name = 'fhome_bg_image';
            prm.file_name = jQuery(this).attr('data-bg-name');

        jQuery.post(url, prm, function(e){
            if(e.result=='success')
            {
                sel.fadeOut(200,function(){ jQuery(this).remove(); });
                console.log(e.result );
            }
        },'json');
    });

    jQuery('.fhome-image-wrapp .box').live('mouseover',function(){ jQuery(this).find('.ioverlay').show(); });

    //-- Slider 1
    jQuery('[name=fhome_bg_image_1]').change(function(e){
        jQuery.each(e.currentTarget.files, function(i, el){
            var data = new FormData();
                data.append('ajax_key', 'add-app-image');
                data.append('meta_name', 'fhome_bg_image_1');
                data.append('app_name', app_name);
                data.append('file_name', el);

            var xhrObject = new XMLHttpRequest();

            xhrObject.open('POST', 'http://'+ajax_url);
            xhrObject.onreadystatechange = function(){
                if(xhrObject.readyState==4 && xhrObject.status==200){
                    var ee = JSON.parse(xhrObject.responseText);
                    if(ee.result=='success'){
                        var obj =
                        '<div class="box">'+
                            '<img src="http://'+site_url+'/lumonata-plugins/global/background/thumbnail/'+ee.data.filename+'" />'+
                            '<div class="ioverlay" style="display: none;">'+
                                '<a data-bg-name="'+ee.data.filename+'" class="fhome-bg1-delete-header">'+
                                    '<img src="http://'+site_url+'/lumonata-plugins/global/images/delete.png">'+
                                '</a>'+
                            '</div>'+
                        '</div>';

                        jQuery('.fhome-image1-wrapp').html(obj);
                    }else{
                        alert('Failed to save main image')
                    }
                }
            }
            xhrObject.send(data);
        });
    });

    jQuery('.fhome-bg1-delete-header').live('click',function(){
        var sel = jQuery(this).parent().parent();
        var url = 'http://'+ajax_url;
        var prm = new Object;
            prm.ajax_key  = 'delete-app-images';
            prm.app_name  = app_name;
            prm.meta_name = 'fhome_bg_image_1';
            prm.file_name = jQuery(this).attr('data-bg-name');

        jQuery.post(url, prm, function(e){
            if(e.result=='success')
            {
                sel.fadeOut(200,function(){ jQuery(this).remove(); });
                console.log(e.result );
            }
        },'json');
    });

    jQuery('.fhome-image1-wrapp .box').live('mouseover',function(){ jQuery(this).find('.ioverlay').show(); });

    //-- Slider 2
    jQuery('[name=fhome_bg_image_2]').change(function(e){
        jQuery.each(e.currentTarget.files, function(i, el){
            var data = new FormData();
                data.append('ajax_key', 'add-app-image');
                data.append('meta_name', 'fhome_bg_image_2');
                data.append('app_name', app_name);
                data.append('file_name', el);

            var xhrObject = new XMLHttpRequest();

            xhrObject.open('POST', 'http://'+ajax_url);
            xhrObject.onreadystatechange = function(){
                if(xhrObject.readyState==4 && xhrObject.status==200){
                    var ee = JSON.parse(xhrObject.responseText);
                    if(ee.result=='success'){
                        var obj =
                        '<div class="box">'+
                            '<img src="http://'+site_url+'/lumonata-plugins/global/background/thumbnail/'+ee.data.filename+'" />'+
                            '<div class="ioverlay" style="display: none;">'+
                                '<a data-bg-name="'+ee.data.filename+'" class="fhome-bg2-delete-header">'+
                                    '<img src="http://'+site_url+'/lumonata-plugins/global/images/delete.png">'+
                                '</a>'+
                            '</div>'+
                        '</div>';

                        jQuery('.fhome-image2-wrapp').html(obj);
                    }else{
                        alert('Failed to save main image')
                    }
                }
            }
            xhrObject.send(data);
        });
    });

    jQuery('.fhome-bg2-delete-header').live('click',function(){
        var sel = jQuery(this).parent().parent();
        var url = 'http://'+ajax_url;
        var prm = new Object;
            prm.ajax_key  = 'delete-app-images';
            prm.app_name  = app_name;
            prm.meta_name = 'fhome_bg_image_2';
            prm.file_name = jQuery(this).attr('data-bg-name');

        jQuery.post(url, prm, function(e){
            if(e.result=='success')
            {
                sel.fadeOut(200,function(){ jQuery(this).remove(); });
                console.log(e.result );
            }
        },'json');
    });

    jQuery('.fhome-image2-wrapp .box').live('mouseover',function(){ jQuery(this).find('.ioverlay').show(); });

    //-- Slider 3
    jQuery('[name=fhome_bg_image_3]').change(function(e){
        jQuery.each(e.currentTarget.files, function(i, el){
            var data = new FormData();
                data.append('ajax_key', 'add-app-image');
                data.append('meta_name', 'fhome_bg_image_3');
                data.append('app_name', app_name);
                data.append('file_name', el);

            var xhrObject = new XMLHttpRequest();

            xhrObject.open('POST', 'http://'+ajax_url);
            xhrObject.onreadystatechange = function(){
                if(xhrObject.readyState==4 && xhrObject.status==200){
                    var ee = JSON.parse(xhrObject.responseText);
                    if(ee.result=='success'){
                        var obj =
                        '<div class="box">'+
                            '<img src="http://'+site_url+'/lumonata-plugins/global/background/thumbnail/'+ee.data.filename+'" />'+
                            '<div class="ioverlay" style="display: none;">'+
                                '<a data-bg-name="'+ee.data.filename+'" class="fhome-bg3-delete-header">'+
                                    '<img src="http://'+site_url+'/lumonata-plugins/global/images/delete.png">'+
                                '</a>'+
                            '</div>'+
                        '</div>';

                        jQuery('.fhome-image3-wrapp').html(obj);
                    }else{
                        alert('Failed to save main image')
                    }
                }
            }
            xhrObject.send(data);
        });
    });

    jQuery('.fhome-bg3-delete-header').live('click',function(){
        var sel = jQuery(this).parent().parent();
        var url = 'http://'+ajax_url;
        var prm = new Object;
            prm.ajax_key  = 'delete-app-images';
            prm.app_name  = app_name;
            prm.meta_name = 'fhome_bg_image_2';
            prm.file_name = jQuery(this).attr('data-bg-name');

        jQuery.post(url, prm, function(e){
            if(e.result=='success')
            {
                sel.fadeOut(200,function(){ jQuery(this).remove(); });
                console.log(e.result );
            }
        },'json');
    });

    jQuery('.fhome-image3-wrapp .box').live('mouseover',function(){ jQuery(this).find('.ioverlay').show(); });

});
