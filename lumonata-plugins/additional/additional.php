<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
    Plugin Name: Lumonata Additional Data
    Plugin URL: http://lumonata.com/
    Description: This plugin is use for adding addtional field in each post and page.
    Author: Ngurah Rai
    Author URL: http://lumonata.com/
    Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

//-- Add the meta data input interface
//-- into each proccess in administrator area.
$apps_array = array(
    'articles',
    'rooms',
    'dining',
    'spa',
    'blogs',
    'about-us',
    'special-offers',
    'gallery',
    'activities',
    'featured'
);

if (is_edit_all() && !(is_save_draft() || is_publish())) {
    foreach ($_POST['select'] as $index=>$post_id) {
        foreach ($apps_array as $apps) {
            add_actions($apps.'_additional_filed_'.$index, 'additional_data_func', $apps);
            add_actions($apps.'_additional_delete', 'additional_delete_func', $apps);
        }

        add_actions('page_additional_data_'.$index, 'additional_data', 'Additional Data', 'page_additional_data_func');
    }
} else {
    foreach ($apps_array as $apps) {
        add_actions($apps.'_additional_filed', 'additional_data_func', $apps);
        add_actions($apps.'_additional_delete', 'additional_delete_func', $apps);
    }

    add_actions('page_additional_data', 'additional_data', 'Additional Data', 'page_additional_data_func');
}

function page_additional_data_func()
{
    global $thepost;

    $i = $thepost->post_index;
    $post_id = $thepost->post_id;

    $content = '
	<link rel="stylesheet" type="text/css" href="http://'.SITE_URL.'/lumonata-plugins/additional/style.css">
	<div class="boxs-wrapp">
    	<div class="boxs">
	    	<label>Destination Page : </label>
	    	<select name="additional_fields[destination]['.$i.']" />
	    		'.get_destination_option($post_id, 'pages').'
	    	</select>
    	</div>
    </div>';

    return $content;
}

function additional_data_func($apps)
{
    global $thepost;

    $i = $thepost->post_index;
    $post_id = $thepost->post_id;

    if (is_publish()) {
        set_brief_image_bottom($post_id, $apps);
        set_brief_image_top($post_id, $apps);
        set_main_images($post_id, $apps);
        set_menu_pdf($post_id, $apps);
        set_article_pdf($post_id, $apps);
    }

    set_template(PLUGINS_PATH."/additional/additional-form.html", 'additional_template_'.$i);
    add_block('additional_block', 'adt', 'additional_template_'.$i);

    add_variable('i', $i);
    add_variable('app_name', $apps);
    add_variable('post_id', $post_id);
    add_variable('site_url', SITE_URL);
    add_variable('ajax_url', SITE_URL.'/additional-ajax/');
    add_variable('plugin_url', SITE_URL.'/lumonata-plugins/additional');
    add_variable('admin_url', SITE_URL.'/lumonata-admin');

    add_variable('price_spa',get_price_spa($i, $post_id, $apps));
    add_variable('time_spa', get_time_spa($i, $post_id, $apps));
    add_variable('get_main_image_form', get_main_image_form($apps));
    add_variable('main_image', get_main_image($i, $post_id, $apps));
    add_variable('promo_code', get_promo_code($i, $post_id, $apps));
    add_variable('price_offers_net', get_price_net($i, $post_id, $apps));
    add_variable('brief_top', get_brief_top($i, $post_id, $apps));
    add_variable('brief_image_top', brief_image_top($i, $post_id, $apps));
    add_variable('brief_bottom', get_brief_bottom($i, $post_id, $apps));
    add_variable('brief_image_bottom', brief_image_bottom($i, $post_id, $apps));

    // add_variable('parent', get_parent($i, $post_id, $apps));
    // add_variable('pdf_file', get_pdf_file($i, $post_id, $apps));
    add_variable('destination_opt', get_destination_option($post_id, $apps));
    add_variable('brief', get_additional_field($post_id, 'brief', $apps));
    add_variable('additional_list_category', get_additional_list_category($i, $post_id, $apps));
    add_variable('brief_text', get_brief_text($i, $post_id, $apps));
    add_variable('amenities', get_amenities($i, $post_id, $apps));
    add_variable('featured_content', get_featured_content($i, $post_id, $apps));
    add_variable('website_link', get_website_link($i, $post_id, $apps));
    add_variable('accomodation_detail', get_accomodation_detail($i, $post_id, $apps));
    add_variable('action_type', $_GET['prc']);

    add_actions('admin_tail', 'get_javascript', 'jquery-ui-1.8.2.custom.min');
    add_actions('admin_tail', 'get_javascript', 'jquery.ui.nestedSortable.js');

    parse_template('additional_block', 'adt', false);
    $templates = return_template('additional_template_'.$i);
    return $templates;
}

function additional_delete_func($apps)
{
    global $thepost;

    if (isset($_POST['id']) && !empty($_POST['id'])) {
        if (is_array($_POST['id'])) {
            foreach ($_POST['id'] as $post_id) {
                $main_image = get_additional_field($post_id, 'main_image', $apps);

                if (!empty($main_image)) {
                    if (file_exists(PLUGINS_PATH.'/additional/background/'.$main_image)) {
                        unlink(PLUGINS_PATH.'/additional/background/'.$main_image);
                    }
                }
            }
        } else {
            $post_id = $_POST['id'];
            $main_image = get_additional_field($post_id, 'main_image', $apps);

            if (!empty($main_image)) {
                if (file_exists(PLUGINS_PATH.'/additional/background/'.$main_image)) {
                    unlink(PLUGINS_PATH.'/additional/background/'.$main_image);
                }
            }
        }
    }
}

function set_menu_pdf($post_id, $apps)
{
    if (empty($post_id)) {
        return;
    }

    $old_pdf = get_additional_field($post_id, 'menu_pdf', $apps);

    if (isset($_FILES['menu_pdf']) && $_FILES['menu_pdf']['error']==0) {
        //-- CHECK FOLDER EXIST
        if (!file_exists(PLUGINS_PATH.'/additional/pdf/')) {
            mkdir(PLUGINS_PATH.'/additional/pdf/');
        }

        $file_name   = $_FILES['menu_pdf']['name'];
        $file_size   = $_FILES['menu_pdf']['size'];
        $file_type   = $_FILES['menu_pdf']['type'];
        $file_source = $_FILES['menu_pdf']['tmp_name'];

        $pdf = '';

        if (is_allow_file_size($file_size, 3145728)) {
            if (is_allow_file_type($file_type, 'pdf')) {
                $destination = PLUGINS_PATH.'/additional/pdf/'.$file_name;

                if (upload($file_source, $destination)) {
                    $pdf = $file_name;

                    if (!empty($old_pdf) && file_exists(PLUGINS_PATH.'/additional/pdf/'.$old_pdf)) {
                        unlink(PLUGINS_PATH.'/additional/pdf/'.$old_pdf);
                    }
                }
            }
        }
    } else {
        $pdf = $old_pdf;
    }

    edit_additional_field($post_id, 'menu_pdf', $pdf, $apps);
}

function set_article_pdf($post_id, $apps)
{
    if (empty($post_id)) {
        return;
    }

    $old_pdf = get_additional_field($post_id, 'pdf_file', $apps);

    if (isset($_FILES['pdf_file'])) {
        //-- CHECK FOLDER EXIST
        if (!file_exists(PLUGINS_PATH.'/additional/pdf/')) {
            mkdir(PLUGINS_PATH.'/additional/pdf/');
        }

        $file_name   = $_FILES['pdf_file']['name'];
        $file_size   = $_FILES['pdf_file']['size'];
        $file_type   = $_FILES['pdf_file']['type'];
        $file_source = $_FILES['pdf_file']['tmp_name'];

        $pdf = '';

        if (is_allow_file_size($file_size)) {
            if (is_allow_file_type($file_type, 'pdf')) {
                $destination = PLUGINS_PATH.'/additional/pdf/'.$file_name;

                if (upload($file_source, $destination)) {
                    $pdf = $file_name;

                    if (!empty($old_pdf) && file_exists(PLUGINS_PATH.'/additional/pdf/'.$old_pdf)) {
                        unlink(PLUGINS_PATH.'/additional/pdf/'.$old_pdf);
                    }
                }
            }
        }
    } else {
        $pdf = $old_pdf;
    }

    edit_additional_field($post_id, 'pdf_file', $pdf, $apps);
}

function set_main_images($post_id, $apps)
{
    if (isset($_POST['main_image'])) {
        //-- CHECK FOLDER EXIST
        if (!file_exists(PLUGINS_PATH.'/additional/background/')) {
            mkdir(PLUGINS_PATH.'/additional/background/');
        }

        preg_match('#^data:image/\w+;base64,#i', $_POST['main_image'], $matches);
        if (!empty($matches)) {
            while (true) {
                $name = uniqid(rand(), true).'.jpg';
                if (!file_exists(PLUGINS_PATH.'/additional/background/'.$name)) {
                    break;
                }
            }

            $background = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['main_image']));
            file_put_contents(PLUGINS_PATH.'/additional/background/'.$name, $background);

            if (file_exists(PLUGINS_PATH.'/additional/background/'.$name)) {
                $filename = $name;
            }

            //-- Delete old image
            $old_bg = get_additional_field($post_id, 'main_image', $apps);
            if (!empty($old_bg) && file_exists(PLUGINS_PATH.'/additional/background/'.$old_bg)) {
                unlink(PLUGINS_PATH.'/additional/background/'.$old_bg);
            }
        } else {
            $filename = $_POST['main_image'];
        }

        edit_additional_field($post_id, 'main_image', $filename, $apps);
    }
}

function set_brief_image_top($post_id, $apps)
{
    if (isset($_POST['brief_image_top'])) {
        //-- CHECK FOLDER EXIST
        if (!file_exists(PLUGINS_PATH.'/additional/background/')) {
            mkdir(PLUGINS_PATH.'/additional/background/');
        }

        preg_match('#^data:image/\w+;base64,#i', $_POST['brief_image_top'], $matches);
        if (!empty($matches)) {
            while (true) {
                $name = uniqid(rand(), true).'.jpg';
                if (!file_exists(PLUGINS_PATH.'/additional/background/'.$name)) {
                    break;
                }
            }

            $background = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['brief_image_top']));
            file_put_contents(PLUGINS_PATH.'/additional/background/'.$name, $background);

            if (file_exists(PLUGINS_PATH.'/additional/background/'.$name)) {
                $filename = $name;
            }

            //-- Delete old image
            $old_bg = get_additional_field($post_id, 'brief_image_top', $apps);
            if (!empty($old_bg) && file_exists(PLUGINS_PATH.'/additional/background/'.$old_bg)) {
                unlink(PLUGINS_PATH.'/additional/background/'.$old_bg);
            }
        } else {
            $filename = $_POST['brief_image_top'];
        }

        edit_additional_field($post_id, 'brief_image_top', $filename, $apps);
    }
}

function set_brief_image_bottom($post_id, $apps)
{
    if (isset($_POST['brief_image_bottom'])) {
        //-- CHECK FOLDER EXIST
        if (!file_exists(PLUGINS_PATH.'/additional/background/')) {
            mkdir(PLUGINS_PATH.'/additional/background/');
        }

        preg_match('#^data:image/\w+;base64,#i', $_POST['brief_image_bottom'], $matches);
        if (!empty($matches)) {
            while (true) {
                $name = uniqid(rand(), true).'.jpg';
                if (!file_exists(PLUGINS_PATH.'/additional/background/'.$name)) {
                    break;
                }
            }

            $background = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['brief_image_bottom']));
            file_put_contents(PLUGINS_PATH.'/additional/background/'.$name, $background);

            if (file_exists(PLUGINS_PATH.'/additional/background/'.$name)) {
                $filename = $name;
            }

            //-- Delete old image
            $old_bg = get_additional_field($post_id, 'brief_image_bottom', $apps);
            if (!empty($old_bg) && file_exists(PLUGINS_PATH.'/additional/background/'.$old_bg)) {
                unlink(PLUGINS_PATH.'/additional/background/'.$old_bg);
            }
        } else {
            $filename = $_POST['brief_image_bottom'];
        }

        edit_additional_field($post_id, 'brief_image_bottom', $filename, $apps);
    }
}

function get_main_image_form($apps)
{
    $include_apps = array(
      'rooms',
      'dining',
      'special-offers',
      'activities',
      'featured'
      );
    $content = '';
    if (in_array($apps, $include_apps)) {
        // $promo_code = get_additional_field($post_id, 'promo_code', $apps);

        $content .= '';
        $content .='
        <div class="additional_data">
          <h2>Main Image</h2>
          <div class="additional_content additional-main-img">
            <div class="slider">
              <h3>Select an image file on your computer (2MB max):</h3>
              <div class="main-image-wrapp clearfixed">{main_image}</div>
              <input type="file" name="m_image" />
            </div>
          </div>
        </div>';
    }
    return $content;
}



function get_promo_code($i, $post_id, $apps)
{
    $include_apps = array('special-offers');
    $content = '';
    if (in_array($apps, $include_apps)) {
        $promo_code = get_additional_field($post_id, 'promo_code', $apps);

        $content .= '';
        $content .='
        <div class="additional_data">
            <h2>Promo Code</h2>
            <div class="additional_content">
            	<div class="boxs-wrapp">
        	    	<div class="boxs">
        		    	<p>Input your brief text for this page : </p>
        		    	<input class="textbox small" name="additional_fields[promo_code][{i}]" value="'.$promo_code.'">
        	    	</div>
        	    </div>
            </div>
        </div>';
    }
    return $content;
}

  function get_price_net($i, $post_id, $apps)
  {
      $include_apps = array('special-offers');
      $content = '';
      if (in_array($apps, $include_apps)) {
          $content = '';
          $price_offers_net = get_additional_field($post_id, 'price_offers_net', $apps);

          $content .= '
          <div class="additional_data">
              <h2>Price Nett</h2>
              <div class="additional_content">
              	<div class="boxs-wrapp">
          	    	<div class="boxs">
          		    	<p>Input your brief text for price :</p>
          		    	<input class="textbox small" name="additional_fields[price_offers_net][{i}]" value="'.$price_offers_net.'">
          	    	</div>
          	    </div>
              </div>
          </div>';
      }
      return $content;
  }


function get_brief_text($i, $post_id, $apps)
{
    $include_apps = array('special-offers');
    $content = '';
    if (in_array($apps, $include_apps)) {
        $brief = get_additional_field($post_id, 'brief', $apps);

        $content .= '';
        $content .='
        <div class="additional_data">
          <h2>Brief Text</h2>
          <div class="additional_content">
            <div class="boxs-wrapp">
              <div class="boxs">
                <p>Input your brief text for this page : </p>
                <textarea class="textbox area" name="additional_fields[brief][{i}]">'.$brief.'</textarea>
              </div>
            </div>
          </div>
        </div>';
    }
    return $content;
}

function get_brief_top($i, $post_id, $apps)
{
    $include_apps = array('about-us');
    $content = '';
    if (in_array($apps, $include_apps)) {
        $brief = get_additional_field($post_id, 'brief_top_text', $apps);
        $content .= '';
        $content .='
      <div class="additional_data">
        <h2>Brief Text Top</h2>
        <div class="additional_content">
          <div class="boxs-wrapp">
            <div class="boxs">
              <p>Input your brief text for this page : </p>
              <input class="textbox area" name="additional_fields[brief_top_text][{i}]" value="'.$brief.'">
            </div>
          </div>
        </div>
        <h2>Brief Image Top</h2>
        <div class="additional_content additional-main-img">
          <div class="slider">
            <h3>Select an image file on your computer (2MB max):</h3>
            <div class="brief-image-top-wrapp clearfixed">{brief_image_top}</div>
            <input type="file" name="brief_image_top" />
          </div>
        </div>
      </div>';
    }
    return $content;
}


function get_brief_bottom($i, $post_id, $apps)
{
    $include_apps = array('about-us');
    $content = '';
    if (in_array($apps, $include_apps)) {
        $brief = get_additional_field($post_id, 'brief_bottom_text', $apps);
        $content .= '';
        $content .='
        <div class="additional_data">
          <h2>Brief Text Bottom</h2>
          <div class="additional_content">
            <div class="boxs-wrapp">
              <div class="boxs">
                <p>Input your brief text for this page : </p>
                <textarea class="textbox area" name="additional_fields[brief_bottom_text][{i}]">'.$brief.'</textarea>
              </div>
            </div>
          </div>
          <h2>Brief Image Bottom</h2>
          <div class="additional_content additional-main-img">
            <div class="slider">
              <h3>Select an image file on your computer (2MB max):</h3>
              <div class="brief-image-bottom-wrapp clearfixed">{brief_image_bottom}</div>
              <input type="file" name="brief_image_bottom" />
            </div>
          </div>
        </div>';
    }
    return $content;
}

function brief_image_top($index, $post_id, $apps)
{
    $content = '';
    $main_image = get_additional_field($post_id, 'brief_image_top', $apps);
    if (!empty($main_image)) {
        $content .= '';
        $content .= '
        <div class="box">
            <img src="http://'.SITE_URL.'/lumonata-plugins/additional/background/'.$main_image.'" />
            <input type="hidden" name="additional_fields[brief_image_top]['.$index.']" value="'.$main_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" data-bg-name="'.$main_image.'" class="bg-img-top-delete-header">
                    <img src="http://'.SITE_URL.'/lumonata-plugins/additional/images/delete.png">
                </a>
            </div>
        </div>';
    }

    return $content;
}

function brief_image_bottom($index, $post_id, $apps)
{
    $content = '';
    $main_image = get_additional_field($post_id, 'brief_image_bottom', $apps);
    if (!empty($main_image)) {
        $content .= '';
        $content .= '
        <div class="box">
            <img src="http://'.SITE_URL.'/lumonata-plugins/additional/background/'.$main_image.'" />
            <input type="hidden" name="additional_fields[brief_image_bottom]['.$index.']" value="'.$main_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" data-bg-name="'.$main_image.'" class="bg-img-bottom-delete-header">
                    <img src="http://'.SITE_URL.'/lumonata-plugins/additional/images/delete.png">
                </a>
            </div>
        </div>';
    }

    return $content;
}



function get_main_image($index, $post_id, $apps)
{
    $content = '';
    $main_image = get_additional_field($post_id, 'main_image', $apps);
    if (!empty($main_image)) {
        $content .= '';
        $content .= '
        <div class="box">
            <img src="http://'.SITE_URL.'/lumonata-plugins/additional/background/'.$main_image.'" />
            <input type="hidden" name="additional_fields[main_image]['.$index.']" value="'.$main_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" data-bg-name="'.$main_image.'" class="bg-delete-header">
                    <img src="http://'.SITE_URL.'/lumonata-plugins/additional/images/delete.png">
                </a>
            </div>
        </div>';
    }

    return $content;
}

function get_parent($index, $post_id, $apps)
{
    global $db;

    if ($apps=='') {
        $content = '';
        $parent  = get_additional_field($post_id, 'parent', $apps);

        $res  = fetch_artciles('type=about-us', false);
        $list = array();
        while ($data = $db->fetch_array($res)) {
            $dest = get_additional_field($data['larticle_id'], 'destination', $apps);
            $dest = (empty($dest) ? 'other' : $dest);
            $list[$dest][] = $data;
        }

        $content .= '
		<div class="additional_data">
		    <h2>Parent Page</h2>
		    <div class="additional_content">
		    	<div class="boxs-wrapp">
			    	<div class="boxs clearfix">
					    <select name="additional_fields[parent]['.$index.']" autocomplete="off">
					    	<option value="">-- Select Page --</option>';

        if (!empty($list)) {
            foreach ($list as $key=>$obj) {
                $content .= '
									<optgroup label="'.ucfirst($key).'">';

                foreach ($obj as $d) {
                    $selected = ($d['larticle_id']==$parent ? 'selected' : '');
                    $content .= '
											<option value="'.$d['larticle_id'].'" '.$selected.'>'.$d['larticle_title'].'</option>';
                }

                $content .= '
									</optgroup>';
            }
        }

        $content .= '
					    </select>
			    	</div>
			    </div>
		    </div>
		</div>';

        return $content;
    }
}

function get_pdf_file($index, $post_id, $apps)
{
    $include_apps = array('articles');

    if (in_array($apps, $include_apps)) {
        $content  = '';
        $pdf_file = get_additional_field($post_id, 'pdf_file', $apps);

        $content .= '
		<div class="additional_data">
		    <h2>PDF File</h2>
		    <div class="additional_content additional-main-img">
		        <div class="slider">
		            <h3>Select an pdf file on your computer (20MB max):</h3>
		            <div class="pdf-file-wrapp clearfixed">';
        if (!empty($pdf_file)) {
            $content .= '
					        <div class="box">
					            <img src="http://'.SITE_URL.'/lumonata-plugins/additional/images/pdf.png" />
					            <span>'.$pdf_file.'</span>
					            <input type="hidden" name="additional_fields[pdf_file]['.$index.']" value="'.$pdf_file.'" />
					            <div class="overlay" style="display: none;">
					                <a data-post-id="'.$post_id.'" data-bg-name="'.$pdf_file.'" class="pdf-delete-file">
					                    <img src="http://'.SITE_URL.'/lumonata-plugins/additional/images/delete.png">
					                </a>
					            </div>
					        </div>';
        }
        $content .= '
		            </div>
		            <input type="file" name="pdf_file"/>
		        </div>
		    </div>
		</div>';

        return $content;
    }
}

function get_destination_option($post_id, $apps)
{
    $destination = get_additional_field($post_id, 'destination', $apps);

    return '
	<option value="">-- No destination --</option>
	<option value="seminyak" '.($destination=='seminyak' ? 'selected' : '').'>Seminyak</option>
	<option value="legian" '.($destination=='legian' ? 'selected' : '').'>Legian</option>
	<option value="airport" '.($destination=='airport' ? 'selected' : '').'>Airport</option>';
}

function get_accomodation_detail($index, $post_id, $apps)
{
    if ($apps=='accommodation') {
        $view = get_additional_field($post_id, 'view', $apps);
        $size = get_additional_field($post_id, 'size', $apps);
        $bed  = get_additional_field($post_id, 'bedroom', $apps);
        $bath = get_additional_field($post_id, 'bathroom', $apps);
        $occp = get_additional_field($post_id, 'occupancy', $apps);

        $amen = get_additional_field($post_id, 'amenities', $apps);

        $content = '
		<div class="additional_data accomodation-detail">
		    <h2>Accomodation Detail</h2>
		    <div class="additional_content">
		    	<div class="boxs-wrapp">
			    	<div class="boxs clearfix">
			    		<label class="push-left border-box">View : </label>
			    		<textarea class="textbox border-box" name="additional_fields[view]['.$index.']">'.$view.'</textarea>
			    	</div>
			    	<div class="boxs clearfix">
			    		<label class="push-left border-box">Size : </label>
			    		<textarea class="push-left textbox border-box" name="additional_fields[size]['.$index.']">'.$size.'</textarea>
			    	</div>
			    	<div class="boxs clearfix">
			    		<label class="push-left border-box">Bed : </label>
			    		<textarea class="push-left textbox border-box" name="additional_fields[bedroom]['.$index.']">'.$bed.'</textarea>
			    	</div>
			    	<div class="boxs clearfix">
			    		<label class="push-left border-box">Bathroom : </label>
			    		<textarea class="push-left textbox border-box" name="additional_fields[bathroom]['.$index.']">'.$bath.'</textarea>
			    	</div>
			    	<div class="boxs clearfix">
			    		<label class="push-left border-box">Occupancy : </label>
			    		<textarea class="push-left textbox border-box" name="additional_fields[occupancy]['.$index.']">'.$occp.'</textarea>
			    	</div>
			    </div>
		    </div>
		</div>';

        return $content;
    }
}

function get_amenities($index, $post_id, $apps)
{
    if ($apps=='accommodation') {
        $amenities = get_additional_field($post_id, 'amenities', $apps);
        $content = '
		<div class="additional_data aminities">
		    <h2>Amenities</h2>
		    <div class="additional_content">
		    	<div class="boxs-wrapp">
			    	<div class="boxs clearfix">
			    		<label class="push-left with-margin">Add new amenities : </label>
			    		<input type="text" name="new_amenities" class="textbox push-left small" />
			    		<button class="the-button add-to-list">Add To List</button>
			    	</div>
			    	<textarea class="amenities-value" name="additional_fields[amenities]['.$index.']">'.$amenities.'</textarea>
		    		<div class="amenities-list clearfix">';
        $amenities_arr = json_decode($amenities, true);
        if (!empty($amenities_arr) && json_last_error() === JSON_ERROR_NONE) {
            foreach ($amenities_arr as $i=>$obj) {
                $content .= '
								<div id="list_'.($i+1).'" class="item"><p>'.$obj.'</p><a>X</a></div>';
            }
        }
        $content .= '
		    		</div>
			    </div>
		    </div>
		</div>';

        return $content;
    }
}

function get_additional_list_category($index, $post_id, $apps)
{
    if ($apps=='special-offers' || $apps=='rooms') {
        $content = '
		<div id="menu-list" class="additional_data description-list">
		    <h2>Additional Description List</h2>
		    <div class="additional_content">
		    	<div class="boxs-wrapp">
		    		<ul id="tab-menu-setting" class="tab-menu-setting clearfix">
		    			<li class="active"><a href="#category-box">Description List</a></li>
		    		</ul>';

        $category = get_additional_field($post_id, 'additional_list_category', $apps);
        $content .= '
	    			<div id="category-box" class="menu-box-wrapper" style="display:block;">
				    	<div class="boxs clearfix category-box">
				    		<label class="push-left with-margin">Add new descriprtion list : </label>
				    		<input type="text" name="new_category" class="textbox" />
				    		<button class="the-button cat-add-to-list">Add To List</button>
				    		<button class="the-button cat-edit-menu">Save Edited</button>
				    	</div>
				    	<textarea class="menu-category-value">'.$category.'</textarea>
			    		<ol class="menu-category-list clearfix sortable">';
        $category_arr = json_decode($category, true);
        if (!empty($category_arr) && json_last_error() === JSON_ERROR_NONE) {
            foreach ($category_arr as $obj) {
                $content .= '
									<li id="list_'.$obj['id'].'">
										<div class="item">
											<p>'.$obj['name'].'</p>
											<a class="edit"></a>
											<a class="delete"></a>
										</div>';
                if (isset($obj['child'])) {
                    $content .= '
											<ol>';
                    foreach ($obj['child'] as $child) {
                        $content .= '
													<li id="list_'.$child['id'].'">
														<div class="item">
															<p>'.$child['name'].'</p>
															<a class="edit"></a>
															<a class="delete"></a>
														</div>
													<li>';
                    }
                    $content .= '
											</ol>';
                }
                $content .= '
									</li>';
            }
        }
        $content .= '
			    		</ol>
			    		<div class="note">
			    			<p><em><b>Important Note : </b>remember to publish after you added or deleted the menu category</em></p>
			    		</div>
			    	</div>
            </div>';


        return $content;
    }
}

function get_featured_content($index, $post_id, $apps)
{
    $content      = '';
    $show_on      = get_additional_field($post_id, 'show_on', $apps);
    $featured     = get_additional_field($post_id, 'featured', $apps);
    $offertype    = get_additional_field($post_id, 'offer_type', $apps);
    $offerblink   = get_additional_field($post_id, 'offer_booking_link', $apps);
    $convvalue    = get_additional_field($post_id, 'conversion_value', $apps);
    $convcurrency = get_additional_field($post_id, 'conversion_currency', $apps);
    $convvalueopt = get_additional_field($post_id, 'conversion_value_opt', $apps);

    if ($apps=='special-offe') {
        $content .= '
		<div class="additional_data">
		    <h2>Offer Type</h2>
		    <div class="additional_content">
		    	<div class="boxs-wrapp">
			    	<div class="boxs">
				    	<label>Input this offer type : </label>
				    	<input type="text" class="textbox small" name="additional_fields[offer_type]['.$index.']" value="'.$offertype.'" />
				    	<p class="note"><i>Ex. Special Promotion or Spa at Maya Discount</i></p>
			    	</div>
			    </div>
		    </div>
		</div>
		<div class="additional_data">
		    <h2>Set as Featured</h2>
		    <div class="additional_content">
		    	<div class="boxs-wrapp">
			    	<div class="boxs">
				    	<label>Set this offer to featured : </label>
				    	<select name="additional_fields[featured]['.$index.']">
				    		<option value="0" '.($featured==0 ? 'selected' : '').'>No</option>
				    		<option value="1" '.($featured==1 ? 'selected' : '').'>Yes</option>
				    	</select>
			    	</div>
			    </div>
		    </div>
		</div>
		<div class="additional_data">
		    <h2>Booking Link</h2>
		    <div class="additional_content">
		    	<div class="boxs-wrapp">
			    	<div class="boxs">
				    	<label>Input this booking link : </label>
				    	<input type="text" class="textbox medium" name="additional_fields[offer_booking_link]['.$index.']" value="'.$offerblink.'" />
				    	<p class="note note-2"><i>note: if empty, system will show popup booking</i></p>
			    	</div>
			    </div>
		    </div>
		</div>
		<div class="additional_data">
		    <h2>Google & Facebook Conversion</h2>
		    <div class="additional_content">
		    	<div class="boxs-wrapp">
		    		<p style="margin:15px 5px;">Use this conversion tracking option, when the Call To Action button is not directed to your Booking Engine</p>
			    	<div class="boxs">
				    	<label style="width:160px; display:inline-block; margin:0;">Track conversion? </label>
				    	<select name="additional_fields[conversion_value_opt]['.$index.']" style="width:150px;"">
				    		<option value="0" '.($convvalueopt==0 ? 'selected' : '').'>No</option>
				    		<option value="1" '.($convvalueopt==1 ? 'selected' : '').'>Yes</option>
				    	</select>
			    	</div>
			    	<div class="boxs">
				    	<label style="width:160px; display:inline-block; margin:0;">Select conversion currency? </label>
				    	<select name="additional_fields[conversion_currency]['.$index.']" style="width:150px;"">
				    		<option value="IDR" '.($convcurrency==0 ? 'selected' : '').'>IDR</option>
				    		<option value="USD" '.($convcurrency==1 ? 'selected' : '').'>USD</option>
				    	</select>
			    	</div>
			    	<div class="boxs">
				    	<label style="width:160px; display:inline-block; margin:0;">Input conversion value : </label>
				    	<input type="text" class="textbox small" name="additional_fields[conversion_value]['.$index.']" value="'.$convvalue.'" />
			    	</div>';

        if (isset($_GET['prc']) && $_GET['prc']=='edit') {
            $data     = fetch_artciles_by_id($post_id);
            $dest     = get_additional_field($post_id, 'destination', $apps);
            $content .= '
				    	<div class="boxs">
					    	<label style="width:160px; display:inline-block; margin:0;">Landing page url : </label>
					    	<input type="text" class="textbox medium" value="http://'.SITE_URL.'/'.$dest.'/'.$apps.'/'.$data['lsef'].'/" readonly />
					    	<a target="_blank" style="margin:0 10px;" href="http://'.SITE_URL.'/'.$dest.'/'.$apps.'/'.$data['lsef'].'/">View Page</a>
				    	</div>';
        }

        $content .= '
			    </div>
		    </div>
		</div>
		<div class="additional_data">
		    <h2>Show On</h2>
		    <div class="additional_content">
		    	<div class="boxs-wrapp">
			    	<div class="boxs">
				    	<label>Set this offer show on: </label>
				    	<select name="additional_fields[show_on]['.$index.']">
				    		<option value="0" '.($show_on==0 ? 'selected' : '').'>Show on both</option>
				    		<option value="1" '.($show_on==1 ? 'selected' : '').'>Show only on desktop</option>
				    		<option value="2" '.($show_on==2 ? 'selected' : '').'>Show only on mobile</option>
				    	</select>
			    	</div>
			    </div>
		    </div>
		</div>';
    }

    return $content;
}

function get_website_link($index, $post_id, $apps)
{
    $content = '';
    $weblink = get_additional_field($post_id, 'website_link', $apps);

    $include_apps = array('dining','activities','special-offers','featured');

    if (in_array($apps, $include_apps)) {
        $content .= '<div class="additional_data">
            <h2>Website Link</h2>
            <div class="additional_content">
              <div class="boxs-wrapp">
                <div class="boxs">
                  <label>Input the website link : </label>
                  <input type="text" class="textbox small" name="additional_fields[website_link]['.$index.']" value="'.$weblink.'" />
                </div>
              </div>
            </div>
        </div>';
    }

    return $content;
}

function get_price_spa($index, $post_id, $apps)
{
    $content = '';
    $weblink = get_additional_field($post_id, 'price_spa', $apps);

    $include_apps = array('spa');

    if (in_array($apps, $include_apps)) {
        $content .= '<div class="additional_data">
            <h2>Price SPA</h2>
            <div class="additional_content">
              <div class="boxs-wrapp">
                <div class="boxs">
                  <label>Price for spa</label>
                  <input type="text" class="textbox small" name="additional_fields[price_spa]['.$index.']" value="'.$weblink.'" />
                </div>
              </div>
            </div>
        </div>';
    }

    return $content;
}

function get_time_spa($index, $post_id, $apps)
{
    $content = '';
    $weblink = get_additional_field($post_id, 'time_spa', $apps);

    $include_apps = array('spa');

    if (in_array($apps, $include_apps)) {
        $content .= '<div class="additional_data">
            <h2>Time SPA</h2>
            <div class="additional_content">
              <div class="boxs-wrapp">
                <div class="boxs">
                  <label>Spend time to spa</label>
                  <input type="text" class="textbox small" name="additional_fields[time_spa]['.$index.']" value="'.$weblink.'" />
                </div>
              </div>
            </div>
        </div>';
    }

    return $content;
}

// function get_exists_menu_category($post_id)
// {
//     $options  = '';
//     $category = get_additional_field($post_id, 'additional_list_category', 'restaurant');
//     $category = json_decode($category, true);
//     if (!empty($category) && json_last_error() === JSON_ERROR_NONE) {
//         foreach ($category as $i=>$obj) {
//             $options .= '<option value="'.$obj['name'].'">'.$obj['name'].'</option>';
//             if (isset($obj['child'])) {
//                 foreach ($obj['child'] as $i=>$child) {
//                     $options .= '<option class="with-tabs" value="'.$obj['name'].' | '.$child['name'].'">'.$child['name'].'</option>';
//                 }
//             }
//         }
//     }
//
//     return $options;
// }

function delete_category_menu($post_id, $app_key, $app_name, $id)
{
    $cat_menu = get_additional_field($post_id, $app_key, $app_name);
    $cat_menu = json_decode($cat_menu, true);
    if (!empty($cat_menu) && json_last_error() === JSON_ERROR_NONE) {
        foreach ($cat_menu as $ii=>$val) {
            if ($val['id']==$id) {
                unset($cat_menu[$ii]);
            }

            if (isset($val['child'])) {
                foreach ($val['child'] as $iii=>$child) {
                    if ($child['id']==$id) {
                        unset($cat_menu[$ii]['child'][$iii]);
                    }
                }
            }
        }

        $new_cat_menu = (empty($cat_menu) ? '' : json_encode(array_values($cat_menu)));
    } else {
        $new_cat_menu = json_encode(array($value));
    }

    if (edit_additional_field($post_id, $app_key, $new_cat_menu, $app_name)) {
        return true;
    } else {
        return false;
    }
}

function delete_main_images($post_id='', $app_name='')
{
    if (empty($post_id)) {
        return true;
    }

    $main_image = get_additional_field($post_id, 'main_image', $app_name);

    if (empty($main_image)) {
        return true;
    }

    if (file_exists(PLUGINS_PATH.'/additional/background/'.$main_image)) {
        unlink(PLUGINS_PATH.'/additional/background/'.$main_image);
    }

    if (edit_additional_field($post_id, 'main_image', '', $app_name)) {
        return true;
    } else {
        return false;
    }
}

function delete_brief_image_top($post_id='', $app_name='')
{
    if (empty($post_id)) {
        return true;
    }

    $main_image = get_additional_field($post_id, 'brief_image_top', $app_name);

    if (empty($main_image)) {
        return true;
    }

    if (file_exists(PLUGINS_PATH.'/additional/background/'.$main_image)) {
        unlink(PLUGINS_PATH.'/additional/background/'.$main_image);
    }

    if (edit_additional_field($post_id, 'brief_image_top', '', $app_name)) {
        return true;
    } else {
        return false;
    }
}

function delete_brief_image_bottom($post_id='', $app_name='')
{
    if (empty($post_id)) {
        return true;
    }

    $main_image = get_additional_field($post_id, 'brief_image_bottom', $app_name);

    if (empty($main_image)) {
        return true;
    }

    if (file_exists(PLUGINS_PATH.'/additional/background/'.$main_image)) {
        unlink(PLUGINS_PATH.'/additional/background/'.$main_image);
    }

    if (edit_additional_field($post_id, 'brief_image_bottom', '', $app_name)) {
        return true;
    } else {
        return false;
    }
}

function delete_pdf_file($post_id='', $app_name='')
{
    if (empty($post_id)) {
        return true;
    }

    $pdf = get_additional_field($post_id, 'pdf_file', $app_name);

    if (empty($pdf)) {
        return true;
    }

    if (file_exists(PLUGINS_PATH.'/additional/pdf/'.$pdf)) {
        unlink(PLUGINS_PATH.'/additional/pdf/'.$pdf);
    }

    if (edit_additional_field($post_id, 'pdf_file', '', $app_name)) {
        return true;
    } else {
        return false;
    }
}

function delete_pdf_menu_file($post_id='', $app_name='')
{
    if (empty($post_id)) {
        return true;
    }

    $pdf = get_additional_field($post_id, 'menu_pdf', $app_name);

    if (empty($pdf)) {
        return true;
    }

    if (file_exists(PLUGINS_PATH.'/additional/pdf/'.$pdf)) {
        unlink(PLUGINS_PATH.'/additional/pdf/'.$pdf);
    }

    if (edit_additional_field($post_id, 'menu_pdf', '', $app_name)) {
        return true;
    } else {
        return false;
    }
}

function execute_additional_ajax_request()
{
    global $db;

    if (is_ajax_request()) {
        if ($_POST['ajax_key'] == 'check-category-menu-exists') {
            echo '{"result":"success"}';
        }

        if ($_POST['ajax_key'] == 'reorder-amenities-list') {
            if (edit_additional_field($_POST['post_id'], $_POST['app_key'], $_POST['value'], $_POST['app_name'])) {
                echo '{"result":"success"}';
            } else {
                echo '{"result":"failed"}';
            }
        }

        if ($_POST['ajax_key'] == 'manage-list-special-offers') {
            if (edit_additional_field($_POST['post_id'], $_POST['app_key'], $_POST['value'], $_POST['app_name'])) {
                if (isset($_POST['old_name']) && isset($_POST['new_name'])) {
                    $old_menu = get_additional_field($_POST['post_id'], 'list_special_offers', $_POST['app_name']);
                    $new_menu = str_replace($_POST['old_name'], $_POST['new_name'], $old_menu);

                    if (edit_additional_field($_POST['post_id'], 'list_special_offers', $new_menu, $_POST['app_name'])) {
                        echo '{"result":"success"}';
                    } else {
                        echo '{"result":"failed"}';
                    }
                } else {
                    echo '{"result":"success"}';
                }
            } else {
                echo '{"result":"failed"}';
            }
        }

        if ($_POST['ajax_key'] == 'reorder-menu-list') {
            if (edit_additional_field($_POST['post_id'], $_POST['app_key'], $_POST['value'], $_POST['app_name'])) {
                echo '{"result":"success"}';
            } else {
                echo '{"result":"failed"}';
            }
        }

        if ($_POST['ajax_key'] == 'manage-category-menu') {
            if (edit_additional_field($_POST['post_id'], $_POST['app_key'], $_POST['value'], $_POST['app_name'])) {
                if (isset($_POST['old_name']) && isset($_POST['new_name'])) {
                    $old_menu = get_additional_field($_POST['post_id'], 'additional_list_category', $_POST['app_name']);
                    $new_menu = str_replace($_POST['old_name'], $_POST['new_name'], $old_menu);

                    if (edit_additional_field($_POST['post_id'], 'additional_list_category', $new_menu, $_POST['app_name'])) {
                        echo '{"result":"success"}';
                    } else {
                        echo '{"result":"failed"}';
                    }
                } else {
                    echo '{"result":"success"}';
                }
            } else {
                echo '{"result":"failed"}';
            }
        }

        if ($_POST['ajax_key'] == 'get-category-menu-option') {
            $options  = '';
            $category = get_additional_field($_POST['post_id'], $_POST['app_key'], $_POST['app_name']);
            $category = json_decode($category, true);
            if (!empty($category) && json_last_error() === JSON_ERROR_NONE) {
                foreach ($category as $i=>$obj) {
                    $options .= '<option value="'.$obj['name'].'">'.$obj['name'].'</option>';
                    if (isset($obj['child'])) {
                        foreach ($obj['child'] as $i=>$child) {
                            $options .= '<option class="with-tabs" value="'.$obj['name'].' | '.$child['name'].'">'.$child['name'].'</option>';
                        }
                    }
                }
            }

            if (empty($options)) {
                echo '{"result":"failed"}';
            } else {
                $res['result'] = 'success';
                $res['data']   = $options;

                echo json_encode($res);
            }
        }

        if ($_POST['ajax_key'] =='get-category-parent') {
            $cat_name = '';
            $category = get_additional_field($_POST['post_id'], $_POST['app_key'], $_POST['app_name']);
            $category = json_decode($category, true);
            if (!empty($category) && json_last_error() === JSON_ERROR_NONE) {
                foreach ($category as $i=>$obj) {
                    if ($obj['name']==$_POST['cat_name']) {
                        $cat_name = $obj['name'];
                        break;
                    }

                    if (isset($obj['child'])) {
                        foreach ($obj['child'] as $i=>$child) {
                            if ($child['name']==$_POST['cat_name']) {
                                $cat_name = trim($obj['name']).' | '.trim($child['name']);
                                break;
                            }
                        }
                    }
                }
            }

            echo $cat_name;
        }

        if ($_POST['ajax_key'] == 'manage-list-menu') {
            if (edit_additional_field($_POST['post_id'], $_POST['app_key'], $_POST['value'], $_POST['app_name'])) {
                echo '{"result":"success"}';
            } else {
                echo '{"result":"failed"}';
            }
        }

        if ($_POST['ajax_key'] =='delete-main-image') {
            if (delete_main_images($_POST['post_id'], $_POST['app_name'])) {
                echo '{"result":"success"}';
            } else {
                echo '{"result":"failed"}';
            }
        }

        if ($_POST['ajax_key'] =='delete-brief-image-top') {
            if (delete_brief_image_top($_POST['post_id'], $_POST['app_name'])) {
                echo '{"result":"success"}';
            } else {
                echo '{"result":"failed"}';
            }
        }

        if ($_POST['ajax_key'] =='delete-brief-image-bottom') {
            if (delete_brief_image_bottom($_POST['post_id'], $_POST['app_name'])) {
                echo '{"result":"success"}';
            } else {
                echo '{"result":"failed"}';
            }
        }

        if ($_POST['ajax_key'] =='delete-pdf-file') {
            if (delete_pdf_file($_POST['post_id'], $_POST['app_name'])) {
                echo '{"result":"success"}';
            } else {
                echo '{"result":"failed"}';
            }
        }

        if ($_POST['ajax_key'] =='delete-pdf-menu-file') {
            if (delete_pdf_menu_file($_POST['post_id'], $_POST['app_name'])) {
                echo '{"result":"success"}';
            } else {
                echo '{"result":"failed"}';
            }
        }
    }

    exit;
}
add_actions('additional-ajax_page', 'execute_additional_ajax_request');
