<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
    Plugin Name: Lumonata Custom Application
    Plugin URL: http://lumonata.com/
    Description: This plugin is use for adding custom post type
    Author: Ngurah Rai
    Author URL: http://lumonata.com/
    Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

//-- Add custom post style css
add_actions('header_elements', 'custom_header_element');
function custom_header_element()
{
    return '<link href="http://'.SITE_URL.'/lumonata-plugins/custom-post/style.css" rel="stylesheet">';
}

/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk membuat custom post type dan menampilkannya ke dalam list menu
| -------------------------------------------------------------------------------------------------------------------------
*/
$apps_array = array(
    'articles'=>'Articles',
    'rooms'=>'Rooms',
    'dining'=>'Dining',
    // 'about-us'=>'About Us',
    'special-offers'=>'Special Offers',
    'gallery'=>'Gallery',
    'spa'=>'SPA',
    'activities'=>'Activities',
  'featured' => 'Why Stay With Us'

);

foreach ($apps_array as $key=>$apps) {
    add_privileges('administrator', $key, 'insert');
    add_privileges('administrator', $key, 'update');
    add_privileges('administrator', $key, 'delete');

    $tabs = array('blogs'=>$apps,'categories'=>'Categories','setting'=>'Setting');

    add_main_menu(array($key=>$apps));
    add_actions($key, 'get_admin_article', $key, $apps.'|'.$apps, $tabs, true, false);
    add_actions('setting_article_extends', 'set_setting_form', $tabs);
}

function set_gallery_size_setting()
{
    $large_gallery_width = (isset($_POST['large_gallery_width']) ? $_POST['large_gallery_width'] : 0);
    update_meta_data('large_gallery_width', $large_gallery_width, $_GET['state']);

    $large_gallery_height = (isset($_POST['large_gallery_height']) ? $_POST['large_gallery_height'] : 0);
    update_meta_data('large_gallery_height', $large_gallery_height, $_GET['state']);

    $medium_gallery_width = (isset($_POST['medium_gallery_width']) ? $_POST['medium_gallery_width'] : 0);
    update_meta_data('medium_gallery_width', $medium_gallery_width, $_GET['state']);

    $medium_gallery_height = (isset($_POST['medium_gallery_height']) ? $_POST['medium_gallery_height'] : 0);
    update_meta_data('medium_gallery_height', $medium_gallery_height, $_GET['state']);

    $thumb_gallery_width = (isset($_POST['thumb_gallery_width']) ? $_POST['thumb_gallery_width'] : 0);
    update_meta_data('thumb_gallery_width', $thumb_gallery_width, $_GET['state']);

    $thumb_gallery_height = (isset($_POST['thumb_gallery_height']) ? $_POST['thumb_gallery_height'] : 0);
    update_meta_data('thumb_gallery_height', $thumb_gallery_height, $_GET['state']);

    $crop_thumb = (isset($_POST['crop_thumb']) ? $_POST['crop_thumb'] : 0);
    update_meta_data('crop_gallery_thumb', $crop_thumb, $_GET['state']);
}

function set_setting_form($tabs)
{
    global $db;

    //-- Run Ajax Request
    if (is_ajax_request()) {
        return execute_post_type_ajax_request();
    }

    //-- Save Setting Data
    $message = '';
    if (is_save_changes()) {
        set_gallery_size_setting();

        $description = '';

        if (isset($_POST['additional_description'])) {
            $description = $_POST['additional_description'];
        }

        foreach ($_POST['title'] as $idx=>$post) {
            $background = set_post_type_background($idx);

            if ($_POST['title'][$idx] != 'Legian') {
                $description = '';
            }

            $meta_value = array(
                'title'=>$_POST['title'][$idx],
                'description'=> $_POST['description'][$idx],
                'background'=> $background,
                'additional_description' => $description ? $description : 'empty',
              );

            $is_edit_success = update_meta_data('post_type_setting', json_encode($meta_value), $_GET['state'], $idx);
        }



        if ($is_edit_success) {
            $message = '<div class="message success">'.$tabs['blogs'].' setting data has been successfully edited.</div>';
        } else {
            $message = '<div class="message error">Something wrong, please try again.</div>';
        }
    }

    //-- Get Setting Data
    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
    $q = $db->prepare_query($s, 'destinations');
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        set_template(PLUGINS_PATH.'/custom-post/setting-form.html', 'setting_form_template');
        add_block('setting_form_list', 'set_form_list', 'setting_form_template');

        while ($d = $db->fetch_array($r)) {
            $data  = get_post_type_setting($d['larticle_id'], $_GET['state']);

            add_variable('dest_id', $d['larticle_id']);
            add_variable('dest_name', $d['larticle_title']);
            add_variable('dest_sef', $d['lsef']);
            add_variable('title', $data['title']);
            add_variable('background', set_background_content($data['background'], $d['larticle_id']));
            add_variable('description', $data['description']);

            parse_template('setting_form_list', 'set_form_list', true);
        }

        $button    = '
		<li>'.button('button=save_changes&label=Save').'</li>
		<li>'.button('button=cancel', get_state_url($_GET['state']).'&tab=blogs').'</li>';

        add_block('setting_form', 'set_form', 'setting_form_template');
        add_actions('section_title', $tabs['blogs']);


        add_variable('app_title', $tabs['blogs'].' Setting');
        add_variable('tab', set_tabs($tabs, 'setting'));
        add_variable('button', $button);
        add_variable('message', $message);
        add_variable('site_url', SITE_URL);
        add_variable('state_url', get_state_url($_GET['state']).'&tab=setting');
        add_variable('gallery_setting', ($_GET['state']=='gallery') ? set_gallery_setting() : '');
        add_variable('description', get_description_spa($_GET['state']));

        parse_template('setting_form', 'set_form', false);
        return return_template('setting_form_template');
    }
}


function get_description_spa($state)
{
    $include_apps = array('spa');
    $content = '';
    if (in_array($state, $include_apps)) {
        $content = '';
        $additional_description = get_meta_data('post_type_setting', $state, '167');
        $json= json_decode($additional_description, true);

        $desc = $json['additional_description'];

        $content .= '
        <fieldset>
        <div class="additional_data">
            <h2>Description</h2>
            <div class="additional_content">
            	<div class="boxs-wrapp">
        	    	<div class="boxs">
                  <textarea name="additional_description" style="margin:0px;width:100%;height:124px;">'.$desc  .'</textarea>
        	    	</div>
        	    </div>
            </div>
        </div>
        </fieldset>';
    }
    return $content;
}

function get_post_type_setting($dest_id, $type, $key='')
{
    $empty = array('title'=>'','description'=>'','background'=>'');
    $value = get_meta_data('post_type_setting', $type, $dest_id);

    if (empty($value)) {
        return $empty;
    }

    $value = json_decode($value, true);
    if ($value === null && json_last_error() !== JSON_ERROR_NONE) {
        return $empty;
    } else {
        return $value;
    }
}

function get_post_by_type_list($destination, $type)
{
    global $db;

    $result = array();

    $s = 'SELECT a.* FROM lumonata_articles AS a
		  INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
		  WHERE a.larticle_type = %s AND a.larticle_status = %s AND b.lkey = %s AND b.lvalue = %s AND b.lapp_name = %s
		  ORDER BY a.lorder';
    $q = $db->prepare_query($s, $type, 'publish', 'destination', $destination, $type);
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        while ($d=$db->fetch_array($r)) {
            $parent  = get_additional_field($d['larticle_id'], 'parent', $type);

            if (empty($parent)) {
                $result[] = $d;
            }
        }
    }

    return $result;
}

function get_post_detail_by_type($destination, $type, $sef)
{
    global $db;

    $s = 'SELECT a.* FROM lumonata_articles AS a
		  INNER JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
		  WHERE a.larticle_type = %s AND a.lsef = %s AND b.lkey = %s AND b.lvalue = %s AND b.lapp_name = %s';
    $q = $db->prepare_query($s, $type, $sef, 'destination', $destination, $type);
    $r = $db->do_query($q);
    $d = $db->fetch_array($r);

    return $d;
}

function set_gallery_setting()
{
    $large_gallery_width   = get_meta_data('large_gallery_width', 'gallery');
    $large_gallery_height  = get_meta_data('large_gallery_height', 'gallery');
    $medium_gallery_width  = get_meta_data('medium_gallery_width', 'gallery');
    $medium_gallery_height = get_meta_data('medium_gallery_height', 'gallery');
    $thumb_gallery_width   = get_meta_data('thumb_gallery_width', 'gallery');
    $thumb_gallery_height  = get_meta_data('thumb_gallery_height', 'gallery');
    $crop_gallery_thumb    = get_meta_data('crop_gallery_thumb', 'gallery');

    $result = '
	<fieldset>
		<div class="additional_data sett">
		    <h2>Resize Setting</h2>
		    <div class="additional_content">
		    	<div class="sett-box clearfix">
			    	<fieldset class="clearfix">
		                <p>Large</p>
		                <div class="cols">
			                <input type="text" placeholder="Width" class="textbox" name="large_gallery_width" value="'.$large_gallery_width.'" />
			                <span>px</span>
			            </div>
		                <div class="cols">
			                <input type="text" placeholder="Height" class="textbox" name="large_gallery_height" value="'.$large_gallery_height.'" />
			                <span>px</span>
			            </div>
		            </fieldset>
			    	<fieldset class="clearfix">
		                <p>Medium</p>
		                <div class="cols">
			                <input type="text" placeholder="Width" class="textbox" name="medium_gallery_width" value="'.$medium_gallery_width.'" />
			                <span>px</span>
			            </div>
		                <div class="cols">
			                <input type="text" placeholder="Height" class="textbox" name="medium_gallery_height" value="'.$medium_gallery_height.'" />
			                <span>px</span>
			            </div>
		            </fieldset>
			    	<fieldset class="clearfix">
		                <p>Thumbnail</p>
		                <div class="cols">
			                <input type="text" placeholder="Width" class="textbox" name="thumb_gallery_width" value="'.$thumb_gallery_width.'" />
			                <span>px</span>
			            </div>
		                <div class="cols">
			                <input type="text" placeholder="Height" class="textbox" name="thumb_gallery_height" value="'.$thumb_gallery_height.'" />
			                <span>px</span>
			            </div>
		            </fieldset>
			    	<fieldset class="clearfix">
		                <input type="checkbox" name="crop_thumb" value="1" '.(empty($crop_gallery_thumb) ? '' : 'checked').' autocomplete="off" />
		                <label>Crop thumbnail size?</label>
		            </fieldset>
			    	<fieldset class="clearfix">
			    		<p>Forced Generate Image</p>
			    		<input type="button" name="regenerate" value="Generate" />
			    		<span>click this button if you want to regenerate new image size base on above setting</span>
			    		<div class="generate-report">
			    			<ul></ul>
			    		</div>
			    	</fieldset>
		        </div>
		    </div>
		</div>
	</fieldset>';

    return $result;
}

function set_post_type_background($idx)
{
    $filename = '';

    if (isset($_POST['bg_image'][$idx])) {
        //-- CHECK FOLDER EXIST
        if (!file_exists(PLUGINS_PATH.'/custom-post/background/')) {
            mkdir(PLUGINS_PATH.'/custom-post/background/');
        }
        if (!file_exists(PLUGINS_PATH.'/custom-post/background/large/')) {
            mkdir(PLUGINS_PATH.'/custom-post/background/large/');
        }
        if (!file_exists(PLUGINS_PATH.'/custom-post/background/thumbnail/')) {
            mkdir(PLUGINS_PATH.'/custom-post/background/thumbnail/');
        }

        preg_match('#^data:image/\w+;base64,#i', $_POST['bg_image'][$idx], $matches);
        if (!empty($matches)) {
            while (true) {
                $name = uniqid(rand(), true).'.jpg';
                if (!file_exists(PLUGINS_PATH.'/custom-post/background/'.$name)) {
                    break;
                }
            }

            $background = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['bg_image'][$idx]));
            file_put_contents(PLUGINS_PATH.'/custom-post/background/'.$name, $background);

            if (file_exists(PLUGINS_PATH.'/custom-post/background/'.$name)) {
                $filename  = $name;

                $file_source = PLUGINS_PATH.'/custom-post/background/'.$name;
                $destination_thumb = PLUGINS_PATH.'/custom-post/background/thumbnail/'.$name;

                $img = new SmartImage($file_source);
                $img->resize(200, 200, true);
                $img->saveImage($destination_thumb, 80);
                $img->close();
            }

            //-- Delete old image
            $old_bg = get_meta_data('post_type_setting', $_GET['state'], $idx);
            $old_bg = json_decode($old_bg, true);
            if (!empty($old_bg['background'])) {
                if (file_exists(PLUGINS_PATH.'/custom-post/background/'.$old_bg['background'])) {
                    unlink(PLUGINS_PATH.'/custom-post/background/'.$old_bg['background']);
                }
                if (file_exists(PLUGINS_PATH.'/custom-post/background/large/'.$old_bg['background'])) {
                    unlink(PLUGINS_PATH.'/custom-post/background/large/'.$old_bg['background']);
                }
                if (file_exists(PLUGINS_PATH.'/custom-post/background/thumbnail/'.$old_bg['background'])) {
                    unlink(PLUGINS_PATH.'/custom-post/background/thumbnail/'.$old_bg['background']);
                }
            }
        } else {
            $filename = $_POST['bg_image'][$idx];
        }
    }

    return $filename;
}

function set_background_content($background, $dest_id)
{
    if (empty($background)) {
        return $background;
    }

    return '
	<div class="box">
        <img src="http://'.SITE_URL.'/lumonata-plugins/custom-post/background/thumbnail/'.$background.'" />
        <input type="hidden" name="bg_image['.$dest_id.']" value="'.$background.'" />
        <div class="overlay" style="display: none;">
            <a data-post-id="'.$dest_id.'" data-bg-name="'.$background.'" class="bg-delete">
                <img src="http://'.SITE_URL.'/lumonata-plugins/custom-post/images/delete.png">
            </a>
        </div>
    </div>';
}

function delete_post_type_background($app_id, $bg_image)
{
    $data = get_post_type_setting($app_id, $_GET['state']);
    if (empty($data['background'])) {
        return true;
    }

    if ($bg_image==$data['background']) {
        if (file_exists(PLUGINS_PATH.'/custom-post/background/'.$bg_image)) {
            unlink(PLUGINS_PATH.'/custom-post/background/'.$bg_image);
        }
        if (file_exists(PLUGINS_PATH.'/custom-post/background/large/'.$bg_image)) {
            unlink(PLUGINS_PATH.'/custom-post/background/large/'.$bg_image);
        }
        if (file_exists(PLUGINS_PATH.'/custom-post/background/thumbnail/'.$bg_image)) {
            unlink(PLUGINS_PATH.'/custom-post/background/thumbnail/'.$bg_image);
        }

        $data['background'] = '';
    }

    if (update_meta_data('post_type_setting', json_encode($data), $_GET['state'], $app_id)) {
        return true;
    } else {
        return false;
    }
}

function execute_post_type_ajax_request()
{
    global $db;

    if ($_POST['ajax_key'] =='delete-post-type-background') {
        if (delete_post_type_background($_POST['app_id'], $_POST['bg_name'])) {
            echo '{"result":"success"}';
            exit;
        } else {
            echo '{"result":"failed"}';
            exit;
        }
    }

    exit;
}
