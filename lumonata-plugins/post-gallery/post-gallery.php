<?php
/*
    Plugin Name: Post Gallery
   	Plugin URL: http://lumonata.com/
    Description: -
    Author: Ngurah Rai
    Author URL: http://lumonata.com/
    Version: 1.0.1

*/

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan galleri di masing-masing post type
| -------------------------------------------------------------------------------------------------------------------------
*/
$apps_array = array('gallery');

foreach($apps_array as $apps)
{
	if(is_edit_all() && !(is_save_draft() || is_publish()))
	{
	    foreach($_POST['select'] as $index=>$post_id)
		{
			add_actions($apps.'_additional_filed_'.$index, 'gallery_image');
	    }
	}
	else
	{
		add_actions($apps.'_additional_filed', 'gallery_image');
	}
}

function gallery_image()
{
	global $thepost;

	$index    = $thepost->post_index;
  $post_id  = $thepost->post_id;
	$app_name = ($_GET['state']=='applications' ? $_GET['sub'] : $_GET['state']);

	add_actions('admin_tail','get_javascript','jquery-ui-1.8.2.custom.min');
	add_actions('admin_tail','get_javascript','jquery.ui.nestedSortable.js');

	set_template(PLUGINS_PATH."/post-gallery/post-gallery-form.html",'post_gallery_template_'.$index);
	add_block('post_gallery','pg','post_gallery_template_'.$index);

	add_variable('iii',$index);
	add_variable('site_url',SITE_URL);
	add_variable('post_id',$post_id);
	add_variable('app_name',$app_name);
	add_variable('admin_url',SITE_URL.'/lumonata-admin');
	add_variable('ajax_url',SITE_URL.'/gallery-ajax/');

	parse_template('post_gallery','pg',false);
	$templates = return_template('post_gallery_template_'.$index);
	return $templates;
}

function get_all_post_gallery($app_key='', $app_name='')
{
	global $db;

	$result  = array();
	$gallery = get_additional_field_by_app($app_key, $app_name);

	if(!empty($gallery))
	{
		foreach($gallery as $gal)
		{
			$gallery_arr = json_decode($gal,true);

			if(!empty($gallery_arr))
			{
				foreach($gallery_arr as $val)
				{
					$s = 'SELECT * FROM lumonata_attachment WHERE lattach_id=%d';
					$q = $db->prepare_query($s,$val);

					if(count_rows($q) > 0)
					{
						$r = $db->do_query($q);
						$d = $db->fetch_array($r);
						$result[] = array(
							'attach_id'=>$d['lattach_id'],
							'attach_title'=>$d['ltitle'],
							'origin_url'=>$d['lattach_loc'],
							'thumb_url'=>$d['lattach_loc_thumb'],
							'medium_url'=>$d['lattach_loc_medium'],
							'large_url'=>$d['lattach_loc_large']
						);
					}
				}
			}
		}
	}

	return $result;
}

function get_post_gallery_image($post_id='', $app_key='', $app_name='')
{
	global $db;

	$result   = array();

	if($post_id  <> 0)
	{
		$attach_id = get_additional_field($post_id, $app_key, $app_name);
		if($attach_id!='')
		{
			$attach_arr = json_decode($attach_id,true);
			if(!empty($attach_arr))
			{
				foreach($attach_arr as $val)
				{
					$s = 'SELECT * FROM lumonata_attachment WHERE lattach_id=%d';
					$q = $db->prepare_query($s,$val);
					if(count_rows($q) > 0)
					{
						$r = $db->do_query($q);
						$d = $db->fetch_array($r);
						$result[] = array(
							'attach_id'=>$d['lattach_id'],
							'origin_url'=>$d['lattach_loc'],
							'thumb_url'=>$d['lattach_loc_thumb'],
							'medium_url'=>$d['lattach_loc_medium'],
							'large_url'=>$d['lattach_loc_large']
						);
					}
				}
			}
		}
	}

	return $result;
}

function insert_post_gallery_image()
{
	global $db;

	if(!defined('FILES_PATH'))
		define('FILES_PATH',ROOT_PATH.'/lumonata-content/files');

	if(!defined('FILES_LOCATION'))
		define('FILES_LOCATION','/lumonata-content/files');

	$folder_name = date("Y",time()).date("m",time());

	$images = get_additional_field($_POST['post_id'],$_POST['app_key'],$_POST['app_name']);
	$images = (empty($images) ? array() : json_decode($images, true));
	$result = array();

	//-- Get Gallery Resize Setting
	$large_gallery_width   = get_meta_data('large_gallery_width','gallery');
	$large_gallery_height  = get_meta_data('large_gallery_height','gallery');
	$medium_gallery_width  = get_meta_data('medium_gallery_width','gallery');
	$medium_gallery_height = get_meta_data('medium_gallery_height','gallery');
	$thumb_gallery_width   = get_meta_data('thumb_gallery_width','gallery');
	$thumb_gallery_height  = get_meta_data('thumb_gallery_height','gallery');
	$crop_gallery_thumb    = get_meta_data('crop_gallery_thumb','gallery');

	//-- Fallback to general image resize
	//-- if gallery setting value empty
	$lg_width   = (empty($large_gallery_width) ? large_image_width() : $large_gallery_width);
	$lg_height  = (empty($large_gallery_height) ? large_image_height() : $large_gallery_height);
	$mg_width   = (empty($medium_gallery_width) ? medium_image_width() : $medium_gallery_width);
	$mg_height  = (empty($medium_gallery_height) ? medium_image_height() : $medium_gallery_height);
	$tg_width   = (empty($thumb_gallery_width) ? thumbnail_image_width() : $thumb_gallery_width);
	$tg_height  = (empty($thumb_gallery_height) ? thumbnail_image_height() : $thumb_gallery_height);

	foreach($_FILES['file_name']['name'] as $idx=>$obj)
	{
		$file_name   = $_FILES['file_name']['name'][$idx];
		$file_size   = $_FILES['file_name']['size'][$idx];
		$file_type   = $_FILES['file_name']['type'][$idx];
		$file_source = $_FILES['file_name']['tmp_name'][$idx];
		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);

				$file_name_ori    = $fix_file_name.$file_ext;
				$file_name_thumb  = $fix_file_name.'-thumbnail'.$file_ext;
				$file_name_medium = $fix_file_name.'-medium'.$file_ext;
				$file_name_large  = $fix_file_name.'-large'.$file_ext;

				if(!is_dir(FILES_PATH.'/'.$folder_name)) mkdir(FILES_PATH.'/'.$folder_name);

				$destination        = FILES_PATH.'/'.$folder_name .'/'.$file_name_ori;
				$destination_thumb  = FILES_PATH.'/'.$folder_name .'/'.$file_name_thumb;
				$destination_medium = FILES_PATH.'/'.$folder_name .'/'.$file_name_medium;
				$destination_large  = FILES_PATH.'/'.$folder_name .'/'.$file_name_large;

				upload_resize($file_source, $destination_large, $file_type, $lg_width, $lg_height);
				upload_resize($file_source, $destination_medium, $file_type, $mg_width, $mg_height);

				if($crop_gallery_thumb==1)
				{
					upload_crop($file_source, $destination_thumb, $file_type, $tg_width, $tg_height);
				}
				else
				{
					upload_resize($file_source, $destination_thumb, $file_type, $tg_width, $tg_height);
				}

				upload($file_source, $destination);

				$lattach_loc        = FILES_LOCATION.'/'.$folder_name .'/'.$file_name_ori;
				$lattach_loc_thumb  = FILES_LOCATION.'/'.$folder_name .'/'.$file_name_thumb;
				$lattach_loc_medium = FILES_LOCATION.'/'.$folder_name .'/'.$file_name_medium;
				$lattach_loc_large  = FILES_LOCATION.'/'.$folder_name .'/'.$file_name_large;

				$s = 'INSERT INTO lumonata_attachment(
							larticle_id,
							lattach_loc,
							lattach_loc_thumb,
							lattach_loc_medium,
							lattach_loc_large,
							ltitle,
							upload_date,
							date_last_update,
							mime_type,
							lorder)
					  VALUES(%d,%s,%s,%s,%s,%s,%s,%s,%s,%d)';

				$q = $db->prepare_query(
							$s,
							$_POST['post_id'],
							$lattach_loc,
							$lattach_loc_thumb,
							$lattach_loc_medium,
							$lattach_loc_large,
							$file_name,
							date('Y-m-d H:i:s',time()),
							date('Y-m-d H:i:s',time()),
							$file_type,
							1);

				if(reset_order_id('lumonata_attachment'))
				{
					if($db->do_query($q))
					{
						$attachid = mysql_insert_id();
						$images[] = $attachid;
						$result[] = array('attach_id'=>$attachid,'thumb_url'=>$lattach_loc_thumb);
					}
				}
			}
		}
	}

	$image_arr = (empty($images) ? '' : json_encode($images));
	edit_additional_field($_POST['post_id'], $_POST['app_key'], $image_arr, $_POST['app_name']);

	return $result;
}

function delete_post_gallery_images()
{
	global $db;

	$img = get_additional_field($_POST['post_id'],$_POST['app_key'],$_POST['app_name']);

	if(!empty($img))
	{
		// DELETE ATTACHMENT ID FROM LIST --
		$attach_arr = json_decode($img, true);
		if(!empty($attach_arr))
		{
			foreach($attach_arr as $key=>$val)
			{
				if($val==$_POST['attach_id'])
				{
					unset($attach_arr[$key]);
				}
			}
		}

		// EDIT ADDITIONAL FIELD VALUE --
		$value = (empty($attach_arr) ? '' : json_encode($attach_arr));
		if(edit_additional_field($_POST['post_id'], $_POST['app_key'], $value, $_POST['app_name']))
		{
			if(delete_attachment($_POST['attach_id']))
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		if(delete_attachment($_POST['attach_id']))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

function reorder_post_gallery_image()
{
	global $db;

	if(!empty($_POST['value']))
	{
		$id = get_additional_field($_POST['post_id'],$_POST['app_key'],$_POST['app_name']);
		if($id!='')
		{
			$s = 'UPDATE lumonata_additional_fields SET lvalue=%s WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s';
			$q = $db->prepare_query($s,$_POST['value'],$_POST['post_id'],$_POST['app_key'],$_POST['app_name']);
			$db->do_query($q);
		}

		return true;
	}
	else
	{
		return false;
	}
}

function regenerate_gallery_image($attach_id='')
{
	global $db;

	$s = 'SELECT * FROM lumonata_attachment WHERE lattach_id=%d';
	$q = $db->prepare_query($s, $attach_id);

	if(count_rows($q) > 0)
	{
		$r = $db->do_query($q);
		$d = $db->fetch_array($r);

		$file_source = ROOT_PATH.$d['lattach_loc'];
		$file_type   = mime_content_type($file_source);

		if(file_exists($file_source))
		{
			$data = pathinfo($file_source);

			//-- Get Gallery Resize Setting
			$large_gallery_width   = get_meta_data('large_gallery_width','gallery');
			$large_gallery_height  = get_meta_data('large_gallery_height','gallery');
			$medium_gallery_width  = get_meta_data('medium_gallery_width','gallery');
			$medium_gallery_height = get_meta_data('medium_gallery_height','gallery');
			$thumb_gallery_width   = get_meta_data('thumb_gallery_width','gallery');
			$thumb_gallery_height  = get_meta_data('thumb_gallery_height','gallery');
			$crop_gallery_thumb    = get_meta_data('crop_gallery_thumb','gallery');

			//-- Fallback to general image resize
			//-- if gallery setting value empty
			$lg_width   = (empty($large_gallery_width) ? large_image_width() : $large_gallery_width);
			$lg_height  = (empty($large_gallery_height) ? large_image_height() : $large_gallery_height);
			$mg_width   = (empty($medium_gallery_width) ? medium_image_width() : $medium_gallery_width);
			$mg_height  = (empty($medium_gallery_height) ? medium_image_height() : $medium_gallery_height);
			$tg_width   = (empty($thumb_gallery_width) ? thumbnail_image_width() : $thumb_gallery_width);
			$tg_height  = (empty($thumb_gallery_height) ? thumbnail_image_height() : $thumb_gallery_height);

			$file_name_thumb  = $data['filename'].'-thumbnail.'.$data['extension'];
			$file_name_medium = $data['filename'].'-medium.'.$data['extension'];
			$file_name_large  = $data['filename'].'-large.'.$data['extension'];

			$destination_thumb  = $data['dirname'].'/'.$file_name_thumb;
			$destination_medium = $data['dirname'].'/'.$file_name_medium;
			$destination_large  = $data['dirname'].'/'.$file_name_large;

			upload_resize($file_source, $destination_large, $file_type, $lg_width, $lg_height);
			upload_resize($file_source, $destination_medium, $file_type, $mg_width, $mg_height);

			if($crop_gallery_thumb==1)
			{
				upload_crop($file_source, $destination_thumb, $file_type, $tg_width, $tg_height);
			}
			else
			{
				upload_resize($file_source, $destination_thumb, $file_type, $tg_width, $tg_height, 'width');
			}

			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function execute_gallery_ajax_request()
{
	global $db;

	if($_POST['ajax_key'] =='refresh-gallery-images')
	{
		$data = get_post_gallery_image($_POST['post_id'], $_POST['app_key'], $_POST['app_name']);

		if(empty($data))
		{
			$res['result'] = 'failed';
		}
		else
		{
			$res['result'] = 'success';
			$res['data']   = $data;
			$res['list']   = get_additional_field($_POST['post_id'],$_POST['app_key'],$_POST['app_name']);
		}

		echo json_encode($res);
	}

	if($_POST['ajax_key'] =='get-gallery-images')
	{
		$data = get_all_post_gallery($_POST['app_key'], $_POST['app_name']);

		if(empty($data))
		{
			$res['result'] = 'failed';
		}
		else
		{
			$res['result'] = 'success';
			$res['data']   = $data;
		}

		echo json_encode($res);
	}

	if($_POST['ajax_key'] =='regenerate-gallery-images')
	{
		if(regenerate_gallery_image($_POST['attach_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-gallery-images')
	{
		$data = insert_post_gallery_image();
		if(empty($data))
		{
			$res['result'] = 'failed';
		}
		else
		{
			$res['result'] = 'success';
			$res['data']   = $data;
		}

		echo json_encode($res);
	}

	if($_POST['ajax_key'] =='delete-gallery-images')
	{
		if(delete_post_gallery_images())
		{
			$res['result'] = 'success';
			$res['data']   = get_additional_field($_POST['post_id'],$_POST['app_key'],$_POST['app_name']);
			echo json_encode($res);
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='reorder-gallery-images')
	{
		if(reorder_post_gallery_image())
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	exit;
}
add_actions('gallery-ajax_page','execute_gallery_ajax_request');

?>
