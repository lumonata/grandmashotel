<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
    Plugin Name: Lumonata Destination Application
    Plugin URL: http://lumonata.com/
    Description: This plugin is use for adding destination post
    Author: Ngurah Rai
    Author URL: http://lumonata.com/
    Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

add_privileges('administrator', 'destinations', 'insert');
add_privileges('administrator', 'destinations', 'update');
add_privileges('administrator', 'destinations', 'delete');

//-- Add custom icon menu
add_actions('header_elements','destinations_header_element');
function destinations_header_element()
{
    return '<link href="http://'.SITE_URL.'/lumonata-plugins/destinations/style.css" rel="stylesheet">';
}

//-- Add sub menu under applications menu
add_main_menu(array('destinations'=>'Destinations'));
add_actions('destinations','set_destinations_data');


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini otomatis akan dijalankan untuk melakukan filter action
	yang akan dilakukan -- (get, add, edit, delete, delete all)
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_destinations_data()
{
	if(is_add_new())
	{
		return add_destinations();
	}
	elseif(is_edit())
	{
		return edit_destinations();
	}
	elseif(is_delete_all())
	{
		return delete_all_destinations();
	}
	elseif(is_confirm_delete())
	{
		foreach($_POST['id'] as $key=>$val)
		{
			delete_destinations($val);
		}
	}
	elseif(is_ajax_request())
	{
		return execute_ajax_request();
	}

	if(is_num_destinations() > 0)
	{
		return get_destinations_list();
	}
	else
	{
		header('location:'.get_state_url('destinations').'&prc=add_new');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengambil jumlah data destinaton
| -------------------------------------------------------------------------------------------------------------------------
*/
function is_num_destinations()
{
	global $db;

	$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
	$q = $db->prepare_query($s,'destinations');
	$r = $db->do_query($q);

	return $db->num_rows($r);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengambil data destinaton dari database
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_destinations_list()
{
	global $db;

	$list   = '';
	$search = (isset($_POST['s']) ? $_POST['s'] : '');
	$page   = (isset($_GET['page']) ? $_GET['page'] : 1);
	$url    = get_state_url('destinations')."&page=";
	$viewed = list_viewed();
	$limit  = ($page-1) * $viewed;
	$start  = ($page - 1) * $viewed + 1;

	if(is_search())
	{
		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s';
		$q = $db->prepare_query($s,'destinations',"%".$_POST['s']."%");
		$r = $db->do_query($q);
		$n = $db->num_rows($r);

		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s LIMIT %d, %d';
		$q = $db->prepare_query($s,'destinations','%'.$_POST['s'].'%',$limit,$viewed);
	}
	else
	{
		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
		$q = $db->prepare_query($s,'destinations');
		$r = $db->do_query($q);
		$n = $db->num_rows($r);

		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s LIMIT %d, %d';
		$q = $db->prepare_query($s,'destinations',$limit,$viewed);
	}

	$button = '
	<li>'.button('button=add_new',get_state_url('destinations').'&prc=add_new').'</li>
	<li>'.button('button=delete&type=submit&enable=false').'</li>';

	set_template(PLUGINS_PATH."/destinations/list.html",'destinations_list_template');

	$r = $db->do_query($q);
	if($db->num_rows($r) > 0)
	{
		add_block('destinations_list','dest_list','destinations_list_template');

		while($d=$db->fetch_array($r))
		{
			add_variable('id',$d['larticle_id']);
			add_variable('title',$d['larticle_title']);
			add_variable('description',limit_words(strip_tags($d['larticle_content']), 50));

			parse_template('destinations_list','dest_list',true);
		}
	}

	add_block('destinations','dest','destinations_list_template');
	add_actions('section_title','Destinations - List');

	add_variable('button',$button);
	add_variable('search_val',$search);
	add_variable('start_order',$start);
	add_variable('state','destinations');
	add_variable('state_url',get_state_url('destinations'));
	add_variable('template_url',TEMPLATE_URL);

	parse_template('destinations','dest',false);
	return return_template('destinations_list_template');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk meyimpan data destinaton ke database
| -------------------------------------------------------------------------------------------------------------------------
*/
function add_destinations()
{
	$message  = '';
	$mess_arr = array();

	$post_title    = (isset($_POST['post_title']) ? $_POST['post_title'] : '');
	$post_content  = (isset($_POST['post_content']) ? rem_slashes($_POST['post_content']) : '');

	if(is_save_changes())
	{
		if(empty($post_title))
		{
			$mess_arr[] = '- Please input your destination title';

		}

		if(empty($post_content))
		{
			$mess_arr[] = '- Please input your destination description';
		}

		if(empty($mess_arr))
		{
			$is_save_success = save_article($post_title, $post_content, 'publish', 'destinations','');

			if($is_save_success)
			{
                $post_id = mysql_insert_id();

				set_informations($post_id);
				set_background_images($post_id);
				set_corporate_images($post_id);
				set_featured($post_id);
				set_slide_images($post_id);
				set_location_images($post_id);

				$message = '<div class="message success">New destination has save succesfully.</div>';
			}
			else
			{
				$message = '<div class="message error">Something wrong, please try again.</div>';
			}
		}
		else
		{
			$message = '<div class="message error"><p>'.implode('<br />', $mess_arr).'</p></div>';
		}
	}

	$button	= '
	<li>'.button('button=save_changes&label=Save').'</li>
	<li>'.button('button=add_new',get_state_url('destinations').'&prc=add_new').'</li>
	<li>'.button('button=cancel',get_state_url('destinations')).'</li>';

	set_template(PLUGINS_PATH.'/destinations/form.html','destinations_form_template');
	add_block('destinations_form','dest_form','destinations_form_template');
	add_actions('section_title','Add New Destination');

	add_variable('form_title','Add New Destination');
	add_variable('post_title',$post_title);
	add_variable('post_content',textarea('post_content',0,$post_content,0,true));
	add_variable('site_url',SITE_URL);
	add_variable('message',$message);
	add_variable('button',$button);
	add_variable('post_id','');

	parse_template('destinations_form','dest_form',false);
	return return_template('destinations_form_template');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengubah data destinaton di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function edit_destinations()
{
	global $db;

	$message  = '';
	$mess_arr = array();

	$post_title   = (isset($_POST['post_title']) ? $_POST['post_title'] : '');
	$post_content = (isset($_POST['post_content']) ? rem_slashes($_POST['post_content']) : '');

	if(is_save_changes())
	{
		if(empty($post_title))
		{
			$mess_arr[] = '- Please input your destination title';
		}

		if(empty($post_content))
		{
			$mess_arr[] = '- Please input your destination description';
		}

		if(empty($mess_arr))
		{
			$is_edit_success = update_article($_GET['id'], $post_title, $post_content, 'publish', 'destinations','');

			if($is_edit_success)
			{
				set_featured($_GET['id']);
				set_background_images($_GET['id']);
				set_corporate_images($_GET['id']);
				set_slide_images($_GET['id']);
				set_location_images($_GET['id']);
				set_informations($_GET['id']);

				$message = '<div class="message success">Destination data has been successfully edited.</div>';
			}
			else
			{
				$message = '<div class="message error">Something wrong, please try again.</div>';
			}
		}
		else
		{
			$message = '<div class="message error"><p>'.implode('<br />', $mess_arr).'</p></div>';
		}
	}

	$button	= '
	<li>'.button('button=save_changes&label=Save').'</li>
	<li>'.button('button=add_new',get_state_url('destinations').'&prc=add_new').'</li>
	<li>'.button('button=cancel',get_state_url('destinations')).'</li>';

	set_template(PLUGINS_PATH.'/destinations/form.html','destinations_form_template');
	add_block('destinations_form','dest_form','destinations_form_template');
	add_actions('section_title','Edit Destination');

	add_actions('admin_tail','get_javascript','jquery-ui-1.8.2.custom.min');
	add_actions('admin_tail','get_javascript','jquery.ui.nestedSortable.js');

	$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_id=%d';
	$q = $db->prepare_query($s,'destinations',$_GET['id']);
	$r = $db->do_query($q);
	if($db->num_rows($r) > 0)
	{
		$d = $db->fetch_array($r);

		add_variable('form_title','Edit Destination');
		add_variable('post_title',$d['larticle_title']);
		add_variable('post_content',textarea('post_content',0,rem_slashes($d['larticle_content']),$_GET['id'],true));
		add_variable('slide_list',set_slide_list($_GET['id']));
		add_variable('location_list',set_location_list($_GET['id']));
		add_variable('background_image',set_background($_GET['id']));
		add_variable('corporate_image',set_corporate($_GET['id']));
		add_variable('state_url',get_state_url('destinations'));

		add_variable('phone_number',get_additional_field($_GET['id'],'phone_number','destinations'));
		add_variable('fax_number',get_additional_field($_GET['id'],'fax_number','destinations'));
		add_variable('restaurant_phone_number',get_additional_field($_GET['id'],'restaurant_phone_number','destinations'));
		add_variable('restaurant_fax_number',get_additional_field($_GET['id'],'restaurant_fax_number','destinations'));
		add_variable('reservation_phone_number',get_additional_field($_GET['id'],'reservation_phone_number','destinations'));
		add_variable('reservation_fax_number',get_additional_field($_GET['id'],'reservation_fax_number','destinations'));
		add_variable('fb_link',get_additional_field($_GET['id'],'fb_link','destinations'));
		add_variable('twitter_link',get_additional_field($_GET['id'],'twitter_link','destinations'));
		add_variable('instagram_link',get_additional_field($_GET['id'],'instagram_link','destinations'));
		add_variable('tripadvisor_link',get_additional_field($_GET['id'],'tripadvisor_link','destinations'));
		add_variable('email_address',get_additional_field($_GET['id'],'email_address','destinations'));
		add_variable('restaurant_email_address',get_additional_field($_GET['id'],'restaurant_email_address','destinations'));
		add_variable('reservation_email_address',get_additional_field($_GET['id'],'reservation_email_address','destinations'));
		add_variable('we_email_address',get_additional_field($_GET['id'],'we_email_address','destinations'));
		add_variable('instagram_user',get_additional_field($_GET['id'],'instagram_user','destinations'));
		add_variable('coordinate_location',get_additional_field($_GET['id'],'coordinate_location','destinations'));
		add_variable('address',get_additional_field($_GET['id'],'address','destinations'));
		add_variable('g_analytic',get_additional_field($_GET['id'],'g_analytic','destinations'));
		add_variable('pixel_script',get_additional_field($_GET['id'],'pixel_script','destinations'));
		add_variable('hotel_code',get_additional_field($_GET['id'],'hotel_code','destinations'));
		add_variable('chain_code',get_additional_field($_GET['id'],'chain_code','destinations'));
		add_variable('template_code',get_additional_field($_GET['id'],'template_code','destinations'));
		add_variable('shell_code',get_additional_field($_GET['id'],'shell_code','destinations'));

    // add_variable('book_now_link',get_additional_field($_GET['id'],'book_now_link','destinations'));

		add_variable('fb_page_name',get_additional_field($_GET['id'],'fb_page_name','destinations'));
		add_variable('twitter_username',get_additional_field($_GET['id'],'twitter_username','destinations'));
		add_variable('instagram_username',get_additional_field($_GET['id'],'instagram_username','destinations'));
		add_variable('tripadvisor_name',get_additional_field($_GET['id'],'tripadvisor_name','destinations'));
		add_variable('skype_name',get_additional_field($_GET['id'],'skype_name','destinations'));

		add_variable('meta_title',get_additional_field($_GET['id'],'meta_title','destinations'));
		add_variable('meta_description',get_additional_field($_GET['id'],'meta_description','destinations'));
		add_variable('meta_keywords',get_additional_field($_GET['id'],'meta_keywords','destinations'));

		add_variable('location_map_title',get_additional_field($_GET['id'],'location_map_title','destinations'));
		add_variable('location_map_desc',get_additional_field($_GET['id'],'location_map_desc','destinations'));

		add_variable('site_url',SITE_URL);
		add_variable('message',$message);
		add_variable('button',$button);
		add_variable('post_id','');

		parse_template('destinations_form','dest_form',false);
		return return_template('destinations_form_template');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus data destinaton di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_destinations($id='')
{
	global $db;

	$s = 'DELETE FROM lumonata_articles WHERE larticle_type=%s AND larticle_id=%d';
	$q = $db->prepare_query($s,'destinations',$id);
	if($db->do_query($q))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus multiple data destinaton di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_all_destinations()
{
	global $db;

	add_actions('section_title','Delete Destination');

	$warning = '
	<h1 class="form-title">Delete Destination</h1>
	<div class="tab_container">
		<div class="single_content destination-wrapp">';
			$warning .= '
			<form action="" method="post">';
				$warning .= '
				<div class="alert_red_form boxs">
					<strong>
						Are you sure want to delete this
						destination'.(count($_POST['select']) > 1 ? 's' : '').' :
					</strong>
					<ol>';
						foreach($_POST['select'] as $key=>$val)
						{
							$s = 'SELECT larticle_title FROM lumonata_articles WHERE larticle_id=%d';
							$q = $db->prepare_query($s,$val);
							$r = $db->do_query($q);
							$d = $db->fetch_array($r);

							$warning .= '
							<li>
								'.$d['larticle_title'].'
								<input type="hidden" name="id[]" value="'.$val.'" />
							</li>';
						}
						$warning .= '
					</ol>
				</div>
				<div class="alert-button-box">
					<input type="submit" name="confirm_delete" value="Yes" class="button" />
					<input type="button" name="confirm_delete" value="No" class="button" onclick="location=\''.get_state_url('destinations').'\'" />
				</div>
			</form>
		</div>
	</div>';

	return $warning;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton background image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_background_images($post_id)
{
	if(isset($_POST['background_image']))
	{
		//-- CHECK FOLDER EXIST
		if(!file_exists(PLUGINS_PATH.'/destinations/background/'))
			mkdir(PLUGINS_PATH.'/destinations/background/');

		preg_match('#^data:image/\w+;base64,#i', $_POST['background_image'], $matches);
		if(!empty($matches))
		{
			while(true){
				$name = uniqid(rand(), true).'.jpg';
				if(!file_exists(PLUGINS_PATH.'/destinations/background/'.$name))
				break;
			}

			$background = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['background_image']));
			file_put_contents(PLUGINS_PATH.'/destinations/background/'.$name, $background);

			if(file_exists(PLUGINS_PATH.'/destinations/background/'.$name))
			{
				$filename = $name;
			}

			//-- Delete old image
			$old_bg = get_additional_field($post_id, 'background_image', 'destinations');
			if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/'.$old_bg))
				unlink(PLUGINS_PATH.'/destinations/background/'.$old_bg);
		}
		else
		{
			$filename = $_POST['background_image'];
		}

	    edit_additional_field($post_id, 'background_image', $filename, 'destinations');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton corporate image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_corporate_images($post_id)
{
	if(isset($_POST['corporate_image']))
	{
		//-- CHECK FOLDER EXIST
		if(!file_exists(PLUGINS_PATH.'/destinations/background/'))
			mkdir(PLUGINS_PATH.'/destinations/background/');

		preg_match('#^data:image/\w+;base64,#i', $_POST['corporate_image'], $matches);
		if(!empty($matches))
		{
			while(true){
				$name = uniqid(rand(), true).'.jpg';
				if(!file_exists(PLUGINS_PATH.'/destinations/background/'.$name))
				break;
			}

			$background = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['corporate_image']));
			file_put_contents(PLUGINS_PATH.'/destinations/background/'.$name, $background);

			if(file_exists(PLUGINS_PATH.'/destinations/background/'.$name))
			{
				$filename = $name;
			}

			//-- Delete old image
			$old_bg = get_additional_field($post_id, 'corporate_image', 'destinations');
			if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/'.$old_bg))
				unlink(PLUGINS_PATH.'/destinations/background/'.$old_bg);
		}
		else
		{
			$filename = $_POST['corporate_image'];
		}

	    edit_additional_field($post_id, 'corporate_image', $filename, 'destinations');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton information di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_informations($post_id)
{
	//-- Phone Number
	// $book_now_link = (isset($_POST['book_now_link']) ? $_POST['book_now_link'] : '');
	// edit_additional_field($post_id, 'book_now_link', $book_now_link, 'destinations');

  //-- Link Book Now
	$phone_number = (isset($_POST['phone_number']) ? $_POST['phone_number'] : '');
	edit_additional_field($post_id, 'phone_number', $phone_number, 'destinations');

	//-- Fax Number
	$fax_number = (isset($_POST['fax_number']) ? $_POST['fax_number'] : '');
	edit_additional_field($post_id, 'fax_number', $fax_number, 'destinations');

	//-- Restaurant Phone Number
	$restaurant_phone_number = (isset($_POST['restaurant_phone_number']) ? $_POST['restaurant_phone_number'] : '');
	edit_additional_field($post_id, 'restaurant_phone_number', $restaurant_phone_number, 'destinations');

	//-- Restaurant Fax Number
	$restaurant_fax_number = (isset($_POST['restaurant_fax_number']) ? $_POST['restaurant_fax_number'] : '');
	edit_additional_field($post_id, 'restaurant_fax_number', $restaurant_fax_number, 'destinations');

	//-- Reservation Phone Number
	$reservation_phone_number = (isset($_POST['reservation_phone_number']) ? $_POST['reservation_phone_number'] : '');
	edit_additional_field($post_id, 'reservation_phone_number', $reservation_phone_number, 'destinations');

	//-- Reservation Fax Number
	$reservation_fax_number = (isset($_POST['reservation_fax_number']) ? $_POST['reservation_fax_number'] : '');
	edit_additional_field($post_id, 'reservation_fax_number', $reservation_fax_number, 'destinations');

	//-- Facebook Link
	$fb_link = (isset($_POST['fb_link']) ? $_POST['fb_link'] : '');
	edit_additional_field($post_id, 'fb_link', $fb_link, 'destinations');

	//-- Twitter Link
	$twitter_link = (isset($_POST['twitter_link']) ? $_POST['twitter_link'] : '');

	edit_additional_field($post_id, 'twitter_link', $twitter_link, 'destinations');

	//-- Instagram Link
	$instagram_link = (isset($_POST['instagram_link']) ? $_POST['instagram_link'] : '');
	edit_additional_field($post_id, 'instagram_link', $instagram_link, 'destinations');

	//-- Tripadvisor Link
	$tripadvisor_link = (isset($_POST['tripadvisor_link']) ? $_POST['tripadvisor_link'] : '');
	edit_additional_field($post_id, 'tripadvisor_link', $tripadvisor_link, 'destinations');

	//-- Email Address
	$email_address = (isset($_POST['email_address']) ? $_POST['email_address'] : '');
	edit_additional_field($post_id, 'email_address', $email_address, 'destinations');

	//-- Restaurant Email Address
	$restaurant_email_address = (isset($_POST['restaurant_email_address']) ? $_POST['restaurant_email_address'] : '');
	edit_additional_field($post_id, 'restaurant_email_address', $restaurant_email_address, 'destinations');

	//-- Reservation Email Address
	$reservation_email_address = (isset($_POST['reservation_email_address']) ? $_POST['reservation_email_address'] : '');
	edit_additional_field($post_id, 'reservation_email_address', $reservation_email_address, 'destinations');

	//-- Wedding Events Email Address
	$we_email_address = (isset($_POST['we_email_address']) ? $_POST['we_email_address'] : '');
	edit_additional_field($post_id, 'we_email_address', $we_email_address, 'destinations');

	//-- Instagram Username
	$instagram_user = (isset($_POST['instagram_user']) ? $_POST['instagram_user'] : '');
	edit_additional_field($post_id, 'instagram_user', $instagram_user, 'destinations');

	//-- Position On Map
	$coordinate_location = (isset($_POST['coordinate_location']) ? $_POST['coordinate_location'] : '');
	edit_additional_field($post_id, 'coordinate_location', $coordinate_location, 'destinations');

	//-- Address
	$address = (isset($_POST['address']) ? $_POST['address'] : '');
	edit_additional_field($post_id, 'address', $address, 'destinations');

	//-- Goggle Analytic
	$g_analytic = (isset($_POST['g_analytic']) ? $_POST['g_analytic'] : '');
	edit_additional_field($post_id, 'g_analytic', $g_analytic, 'destinations');

	//-- Pixel
	$pixel_script = (isset($_POST['pixel_script']) ? $_POST['pixel_script'] : '');
	edit_additional_field($post_id, 'pixel_script', $pixel_script, 'destinations');

	//-- Hotel Code
	$hotel_code = (isset($_POST['hotel_code']) ? $_POST['hotel_code'] : '');
	edit_additional_field($post_id, 'hotel_code', $hotel_code, 'destinations');

	//-- Chain Code
	$chain_code = (isset($_POST['chain_code']) ? $_POST['chain_code'] : '');
	edit_additional_field($post_id, 'chain_code', $chain_code, 'destinations');

	//-- Template Code
	$template_code = (isset($_POST['template_code']) ? $_POST['template_code'] : '');
	edit_additional_field($post_id, 'template_code', $template_code, 'destinations');

	//-- Shell Code
	$shell_code = (isset($_POST['shell_code']) ? $_POST['shell_code'] : '');
	edit_additional_field($post_id, 'shell_code', $shell_code, 'destinations');

	//-- Meta Title
	$meta_title = (isset($_POST['meta_title']) ? $_POST['meta_title'] : '');
	edit_additional_field($post_id, 'meta_title', $meta_title, 'destinations');

	//-- Meta Desc
	$meta_description = (isset($_POST['meta_description']) ? $_POST['meta_description'] : '');
	edit_additional_field($post_id, 'meta_description', $meta_description, 'destinations');

	//-- Meta keywords
	$meta_keywords = (isset($_POST['meta_keywords']) ? $_POST['meta_keywords'] : '');
	edit_additional_field($post_id, 'meta_keywords', $meta_keywords, 'destinations');

	//-- Facebook Name
	$fb_page_name = (isset($_POST['fb_page_name']) ? $_POST['fb_page_name'] : '');
	edit_additional_field($post_id, 'fb_page_name', $fb_page_name, 'destinations');

	//-- Twitter Username
	$twitter_username = (isset($_POST['twitter_username']) ? $_POST['twitter_username'] : '');
	edit_additional_field($post_id, 'twitter_username', $twitter_username, 'destinations');

	//-- Instagram Username
	$instagram_username = (isset($_POST['instagram_username']) ? $_POST['instagram_username'] : '');
	edit_additional_field($post_id, 'instagram_username', $instagram_username, 'destinations');

	//-- Tripadvisor Name
	$tripadvisor_name = (isset($_POST['tripadvisor_name']) ? $_POST['tripadvisor_name'] : '');
	edit_additional_field($post_id, 'tripadvisor_name', $tripadvisor_name, 'destinations');

	//-- Skype Name
	$skype_name = (isset($_POST['skype_name']) ? $_POST['skype_name'] : '');
	edit_additional_field($post_id, 'skype_name', $skype_name, 'destinations');

	//-- Location Map Title
	$location_map_title = (isset($_POST['location_map_title']) ? $_POST['location_map_title'] : '');
	edit_additional_field($post_id, 'location_map_title', $location_map_title, 'destinations');

	//-- Location Map Description
	$location_map_desc = (isset($_POST['location_map_desc']) ? $_POST['location_map_desc'] : '');
	edit_additional_field($post_id, 'location_map_desc', $location_map_desc, 'destinations');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton featured di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_featured($post_id)
{
	if(isset($_POST['featured']))
	{
		$featured = array_values($_POST['featured']);
		$value = json_encode($featured);
        edit_additional_field($post_id, 'featured', $value, 'destinations');
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton slide image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_slide_images($post_id)
{
	if(isset($_POST['slide_image']))
	{
		//-- CHECK FOLDER EXIST
		if(!file_exists(PLUGINS_PATH.'/destinations/slider/'))
			mkdir(PLUGINS_PATH.'/destinations/slider/');
		if(!file_exists(PLUGINS_PATH.'/destinations/slider/large/'))
			mkdir(PLUGINS_PATH.'/destinations/slider/large/');
		if(!file_exists(PLUGINS_PATH.'/destinations/slider/thumbnail/'))
			mkdir(PLUGINS_PATH.'/destinations/slider/thumbnail/');

		$file_arr  = array();
		$title_arr = array();
		$desc_arr  = array();

		$old_slide_arr = get_additional_field($post_id, 'slide_image', 'destinations');
		$old_slide_arr = json_decode($old_slide_arr,true);

		foreach($_POST['slide_image'] as $i=>$slide)
		{
			preg_match('#^data:image/\w+;base64,#i', $slide, $matches);
			if(!empty($matches))
			{
				while(true){
					$name = uniqid(rand(), true).'.jpg';
					if(!file_exists(PLUGINS_PATH.'/destinations/slider/'.$name))
					break;
				}

				$slide = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $slide));
				file_put_contents(PLUGINS_PATH.'/destinations/slider/'.$name, $slide);

				if(file_exists(PLUGINS_PATH.'/destinations/slider/'.$name))
				{
					$file_arr[]  = $name;
					$title_arr[] = $_POST['slide_title'][$i];
					$desc_arr[]  = $_POST['slide_description'][$i];

					$file_source = PLUGINS_PATH.'/destinations/slider/'.$name;
					$destination = PLUGINS_PATH.'/destinations/slider/large/'.$name;
					$destination_thumb = PLUGINS_PATH.'/destinations/slider/thumbnail/'.$name;

					$img = new SmartImage($file_source);
					$img->resize(1000, 1000);
					$img->saveImage($destination, 80);

					$img->resize(200, 200, true);
					$img->saveImage($destination_thumb, 80);
					$img->close();
				}

				//-- Delete old image
				if(isset($old_slide_arr[$i]))
				{
					if(file_exists(PLUGINS_PATH.'/destinations/slider/'.$old_slide_arr[$i]))
						unlink(PLUGINS_PATH.'/destinations/slider/'.$old_slide_arr[$i]);
					if(file_exists(PLUGINS_PATH.'/destinations/slider/large/'.$old_slide_arr[$i]))
						unlink(PLUGINS_PATH.'/destinations/slider/large/'.$old_slide_arr[$i]);
					if(file_exists(PLUGINS_PATH.'/destinations/slider/thumbnail/'.$old_slide_arr[$i]))
						unlink(PLUGINS_PATH.'/destinations/slider/thumbnail/'.$old_slide_arr[$i]);
				}
			}
			else
			{
				$file_arr[]  = $slide;
				$title_arr[] = $_POST['slide_title'][$i];
				$desc_arr[]  = $_POST['slide_description'][$i];
			}
		}

		$file = (empty($file_arr) ? '' : json_encode($file_arr));
	    edit_additional_field($post_id, 'slide_image', $file, 'destinations');


		$title = (empty($title_arr) ? '' : json_encode($title_arr));
	    edit_additional_field($post_id, 'slide_title', $title, 'destinations');

		$desc = (empty($desc_arr) ? '' :  json_encode($desc_arr));
	    edit_additional_field($post_id, 'slide_description', $desc, 'destinations');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton location image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_location_images($post_id)
{
	if(isset($_POST['location_image']))
	{
		//-- CHECK FOLDER EXIST
		if(!file_exists(PLUGINS_PATH.'/destinations/location/'))
			mkdir(PLUGINS_PATH.'/destinations/location/');
		if(!file_exists(PLUGINS_PATH.'/destinations/location/large/'))
			mkdir(PLUGINS_PATH.'/destinations/location/large/');
		if(!file_exists(PLUGINS_PATH.'/destinations/location/thumbnail/'))
			mkdir(PLUGINS_PATH.'/destinations/location/thumbnail/');

		$file_arr  = array();
		$title_arr = array();
		$desc_arr  = array();

		$old_loc_arr = get_additional_field($post_id, 'location_image', 'destinations');
		$old_loc_arr = json_decode($old_loc_arr,true);

		foreach($_POST['location_image'] as $i=>$slide)
		{
			preg_match('#^data:image/\w+;base64,#i', $slide, $matches);
			if(!empty($matches))
			{
				while(true){
					$name = uniqid(rand(), true).'.jpg';
					if(!file_exists(PLUGINS_PATH.'/destinations/location/'.$name))
					break;
				}

				$slide = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $slide));
				file_put_contents(PLUGINS_PATH.'/destinations/location/'.$name, $slide);

				if(file_exists(PLUGINS_PATH.'/destinations/location/'.$name))
				{
					$file_arr[]  = $name;
					$title_arr[] = $_POST['location_title'][$i];
					$desc_arr[]  = $_POST['location_description'][$i];

					$file_source = PLUGINS_PATH.'/destinations/location/'.$name;
					$destination = PLUGINS_PATH.'/destinations/location/large/'.$name;
					$destination_thumb = PLUGINS_PATH.'/destinations/location/thumbnail/'.$name;

					$img = new SmartImage($file_source);
					$img->resize(1000, 1000);
					$img->saveImage($destination, 80);

					$img->resize(200, 200, true);
					$img->saveImage($destination_thumb, 80);
					$img->close();
				}

				//-- Delete old image
				if(isset($old_loc_arr[$i]))
				{
					if(file_exists(PLUGINS_PATH.'/destinations/location/'.$old_loc_arr[$i]))
						unlink(PLUGINS_PATH.'/destinations/location/'.$old_loc_arr[$i]);
					if(file_exists(PLUGINS_PATH.'/destinations/location/large/'.$old_loc_arr[$i]))
						unlink(PLUGINS_PATH.'/destinations/location/large/'.$old_loc_arr[$i]);
					if(file_exists(PLUGINS_PATH.'/destinations/location/thumbnail/'.$old_loc_arr[$i]))
						unlink(PLUGINS_PATH.'/destinations/location/thumbnail/'.$old_loc_arr[$i]);
				}
			}
			else
			{
				$file_arr[]  = $slide;
				$title_arr[] = $_POST['location_title'][$i];
				$desc_arr[]  = $_POST['location_description'][$i];
			}
		}

		$file = (empty($file_arr) ? '' : json_encode($file_arr));
	    edit_additional_field($post_id, 'location_image', $file, 'destinations');

		$title = (empty($title_arr) ? '' : json_encode($title_arr));
	    edit_additional_field($post_id, 'location_title', $title, 'destinations');

		$desc = (empty($desc_arr) ? '' :  json_encode($desc_arr));
	    edit_additional_field($post_id, 'location_description', $desc, 'destinations');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk reorder position slide
| -------------------------------------------------------------------------------------------------------------------------
*/
function reorder_slide_image($post_id)
{
	if(isset($_POST['slide_image']))
	{
		$file_arr  = array();
		$title_arr = array();
		$desc_arr  = array();

		foreach($_POST['slide_image'] as $i=>$slide)
		{
			preg_match('#^data:image/\w+;base64,#i', $slide, $matches);
			if(empty($matches))
			{
				$file_arr[]  = $slide;
				$title_arr[] = $_POST['slide_title'][$i];
				$desc_arr[]  = $_POST['slide_description'][$i];
			}
		}

		$file = (empty($file_arr) ? '' : json_encode($file_arr));
	    edit_additional_field($post_id, 'slide_image', $file, 'destinations');

		$title = (empty($title_arr) ? '' : json_encode($title_arr));
	    edit_additional_field($post_id, 'slide_title', $title, 'destinations');

		$desc = (empty($desc_arr) ? '' :  json_encode($desc_arr));
	    edit_additional_field($post_id, 'slide_description', $desc, 'destinations');
	}
}

function reorder_loc_slide_image($post_id)
{
	if(isset($_POST['location_image']))
	{
		$file_arr  = array();
		$title_arr = array();
		$desc_arr  = array();

		foreach($_POST['location_image'] as $i=>$slide)
		{
			preg_match('#^data:image/\w+;base64,#i', $slide, $matches);
			if(empty($matches))
			{
				$file_arr[]  = $slide;
				$title_arr[] = $_POST['location_title'][$i];
				$desc_arr[]  = $_POST['location_description'][$i];
			}
		}

		$file = (empty($file_arr) ? '' : json_encode($file_arr));
	    edit_additional_field($post_id, 'location_image', $file, 'destinations');

		$title = (empty($title_arr) ? '' : json_encode($title_arr));
	    edit_additional_field($post_id, 'location_title', $title, 'destinations');

		$desc = (empty($desc_arr) ? '' :  json_encode($desc_arr));
	    edit_additional_field($post_id, 'location_description', $desc, 'destinations');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus background image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_background_image($post_id='',$bg_name='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'background_image','destinations');
	if($bg_image==$bg_name)
	{
		if(file_exists(PLUGINS_PATH.'/destinations/background/'.$bg_image))
			unlink(PLUGINS_PATH.'/destinations/background/'.$bg_image);
	}

	if(edit_additional_field($post_id, 'background_image', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus corporate image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_corporate_image($post_id='',$cp_name='')
{
	if(empty($post_id)) return true;

	$cp_image = get_additional_field($post_id,'corporate_image','destinations');
	if($cp_image==$cp_name)
	{
		if(file_exists(PLUGINS_PATH.'/destinations/background/'.$cp_image))
			unlink(PLUGINS_PATH.'/destinations/background/'.$cp_image);
	}

	if(edit_additional_field($post_id, 'corporate_image', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus slide image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_slide_image($post_id='',$slide_name='')
{
	if(empty($post_id)) return true;

	$slide_image = get_additional_field($post_id,'slide_image','destinations');
	$slide_title = get_additional_field($post_id,'slide_title','destinations');
	$slide_description = get_additional_field($post_id,'slide_description','destinations');

	$slide = json_decode($slide_image,true);
	$title = json_decode($slide_title,true);
	$desc  = json_decode($slide_description,true);

	foreach($slide as $key=>$obj)
	{
		if($obj==$slide_name)
		{
			if(file_exists(PLUGINS_PATH.'/destinations/slider/thumbnail/'.$obj))
				unlink(PLUGINS_PATH.'/destinations/slider/thumbnail/'.$obj);
			if(file_exists(PLUGINS_PATH.'/destinations/slider/large/'.$obj))
				unlink(PLUGINS_PATH.'/destinations/slider/large/'.$obj);
			if(file_exists(PLUGINS_PATH.'/destinations/slider/'.$obj))
				unlink(PLUGINS_PATH.'/destinations/slider/'.$obj);

			unset($slide[$key]);
			unset($title[$key]);
			unset($desc[$key]);
			break;
		}
	}

	$new_slide = (empty($slide) ? '' : json_encode(array_values($slide)));
	$new_title = (empty($title) ? '' : json_encode(array_values($title)));
	$new_desc  = (empty($desc) ? '' : json_encode(array_values($desc)));

	if(edit_additional_field($post_id, 'slide_image', $new_slide, 'destinations'))
	{
		edit_additional_field($post_id, 'slide_title', $new_title, 'destinations');
		edit_additional_field($post_id, 'slide_description', $new_desc, 'destinations');
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus location image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_location_image($post_id='',$loc_name='')
{
	if(empty($post_id)) return true;

	$location_image = get_additional_field($post_id,'location_image','destinations');
	$location_description = get_additional_field($post_id,'location_description','destinations');

	$loc  = json_decode($location_image,true);
	$desc = json_decode($location_description,true);

	foreach($loc as $key=>$obj)
	{
		if($obj==$loc_name)
		{
			if(file_exists(PLUGINS_PATH.'/destinations/location/thumbnail/'.$obj))
				unlink(PLUGINS_PATH.'/destinations/location/thumbnail/'.$obj);
			if(file_exists(PLUGINS_PATH.'/destinations/location/large/'.$obj))
				unlink(PLUGINS_PATH.'/destinations/location/large/'.$obj);
			if(file_exists(PLUGINS_PATH.'/destinations/location/'.$obj))
				unlink(PLUGINS_PATH.'/destinations/location/'.$obj);

			unset($loc[$key]);
			unset($desc[$key]);
			break;
		}
	}

	$new_loc  = (empty($loc) ? '' : json_encode(array_values($loc)));
	$new_desc = (empty($desc) ? '' : json_encode(array_values($desc)));

	if(edit_additional_field($post_id, 'location_image', $new_loc, 'destinations'))
	{
		edit_additional_field($post_id, 'location_description', $new_desc, 'destinations');
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan background image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_background($post_id='')
{
	$result   = '';
	$bg_image = get_additional_field($post_id,'background_image','destinations');

	if(!empty($bg_image))
	{
		$result .= '
        <div class="box">
            <img src="http://'.SITE_URL.'/lumonata-plugins/destinations/background/'.$bg_image.'" />
            <input type="hidden" name="background_image" value="'.$bg_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" data-bg-name="'.$bg_image.'" class="bg-delete-header">
                    <img src="http://'.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan corporate image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_corporate($post_id='')
{
	$result   = '';
	$bg_image = get_additional_field($post_id,'corporate_image','destinations');

	if(!empty($bg_image))
	{
		$result .= '
        <div class="box">
            <img src="http://'.SITE_URL.'/lumonata-plugins/destinations/background/'.$bg_image.'" />
            <input type="hidden" name="corporate_image" value="'.$bg_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" data-cp-name="'.$bg_image.'" class="cp-delete-header">
                    <img src="http://'.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan list slide image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_slide_list($post_id='')
{
	$result = '';

	$slide_image = get_additional_field($post_id,'slide_image','destinations');
	$slide_title = get_additional_field($post_id,'slide_title','destinations');
	$slide_description = get_additional_field($post_id,'slide_description','destinations');

	$slide = json_decode($slide_image,true);
	$title = json_decode($slide_title,true);
	$desc  = json_decode($slide_description,true);

	if(!empty($slide) && is_array($slide))
	{
		foreach($slide as $i=>$obj)
		{
			$result .= '
			<li id="list_'.$i.'" class="clearfixed">
				<div class="list-box">
					<div class="inner clearfixed">
			            <div class="box" style="background-image:url(http://'.SITE_URL.'/lumonata-plugins/destinations/slider/'.$obj.')">
			                <input class="s-image" type="hidden" name="slide_image['.$i.']" value="'.$obj.'" />
			                <div class="overlay" style="display: none;">
			                    <a data-post-id="'.$post_id.'" data-slide-name="'.$obj.'" class="delete-header">
			                        <img src="http://'.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
			                    </a>
			                </div>
			            </div>
			            <div class="field">
			            	<input class="textbox s-title" name="slide_title['.$i.']" type="text" value="'.(isset($title[$i]) ? $title[$i] : '').'" placeholder="Title" />
			            	<textarea class="textbox s-desc" name="slide_description['.$i.']" placeholder="Description">'.(isset($desc[$i]) ? $desc[$i] : '').'</textarea>
			            </div>
			        </div>
		        </div>
		    </li>';
		}
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan list location image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_location_list($post_id='')
{
	$result = '';

	$location_image = get_additional_field($post_id,'location_image','destinations');
	$location_title = get_additional_field($post_id,'location_title','destinations');
	$location_description = get_additional_field($post_id,'location_description','destinations');

	$loc  = json_decode($location_image,true);
	$title = json_decode($location_title,true);
	$desc = json_decode($location_description,true);

	if(!empty($loc) && is_array($loc))
	{
		foreach($loc as $i=>$obj)
		{
			$result .= '
			<li id="loclist_'.$i.'" class="clearfixed">
				<div class="list-box">
					<div class="inner clearfixed">
			            <div class="box" style="background-image:url(http://'.SITE_URL.'/lumonata-plugins/destinations/location/'.$obj.')">
			                <input class="l-image" type="hidden" name="location_image['.$i.']" value="'.$obj.'" />
			                <div class="overlay" style="display: none;">
			                    <a data-post-id="'.$post_id.'" data-location-name="'.$obj.'" class="loc-delete-header">
			                        <img src="http://'.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
			                    </a>
			                </div>
			            </div>
			            <div class="field">
			            	<input class="textbox l-title" name="location_title['.$i.']" type="text" value="'.(isset($title[$i]) ? $title[$i] : '').'" placeholder="Title" />
			            	<textarea class="textbox l-desc" name="location_description['.$i.']" placeholder="Description">'.(isset($desc[$i]) ? $desc[$i] : '').'</textarea>
			            </div>
			        </div>
		        </div>
		    </li>';
		}
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi-fungsi lainnya
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_background_image($post_id)
{
	$result   = '';
	$filename = get_additional_field($post_id,'background_image','destinations');
	if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/'.$filename))
	{
		$result = 'http://'.SITE_URL.'/lumonata-plugins/destinations/background/'.$filename;
	}

	return $result;
}

function get_corporate_image($post_id)
{
	$result   = '';
	$filename = get_additional_field($post_id,'corporate_image','destinations');
	if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/'.$filename))
	{
		$result = 'http://'.SITE_URL.'/lumonata-plugins/destinations/background/'.$filename;
	}

	return $result;
}

function get_position_on_map($post_id)
{
	$coordinate = get_additional_field($post_id,'coordinate_location','destinations');
	return $coordinate;
}

function get_address($post_id)
{
	$address = get_additional_field($post_id,'address','destinations');
	return nl2br($address);
}

function get_g_analytic($post_id)
{
	$g_analytic = get_additional_field($post_id,'g_analytic','destinations');
	return $g_analytic;
}

function get_pixel_script($post_id)
{
	$pixel_script = get_additional_field($post_id,'pixel_script','destinations');
	return str_replace('&amp;', '&', $pixel_script);
}

function get_booking_link($post_id)
{
	$booking_link = get_additional_field($post_id,'booking_link','destinations');
	return $booking_link;
}

function get_hotel_code($post_id)
{
	$hotel_code = get_additional_field($post_id,'hotel_code','destinations');
	return $hotel_code;
}

function get_template_code($post_id)
{
	$template_code = get_additional_field($post_id,'template_code','destinations');
	return $template_code;
}

function get_shell_code($post_id)
{
	$shell_code = get_additional_field($post_id,'shell_code','destinations');
	return $shell_code;
}

function get_chain_code($post_id)
{
	$chain_code = get_additional_field($post_id,'chain_code','destinations');
	return $chain_code;
}

function get_phone_number($post_id)
{
	$phone_number = get_additional_field($post_id,'phone_number','destinations');
	return $phone_number;
}

// function get_book_now_link($post_id)
// {
// 	$book_now_link = get_additional_field($post_id,'book_now_link','destinations');
// 	return $book_now_link;
// }

function get_fax_number($post_id)
{
	$fax_number = get_additional_field($post_id,'fax_number','destinations');
	return $fax_number;
}

function get_restaurant_phone_number($post_id)
{
	$restaurant_phone_number = get_additional_field($post_id,'restaurant_phone_number','destinations');
	return $restaurant_phone_number;
}

function get_restaurant_fax_number($post_id)
{
	$restaurant_fax_number = get_additional_field($post_id,'restaurant_fax_number','destinations');
	return $restaurant_fax_number;
}

function get_reservation_phone_number($post_id)
{
	$reservation_phone_number = get_additional_field($post_id,'reservation_phone_number','destinations');
	return $reservation_phone_number;
}

function get_reservation_fax_number($post_id)
{
	$reservation_fax_number = get_additional_field($post_id,'reservation_fax_number','destinations');
	return $reservation_fax_number;
}

function get_email_address($post_id)
{
	$email_address = get_additional_field($post_id,'email_address','destinations');
	return $email_address;
}

function get_restaurant_email_address($post_id)
{
	$restaurant_email_address = get_additional_field($post_id,'restaurant_email_address','destinations');
	return $restaurant_email_address;
}

function get_reservation_email_address($post_id)
{
	$reservation_email_address = get_additional_field($post_id,'reservation_email_address','destinations');
	return $reservation_email_address;
}

function get_we_email_address($post_id)
{
	$we_email_address = get_additional_field($post_id,'we_email_address','destinations');
	return $we_email_address;
}

function get_fb_link($post_id)
{
	$fb_link = get_additional_field($post_id,'fb_link','destinations');
	return $fb_link;
}

function get_instagram_link($post_id)
{
	$instagram_link = get_additional_field($post_id,'instagram_link','destinations');
	return $instagram_link;
}

function get_twitter_link($post_id)
{
	$twitter_link = get_additional_field($post_id,'twitter_link','destinations');
	return $twitter_link;
}

function get_tripadvisor_link($post_id)
{
	$tripadvisor_link = get_additional_field($post_id,'tripadvisor_link','destinations');
	return $tripadvisor_link;
}

function get_instagram_user($post_id)
{
	$instagram_user = get_additional_field($post_id,'instagram_user','destinations');
	return $instagram_user;
}

function get_fb_page_name($post_id)
{
	$fb_page_name = get_additional_field($post_id,'fb_page_name','destinations');
	return $fb_page_name;
}

function get_instagram_username($post_id)
{
	$instagram_username = get_additional_field($post_id,'instagram_username','destinations');
	return $instagram_username;
}

function get_twitter_username($post_id)
{
	$twitter_username = get_additional_field($post_id,'twitter_username','destinations');
	return $twitter_username;
}

function get_tripadvisor_name($post_id)
{
	$tripadvisor_name = get_additional_field($post_id,'tripadvisor_name','destinations');
	return $tripadvisor_name;
}

function get_skype_name($post_id)
{
	$skype_name = get_additional_field($post_id,'skype_name','destinations');
	return $skype_name;
}

function get_location_map_title($post_id)
{
	$location_map_title = get_additional_field($post_id,'location_map_title','destinations');
	return $location_map_title;
}

function get_location_map_desc($post_id)
{
	$location_map_desc = get_additional_field($post_id,'location_map_desc','destinations');
	return $location_map_desc;
}

function get_slide_list($post_id)
{
	$images = get_additional_field($post_id,'slide_image','destinations');
	$title  = get_additional_field($post_id,'slide_title','destinations');
	$desc   = get_additional_field($post_id,'slide_description','destinations');

	$images_arr = json_decode($images,true);
	$title_arr  = json_decode($title,true);
	$desc_arr   = json_decode($desc,true);

	$result = array();
	if(!empty($images_arr) && is_array($images_arr))
	{
		foreach($images_arr as $i=>$obj)
		{
			$result[$i] = array(
				'image_thumb'=>'http://'.SITE_URL.'/lumonata-plugins/destinations/slider/thumbnail/'.$obj,
				'image'=>'http://'.SITE_URL.'/lumonata-plugins/destinations/slider/large/'.$obj,
				'title'=>$title_arr[$i],
				'desc'=>$desc_arr[$i]
			);
		}
	}

	return $result;
}

function get_location_list($post_id)
{
	$locs  = get_additional_field($post_id,'location_image','destinations');
	$title = get_additional_field($post_id,'location_title','destinations');
	$desc  = get_additional_field($post_id,'location_description','destinations');

	$locs_arr  = json_decode($locs,true);
	$title_arr = json_decode($title,true);
	$desc_arr  = json_decode($desc,true);

	$result = array();
	if(!empty($locs_arr) && is_array($locs_arr))
	{
		foreach($locs_arr as $i=>$obj)
		{
			$result[$i] = array(
				'image_thumb'=>'http://'.SITE_URL.'/lumonata-plugins/destinations/location/thumbnail/'.$obj,
				'image'=>'http://'.SITE_URL.'/lumonata-plugins/destinations/location/large/'.$obj,
				'title'=>$title_arr[$i],
				'desc'=>$desc_arr[$i]
			);
		}
	}

	return $result;
}

// function get_instagram_photo($post_id)
// {
// 	require_once(ROOT_PATH.'/lumonata-admin/includes/instagram-api-php/src/InstagramException.php');
// 	require_once(ROOT_PATH.'/lumonata-admin/includes/instagram-api-php/src/Instagram.php');
//
// 	$username   = get_instagram_user($post_id);
// 	$app_id     = get_meta_data('ig_client_id','global_setting');
// 	$app_secret = get_meta_data('ig_client_secret','global_setting');
// 	$app_token  = get_meta_data('ig_token','global_setting');
//
// 	if(!empty($app_id) && !empty($app_secret))
// 	{
// 		$instagram = new MetzWeb\Instagram\Instagram($app_id);
//
// 		$instagram->setAccessToken($app_token);
// 		$users = $instagram->searchAuthUser($username);
//
// 		$list = '';
// 	    if(isset($users->data[0]->id))
// 	    {
// 	    	$userid = $users->data[0]->id;
// 	    	$media  = $instagram->getUserMedia($userid, 8);
//
// 	    	if(isset($media->data) && !empty($media->data))
// 	        {
// 	            foreach($media->data as $i=>$obj)
// 	            {
// 	            	$imgs  = $obj->images->standard_resolution->url;
// 	                $list .= '
// 	                <div class="box">
// 	                    <a href="'.$obj->link.'" target="_blank" rel="instagram">
// 	                        <img class="b-lazy" src="'.$imgs.'" data-src="'.$imgs.'"/>
// 	                    </a>
// 	                </div>';
//
// 	                if($i==7) break;
// 	            }
// 	        }
// 	    }
//
//     	return $list;
// 	}
// }

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menjalankan ajax request
| -------------------------------------------------------------------------------------------------------------------------
*/
function execute_ajax_request()
{
	global $db;

	if($_POST['ajax_key'] =='search-destination')
	{
	    $result = '';

		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s';
	    $q = $db->prepare_query($s,'destinations',"%".$_POST['search_val']."%");
	   	$r = $db->do_query($q);
		if($db->num_rows($r) > 0)
	    {
			while($d=$db->fetch_array($r))
			{
				$result .= '
                <div class="list_item boxs clearfix" id="theitem_'.$d['larticle_id'].'">
                    <div class="boxs title push-left">
                        <input type="checkbox" name="select[]" class="title_checkbox select" value="'.$d['larticle_id'].'" />
                        <label>'.$d['larticle_title'].'</label>
                    </div>
                    <div class="boxs description push-left"><p>'.limit_words(strip_tags($d['larticle_content']), 50).'</p></div>
                    <div class="the_navigation_list">
                        <div class="list_navigation" style="display:none;" id="the_navigation_'.$d['larticle_id'].'">
                            <a href="'.get_state_url('destinations').'&prc=edit&id='.$d['larticle_id'].'">Edit</a> |
                            <a href="javascript:;" class="delete_link" id="'.$d['larticle_id'].'" rel="delete_'.$d['larticle_id'].'">Delete</a>
                        </div>
                    </div>
                </div>';
			}
        }
		else
		{
			$result = '
			<div class="alert_yellow_form">
				No result found for <em>"'.$_POST['search_val'].'"</em>.
				Check your spellling or try another terms
			</div>';
        }

		echo $result;
	}

	if($_POST['ajax_key'] =='delete-destination')
	{
		if(delete_destinations($_POST['dest_id']))
		{
			echo '{"result":"success"}';
			exit;
		}
		else
		{
			echo '{"result":"failed"}';
			exit;
		}
	}

	if($_POST['ajax_key'] =='delete-slide-image')
	{
		if(delete_slide_image($_POST['post_id'], $_POST['slide_name']))
		{
			echo '{"result":"success"}';
			exit;
		}
		else
		{
			echo '{"result":"failed"}';
			exit;
		}
	}

	if($_POST['ajax_key'] =='delete-location-image')
	{
		if(delete_location_image($_POST['post_id'], $_POST['loc_name']))
		{
			echo '{"result":"success"}';
			exit;
		}
		else
		{
			echo '{"result":"failed"}';
			exit;
		}
	}

	if($_POST['ajax_key'] =='delete-background-image')
	{
		if(delete_background_image($_POST['post_id'], $_POST['bg_name']))
		{
			echo '{"result":"success"}';
			exit;
		}
		else
		{
			echo '{"result":"failed"}';
			exit;
		}
	}

	if($_POST['ajax_key'] =='delete-corporate-image')
	{
		if(delete_corporate_image($_POST['post_id'], $_POST['cp_name']))
		{
			echo '{"result":"success"}';
			exit;
		}
		else
		{
			echo '{"result":"failed"}';
			exit;
		}
	}

	if($_POST['ajax_key'] =='reorder-slide-image')
	{
		reorder_slide_image($_POST['post_id']);
	}

	if($_POST['ajax_key'] =='reorder-loc-slide-image')
	{
		reorder_loc_slide_image($_POST['post_id']);
	}

	exit;
}

?>
