<?php
/*
    Plugin Name: Post Video
   	Plugin URL: http://lumonata.com/
    Description: -
    Author: Ngurah Rai
    Author URL: http://lumonata.com/
    Version: 1.0.1
    
*/

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan galleri video di masing-masing post type
| -------------------------------------------------------------------------------------------------------------------------
*/
$apps_array = array('gallery');

foreach( $apps_array as $apps )
{
	if( is_edit_all() && !(is_save_draft() || is_publish()) )
	{
	    foreach( $_POST['select'] as $index=>$post_id )
		{
			add_actions($apps.'_additional_filed_'.$index, 'gallery_video');
	    }
	}
	else
	{
		add_actions($apps.'_additional_filed', 'gallery_video');
	}
}


function gallery_video()
{
	global $thepost;
	
	$index    = $thepost->post_index;
    $post_id  = $thepost->post_id;
	$app_name = ($_GET['state']=='applications' ? $_GET['sub'] : $_GET['state']);

	add_actions('admin_tail','get_javascript','jquery-ui-1.8.2.custom.min');
	add_actions('admin_tail','get_javascript','jquery.ui.nestedSortable.js');
	
	set_template(PLUGINS_PATH."/post-video/post-video-form.html",'post_video_template_'.$index);
	add_block('post_video','pg','post_video_template_'.$index);
	
	add_variable('iii',$index);
	add_variable('site_url',SITE_URL);
	add_variable('post_id',$post_id);
	add_variable('app_name', $app_name);
	add_variable('admin_url',SITE_URL.'/lumonata-admin');
	add_variable('ajax_url',SITE_URL.'/video-ajax/');
	add_variable('video_list', get_gallery_video($post_id, $app_name));
	
	parse_template('post_video','pg',false);
	$templates = return_template('post_video_template_'.$index);	
	return $templates;
}

function get_gallery_video( $post_id, $app_name )
{
	$video = get_additional_field( $post_id, 'gallery_video', $app_name, false);

	if( !empty($video) )
	{
		$video   = json_decode( $video, true );
		$content = '' ;

		foreach( $video as $key=>$obj )
		{
			$content .= '
			<li id="item_'.$key.'" class="item">
				<div class="video-item">
					<span class="overlay">
						<span class="actions">
							<a rel="'.$key.'">DELETE</a>
						</span>
					</span>
					'.$obj['content'].'
				</div>
			</li>';
		}

		return $content;
	}
}

function set_gallery_video( $post )
{
	$video = get_additional_field( $post['post_id'], 'gallery_video', $post['app_name'], false);
	$url   = '';

	if( $post['type']==1 )
	{
		$url = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","//www.youtube.com/embed/$1", $post['content']);
		$post['content'] = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe src=\"".$url."\" frameborder=\"0\" allowfullscreen></iframe>", $post['content']);
	}

	if( !empty($video) )
	{
		$video   = json_decode( $video );
		$video[] = array('type'=>$post['type'], 'content'=>$post['content'], 'url'=>$url);
	}
	else
	{
		$video[] = array('type'=>$post['type'], 'content'=>$post['content'], 'url'=>$url);
	}

	if( edit_additional_field( $post['post_id'], 'gallery_video', json_encode($video), $post['app_name'], false) )
	{
		$prm['result'] = 'success';
		$prm['data']   = $video;

		return json_encode($prm);
	}
	else
	{
		return '{"result":"failed"}';
	}
}

function delete_gallery_video( $post )
{
	$index = $post['index'];
	$video = get_additional_field( $post['post_id'], 'gallery_video', $post['app_name'], false);

	if( !empty($video) )
	{
		$video = json_decode( $video, true );

		if( isset($video[$index]) )
		{
			unset($video[$index]);

			$video = array_values($video);

			if( edit_additional_field( $post['post_id'], 'gallery_video', json_encode($video), $post['app_name'], false) )
			{
				$new_video = get_additional_field( $post['post_id'], 'gallery_video', $post['app_name'], false);
				$new_video = json_decode($new_video, true);

				$prm['result'] = 'success';
				$prm['data']   = $new_video;

				return json_encode($prm);
			}
			else
			{
				return '{"result":"failed"}';
			}
		}
		else
		{
			return '{"result":"failed"}';
		}
	}
	else
	{
		return '{"result":"failed"}';
	}
}

function execute_video_ajax_request()
{
	global $db;
	
	if( isset($_POST['ajax_key']) && $_POST['ajax_key']=='add-gallery-video' )
	{
		echo set_gallery_video($_POST);
	}
	
	if( isset($_POST['ajax_key']) && $_POST['ajax_key']=='delete-gallery-video' )
	{
		echo delete_gallery_video($_POST);
	}

	exit;
}
add_actions('video-ajax_page','execute_video_ajax_request');