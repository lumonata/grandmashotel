/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : maya

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2016-03-31 10:05:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lumonata_additional_fields
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_additional_fields`;
CREATE TABLE `lumonata_additional_fields` (
  `lapp_id` bigint(20) NOT NULL,
  `lkey` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lvalue` text CHARACTER SET utf8 NOT NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`lapp_id`,`lkey`),
  KEY `key` (`lkey`),
  KEY `app_name` (`lapp_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_additional_fields
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_articles
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_articles`;
CREATE TABLE `lumonata_articles` (
  `larticle_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_title` text CHARACTER SET utf8 NOT NULL,
  `larticle_brief` text CHARACTER SET utf8 NOT NULL,
  `larticle_content` longtext CHARACTER SET utf8 NOT NULL,
  `larticle_status` varchar(20) CHARACTER SET utf8 NOT NULL,
  `larticle_type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lcomment_count` bigint(20) NOT NULL,
  `lcount_like` bigint(20) NOT NULL,
  `lsef` text CHARACTER SET utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL DEFAULT '1',
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` bigint(20) NOT NULL,
  `ldlu` datetime NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY (`larticle_id`),
  KEY `article_title` (`larticle_title`(255)),
  KEY `type_status_date_by` (`larticle_type`,`larticle_status`,`lpost_date`,`lpost_by`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_articles
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_attachment
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_attachment`;
CREATE TABLE `lumonata_attachment` (
  `lattach_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_id` bigint(20) NOT NULL,
  `lattach_loc` text CHARACTER SET utf8 NOT NULL,
  `lattach_loc_thumb` text CHARACTER SET utf8 NOT NULL,
  `lattach_loc_medium` text CHARACTER SET utf8 NOT NULL,
  `lattach_loc_large` text CHARACTER SET utf8 NOT NULL,
  `ltitle` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lalt_text` text CHARACTER SET utf8 NOT NULL,
  `lcaption` varchar(200) CHARACTER SET utf8 NOT NULL,
  `mime_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL DEFAULT '0',
  `upload_date` datetime NOT NULL,
  `date_last_update` datetime NOT NULL,
  PRIMARY KEY (`lattach_id`),
  KEY `article_id` (`larticle_id`),
  KEY `attachment_title` (`ltitle`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_comments
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_comments`;
CREATE TABLE `lumonata_comments` (
  `lcomment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lcomment_parent` bigint(20) NOT NULL,
  `larticle_id` bigint(20) NOT NULL,
  `lcomentator_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lcomentator_email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lcomentator_url` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lcomentator_ip` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lcomment_date` datetime NOT NULL,
  `lcomment` text CHARACTER SET utf8 NOT NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lcomment_like` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `lcomment_type` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT 'like,comment,like_comment',
  PRIMARY KEY (`lcomment_id`),
  KEY `lcomment_status` (`lcomment_status`),
  KEY `lcomment_userid` (`luser_id`),
  KEY `lcomment_type` (`lcomment_type`),
  KEY `larticle_id` (`larticle_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_comments
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_friendship
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friendship`;
CREATE TABLE `lumonata_friendship` (
  `lfriendship_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `lfriend_id` bigint(20) NOT NULL,
  `lstatus` varchar(20) NOT NULL COMMENT 'connected, onrequest, pending, unfollow',
  PRIMARY KEY (`lfriendship_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lumonata_friendship
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_friends_list
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friends_list`;
CREATE TABLE `lumonata_friends_list` (
  `lfriends_list_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `llist_name` varchar(300) CHARACTER SET utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL,
  PRIMARY KEY (`lfriends_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lumonata_friends_list
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_friends_list_rel
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friends_list_rel`;
CREATE TABLE `lumonata_friends_list_rel` (
  `lfriendship_id` bigint(20) NOT NULL,
  `lfriends_list_id` bigint(20) NOT NULL,
  PRIMARY KEY (`lfriendship_id`,`lfriends_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lumonata_friends_list_rel
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_meta_data
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_meta_data`;
CREATE TABLE `lumonata_meta_data` (
  `lmeta_id` int(11) NOT NULL AUTO_INCREMENT,
  `lmeta_name` varchar(200) NOT NULL,
  `lmeta_value` longtext NOT NULL,
  `lapp_name` varchar(200) NOT NULL,
  `lapp_id` int(11) NOT NULL,
  PRIMARY KEY (`lmeta_id`),
  KEY `meta_name` (`lmeta_name`),
  KEY `app_name` (`lapp_name`),
  KEY `app_id` (`lapp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lumonata_meta_data
-- ----------------------------
INSERT INTO `lumonata_meta_data` VALUES ('1', 'time_zone', 'Asia/Singapore', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('2', 'site_url', 'localhost/maya', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('3', 'web_title', 'Maya Resort', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('4', 'smtp_server', 'localhost', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('5', 'smtp', 'mail.gmail.com', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('6', 'email', 'ngurah@lumonata.com', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('7', 'web_tagline', '', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('8', 'invitation_limit', '10', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('9', 'date_format', 'F j, Y', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('10', 'time_format', 'H:i', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('11', 'post_viewed', '6', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('12', 'rss_viewed', '15', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('13', 'rss_view_format', 'full_text', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('14', 'list_viewed', '50', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('15', 'email_format', 'html', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('16', 'text_editor', 'tiny_mce', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('17', 'thumbnail_image_size', '300:300', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('18', 'large_image_size', '1024:1024', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('19', 'medium_image_size', '700:700', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('20', 'is_allow_comment', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('21', 'is_login_to_comment', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('22', 'is_auto_close_comment', '0', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('23', 'days_auto_close_comment', '15', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('24', 'is_break_comment', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('25', 'comment_page_displayed', 'last', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('26', 'comment_per_page', '3', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('27', 'save_changes', 'Save Changes', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('28', 'is_rewrite', 'yes', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('29', 'is_allow_post_like', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('30', 'is_allow_comment_like', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('31', 'alert_on_register', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('32', 'alert_on_comment', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('33', 'alert_on_comment_reply', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('34', 'alert_on_liked_post', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('35', 'alert_on_liked_comment', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('36', 'web_name', 'Maya Resort', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('37', 'meta_description', '', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('38', 'meta_keywords', 'Maya Resort', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('39', 'meta_title', 'Maya Resort', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('40', 'status_viewed', '4', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('41', 'update', 'true', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('42', 'the_date_format', 'F j, Y', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('43', 'the_time_format', 'H:i', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('44', 'thumbnail_image_width', '300', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('45', 'thumbnail_image_height', '300', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('46', 'medium_image_width', '700', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('47', 'medium_image_height', '700', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('48', 'large_image_width', '1024', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('49', 'large_image_height', '1024', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('50', 'email_per_send', '5', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('51', 'newsletter_notif_email', 'ngurah@lumonata.com', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('52', 'newsletter_smtp', 'mail.lumonatalabs.com', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('53', 'front_theme', 'custom', 'themes', '0');
INSERT INTO `lumonata_meta_data` VALUES ('54', 'admin_theme', 'default', 'themes', '0');
INSERT INTO `lumonata_meta_data` VALUES ('55', 'custome_bg_color', 'FFF', 'themes', '0');

-- ----------------------------
-- Table structure for lumonata_notifications
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_notifications`;
CREATE TABLE `lumonata_notifications` (
  `lnotification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lpost_id` bigint(20) NOT NULL,
  `lpost_owner` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `laffected_user` bigint(20) NOT NULL,
  `laction_name` varchar(50) NOT NULL,
  `laction_date` date NOT NULL,
  `lstatus` varchar(10) NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY (`lnotification_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lumonata_notifications
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_rules
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_rules`;
CREATE TABLE `lumonata_rules` (
  `lrule_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lparent` bigint(20) NOT NULL,
  `lname` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lsef` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ldescription` text CHARACTER SET utf8 NOT NULL,
  `lrule` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lgroup` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lcount` bigint(20) NOT NULL DEFAULT '0',
  `lorder` bigint(20) NOT NULL DEFAULT '1',
  `lsubsite` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT 'arunna',
  PRIMARY KEY (`lrule_id`),
  KEY `rules_name` (`lname`),
  KEY `sef` (`lsef`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_rules
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_rule_relationship
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_rule_relationship`;
CREATE TABLE `lumonata_rule_relationship` (
  `lapp_id` bigint(20) NOT NULL,
  `lrule_id` bigint(20) NOT NULL,
  `lorder_id` bigint(20) NOT NULL,
  PRIMARY KEY (`lapp_id`,`lrule_id`),
  KEY `taxonomy_id` (`lrule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_rule_relationship
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_users
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_users`;
CREATE TABLE `lumonata_users` (
  `luser_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lusername` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ldisplay_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lpassword` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lemail` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lregistration_date` datetime NOT NULL,
  `luser_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lactivation_key` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lavatar` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lsex` int(11) NOT NULL COMMENT '1=male,2=female',
  `lbirthday` date NOT NULL,
  `lstatus` int(11) NOT NULL COMMENT '0=pendding activation, 1=active,2=blocked',
  `ldlu` datetime NOT NULL,
  PRIMARY KEY (`luser_id`),
  KEY `username` (`lusername`),
  KEY `display_name` (`ldisplay_name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_users
-- ----------------------------
INSERT INTO `lumonata_users` VALUES ('1', 'admin', 'Administrator', 'fcea920f7412b5da7be0cf42b8c93759', 'support@lumonata.com', '0000-00-00 00:00:00', 'administrator', '', 'man-1.jpg|man-2.jpg|man-3.jpg', '1', '2011-03-19', '1', '2015-03-09 15:52:57');
