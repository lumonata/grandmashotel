/*
Navicat MySQL Data Transfer

Source Server         : Server
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : maya

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-05-16 14:07:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lumonata_additional_fields
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_additional_fields`;
CREATE TABLE `lumonata_additional_fields` (
  `lapp_id` bigint(20) NOT NULL,
  `lkey` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lvalue` text CHARACTER SET utf8 NOT NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`lapp_id`,`lkey`),
  KEY `key` (`lkey`),
  KEY `app_name` (`lapp_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_additional_fields
-- ----------------------------
INSERT INTO `lumonata_additional_fields` VALUES ('1', 'destination', 'corporate', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('1', 'meta_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('1', 'meta_description', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('1', 'meta_keywords', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('2', 'destination', 'corporate', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('2', 'meta_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('2', 'meta_description', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('2', 'meta_keywords', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('3', 'destination', 'corporate', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('3', 'meta_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('3', 'meta_description', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('3', 'meta_keywords', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('4', 'destination', 'corporate', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('4', 'meta_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('4', 'meta_description', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('4', 'meta_keywords', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('5', 'destination', 'corporate', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('5', 'meta_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('5', 'meta_description', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('5', 'meta_keywords', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'brief', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'featured', '1', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('58', 'brief', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('58', 'featured', '1', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'offer_type', 'Flexible Promotion', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('58', 'offer_type', 'Special Promotion', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'offer_type', 'Seasonal Promotion', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'restaurant_phone_number', '+62 361 977 888', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'slide_image', '[\"2836157636e10b38df3.29539683.jpg\",\"14093570128616d04d1.86440090.jpg\",\"19876570128091592d0.32708697.jpg\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('137', 'main_image', '1505857674d62e41576.26362453.jpg', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'slide_title', '[\"asdas\",\"Morbi Tincidunt Metus Vitae\",\"Dining experience at Reef\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'slide_description', '[\"asdasda\",\"Lorem ipsum dolor sit amet, consectetur adipiscing elit\",\"The morning begins with a glimpse of paradise at Reef as you celebrate each new day.\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'location_image', '[\"19327576370a3cf5020.42374640.jpg\",\"1182576370a3943a47.14700153.jpg\",\"466657637173cf49b5.67663062.jpg\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'location_description', '[\"sdgfsdgsdgsdg\",\"asdasda\",\"\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'phone_number', '+62 361 977 888', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'fax_number', '+62 361 977 555', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'fb_link', 'http://facebook.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'twitter_link', 'http://twitter.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'instagram_link', 'http://instagram.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'tripadvisor_link', 'http://tripadvisor.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'email_address', 'info@mayaubud.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'address', 'Jalan Gunung Sari Peliatan,\r\nP. O. Box 1001 Ubud,\r\nBali 80571 - Indonesia', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'background_image', '16705571868f5d68873.99256772.jpg', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'instagram_user', 'mayasanur', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'coordinate_location', '-8.6975,115.26371', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'featured', '1', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'reservation_email_address', 'reservations@mayaubud.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'website_link', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'brief', 'Enjoy 50% Discount from the normal internet rate for stay period 1 April â€“ 14 July 2016.', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'background_image', '2994957020bf4037245.35594272.jpg', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'slide_image', '[\"1363357199a4e823ff4.09855386.jpg\",\"1098257199a4f4e8012.01655365.jpg\",\"208757199a50155742.87239557.jpg\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'slide_title', '[\"Centrally Located in Sanur\",\"Modern and Luxurious Facilities\",\"A Great Dining Experince\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'slide_description', '[\"25 minutes from the Airport, as well as within 45 minutes from many of Bali\'s famous holiday destinations\",\"A combination of modern convenience and the authentic essence of Bali\",\"Three dynamic dining venues, plus a beachfront tree bar with panoramic ocean views on all three levels\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'location_image', '[\"277257199a50b7d3e7.78627824.jpg\",\"2572557199a514d0c99.31626943.jpg\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'location_description', '[\"Three dynamic dining venues, plus a beachfront tree bar with panoramic ocean views on all three levels\",\"25 minutes from the Airport, as well as within 45 minutes from many of Bali\'s famous holiday destinations\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'phone_number', '+62 361 849 7800', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'fax_number', '+62 361 849 7808', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'fb_link', 'http://facebook.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'twitter_link', 'http://twitter.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'instagram_link', 'http://instagram.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'tripadvisor_link', 'http://tripadvisor.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'email_address', 'info@mayasanur.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'address', 'Jalan Danau Tamblingan No. 89M,\r\nP. O. Box 3253 Sanur,\r\nBali 80228 - Indonesia', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'instagram_user', 'mayaubud', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'coordinate_location', '-8.5131187,115.2777461', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('46', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('46', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('46', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('46', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('46', 'post_gallery_image', '[\"121\"]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('46', 'destination', 'ubud', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'frame_video', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'post_gallery_image', '[\"88\",\"87\",\"89\",\"91\",\"90\"]', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'destination', 'ubud', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'meta_title', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'meta_description', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'meta_keywords', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('39', 'frame_video', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('39', 'post_gallery_image', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"92\",\"93\",\"94\",\"95\",\"96\"]', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('39', 'destination', 'ubud', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('39', 'meta_title', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('39', 'meta_description', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('39', 'meta_keywords', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('40', 'frame_video', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('40', 'post_gallery_image', '[\"53\",\"54\",\"55\",\"56\",\"57\",\"97\",\"98\",\"99\",\"100\",\"101\"]', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('40', 'destination', 'ubud', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('40', 'meta_title', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('40', 'meta_description', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('40', 'meta_keywords', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('41', 'frame_video', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('41', 'post_gallery_image', '[\"58\",\"59\",\"60\",\"61\",\"62\",\"102\",\"103\",\"104\",\"105\",\"106\"]', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('41', 'destination', 'ubud', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('41', 'meta_title', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('41', 'meta_description', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('41', 'meta_keywords', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('42', 'frame_video', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('42', 'post_gallery_image', '[\"63\",\"64\",\"65\",\"66\",\"107\",\"108\",\"109\",\"110\"]', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('42', 'destination', 'ubud', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('42', 'meta_title', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('42', 'meta_description', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('42', 'meta_keywords', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'frame_video', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'post_gallery_image', '{\"10\":268,\"11\":269,\"12\":270,\"13\":271,\"14\":272,\"15\":273,\"16\":274,\"17\":275,\"18\":276,\"19\":277}', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'destination', 'ubud', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'meta_title', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'meta_description', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'meta_keywords', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'food_drink_menu', '[{\"name\":\"Hatten, Pino de Bali\",\"desc\":\"Alphonse-LavallÃ©e &amp; Belgia Grapes, Bali, NV. (Sweet wine/375ml bottle - 75ml glass)\",\"price\":\"103\",\"category\":\"Wine by the Glass | Sweet Note\"},{\"name\":\"Hatten, Tunjung, Brut Sparkling\",\"desc\":\"Probolinggo Biru Grape, Bali, NV\",\"price\":\"111\",\"category\":\"Wine by the Glass | Sparkling / Bubbles\"},{\"name\":\"Jansz Estate, Naked Range, Sparkling Brut\",\"desc\":\"Yarra Valley, Australia, NV\",\"price\":\"150\",\"category\":\"Wine by the Glass | Sparkling / Bubbles\"},{\"name\":\"Ken Forrester, Petit Chenin Blanc\",\"desc\":\"Stellenbosch, South Africa, 2014\",\"price\":\"141\",\"category\":\"Wine by the Glass | White &amp; RosÃ©\"},{\"name\":\"Ruffino, DOC Orvietto Classico\",\"desc\":\"Umbria, Italy, 2013\",\"price\":\"139\",\"category\":\"Wine by the Glass | White &amp; RosÃ©\"},{\"name\":\"Domaine du Tariquet, Sauvignon Blanc\",\"desc\":\"IGP CÃ´tes de Gascogne, France, 2013\",\"price\":\"148\",\"category\":\"Wine by the Glass | White &amp; RosÃ©\"},{\"name\":\"Artisan, Chardonnay\",\"desc\":\"Western Australia grapes crafted in Bali, 2013\",\"price\":\"96\",\"category\":\"Wine by the Glass | White &amp; RosÃ©\"},{\"name\":\"Cape Discovery RosÃ©\",\"desc\":\"Australian Grapes Crafted in Bali, 2014\",\"price\":\"78\",\"category\":\"Wine by the Glass | White &amp; RosÃ©\"},{\"name\":\"Hatten Two Islands, Pinot Grigio\",\"desc\":\"Australian Grapes Crafted in Bali, 2014\",\"price\":\"92\",\"category\":\"Wine by the Glass | White &amp; RosÃ©\"},{\"name\":\"Argento, Chardonnay, Mendoza\",\"desc\":\"Argentina, 2014\",\"price\":\"141\",\"category\":\"Wine by the Glass | White &amp; RosÃ©\"},{\"name\":\"Hatten Two Islands, Chardonnay\",\"desc\":\"Australian Grapes Crafted in Bali, 2014\",\"price\":\"92\",\"category\":\"Wine by the Glass | White &amp; RosÃ©\"},{\"name\":\"Ken Forrester, Petit Cabernet~Merlot,\",\"desc\":\"Stellenbosch, South Africa, 2013\",\"price\":\"141\",\"category\":\"Wine by the Glass | Red\"},{\"name\":\"Patch Block, Pinot Noir\",\"desc\":\"IGP Pays dâ€™Oc, France, 2012\",\"price\":\"142\",\"category\":\"Wine by the Glass | Red\"},{\"name\":\"Hatten, Two Islands, Cabernet~Merlot\",\"desc\":\"Australian Grapes Crafted in Bali, 2014\",\"price\":\"92\",\"category\":\"Wine by the Glass | Red\"},{\"name\":\"Kaiken, Malbec Reserva, Mendoza\",\"desc\":\"Argentina, 2012\",\"price\":\"132\",\"category\":\"Wine by the Glass | Red\"},{\"name\":\"Hatten, Two Islands, Shiraz\",\"desc\":\"Australian Grapes Crafted in Bali, 2014\",\"price\":\"92\",\"category\":\"Wine by the Glass | Red\"},{\"name\":\"PrÃ³ximo by MarquÃ©s de Riscal, Tempranillo\",\"desc\":\"DOC Rioja, Spain, 2013\",\"price\":\"129\",\"category\":\"Wine by the Glass | Red\"},{\"name\":\"Gold Mount, Shiraz, Hunter Valley\",\"desc\":\"Australia, 2013\",\"price\":\"139\",\"category\":\"Wine by the Glass | Red\"},{\"name\":\"Bowl of Mixed Marinated Olives\",\"desc\":\"-\",\"price\":\"35\",\"category\":\"Bar Bites\"},{\"name\":\"Edamame\",\"desc\":\"steamed soy beans with sea salt\",\"price\":\"35\",\"category\":\"Bar Bites\"},{\"name\":\"Hand Cut Potato Wedges\",\"desc\":\"home cooked wedges tossed in our own spiced salt\",\"price\":\"35\",\"category\":\"Bar Bites\"},{\"name\":\"Balinese Vegetable Spring Rolls\",\"desc\":\"crispy fried vegetables spring rolls served with pickles and sweet sour sauce\",\"price\":\"45\",\"category\":\"Bar Bites\"},{\"name\":\"Vietnamese Fresh Spring Rolls\",\"desc\":\"prawn with nouc cham sauce\",\"price\":\"65\",\"category\":\"Bar Bites\"},{\"name\":\"Vietnamese Fresh Spring Rolls\",\"desc\":\"vegetarian with nouc tuong sauce\",\"price\":\"60\",\"category\":\"Bar Bites\"},{\"name\":\"Shrimp Fire Crackers\",\"desc\":\"tempura style shrimp, spicy aioli\",\"price\":\"70\",\"category\":\"Bar Bites\"},{\"name\":\"Grilled Chicken Winglets\",\"desc\":\"served with tangy relish\",\"price\":\"65\",\"category\":\"Bar Bites\"},{\"name\":\"Salt and Pepper Squid\",\"desc\":\"tender squid fired in a Sichuan spice mix\",\"price\":\"60\",\"category\":\"Bar Bites\"},{\"name\":\"Crunchy Peanut &amp; Chicken Tempura Satay\",\"desc\":\"-\",\"price\":\"70\",\"category\":\"Bar Bites\"},{\"name\":\"Spicy Ginger\",\"desc\":\"light rum, mint, lime wedges, white sugar, topped with ginger ale\",\"price\":\"80\",\"category\":\"Drinks | Cuban Fushion\"},{\"name\":\"Tropical Paradise\",\"desc\":\"light rum, coconut liqueur, mint, lime wedges, white sugar, topped with soda water\",\"price\":\"80\",\"category\":\"Drinks | Cuban Fushion\"},{\"name\":\"Mango Minty\",\"desc\":\"light rum, mango puree, mint, lime wedges, white sugar, topped with soda water\",\"price\":\"80\",\"category\":\"Drinks | Cuban Fushion\"},{\"name\":\"Caipirinha \",\"desc\":\"light rum, lime wedges, white sugar\",\"price\":\"80\",\"category\":\"Drinks | Cuban Fushion\"},{\"name\":\"Mojito\",\"desc\":\"light rum, mint leaves, lime, soda\",\"price\":\"80\",\"category\":\"Drinks | Cuban Fushion\"},{\"name\":\"Lychee Martini\",\"desc\":\"vodka, triple sec, lychee liqueur and sweet sour\",\"price\":\"80\",\"category\":\"Drinks | Shaken &amp; Stirred\"},{\"name\":\"Apple Tiny\",\"desc\":\"vodka, triple sec, lime juice, green apple and simple syrup\",\"price\":\"80\",\"category\":\"Drinks | Shaken &amp; Stirred\"},{\"name\":\"Classic Martini\",\"desc\":\"gin and dry vermouth with either an olive or a twist\",\"price\":\"85\",\"category\":\"Drinks | Shaken &amp; Stirred\"},{\"name\":\"Negroni\",\"desc\":\"gin, sweet vermouth and campari\",\"price\":\"120\",\"category\":\"Drinks | Shaken &amp; Stirred\"},{\"name\":\"California Dream\",\"desc\":\"tequila, dry vermouth and sweet vermouth\",\"price\":\"110\",\"category\":\"Drinks | Shaken &amp; Stirred\"},{\"name\":\"English Breakfast Tea\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Gourmet Tea\"},{\"name\":\"Earl Grey Tea\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Gourmet Tea\"},{\"name\":\"Strawberry &amp; Mango Tea\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Exotic Fruit Tea\"},{\"name\":\"Lychee Tea\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Exotic Fruit Tea\"},{\"name\":\"Lemon Tea\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Exotic Fruit Tea\"},{\"name\":\"Ginger Tea\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Exotic Fruit Tea\"},{\"name\":\"Jasmine Tea\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Green Tea\"},{\"name\":\"Peppermint Tea\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Green Tea\"},{\"name\":\"Oolong Tea\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Green Tea\"},{\"name\":\"Chamomile\",\"desc\":\"-\",\"price\":\"24\",\"category\":\"Tea &amp; Coffee | Herbal Infusion\"},{\"name\":\"Regular Iced Tea\",\"desc\":\"-\",\"price\":\"28\",\"category\":\"Tea &amp; Coffee | Iced tea\"},{\"name\":\"Iced Lemon Tea\",\"desc\":\"-\",\"price\":\"28\",\"category\":\"Tea &amp; Coffee | Iced tea\"},{\"name\":\"Iced Lychee Tea\",\"desc\":\"-\",\"price\":\"28\",\"category\":\"Tea &amp; Coffee | Iced tea\"},{\"name\":\"Cosmopolitan\",\"desc\":\"vodka, triple sec and cranberry juice\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Ginger Martini\",\"desc\":\"vodka, ginger, triples sec, lemon juice, and simple syrup\",\"price\":\"80\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Kamikaze\",\"desc\":\"vodka, triple sec, and lime juice\",\"price\":\"80\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Basilito\",\"desc\":\"gin, lemongrass, sweet basil, lime juice, simple syrup and tonic water\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Long Island Iced Tea\",\"desc\":\"vodka, light rum, dark rum, dry gin, tequila,ntriples sec, lime juice topped with cola\",\"price\":\"100\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"King Banana\",\"desc\":\"blend of vodka, coffee liqueur, banana liqueur, fresh banana, and cream\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Cucumber Mint Martini\",\"desc\":\"vodka, triple sec, fresh cucumber, mint and simple syrup\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Berry Mule\",\"desc\":\"Tri Berry (strawberry, raspberry, blueberry), vodka infused rosemary, lime, mint leaf topped with ginger ale\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Sunset Colada\",\"desc\":\"blend of light rum, coconut liqueur, cream coconut, pineapple &amp; orange juice and grenadine syrup\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Bay Breeze\",\"desc\":\"vodka, cranberry juice and pineapple juice\",\"price\":\"80\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Hawaiian Vodka\",\"desc\":\"vodka, blue curacao, pineapple, orange juice, lime juice and dash of grenadine syrup\",\"price\":\"80\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Planters Punch\",\"desc\":\"light rum, dark rum, orange juice, pineapple juice and lime juice\",\"price\":\"80\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Tequila Sunrise\",\"desc\":\"tequila, orange juice and dash of grenadine syrup\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Daiquiri\",\"desc\":\"classic or choice of (banana, pineapple, strawberry, and mango) with light rum, triple sec, and lime juice\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Margarita\",\"desc\":\"classic or choices of mango, strawberry, triberry (strawberry, raspberry, blueberry) with tequila, triple sec, lime juice, and simple syrup\",\"price\":\"100\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Illusion\",\"desc\":\"vodka, coconut liqueur, blue curacaos, melon liqueur and pineapple juice\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Pinacolada\",\"desc\":\"light rum, coconut liqueur, coconut cream and pineapple juice\",\"price\":\"85\",\"category\":\"Drinks | Urban Mixer\"},{\"name\":\"Pineapple Float\",\"desc\":\"blend of banana and pineapple floated in sprite and grenadine syrup\",\"price\":\"30\",\"category\":\"Drinks | Fresh &amp; Fruity\"},{\"name\":\"Basil Cooler\",\"desc\":\"basil leaf, lemongrass, lime Juice and simple syrup\",\"price\":\"30\",\"category\":\"Drinks | Fresh &amp; Fruity\"},{\"name\":\"Vitalizer\",\"desc\":\"green Smith apple, Cucumber, mint leaf, lemon juice and simple syrup\",\"price\":\"30\",\"category\":\"Drinks | Fresh &amp; Fruity\"},{\"name\":\"Cream Berry\",\"desc\":\"pineapple juice, strawberry and cream coconut\",\"price\":\"30\",\"category\":\"Drinks | Fresh &amp; Fruity\"},{\"name\":\"Minty Lemon\",\"desc\":\"blend of mint leaf, lime juice and simple syrup\",\"price\":\"30\",\"category\":\"Drinks | Fresh &amp; Fruity\"},{\"name\":\"Mango Tango\",\"desc\":\"floated of mango and strawberry\",\"price\":\"30\",\"category\":\"Drinks | Fresh &amp; Fruity\"},{\"name\":\"Sunshine Colada\",\"desc\":\"blend of pineapple juice, cream coconut and dash grenadine syrup\",\"price\":\"30\",\"category\":\"Drinks | Fresh &amp; Fruity\"},{\"name\":\"Smoothie\",\"desc\":\"choices of strawberry, peach, banana, mango, triberry (strawberry, raspberry, and blueberry) blend with plain yoghurt, honey and fresh milk\",\"price\":\"30\",\"category\":\"Drinks | Fresh &amp; Fruity\"},{\"name\":\"Spicy Ginger\",\"desc\":\"Light rum, mint, lime wedges, white sugar, topped with ginger ale\",\"price\":\"80\",\"category\":\"Cuban Fusion\"},{\"name\":\"Tropical Paradise\",\"desc\":\"Light rum, coconut liqueur, mint, lime wedges, white sugar, topped with soda water\",\"price\":\"80\",\"category\":\"Cuban Fusion\"},{\"name\":\"Mango Minty\",\"desc\":\"Light rum, mango puree, mint, lime wedges, white sugar, topped with soda water\",\"price\":\"80\",\"category\":\"Cuban Fusion\"},{\"name\":\"Caipirinha\",\"desc\":\"Light rum, lime wedges, white sugar\",\"price\":\"80\",\"category\":\"Cuban Fusion\"},{\"name\":\"Mojito\",\"desc\":\"Light rum, mint leaves, lime, soda\",\"price\":\"80\",\"category\":\"Cuban Fusion\"},{\"name\":\"Lychee Martini\",\"desc\":\"Vodka, triple sec, lychee liqueur and sweet sour\",\"price\":\"80\",\"category\":\"Shaken &amp; Stirred\"},{\"name\":\"Apple Tiny\",\"desc\":\"Vodka, triple sec, lime juice, green apple and simple syrup\",\"price\":\"80\",\"category\":\"Shaken &amp; Stirred\"},{\"name\":\"Classic Martini\",\"desc\":\"Gin and dry vermouth with either an olive or a twist\",\"price\":\"85\",\"category\":\"Shaken &amp; Stirred\"},{\"name\":\"Negroni\",\"desc\":\"Gin, sweet vermouth and campari\",\"price\":\"120\",\"category\":\"Shaken &amp; Stirred\"},{\"name\":\"California Dream\",\"desc\":\"Tequila, dry vermouth and sweet vermouth\",\"price\":\"110\",\"category\":\"Shaken &amp; Stirred\"},{\"name\":\"Cosmopolitan\",\"desc\":\"Vodka, triple sec and cranberry juice\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Ginger Martini\",\"desc\":\"Vodka, ginger, triples sec, lemon juice, and simple syrup\",\"price\":\"80\",\"category\":\"Urban Mixer\"},{\"name\":\"Kamikaze\",\"desc\":\"Vodka, triple sec, and lime juice\",\"price\":\"80\",\"category\":\"Urban Mixer\"},{\"name\":\"Basilito\",\"desc\":\"Gin, lemongrass, sweet basil, lime juice, simple syrup and tonic water\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Long Island Iced Tea\",\"desc\":\"Vodka, light rum, dark rum, dry gin, tequila, triples sec, lime juice topped with cola\",\"price\":\"100\",\"category\":\"Urban Mixer\"},{\"name\":\"King Banana\",\"desc\":\"Blend of vodka, coffee liqueur, banana liqueur, fresh banana, and cream\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Cucumber Mint Martini\",\"desc\":\"Vodka, triple sec, fresh cucumber, mint and simple syrup\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Berry Mule\",\"desc\":\"Tri Berry (strawberry, raspberry, blueberry), vodka infused rosemary, lime, mint leaf topped with ginger ale\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Sunset Colada\",\"desc\":\"Blend of light rum, coconut liqueur, cream coconut, pineapple &amp; orange juice and grenadine syrup\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Bay Breeze\",\"desc\":\"Vodka, cranberry juice and pineapple juice\",\"price\":\"80\",\"category\":\"Urban Mixer\"},{\"name\":\"Hawaiian Vodka\",\"desc\":\"Vodka, blue curacao, pineapple, orange juice, lime juice and dash of grenadine syrup\",\"price\":\"80\",\"category\":\"Urban Mixer\"},{\"name\":\"Planters Punch\",\"desc\":\"Light rum, dark rum, orange juice, pineapple juice and lime juice\",\"price\":\"80\",\"category\":\"Urban Mixer\"},{\"name\":\"Tequila Sunrise\",\"desc\":\"Tequila, orange juice and dash of grenadine syrup\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Daiquiri\",\"desc\":\"Classic or choice of (banana, pineapple, strawberry, and mango) with light rum, triple sec, and lime juice\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Margarita\",\"desc\":\"Classic or choices of mango, strawberry, triberry (strawberry, raspberry, blueberry) with tequila, triple sec, lime juice,\\nand simple syrup\",\"price\":\"100\",\"category\":\"Urban Mixer\"},{\"name\":\"Illusion\",\"desc\":\"Vodka, coconut liqueur, blue curacaos, melon liqueur and pineapple juice\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Pinacolada\",\"desc\":\"Light rum, coconut liqueur, coconut cream and pineapple juice\",\"price\":\"85\",\"category\":\"Urban Mixer\"},{\"name\":\"Pineapple Float\",\"desc\":\"Blend of banana and pineapple floated in sprite and grenadine syrup\",\"price\":\"30\",\"category\":\"Fresh Fruity\"},{\"name\":\"Basil Cooler\",\"desc\":\"Basil leaf, lemongrass, lime Juice and simple syrup\",\"price\":\"30\",\"category\":\"Fresh Fruity\"},{\"name\":\"Vitalizer\",\"desc\":\"Green Smith apple, Cucumber, mint leaf, lemon juice and simple syrup\",\"price\":\"30\",\"category\":\"Fresh Fruity\"},{\"name\":\"Cream Berry\",\"desc\":\"Pineapple juice, strawberry and cream coconut\",\"price\":\"30\",\"category\":\"Fresh Fruity\"},{\"name\":\"Minty lemon\",\"desc\":\"Blend of mint leaf, lime juice and simple syrup\",\"price\":\"30\",\"category\":\"Fresh Fruity\"},{\"name\":\"Mango Tango\",\"desc\":\"Floated of mango and strawberry\",\"price\":\"30\",\"category\":\"Fresh Fruity\"},{\"name\":\"Sunshine Colada\",\"desc\":\"Blend of pineapple juice, cream coconut and dash grenadine syrup\",\"price\":\"30\",\"category\":\"Fresh Fruity\"},{\"name\":\"Smoothie\",\"desc\":\"Choices of strawberry, peach, banana, mango, triberry (strawberry, raspberry, and blueberry) blend with plain yoghurt, honey and fresh milk\",\"price\":\"30\",\"category\":\"Fresh Fruity\"},{\"name\":\"Bintang\",\"desc\":\"330ml\",\"price\":\"55\",\"category\":\"Cool &amp; Casual | Beer\"},{\"name\":\"Carlsberg\",\"desc\":\"330ml\",\"price\":\"78\",\"category\":\"Cool &amp; Casual | Beer\"},{\"name\":\"Sapporo\",\"desc\":\"330ml\",\"price\":\"110\",\"category\":\"Cool &amp; Casual | Beer\"},{\"name\":\"Heineken\",\"desc\":\"330ml\",\"price\":\"113\",\"category\":\"Cool &amp; Casual | Beer\"},{\"name\":\"Corona\",\"desc\":\"335ml\",\"price\":\"118\",\"category\":\"Cool &amp; Casual | Beer\"},{\"name\":\"Equil Natural\",\"desc\":\"700ml\",\"price\":\"60\",\"category\":\"Cool &amp; Casual | Water\"},{\"name\":\"Equil Sparkling\",\"desc\":\"700ml\",\"price\":\"60\",\"category\":\"Cool &amp; Casual | Water\"},{\"name\":\"San Pellegrino\",\"desc\":\"500ml\",\"price\":\"70\",\"category\":\"Cool &amp; Casual | Water\"},{\"name\":\"Aqua Panna\",\"desc\":\"500ml\",\"price\":\"70\",\"category\":\"Cool &amp; Casual | Water\"},{\"name\":\"San Pellegrino\",\"desc\":\"750ml\",\"price\":\"95\",\"category\":\"Cool &amp; Casual | Water\"},{\"name\":\"Aqua Panna\",\"desc\":\"750ml\",\"price\":\"95\",\"category\":\"Cool &amp; Casual | Water\"},{\"name\":\"Coca Cola\",\"desc\":\"330ml\",\"price\":\"25\",\"category\":\"Cool &amp; Casual | Soft Drinks\"},{\"name\":\"Fanta Strawberry\",\"desc\":\"330ml\",\"price\":\"25\",\"category\":\"Cool &amp; Casual | Soft Drinks\"},{\"name\":\"Sprite\",\"desc\":\"330ml\",\"price\":\"25\",\"category\":\"Cool &amp; Casual | Soft Drinks\"},{\"name\":\"Diet Coke\",\"desc\":\"330ml\",\"price\":\"25\",\"category\":\"Cool &amp; Casual | Soft Drinks\"},{\"name\":\"Schweppes Soda Water\",\"desc\":\"330ml\",\"price\":\"25\",\"category\":\"Cool &amp; Casual | Soft Drinks\"},{\"name\":\"Schweppes Tonic Water\",\"desc\":\"330ml\",\"price\":\"25\",\"category\":\"Cool &amp; Casual | Soft Drinks\"},{\"name\":\"Schweppes Ginger Ale\",\"desc\":\"330ml\",\"price\":\"25\",\"category\":\"Cool &amp; Casual | Water\"},{\"name\":\"Schweppes Ginger Ale\",\"desc\":\"330ml\",\"price\":\"25\",\"category\":\"Cool &amp; Casual | Soft Drinks\"},{\"name\":\"Chilled Juices\",\"desc\":\"Orange, apple, mango, lime, pineapple, guava\",\"price\":\"20\",\"category\":\"Cool &amp; Casual\"},{\"name\":\"Fresh Juices\",\"desc\":\"Pineapple, watermelon, papaya, banana, mixed fruit juice\",\"price\":\"25\",\"category\":\"Cool &amp; Casual\"},{\"name\":\"Absolute Blue\",\"desc\":\"40ml\",\"price\":\"70\",\"category\":\"A Quick Shot 2 | Vodka\"},{\"name\":\"Russian Standard\",\"desc\":\"40ml\",\"price\":\"85\",\"category\":\"A Quick Shot 2 | Vodka\"},{\"name\":\"Grey Goose\",\"desc\":\"40ml\",\"price\":\"95\",\"category\":\"A Quick Shot 2 | Vodka\"},{\"name\":\"Havana Club Anejo 3 anos\",\"desc\":\"40ml\",\"price\":\"70\",\"category\":\"A Quick Shot 2 | Rum\"},{\"name\":\"Bacardi Gold\",\"desc\":\"40ml\",\"price\":\"72\",\"category\":\"A Quick Shot 2 | Rum\"},{\"name\":\"Myerâ€™s Dark Rum\",\"desc\":\"40ml\",\"price\":\"72\",\"category\":\"A Quick Shot 2 | Rum\"},{\"name\":\"Gordonâ€™s Dry Gin\",\"desc\":\"40ml\",\"price\":\"73\",\"category\":\"A Quick Shot 2 | Gin\"},{\"name\":\"Tanqueray\",\"desc\":\"40ml\",\"price\":\"70\",\"category\":\"A Quick Shot 2 | Gin\"},{\"name\":\"Beefeater\",\"desc\":\"40ml\",\"price\":\"75\",\"category\":\"A Quick Shot 2 | Gin\"},{\"name\":\"Bombay Sapphire\",\"desc\":\"40ml\",\"price\":\"85\",\"category\":\"A Quick Shot 2 | Gin\"},{\"name\":\"Sauza gold\",\"desc\":\"40ml\",\"price\":\"85\",\"category\":\"A Quick Shot 2 | Tequila\"},{\"name\":\"Sauza Hornitos Reposado\",\"desc\":\"40ml\",\"price\":\"95\",\"category\":\"A Quick Shot 2 | Tequila\"},{\"name\":\"St. Remy FrenchBrandy Napoleon\",\"desc\":\"40ml\",\"price\":\"85\",\"category\":\"A Quick Shot 2 | Brandy\"},{\"name\":\"Remy Martin VSOP\",\"desc\":\"40ml\",\"price\":\"170\",\"category\":\"A Quick Shot 2 | Cognac\"},{\"name\":\"Calvados Coeur de Lion Drouhin\",\"desc\":\"40ml\",\"price\":\"110\",\"category\":\"A Quick Shot 2 | Calvados\"},{\"name\":\"Fontaine Absinthe\",\"desc\":\"40ml\",\"price\":\"160\",\"category\":\"A Quick Shot 2 | Absinthe\"},{\"name\":\"Johnnie Walker Black\",\"desc\":\"40ml\",\"price\":\"140\",\"category\":\"A Quick Shot 2 | Gin\"},{\"name\":\"Chivas Regal 12 years\",\"desc\":\"40ml\",\"price\":\"120\",\"category\":\"A Quick Shot 2 | Scotch Whisky\"},{\"name\":\"Jameson\",\"desc\":\"40ml\",\"price\":\"100\",\"category\":\"A Quick Shot 2 | Irish Whisky\"},{\"name\":\"Jack Danielâ€™s original\",\"desc\":\"40ml\",\"price\":\"100\",\"category\":\"A Quick Shot 2 | American Whisky and Bourbon\"}]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'food_drink_submenu', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('44', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('44', 'post_gallery_image', '[\"116\",\"117\",\"118\"]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('44', 'destination', 'ubud', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('44', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('44', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('44', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'post_gallery_image', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'destination', 'ubud', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('47', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('47', 'post_gallery_image', '[\"122\"]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('47', 'destination', 'ubud', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('47', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('47', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('47', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('48', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('48', 'post_gallery_image', '[\"123\",\"124\"]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('48', 'destination', 'ubud', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('48', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('48', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('48', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'post_gallery_image', '[\"125\"]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'destination', 'ubud', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('50', 'frame_video', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('50', 'post_gallery_image', '[\"126\"]', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('50', 'destination', 'ubud', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('50', 'meta_title', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('50', 'meta_description', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('50', 'meta_keywords', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'brief', 'Rooms in the East wing face either up or down the Petanu River Valley. Rooms in the West wing overlook the rice terraces of Peliatan. Rooms are 43m2 (forty-three square meters), including the entrance hall, bathroom and private balcony.', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'view', 'Room in the East wing face either up or down the Petanu River Valley. Rooms in the West wing overlook the rice terraces of Peliatan.', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'food_drink_menu_category', '[{\"id\":\"21\",\"name\":\"A Quick Shot 2\",\"child\":[{\"id\":\"27\",\"name\":\"Vodka\"},{\"id\":\"28\",\"name\":\"Rum\"},{\"id\":\"29\",\"name\":\"Gin\"},{\"id\":\"30\",\"name\":\"Tequila\"},{\"id\":\"31\",\"name\":\"Brandy\"},{\"id\":\"32\",\"name\":\"Cognac\"},{\"id\":\"33\",\"name\":\"Calvados\"},{\"id\":\"34\",\"name\":\"Absinthe\"},{\"id\":\"35\",\"name\":\"Scotch Whisky\"},{\"id\":\"36\",\"name\":\"Irish Whisky\"},{\"id\":\"37\",\"name\":\"American Whisky and Bourbon\"},{\"id\":\"38\",\"name\":\"Canadian Whisky\"},{\"id\":\"39\",\"name\":\"Single Malt Whisky\"},{\"id\":\"40\",\"name\":\"Vermouth\"},{\"id\":\"41\",\"name\":\"Aperitif\"},{\"id\":\"42\",\"name\":\"Liquors\"}]},{\"id\":\"1\",\"name\":\"Wine by the Glass\",\"child\":[{\"id\":\"3\",\"name\":\"White &amp; RosÃ©\"},{\"id\":\"4\",\"name\":\"Red\"},{\"id\":\"5\",\"name\":\"Sparkling / Bubbles\"},{\"id\":\"6\",\"name\":\"Sweet Note\"}]},{\"id\":\"16\",\"name\":\"Cuban Fusion\"},{\"id\":\"17\",\"name\":\"Shaken &amp; Stirred\"},{\"id\":\"18\",\"name\":\"Urban Mixer\"},{\"id\":\"19\",\"name\":\"Fresh Fruity\"},{\"id\":\"20\",\"name\":\"Cool &amp; Casual\",\"child\":[{\"id\":\"22\",\"name\":\"Beer\"},{\"id\":\"23\",\"name\":\"Water\"},{\"id\":\"24\",\"name\":\"Soft Drinks\"}]},{\"id\":\"10\",\"name\":\"Tea &amp; Coffee\",\"child\":[{\"id\":\"11\",\"name\":\"Gourmet Tea\"},{\"id\":\"12\",\"name\":\"Exotic Fruit Tea\"},{\"id\":\"13\",\"name\":\"Green Tea\"},{\"id\":\"14\",\"name\":\"Herbal Infusion\"},{\"id\":\"15\",\"name\":\"Iced tea\"},{\"id\":\"43\",\"name\":\"Coffee\"},{\"id\":\"44\",\"name\":\"Chocolate\"},{\"id\":\"45\",\"name\":\"Ice Coffee\"}]},{\"id\":\"2\",\"name\":\"Bar Bites\"}]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'brief', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('101', 'post_gallery_image', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('101', 'meta_title', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('100', 'main_image', '15997571ec08d368bc3.02395513.jpg', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('101', 'frame_video', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('101', 'brief', 'coba aja dulu', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('101', 'destination', 'ubud', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'brief', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'view', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'size', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'bedroom', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'bathroom', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'occupancy', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'amenities', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'restaurant_fax_number', '+62 361 977 555', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'restaurant_email_address', 'foodandbeverage@mayaubud.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'restaurant_phone_number', '+62 361 849 7800', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'restaurant_fax_number', '+62 361 849 7808', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'restaurant_email_address', 'foodandbeverage@mayasanur.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'reservation_phone_number', '+62 361 977 888', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'reservation_fax_number', '+62 361 977 555', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'reservation_phone_number', '+62 361 849 7800', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'reservation_fax_number', '+62 361 849 7808', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'location_title', '[\"Dining experience at Reef\",\"Centrally Located in Sanur\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'reservation_email_address', 'reservations@mayasanur.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'location_title', '[\"Morbi Tincidunt Metus Vitae\",\"Dining experience at Reef\",\"\"]', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('101', 'main_image', '13532571ec0d4293109.67809142.jpg', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('101', 'website_link', 'http://mayasari.com', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('101', 'meta_description', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('100', 'meta_title', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('100', 'meta_description', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('100', 'meta_keywords', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('100', 'destination', 'ubud', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('100', 'post_gallery_image', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'offer_type', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'featured', '0', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('100', 'frame_video', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('100', 'brief', 'coba aja dulu', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('44', 'website_link', 'http://mayasari.com', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'brief', 'Welcoming you to a resort that bordered by the Petanu River valley', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'brief', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'website_link', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'main_image', '269075719f5174fe976.36401257.jpg', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('44', 'brief', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'main_image', '211645719f290c033b9.54733849.jpg', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('51', 'frame_video', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('51', 'post_gallery_image', '[\"127\"]', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('51', 'meta_title', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('51', 'meta_description', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('51', 'meta_keywords', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('51', 'destination', 'ubud', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('52', 'frame_video', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('52', 'destination', 'ubud', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('52', 'post_gallery_image', '[\"128\"]', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('52', 'meta_title', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('52', 'meta_description', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('52', 'meta_keywords', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('53', 'frame_video', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('53', 'destination', 'ubud', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('53', 'post_gallery_image', '[\"129\"]', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('53', 'meta_title', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('53', 'meta_description', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('53', 'meta_keywords', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'frame_video', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'destination', 'ubud', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'post_gallery_image', '[\"130\",\"131\"]', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'meta_title', 'Shops Aja Bro', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'meta_description', 'cuma mau tau aja dah', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'meta_keywords', 'shops', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('57', 'meta_title', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('57', 'post_gallery_image', '[\"133\"]', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('57', 'frame_video', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'frame_video', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'meta_title', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'meta_description', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'meta_keywords', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'post_gallery_image', '[\"132\"]', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('57', 'meta_description', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('57', 'meta_keywords', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('58', 'frame_video', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('58', 'post_gallery_image', '[\"134\"]', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('58', 'meta_title', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('58', 'meta_description', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('58', 'meta_keywords', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'frame_video', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'post_gallery_image', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'meta_title', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'meta_description', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'meta_keywords', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'frame_video', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'post_gallery_image', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'meta_title', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'meta_description', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'meta_keywords', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('62', 'frame_video', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('62', 'destination', 'ubud', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('62', 'post_gallery_image', '[\"137\",\"138\",\"139\",\"140\",\"141\",\"142\",\"143\",\"144\",\"145\",\"146\",\"147\",\"148\"]', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'destination', 'ubud', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('57', 'destination', 'ubud', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('58', 'destination', 'ubud', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'destination', 'ubud', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'destination', 'ubud', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('62', 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('62', 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('62', 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('63', 'frame_video', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('63', 'destination', 'ubud', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('63', 'post_gallery_image', '[\"149\",\"150\",\"151\",\"152\",\"153\",\"154\",\"155\",\"156\",\"157\",\"158\",\"159\",\"160\",\"161\",\"162\",\"163\",\"164\",\"165\",\"166\",\"167\",\"168\"]', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('63', 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('63', 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('63', 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('64', 'frame_video', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('64', 'destination', 'ubud', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('64', 'post_gallery_image', '[\"169\",\"170\",\"171\",\"172\",\"173\",\"174\",\"175\",\"176\",\"177\",\"178\",\"179\",\"180\",\"181\",\"182\",\"183\",\"184\",\"185\",\"186\",\"187\",\"188\"]', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('64', 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('64', 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('64', 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('65', 'frame_video', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('65', 'destination', 'ubud', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('65', 'post_gallery_image', '[\"189\",\"190\",\"191\",\"192\",\"193\",\"194\",\"195\",\"196\",\"197\",\"198\",\"199\",\"200\",\"201\",\"202\",\"203\",\"204\",\"205\",\"206\",\"207\",\"208\"]', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('65', 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('65', 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('65', 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('66', 'frame_video', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('66', 'destination', 'ubud', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('66', 'post_gallery_image', '[\"209\",\"210\",\"211\",\"212\",\"213\",\"214\"]', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('66', 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('66', 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('66', 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('67', 'frame_video', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('67', 'destination', 'ubud', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('67', 'post_gallery_image', '[\"215\",\"216\",\"217\",\"218\",\"219\",\"220\",\"221\"]', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('67', 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('67', 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('67', 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('68', 'frame_video', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('68', 'destination', 'ubud', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('68', 'post_gallery_image', '[\"222\",\"223\",\"224\",\"225\",\"226\",\"227\",\"228\",\"229\",\"230\",\"231\",\"232\"]', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('68', 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('68', 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('68', 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'size', 'Rooms are 43m2 (forty-three square meters), including the entrance hall, bathroom and private balcony.', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'bedroom', 'Each room has either one king-size (2m x 2m) or two separate single bed (1m x 2m).', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'bathroom', 'One shower and one bath thub.', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'occupancy', 'Each room can accommodate a third person on the day bed/soda maximum three (3) persons per room. An infant\'s cot may be added if required.', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('38', 'amenities', '[\"Air-conditioning\",\"Color satellite television\",\"International Direct Dial (IDD) telephone\",\"Refrigerator/mini bar\",\"Electronic safeTea/coffee-making facilities\",\"Hairdryer\",\"Wifi\",\"Private Balcony\"]', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'corporate_image', '17030572061bab6f709.52500796.jpg', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'brief', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'destination', 'sanur', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'food_drink_submenu', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'food_drink_menu', '[{\"name\":\"Green Pea Cappuccino \",\"desc\":\"smoked salt prawn\\\\n\\\\nnasdasnngdfgdfgdfgdf \\\\ndfgdfgdfgdfgdf\",\"price\":\"8888\",\"category\":\"Maya Sari Selection | Starters\",\"type\":\"gluten-free, vegetarian\"},{\"name\":\"Soto Ayam\",\"desc\":\"turmeric scented chicken and glass noodle pot\",\"price\":\"0\",\"category\":\"Maya Sari Selection | Starters\",\"type\":\"indonesian, considered-spicy\"},{\"name\":\"Gado-Gado \",\"desc\":\"blanched Indonesian vegetables with spicy peanut dip and belinjo nut crackers\",\"price\":\"0\",\"category\":\"Maya Sari Selection | Starters\",\"type\":\"healthy-choice, indonesian\"},{\"name\":\"Homemade Pumpkin and Pear Agnolotti with Spinach\",\"desc\":\"toasted hazelnut and sage butter\",\"price\":\"0\",\"category\":\"Main Courses\",\"type\":\"seasonal\"},{\"name\":\"The Maya Nasi Goreng\",\"desc\":\"stir fried rice, sambals and soya, chicken satay &amp; chicken leg, prawns, pickles and a fried egg\",\"price\":\"0\",\"category\":\"Main Courses\",\"type\":\"\"},{\"name\":\"Tempeh Crusted Sea Bass Fillet\",\"desc\":\"ginger-coconut scented pumpkin puree, water spinach and pickled cucumber\",\"price\":\"0\",\"category\":\"Main Courses\",\"type\":\"\"},{\"name\":\"Bali Fruits\",\"desc\":\"market sweets with mango sorbet\",\"price\":\"0\",\"category\":\"Desserts\",\"type\":\"\"},{\"name\":\"Iced Peanut Tofu\",\"desc\":\"lychee &amp; rambutan compote, coconut cream\",\"price\":\"0\",\"category\":\"Desserts\",\"type\":\"\"},{\"name\":\"Chocolate Ginger Terrine\",\"desc\":\"caramelized banana vanilla ice cream\",\"price\":\"0\",\"category\":\"Desserts\",\"type\":\"\"},{\"name\":\"Green Pea Cappuccino\",\"desc\":\"smoked salt prawn\",\"price\":\"0\",\"category\":\"Dinner Size Paper | Starters\",\"type\":\"\"},{\"name\":\"Soto Ayam\",\"desc\":\"turmeric scented chicken and glass noodle pot\",\"price\":\"0\",\"category\":\"Dinner Size Paper | Starters\",\"type\":\"\"},{\"name\":\"Gado-Gado\",\"desc\":\"blanched Indonesian vegetables with spicy peanut dip and belinjo nut crackers\",\"price\":\"0\",\"category\":\"Dinner Size Paper | Starters\",\"type\":\"\"},{\"name\":\"Organic Bedugul\",\"desc\":\"zucchini flowers and whipped herbed chevre and honey\",\"price\":\"0\",\"category\":\"Dinner Size Paper | Starters\",\"type\":\"\"},{\"name\":\"Cured Pacific Salmon\",\"desc\":\"with orange zest &amp; honey mustard dressing, avocado &amp; grilled pepper salad\",\"price\":\"0\",\"category\":\"Dinner Size Paper | Starters\",\"type\":\"\"},{\"name\":\"Prawn\",\"desc\":\"avocado cress stack on miso &amp; wasabi dressing, smoked salmon sus \",\"price\":\"0\",\"category\":\"Dinner Size Paper | Starters\",\"type\":\"\"}]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'food_drink_menu_category', '[{\"id\":\"5\",\"name\":\"Dinner Size Paper\",\"child\":[{\"id\":\"6\",\"name\":\"Starters\"},{\"id\":\"8\",\"name\":\"Desserts\"}]},{\"id\":\"7\",\"name\":\"Main Courses\"},{\"id\":\"1\",\"name\":\"Maya Sari Selection\",\"child\":[{\"id\":\"2\",\"name\":\"Starters\"},{\"id\":\"4\",\"name\":\"Desserts\"},{\"id\":\"3\",\"name\":\"Main Courses\"}]},{\"id\":\"9\",\"name\":\"asdasd\"}]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'location_map_title', 'Located 35 KmNortheast of Bali\'s International Airport', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'location_map_desc', 'test', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'post_gallery_image', '[\"235\",\"236\",\"237\"]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'corporate_image', '22343572061d21b3dd0.82136135.jpg', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'main_image', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'website_link', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('103', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('103', 'brief', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('103', 'destination', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('103', 'website_link', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('103', 'post_gallery_image', '[\"253\",\"254\",\"255\",\"256\"]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('103', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('103', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('103', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('105', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('105', 'brief', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('105', 'destination', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('105', 'website_link', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('105', 'post_gallery_image', '[\"257\",\"258\"]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('105', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('105', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('105', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('101', 'meta_keywords', '', 'spa-and-wellness');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'restaurant_phone_number', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'phone_number', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'fax_number', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'restaurant_fax_number', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'reservation_phone_number', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'reservation_fax_number', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'fb_link', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'twitter_link', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'instagram_link', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'tripadvisor_link', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'email_address', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'restaurant_email_address', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'reservation_email_address', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'instagram_user', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'coordinate_location', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('110', 'address', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('126', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('126', 'loc_addr_1', 'Jl. Sanur Beach Street Walk, Sanur Kaja, Denpasar Sel., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('125', 'loc_type', 'Hotel', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('125', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('125', 'loc_cordinate', '-8.661185,115.196075', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('124', 'loc_addr_1', 'Jl. Sedap Malam No.531, Sanur Kaja, Denpasar Sel., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('124', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('124', 'loc_cordinate', '-8.672725,115.252380', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('124', 'loc_type', 'Hotel', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('124', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('125', 'loc_addr_1', 'GN.Abang IV No.44, Tegal Kertha, Denpasar Bar., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'booking_link', 'https://gc.synxis.com/rez.aspx?Hotel=63387&amp;Chain=9084&amp;template=GCOMS2&amp;shell=GCF2', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'menu_pdf', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'brief', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('122', 'menu_pdf', '', 'blogs');
INSERT INTO `lumonata_additional_fields` VALUES ('122', 'meta_keywords', '', 'blogs');
INSERT INTO `lumonata_additional_fields` VALUES ('122', 'meta_description', '', 'blogs');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'menu_pdf', 'GOH-2014-2015.pdf', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('45', 'menu_pdf', '1205-NGURAH-REPORT.pdf', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('122', 'frame_video', '', 'blogs');
INSERT INTO `lumonata_additional_fields` VALUES ('122', 'brief', '', 'blogs');
INSERT INTO `lumonata_additional_fields` VALUES ('122', 'destination', '', 'blogs');
INSERT INTO `lumonata_additional_fields` VALUES ('122', 'meta_title', '', 'blogs');
INSERT INTO `lumonata_additional_fields` VALUES ('116', 'loc_addr_1', 'Jl. Nyuh Bojog No.9-13, Ubud, Kabupaten Gianyar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('116', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('116', 'loc_cordinate', '-8.518721,115.258863', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('116', 'loc_type', 'Attractions', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('116', 'loc_corporate', 'ubud', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('117', 'loc_addr_1', 'Jl. Hanoman No.10X, Ubud, Ubud, Kabupaten Gianyar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('117', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('117', 'loc_cordinate', '-8.517787,115.263713', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('117', 'loc_type', 'Restaurants', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('117', 'loc_corporate', 'ubud', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('118', 'loc_addr_1', 'Jl. Raya Goa Gajah No.99, Kemenuh, Sukawati, Kabupaten Gianyar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('118', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('118', 'loc_cordinate', '-8.523475,115.287187', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('118', 'loc_type', 'Attractions', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('118', 'loc_corporate', 'ubud', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('119', 'loc_addr_1', 'Jl. Raya Penestanan, Sayan, Ubud, Kabupaten Gianyar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('119', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('119', 'loc_cordinate', '-8.505564,115.254228', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('119', 'loc_type', 'Attractions', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('119', 'loc_corporate', 'ubud', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('120', 'loc_addr_1', 'Jl. Raya Pengosekan Ubud No.63 X, Ubud, Ubud, Kabupaten Gianyar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('120', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('120', 'loc_cordinate', '-8.518933,115.264571', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('120', 'loc_type', 'Activities', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('120', 'loc_corporate', 'ubud', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('121', 'loc_addr_1', 'Jl. Monkey Forest No.15, Ubud, Ubud, Kabupaten Gianyar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('121', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('121', 'loc_cordinate', '-8.512906,115.260666', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('121', 'loc_type', 'Restaurants', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('121', 'loc_corporate', 'ubud', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('125', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'location_text', 'Maya Ubud Resort &amp; Spa sits in splendid seclusion, bordered by the dramatic Petanu River Valley to the east and the vibrant rice fields of Peliatan on the west. The ten hectare tract of land is itself a tranquil haven of tropical gardens and architect-designed thatched buildings, private pool villas and luxury guestrooms.\r\n\r\nIt is a leisurely twenty-five minute stroll from Maya Ubud to the colorful and bustling village of Ubud, with its fascinating market, art museums and galleries, shopping boutiques and restaurants. A complimentary shuttle service is available between the resort and the central market daily from 9.00 am - 5.00 pm.', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'location_text', 'A cosmopolitan seaside destination that has attracted discerning travellers since the 1930s, Sanur embodies Baliâ€™s relaxed chic lifestyle. Strategically set between the dynamic sidewalk scene and an extensive boardwalk that runs along the length of a tranquil white-sand coastline, Maya Sanurâ€™s location invites you to experience the freedom and convenience of walking to an eclectic collection of international boutiques, restaurants, cafes, and bars.\r\n\r\nConveniently located approximately 25 minutes from Ngurah Rai International Airport, as well as within 45 minutes from many of Baliâ€™s famous holiday destinations, including Ubud, Seminyak, Kuta and Nusa Dua.', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('126', 'loc_cordinate', '-8.674953, 115.263541', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('126', 'loc_type', 'Hotel', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('126', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'hotel_code', '63387', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'template_code', 'GCFMS', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'shell_code', 'SHMS', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'chain_code', '9084', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'hotel_code', '23470', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'chain_code', '9084', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'template_code', 'GCFMU', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'shell_code', 'SHMU', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('127', 'loc_addr_1', 'Gg. Gadung, Sumerta Kaja, Denpasar Tim., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('127', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('127', 'loc_cordinate', '-8.653439,115.231811', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('127', 'loc_type', 'Miscellaneous', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('127', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('128', 'destination', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('128', 'meta_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('128', 'meta_description', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('128', 'meta_keywords', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'meta_title', 'Coba Title', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'meta_description', 'Coba description', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'meta_keywords', 'coba keyword', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('129', 'loc_addr_1', 'Jl. Tanjung No.6, Dangin Puri Kangin, Denpasar Utara, Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('129', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('129', 'loc_cordinate', '-8.675779,115.237617', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('129', 'loc_type', 'Shops', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('129', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('130', 'loc_addr_1', 'Jl. Tukad Yeh Aya No.107, Panjer, Denpasar Sel., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('130', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('130', 'loc_cordinate', '-8.677476,115.233841', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('130', 'loc_type', 'Restaurants', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('130', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('131', 'loc_addr_1', 'Jl. Waribang No.10, Kesiman Petilan, Denpasar Tim., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('131', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('131', 'loc_cordinate', '-8.647269,115.245514', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('131', 'loc_type', 'Attractions', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('131', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('132', 'loc_addr_1', 'Gg. Marga Agung, Pemecutan Klod, Denpasar Bar., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('132', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('132', 'loc_cordinate', '-8.692409,115.185089', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('132', 'loc_type', 'Miscellaneous', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('132', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('133', 'loc_addr_1', 'Jl. Tukad Batangrahi XI AI No.3, Panjer, Denpasar Sel., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('133', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('133', 'loc_cordinate', '-8.686300,115.231438', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('133', 'loc_type', 'Hotel', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('133', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('134', 'loc_addr_1', 'Jl. Slamet Riyadi I No.45, Dauh Puri, Denpasar Bar., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('134', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('134', 'loc_cordinate', '-8.667973,115.220452', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('134', 'loc_type', 'Shops', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('134', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('135', 'loc_addr_1', 'Jl. Tukad Oos, Renon, Denpasar Sel., Kota Denpasar, Bali, Indonesia', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('135', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('135', 'loc_cordinate', '-8.685622,115.245171', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('135', 'loc_type', 'Shops', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('135', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('136', 'loc_addr_1', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('136', 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('136', 'loc_cordinate', '-8.684349,115.235214', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('136', 'loc_type', 'Attractions', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('136', 'loc_corporate', 'sanur', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'fb_page_name', 'Maya Ubud Resort &amp; Spa', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'twitter_username', '@mayaubud', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'instagram_username', '@mayaubud', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'tripadvisor_name', 'Maya Ubud Resort &amp; Spa', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'menu_pdf', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'menu_pdf', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'meta_title', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'meta_description', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'meta_keywords', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'fb_page_name', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'twitter_username', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'instagram_username', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'tripadvisor_name', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'location_map_title', 'Stylish BeachfrontResort', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'location_map_desc', 'Attracted discerning travellers since the 1930s, Sanur embodies Baliâ€™s relaxed chic lifestyle', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('137', 'meta_keywords', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('137', 'frame_video', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('137', 'brief', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('137', 'destination', 'ubud', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('137', 'meta_title', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('137', 'meta_description', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('137', 'menu_pdf', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('137', 'pdf_file', 'VIP-International-Traveller-2-2015.pdf', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('138', 'frame_video', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('138', 'brief', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('138', 'destination', 'ubud', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('138', 'meta_title', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('138', 'meta_description', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('138', 'meta_keywords', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('138', 'main_image', '1424357674db4389c99.97580377.jpg', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('138', 'menu_pdf', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('138', 'pdf_file', 'Mice-BTN-August-Oct-2015.pdf', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('139', 'frame_video', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('139', 'brief', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('139', 'destination', 'ubud', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('139', 'meta_title', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('139', 'meta_description', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('139', 'meta_keywords', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('139', 'main_image', '272057674dea86c344.30458433.jpg', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('139', 'menu_pdf', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('139', 'pdf_file', 'AirAsia-Travel-3-Sixty-December-2015.pdf', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('140', 'frame_video', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('140', 'brief', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('140', 'destination', 'ubud', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('140', 'meta_title', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('140', 'meta_description', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('140', 'meta_keywords', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('140', 'main_image', '3029757674e2447d938.95331809.jpg', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('140', 'menu_pdf', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('140', 'pdf_file', 'Luxury-Hotels-Resorts-January-2015.pdf', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('141', 'frame_video', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('141', 'brief', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('141', 'destination', 'ubud', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('141', 'meta_title', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('141', 'meta_description', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('141', 'meta_keywords', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('141', 'main_image', '1420857674e65c025d9.45716377.jpg', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('141', 'menu_pdf', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('141', 'pdf_file', 'Asia-Dreams-Jan-Feb-2015.pdf', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('142', 'frame_video', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('142', 'brief', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('142', 'destination', 'ubud', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('142', 'meta_title', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('142', 'meta_description', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('142', 'meta_keywords', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('142', 'main_image', '2092157674eb5c27c50.64596821.jpg', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('142', 'menu_pdf', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('142', 'pdf_file', 'Hello-Bali-October-2014.compressed.pdf', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('143', 'frame_video', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('143', 'brief', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('143', 'destination', 'ubud', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('143', 'meta_title', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('143', 'meta_description', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('143', 'meta_keywords', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('143', 'main_image', '1283057674f0a8857d8.20321905.jpg', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('143', 'menu_pdf', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('143', 'pdf_file', 'Hello-Bali-Sept-2014.pdf', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('144', 'frame_video', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('144', 'brief', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('144', 'destination', 'ubud', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('144', 'meta_title', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('144', 'meta_description', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('144', 'meta_keywords', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('144', 'main_image', '1100757674f37027142.16603494.jpg', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('144', 'menu_pdf', '', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('144', 'pdf_file', 'Hello-Bali-Guide-Book-2014.pdf', 'articles');
INSERT INTO `lumonata_additional_fields` VALUES ('70', 'pdf_file', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('146', 'frame_video', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('146', 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('146', 'destination', 'ubud', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('146', 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('146', 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('146', 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('146', 'menu_pdf', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('146', 'pdf_file', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('146', 'main_image', '1639576b7ffabf7b36.17959374.jpg', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('147', 'frame_video', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('147', 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('147', 'destination', 'ubud', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('147', 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('147', 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('147', 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('147', 'main_image', '31394576b80103bea30.53476831.jpg', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('147', 'menu_pdf', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('147', 'pdf_file', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('148', 'frame_video', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('148', 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('148', 'destination', 'ubud', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('148', 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('148', 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('148', 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('148', 'main_image', '19476576b80208fe1d5.13205580.jpg', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('148', 'menu_pdf', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('148', 'pdf_file', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('149', 'frame_video', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('149', 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('149', 'destination', 'ubud', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('149', 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('149', 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('149', 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('149', 'main_image', '11453576b80355bb634.13372091.jpg', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('149', 'menu_pdf', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('149', 'pdf_file', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('150', 'frame_video', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('150', 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('150', 'destination', 'ubud', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('150', 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('150', 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('150', 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('150', 'main_image', '17203576b804e255cd9.04447015.jpg', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('150', 'menu_pdf', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('150', 'pdf_file', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES ('53', 'brief', 'Content needed', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('53', 'menu_pdf', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('53', 'pdf_file', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('49', 'food_drink_menu_category', '[{\"id\":\"1\",\"name\":\"gdgdf\"}]', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'we_email_address', 'sm@mayaubud.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'we_email_address', 'sm@mayasanur.com', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('151', 'frame_video', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('151', 'brief', 'Content needed', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('151', 'destination', 'ubud', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('151', 'meta_title', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('151', 'meta_description', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('151', 'meta_keywords', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('151', 'menu_pdf', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('151', 'pdf_file', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('151', 'post_gallery_image', '[262]', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'offer_booking_link', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'show_on', '0', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('152', 'frame_video', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('152', 'brief', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('152', 'destination', 'sanur', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('152', 'meta_title', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('152', 'meta_description', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('152', 'meta_keywords', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('152', 'menu_pdf', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('152', 'pdf_file', '', 'wedding-events');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'skype_name', 'reservation.mayaresort', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'skype_name', 'reservation.mayasanur', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'offer_booking_link', 'http://bla.com', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('7', 'g_analytic', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('6', 'g_analytic', '', 'destinations');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'pdf_file', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'menu_pdf', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('43', 'pdf_file', '', 'accommodation');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'frame_video', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'brief', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'destination', 'ubud', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'main_image', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'parent', '52', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'meta_title', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'meta_description', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'meta_keywords', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'menu_pdf', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('155', 'pdf_file', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'frame_video', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'brief', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'destination', 'ubud', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'main_image', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'parent', '155', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'meta_title', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'meta_description', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'meta_keywords', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'menu_pdf', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('156', 'pdf_file', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'frame_video', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'brief', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'destination', 'ubud', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'main_image', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'parent', '155', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'meta_title', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'meta_description', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'meta_keywords', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'menu_pdf', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('157', 'pdf_file', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'parent', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('54', 'pdf_file', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('164', 'frame_video', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('164', 'brief', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('163', 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('163', 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('163', 'menu_pdf', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('163', 'pdf_file', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('60', 'show_on', '1', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('56', 'pdf_file', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('163', 'frame_video', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('163', 'brief', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('163', 'destination', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('163', 'website_link', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('163', 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('0', 'menu_pdf', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('0', 'pdf_file', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES ('164', 'destination', 'ubud', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('164', 'parent', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('164', 'meta_title', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('164', 'meta_description', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('164', 'meta_keywords', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('164', 'menu_pdf', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('164', 'pdf_file', '', 'about-us');
INSERT INTO `lumonata_additional_fields` VALUES ('165', 'frame_video', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('165', 'brief', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('165', 'destination', 'ubud', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('165', 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('165', 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('165', 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('165', 'menu_pdf', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('165', 'pdf_file', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('165', 'gallery_video', '[{\"type\":\"0\",\"content\":\"<iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m0!3m2!1sen!2sid!4v1482219354853!6m8!1m7!1s7JXEwydbgmYAAAQ7LvrvPw!2m2!1d-8.511728194994113!2d115.2773140117656!3f331.06688411130153!4f-5.0124264562234515!5f0.7820865974627469\\\"width=\\\"800\\\" height=\\\"600\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\"allowfullscreen><\\/iframe>\"},{\"type\":\"0\",\"content\":\"<iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m0!3m2!1sen!2sid!4v1482219794556!6m8!1m7!1sz3yxPgfOW_UAAAQ7Lv_reg!2m2!1d-8.511320740553261!2d115.2777512689106!3f76.30160567826593!4f-15.186314583106281!5f0.7820865974627469\\\"width=\\\"800\\\" height=\\\"600\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\"allowfullscreen><\\/iframe>\"},{\"type\":\"0\",\"content\":\"<iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m0!3m2!1sen!2sid!4v1482219836228!6m8!1m7!1s_EPYZ6dUggsAAAQ7Lv9c1Q!2m2!1d-8.511622461643231!2d115.2780749916985!3f309!4f0!5f0.7820865974627469\\\"width=\\\"800\\\" height=\\\"600\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\"allowfullscreen><\\/iframe>\"},{\"type\":\"0\",\"content\":\"<iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m0!3m2!1sen!2sid!4v1482219625181!6m8!1m7!1sDZpxk4EKUB0AAAQ7Lv0I3A!2m2!1d-8.51148377514642!2d115.2778756017881!3f194!4f0!5f0.7820865974627469\\\"width=\\\"800\\\" height=\\\"600\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\"allowfullscreen><\\/iframe>\"},{\"type\":\"1\",\"content\":\"<iframe src=\\\"\\/\\/www.youtube.com\\/embed\\/HNTODGBqgA8\\\" frameborder=\\\"0\\\" allowfullscreen><\\/iframe>\",\"url\":\"\\/\\/www.youtube.com\\/embed\\/HNTODGBqgA8\"}]', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'offer_booking_link', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'conversion_value', '200000', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'show_on', '0', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'menu_pdf', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'pdf_file', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'main_image', '2788558a511d8185fb2.14208738.jpg', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'conversion_value_opt', '1', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'frame_video', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'brief', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'destination', 'sanur', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'offer_type', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'featured', '0', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'offer_booking_link', 'http://google.com', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'conversion_value_opt', '0', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'conversion_value', '350000', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'show_on', '0', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'meta_title', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'meta_description', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'meta_keywords', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'menu_pdf', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'pdf_file', '', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('166', 'conversion_currency', 'IDR', 'special-offer');
INSERT INTO `lumonata_additional_fields` VALUES ('59', 'conversion_currency', 'IDR', 'special-offer');

-- ----------------------------
-- Table structure for lumonata_articles
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_articles`;
CREATE TABLE `lumonata_articles` (
  `larticle_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_title` text CHARACTER SET utf8 NOT NULL,
  `larticle_brief` text CHARACTER SET utf8 NOT NULL,
  `larticle_content` longtext CHARACTER SET utf8 NOT NULL,
  `larticle_status` varchar(20) CHARACTER SET utf8 NOT NULL,
  `larticle_type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lcomment_count` bigint(20) NOT NULL,
  `lcount_like` bigint(20) NOT NULL,
  `lsef` text CHARACTER SET utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL DEFAULT '1',
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime NOT NULL,
  `lupdated_by` bigint(20) NOT NULL,
  `ldlu` datetime NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY (`larticle_id`),
  KEY `article_title` (`larticle_title`(255)),
  KEY `type_status_date_by` (`larticle_type`,`larticle_status`,`lpost_date`,`lpost_by`)
) ENGINE=MyISAM AUTO_INCREMENT=167 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_articles
-- ----------------------------
INSERT INTO `lumonata_articles` VALUES ('1', 'Vision', '', '<p>Aenean vitae nunc enim. Donec eget quam suscipit, tempus justo ac, porta justo. Phasellus vel ornare ipsum. Phasellus vitae euismod justo, nec malesuada sem. Etiam suscipit commodo porttitor. Nulla ut iaculis turpis, quis pretium turpis. Curabitur eu tincidunt erat. Etiam tempor egestas felis, vel sodales velit pulvinar ut. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eros magna, maximus non ante at, pretium pulvinar dolor. Quisque vulputate in elit pellentesque tincidunt. Proin at lacinia nibh. Fusce nisi mauris, pellentesque at molestie ut, egestas quis massa.</p>', 'publish', 'pages', 'allowed', '0', '0', 'vision', '156', '1', '2016-03-31 12:00:22', '1', '2016-06-22 10:58:01', '0');
INSERT INTO `lumonata_articles` VALUES ('2', 'People', '', '<p>Vestibulum volutpat nisi eget dictum tristique. Sed et viverra magna. Proin pharetra nibh eu odio fringilla aliquet. Duis finibus lacus et consequat consequat. Integer scelerisque consequat tortor posuere suscipit. Vivamus non arcu nibh. Sed mattis faucibus nulla eu feugiat. Integer tempus neque eget dignissim sodales. Maecenas malesuada lacus vel laoreet pulvinar. Aliquam erat volutpat.</p>', 'unpublish', 'pages', 'allowed', '0', '0', 'people', '155', '1', '2016-03-31 12:01:15', '1', '2016-06-24 10:15:33', '0');
INSERT INTO `lumonata_articles` VALUES ('3', 'Sustainability', '', '<h3><strong>SUSTAINABILITY</strong></h3>\r\n<p>Since its inception 2001, Maya Resorts have been incorporating sustainable tourism principles and                                  practices into its operation. We continuously strive to refine our level of understanding of                                  sustainability and periodically take time to review our sustainable practices in order to expand these                                  practices and implement improvements wherever possible.</p>\r\n<p>We aim to move towards sustainability where all concerns need to be integrated into a business                                  strategy that leads the resort to be mor resilient, pro-active to future challenges and opportunities.                                  Our Sustainable Management Plan ensures long term profitability for the hotel, which will benefit its                                  owners, its employees and its neigbors. Therefore, in May 2011 we took up the challenge to obtain                                  Green Globe certification.</p>\r\n<p>Green Globe is the premier global certification for sustainable travel and tourism. Green Globe                                  Certification offers the world&rsquo;s most recognized and longest running program allowing us, as one of                                  the green leaders in the travel and tourism industry to confidently promote our environmental                                  credentials along with our commitment to the people and prosperity of our destination, the village                                  of Ubud, Bali.</p>\r\n<p>&nbsp;</p>\r\n<p>Our Sustainability Management Plan encompasses 4 key areas:</p>\r\n<p><strong>ENVIRONMENTAL</strong><br /> To be actively involved in conserving resources, reducing pollution, conserving biodiversity,                                  ecosystems and landscapes.</p>\r\n<hr />\r\n<p><strong>SOCIO-CULTURAL</strong><br /> To be involved in corporate social responsibility actions, community development, local                                  employment, fair trade, support local entrepreneurs, respect local communities,                                  implement a policy against commercial exploitation, equitable hiring, employee protection                                  and last but not least, that our business do not jeopardize the provision of basic                                  services, such as water, energy, or sanitation to neighboring communities.</p>\r\n<hr />\r\n<p><strong>QUALITY</strong><br /> Any activity that can sustain itself economically through creating competitive advantages                                  within the industry through with inspired service that not only meets, but exceeds guest                                  expectations; it continues to contribute to the economic well-being of the surrounding                                  community through local ownership, employment, buying local products, etc. A sustainable                                  business should benefit its owners, its employees and its neighborhood.</p>\r\n<hr />\r\n<p><strong>HEALTH &amp; SAFETY</strong><br /> The resort complies with all established health and safety regulations, and ensures that                                  both guest and staff protection instruments are in place.</p>\r\n<p>&nbsp;</p>\r\n<p>We are very aware that sustainability is an ongoing journey, therefore the Sustainability                                  Management Plan, Environmental Policy and Purchasing Policy will be reviewed annually.</p>\r\n<h3><strong>PLANT-A-TREE CAMPAIGN</strong></h3>\r\n<p>Join the Maya Ubud &ldquo;plant-a-tree campaign&rdquo; and help Bali&rsquo;s island bird sanctuary on Nusa Penida.</p>\r\n<p>Maya Ubud is supporting Friends of National Park Foundation (FNPF) &ndash;                                  <a href=\"http://www.fnpf.org\">www.fnpf.org</a>, a local non-profit organization, to assist in their                                  reforestation program on the adjacent island of Nusa Penida, with the aim to restore forest cover                                  from only 5% as of now, to 30% in the future. This reforestation program is also in conjunction                                  with their plan to increase the natural habitat of the endangered species of                                  Jalak Bali (White Balinese Starling) as a sanctuary and breeding ground.</p>\r\n<p>After 2 years of conservation education programmes combined with understanding of Balinese culture,                                  customs and Hindu traditions, FNPF persuaded every village on Nusa Penida island to apply a traditional                                  Balinese village regulation (awig-awig) &nbsp;to protect birds. As a result, the threat from poaching                                  and bird traders was eliminated and the whole island was effectively transformed into a unique, &nbsp;                                 unofficial bird sanctuary.</p>\r\n<p>Hundreds of Bali Starlings and the Java Sparrows have been successfully released to live and breed                                  safely in the wild of Nusa Penida for the past five years under the protection of Nusa Penida                                  communities and through environmental awareness education of the children.</p>\r\n<p>We invite you to support FNPF program by donating USD 20 which covers the cost of growing four                                  saplings from seeds in the FNPF nursery, planting on the land and three years of care. After this time                                  the trees will be strong enough to survive.</p>\r\n<p>You may place your donation in an envelope and leave it at the Front Desk, or charge your room                                  account. All donations will be acknowledged by the FNPF. Donors will receive the FNPF annual newsletter                                  describing the progress of the reforestation project.</p>\r\n<p><a href=\"http://www.mayaresorts.com/en/wp-content/uploads/2015/10/Maya-Ubud-Sustainability-Report-2014.pdf\">Download our Sustainability Booklet here</a>.</p>', 'publish', 'pages', 'allowed', '0', '0', 'sustainability', '154', '1', '2016-03-31 12:02:37', '1', '2016-03-31 15:08:24', '0');
INSERT INTO `lumonata_articles` VALUES ('4', 'Affiliates', '', '<p>Aliquam rhoncus magna id mauris tempus congue. Cras semper vulputate risus in pellentesque. Aliquam vitae justo et leo congue dictum in blandit felis. Class aptent taciti sociosqu ad l itora torquent per conubia nostra, per inceptos himenaeos. Mauris ornare ante velit, eu ullamcorper velit finibus in. Sed nisi purus, eleifend et purus sit amet, egestas lobortis erat. Duis dignissim magna risus, ac lobortis mauris fringilla id.</p>', 'publish', 'pages', 'allowed', '0', '0', 'affiliates', '153', '1', '2016-03-31 12:03:07', '1', '2016-03-31 12:03:07', '0');
INSERT INTO `lumonata_articles` VALUES ('5', 'Contact', '', '<div class=\"row clearfix\">\r\n<div class=\"cols\"><strong>CORPORATE OFFICE</strong>\r\n<p>Maya Resorts, Bali<br /> Jalan Danau Tamblingan No. 89M, P. O. Box 3253<br /> Sanur, Bali, Indonesia 80228<br /> P: +62 361 849 7800<br /> F: +62 361 849 7808<br /> E: info@mayasanur.com</p>\r\n</div>\r\n<div class=\"cols\"><strong>SALES &amp; MARKETING HEAD OFFICE</strong>\r\n<p>Maya Resorts, Bali<br /> Jalan Danau Tamblingan No. 89M, P. O. Box 3253<br /> Sanur, Bali, Indonesia 80228<br /> P: +62 361 849 7800<br /> F: +62 361 849 7808<br /> E: sales@mayaubud.com (for Maya Ubud Resort &amp; Spa)<br /> sales@mayasanur.com (for Maya Sanur Resort &amp; Spa)</p>\r\n</div>\r\n</div>\r\n<div class=\"row clearfix\">\r\n<div class=\"cols\"><strong>MAYA UBUD RESORT &amp; SPA</strong>\r\n<p>Jalan Gunung Sari Peliatan, P. O. Box 1001<br /> Ubud, Bali, Indonesia 80571<br /> P: +62 361 977 888<br /> F: +62 361 977 555<br /> E: info@mayaubud.com<br /><br /> <strong>Reservations &amp; Requests</strong><br /> P: +62 361 977 888<br /> E: reservations@mayaubud.com<br /><br /> <strong>Spa Treatments</strong><br /> P: +62 361 977 888<br /> E: spa@mayaubud.com<br /><br /> <strong>Food &amp; Beverage, Restaurants and Bar</strong><br /> P: +62 361 977 888<br /> E: foodandbeverage@mayaubud.com</p>\r\n</div>\r\n<div class=\"cols\"><strong>MAYA SANUR RESORT &amp; SPA</strong>\r\n<p>Jalan Danau Tamblingan No. 89M, P. O. Box 3253<br /> Sanur, Bali, Indonesia 80228<br /> P: +62 361 849 7800<br /> F: +62 361 849 7808<br /> E: info@mayasanur.com<br /><br /> <strong>Reservations &amp; Requests</strong><br /> P: +62 361 849 7800<br /> E: reservations@mayasanur.com<br /><br /> <strong>Spa Treatments</strong><br /> P: +62 361 849 7800<br /> E: spa@mayasanur.com<br /><br /> <strong>Food &amp; Beverage, Restaurants and Bar</strong><br /> P: +62 361 849 7800<br /> E: foodandbeverage@mayasanur.com</p>\r\n</div>\r\n</div>', 'publish', 'pages', 'allowed', '0', '0', 'contact', '152', '1', '2016-03-31 12:04:47', '1', '2016-03-31 13:50:03', '0');
INSERT INTO `lumonata_articles` VALUES ('6', 'Ubud', '', '<p>Maya Ubud Resort &amp; Spa is bordered by the Petanu River valley to the east and the verdant rice fields of Peliatan on the west.</p>', 'publish', 'destinations', '', '0', '0', 'ubud', '151', '1', '2016-03-31 17:37:08', '1', '2016-07-01 14:49:45', '0');
INSERT INTO `lumonata_articles` VALUES ('7', 'Sanur', '', '<p>Maya Sanur Resort &amp; Spa is a stylish beachfront resort with 103 thoughtfully designed rooms; three elegant swimming pools, including a 158-meter divided lagoon pool; the award-winning Spa at Maya, and three dynamic dining venues, plus a beachfront tree bar with panoramic ocean views on all three levels.</p>', 'publish', 'destinations', '', '0', '0', 'sanur', '150', '1', '2016-03-31 17:39:24', '1', '2016-07-01 14:49:59', '0');
INSERT INTO `lumonata_articles` VALUES ('101', 'Spa Sample', '', '<p>lorem ipsum...</p>', 'publish', 'spa-and-wellness', 'allowed', '0', '0', 'spa-sample', '61', '1', '2016-04-26 09:13:56', '1', '2016-04-26 09:14:16', '0');
INSERT INTO `lumonata_articles` VALUES ('100', 'Spa Sample', '', '<p>lorem ipsum...</p>', 'publish', 'spa-and-wellness', 'allowed', '0', '0', 'spa-sample', '62', '1', '2016-04-26 09:12:45', '1', '2016-04-26 09:12:45', '0');
INSERT INTO `lumonata_articles` VALUES ('38', 'Superior Room', '', '', 'publish', 'accommodation', 'allowed', '0', '0', 'superior-room', '119', '1', '2016-04-07 16:40:58', '1', '2016-04-22 17:44:48', '0');
INSERT INTO `lumonata_articles` VALUES ('70', 'Reef', '', '<p style=\"text-align: justify\">Enjoy the all-day dining experience at Reef, Maya Sanur&rsquo;s elegant beachfront signature restaurant.</p>\r\n<p style=\"text-align: justify\">The morning begins with a glimpse of paradise at Reef as you celebrate each new day.<br /> A beautifully crafted marble and wood buffet offers a tempting array of  classic breakfast favourites, as well as unique Asian-inspired fare. For  something a little more hearty, such as eggs, waffles or crepes  prepared just to your liking, our chefs will work their magic from  Reef&rsquo;s dynamic display kitchen.</p>\r\n<p style=\"text-align: justify\">Indulge in a relaxed casual lunch at  Reef within the comfort of covered dining space, or on a gazebo amongst  poolside reflecting ponds and frangipani trees. Reef&rsquo;s balanced lunch  menu includes healthy salads, featuring fresh produce grown in the cool  hinterlands of Bali, as well as hearty sandwiches, wood-fired pizzas and  fresh seafood.</p>\r\n<p style=\"text-align: justify\">Enhance your next romantic culinary  journey with the illusion of floating atop Reef&rsquo;s reflecting pools, as  five private gazebos invite you to experience Bali&rsquo;s legendary  enchantment. Our culinary team will impress you with a tantalizing  selection of freshly-caught fish, locally sourced shellfish, imported  beef, and Western dishes including pasta and pizza.</p>', 'publish', 'restaurant', 'allowed', '0', '0', 'reef', '87', '1', '2016-04-18 15:28:54', '1', '2016-06-30 13:47:52', '0');
INSERT INTO `lumonata_articles` VALUES ('39', 'Deluxe Room', '', '<p>Rooms in the East wing face either up or down the Petanu River Valley. Rooms in the West wing overlook the rice terraces of Peliatan.</p>\r\n<p>Rooms are 43m2 (forty-three square meters), including the entrance hall, bathroom and private balcony.</p>\r\n<p>All rooms have en suite bathroom with bath tub with overhead shower and hand shower, vanity unit and toilet; air-conditioning; color satellite television; International Direct Dial (IDD) telephone; refrigerator/mini bar; electronic safe; tea/coffee-making facilities; hairdryer; private balcony and wifi</p>\r\n<p>Each room has either one king-size bed (2m x 2m) or two separate single beds (1m x 2m). Each room can accommodate a third person on the day bed/sofa maximum three (3) persons per room. An infant&rsquo;s cot may be added if required.</p>', 'publish', 'accommodation', 'allowed', '0', '0', 'deluxe-room', '118', '1', '2016-04-08 15:01:09', '1', '2016-04-08 15:50:10', '0');
INSERT INTO `lumonata_articles` VALUES ('40', 'Superior Garden Villa', '', '<p>All villas are 50m2 (fifty square meters) &ndash; including terrace. Set on a total personal footprint from 150m2. All villas have en suite bathroom with bath tub with overhead shower and hand shower, vanity unit and toilet; air conditioning; colour satellite television, IDD telephone; refrigerator/mini bar; electronic safe; tea/coffee-making facilities; hairdryer; private terrace and wifi.</p>\r\n<p>Each villa is furnished with canopied beds with netting drapes. They have either one king-size mattress (2m x 2m) or twin mattresses (1m x 2m) on the same bed base set 25cm apart. An extra bed can be added to accommodate a third person after the sofa has been removed &ndash; maximum three persons per room. An infant&rsquo;s cot may be added if required.</p>', 'publish', 'accommodation', 'allowed', '0', '0', 'superior-garden-villa', '117', '1', '2016-04-08 15:02:52', '1', '2016-04-08 15:51:10', '0');
INSERT INTO `lumonata_articles` VALUES ('41', 'Deluxe Pool Villa', '', '<p>All villas are 50m2 (fifty square meters)- including terrace. Set on a total personal footprint from 150m2.</p>\r\n<p>All villas have en suite bathroom with bath tub with overhead shower and hand shower, vanity unit and toilet; air conditioning; colour satellite television, IDD telephone; refrigerator/mini bar; electronic safe; tea/coffee-making facilities; hairdryer; private terrace and wifi.</p>\r\n<p>Pool villas have a plunge pool (4m x 2m x 1.5m deep) and outdoor shower.</p>\r\n<p>Each villa is furnished with canopied beds with netting drapes. They have either one king-size mattress (2m x 2m) or twin mattresses (1m x 2m) on the same bed base set 25cm apart. An extra bed can be added to accommodate a third person after the sofa has been removed &ndash; maximum three persons per room. An infant&rsquo;s cot may be added if required.</p>', 'publish', 'accommodation', 'allowed', '0', '0', 'deluxe-pool-villa', '116', '1', '2016-04-08 15:04:03', '1', '2016-04-08 15:52:19', '0');
INSERT INTO `lumonata_articles` VALUES ('42', 'Duplex Pool Villa', '', '<p>The two duplex pool villas overlook the Petanu River Valley, each with an open-air jaccuzzi and private swimming pool. Villas are 230m2 (two hundred thirty square meters), including pool, bathroom and private balcony.</p>\r\n<p>The soft light of morning heralds another perfect day in paradise as the senses awaken to the promise of things to come. Eyes open to a profusion of colour from nature&rsquo;s own palette. From this tranquil setting can be enjoyed the serene beauty of the lush river valley.</p>\r\n<p>It is here that the special magic of Bali is felt&hellip; a magic that purifies the spirit and begins another day of discovery and fulfilled expectations.</p>', 'publish', 'accommodation', 'allowed', '0', '0', 'duplex-pool-villa', '115', '1', '2016-04-08 15:05:07', '1', '2016-04-08 15:52:58', '0');
INSERT INTO `lumonata_articles` VALUES ('43', 'Presidential Villa', '', '<p style=\"text-align: left\">The Petanu Presidential Villa sits overlooking the swirling river in the valley below, savored from the vast pool deck and master bedroom terrace.</p>\r\n<p style=\"text-align: left\">The classic gated and thatched entranceway is a hint to the realm of personal luxury that waits within. A panorama of tropic vibrancy stretches from across the valley. Foliage reflecting a score of hues offsets delicate blossoms and fragrances.</p>\r\n<p style=\"text-align: left\">Every detail embodies the creative hand of Balinese craftsmen to heighten the senses and calm the spirit. Modern and traditional materials blend, whilst every comfort is thoughtfully designed to create a unique environment of understated luxury and quiet elegance.</p>', 'publish', 'accommodation', 'allowed', '0', '0', 'presidential-villa', '114', '1', '2016-04-08 15:06:07', '1', '2016-07-11 09:25:47', '0');
INSERT INTO `lumonata_articles` VALUES ('44', 'Maya Sari', '', '<p>Maya Sari is the signature restaurant, with indoor dining and alfresco service on the terrace. It adapts seamlessly and with a fresh persona, from breakfast through to dinner.</p>\r\n<p>The sumptuous sunrise buffet is enhanced with classic Balinese misty mornings as viewed toward the river valley. Dinner evokes a touch of casual elegance with international and local favorites from the ala carte menu. The wine list is compiled from the old and new worlds, with many other accompanying beverages.</p>', 'publish', 'restaurant', 'allowed', '0', '0', 'maya-sari', '113', '1', '2016-04-08 16:10:40', '1', '2016-04-25 10:04:25', '0');
INSERT INTO `lumonata_articles` VALUES ('45', 'Asiatique', '', '<p>Maya Sari Asiatique an innovative Asian-style restaurant that features an authentic teppanyaki counter. The menu includes selected specialty dishes from countries of the region, including Thailand, China, India, Japan and of course, the Indonesian Archipelago. Select the preferred level of spiciness and accompany the meal with long refreshing drinks whilst enjoying the simple setting that opens onto a courtyard fountain and informal gardens.</p>', 'publish', 'restaurant', 'allowed', '0', '0', 'asiatique', '112', '1', '2016-04-08 16:15:26', '1', '2016-05-23 12:38:02', '0');
INSERT INTO `lumonata_articles` VALUES ('46', 'River Cafe', '', '<p>Take the elevator down to the Spa at Maya level to discover the infinity-edged rock swimming pool and the River Caf&eacute;. Elevated just above the banks of the swirling Petanu River, the River Caf&eacute;&rsquo;s casual setting is the place to enjoy a menu of snacks, refreshments and luncheon choices. A selection of conscious cuisine dishes and salads are featured, as well as the best personal pizzas in all of Ubud.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>River Cafe&rsquo;s Conscious Cuisine</strong></p>\r\n<p>Wholesome food at it&rsquo;s finest, The River Caf&eacute; is all about helping you maintain a complete balance of wellness whilst at Maya Ubud.</p>\r\n<p>As we have evolved things have become more and more refined. The natural processes of storage and cooking have been replaced with chemicals, microwaves and instant food options. At the River Caf&eacute; try to use the natural elements in food to assist the body in its day to day processes without placing unnecessary strain on it.</p>\r\n<p>We use fresh organic vegetables and herbs from the hills of Bedugul. We are proud supporters of the Slow Food movement and follow its principles including using sustainable produce and only when in season.</p>\r\n<p>Rather than refined white flour we offer wheat alternatives such as quinoa, buckwheat and coconut flour. Instead of breads to start your meal we offer crudite with tahini.</p>\r\n<p>We have a full selection of vegetarian, gluten free along with &lsquo;conscious cuisine&rsquo; options. Clean, fresh, natural and crisp. Textures and ingredients are balanced to nourish the body, mind and soul.</p>', 'publish', 'restaurant', 'allowed', '0', '0', 'river-cafe', '111', '1', '2016-04-08 16:22:59', '1', '2016-04-08 16:22:59', '0');
INSERT INTO `lumonata_articles` VALUES ('47', 'Bar Bedulu', '', '<p>Warm rich woods of Bali and deep comfy club chairs beneath a traditional Balinese thatched roof, sets the congenial atmosphere for the breezy Bar Bedulu. Located just off the main lobby, panoramic views of the valley accompany favorite libations and thirst-quenchers, ice cold brews and creative concotions prepared by an innovative barman.</p>\r\n<p>Snacks may be ordered at any time. Complimentary afternoon tea is served each day from 4.00 pm to 5.00 pm.</p>', 'publish', 'restaurant', 'allowed', '0', '0', 'bar-bedulu', '110', '1', '2016-04-08 16:24:29', '1', '2016-04-08 16:24:29', '0');
INSERT INTO `lumonata_articles` VALUES ('48', 'Romance at Maya', '', '<p>Indulgence your senses in a memorable romantic dinner under the star-studded moonlit sky alongside your loved one.<br /> Aglow with dozens of candles and surrounded by tropical flowers, your personal waiter tends to you as if you&rsquo;re the only two people in the world.</p>', 'publish', 'restaurant', 'allowed', '0', '0', 'romance-at-maya', '109', '1', '2016-04-08 16:25:55', '1', '2016-04-08 16:25:55', '0');
INSERT INTO `lumonata_articles` VALUES ('49', 'Candle Light Dinner', '', '<p>Indulge your senses and share great dining with your loved one. Served 4 courses Maya cuisine selections at the restaurant or in the privacy of your room.</p>', 'publish', 'restaurant', 'allowed', '0', '0', 'candle-light-dinner', '108', '1', '2016-04-08 16:27:00', '1', '2016-04-22 17:55:35', '0');
INSERT INTO `lumonata_articles` VALUES ('50', 'Wedding in Paradise', '', '<p>As you walk down this natural aisle with a gentle breeze in your hair accompanied by the song of birds and the sound of the swirling river waters below as your wedding march, exchange vows on the breathtaking edge of the river valley.</p>\r\n<p>Ubud a magical part of Bali is the ideal location for your wedding.</p>', 'publish', 'wedding-events', 'allowed', '0', '0', 'wedding-in-paradise', '107', '1', '2016-04-08 16:31:59', '1', '2016-04-08 16:31:59', '0');
INSERT INTO `lumonata_articles` VALUES ('51', 'Architecture &amp; Design', '', '<p>Designed by awards winning architect, Budiman Hendropurnomo of Denton Corker Marshall, Maya Ubud Resort &amp; Spa is a combination of new and traditional concepts in design; there are no intricate Balinese paintings, masks or statues; instead a deeper, more ancient concept is celebrated through its landscape and architecture.</p>\r\n<p>Its setting is inspired by traditional Balinese lore of orienting villages along a north-south sacred axis (kaja-kelod), linking the mountains in the centre of the island, the realm of the gods (kaja), to the surrounding seas, the domain of demons (kelod).</p>\r\n<p>A central ceremonial walkway along the central ridge connects important public spaces from the porte-cochere, through the lobby and down to the spa. The villas or dwellings are then positioned on either side creating a village like axis that follows the contours of the land. Four parallel lines in the form of massive stone walls  emphasize this ceremonial spine, the subtlety of which may not be immediately apparent to the casual observer.</p>\r\n<p>Modern functional spaces freely intermingle with these walls, with some going further, as in the case of the restaurant and swimming pool giving the impression that they are suspended between the sky above and the river valley below. The design concept of Maya Ubud Resort &amp; Spa is a celebration of Balinese culture and heritage.</p>\r\n<p>Weathered recycled timbers with modern natural materials are used to create unique interiors rich in character. Old cart wheels have been fashioned into mirror frames, table tops were formerly teak railway sleepers and traditional fish traps and baskets are handcrafted into lamp shades.</p>\r\n<p>Indonesian heritage is represented in the eclectic collection of antiques in the open plan lobby, the restaurants and villas. Modern Balinese style is reflected in the architecture with the use of thatched roofs, blending the buildings into the surrounding environment.</p>', 'publish', 'about-us', 'allowed', '0', '0', 'architecture-design', '16', '1', '2016-04-12 15:56:51', '1', '2016-07-27 03:28:16', '0');
INSERT INTO `lumonata_articles` VALUES ('52', 'Facilities &amp; Activities', '', '<p><strong>Restaurants &amp; Bar</strong></p>\r\n<p>Maya Ubud offers an array of dining and cuisine alternatives. Choose  the style to suit the mood &ndash; from bountiful buffet breakfasts to  candlelight dinners that create special and memorable moments.  Experience sumptuous sunrise buffets enhanced with river valley views at  Maya Sari Restaurant, Asian-style restaurant that features an authentic  teppanyaki counter at Asiatique, savor healthy spa-inspired dishes,  pasta and great pizzas and enjoy the panoramic views of the valley to  accompany refreshing favorite cocktails and ice cold brews.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Spa at Maya</strong></p>\r\n<p>Balancing the human senses through holistic harmony is elevated to  the highest level at the Spa at Maya. From the simplest treatment to an  extended sensory journey, skilled and caring therapists have but one  goal &ndash; to create memorable experiences for guests. These sensual  enjoyments cover the sensations of relaxation, renewal, revitalization  and refreshment&nbsp;beside the swirling waters of Petanu River, amidst the  lushness of the rainforest for a feeling of total euphoria.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Departure Lounge</strong></p>\r\n<p><span class=\"Apple-style-span\">For our late departing guests vacating  their rooms at 12.00 noon an air conditioned lounge is available for  their convenience. The facilities of the lounge include complimentary  tea &amp; coffee service, showers &amp; toilet facilities, reading  material, wireless internet connection, television room and an open air  garden lounge for smokers. Baggage storage and lockers are available  through the Concierge.</span></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Fitness Center</strong></p>\r\n<p>The fully air-conditioned Fitness Center features the latest  state-of-the-art exercise equipment offering our resort guests the  opportunity to enjoy their regular workout whilst on vacation.&nbsp; The  Fitness Center is open daily from 6.00 am to 8.00 pm and offers the  services of a Personal Trainer.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Pools</strong></p>\r\n<p>Two pools set amidst the forest green, overhanging the river valley  and at the riverside and private swimming pools for each deluxe villa,  duplex villa and presidential villa. Suspended between sky above and  valley below, adrift in beauty and tranquility, a chance to retreat and  enjoy the breathtaking vistas.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Tennis</strong></p>\r\n<p>Dedicated to the pursuits of physical and spiritual wellbeing, Maya  Ubud Resort &amp; Spa tennis facilities offer you the opportunity to  practice your volley and serve. Hardcourt, floodlit for night play,  racquets and balls and practice partners available on request. Open for  public. Available from 9.00 am to 8.00 pm. For a practice partner &amp;  tennis lesson, one day&rsquo;s notice is necessary.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Business Center &amp; Library</strong></p>\r\n<p>For your convenience, Maya Ubud Resort &amp; Spa offers complimentary  desktop Apple iMac computers with high speed internet connection and  printer at the Hotel Business Center and Library located in the main  lobby. Wireless internet connection service is also available in all  guest accommodations and public areas free of charge. The operating  hours are open 24 hours a day, 7 days a week. Our professional and  courteous staff shall be ready to assist you with any of your business  needs.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Bale Raga Yoga Studio</strong></p>\r\n<p>Located at the upper level of the fitness center, a generous 187  square meter space provides an excellent venue for daily yoga in a  tranquil atmosphere, with stunning views over the resort gardens and  adjacent rice terraces.</p>', 'publish', 'about-us', 'allowed', '0', '0', 'facilities-activities', '12', '1', '2016-04-13 10:19:19', '1', '2016-07-27 03:28:16', '0');
INSERT INTO `lumonata_articles` VALUES ('53', 'Awards', '', '<p>[awards_content]</p>', 'publish', 'about-us', 'allowed', '0', '0', 'awards', '11', '1', '2016-04-13 10:23:13', '1', '2016-07-27 03:28:16', '0');
INSERT INTO `lumonata_articles` VALUES ('54', 'Shops', '', '<p><a href=\"https://www.youtube.com/watch?v=92FwHPpClcA\">Testing</a></p>\r\n<p>Ubud is now considered to be the artistic and cultural center of  Bali, the home of many famous Balinese artists, museums and galleries.  Maya Ubud Resort &amp; Spa provides a free shuttle service to the Ubud  Central Market, every hour on the hour. The first shuttle departs at  9.00 am each morning, with the last service of the day departing the  hotel at 5.00 pm.</p>\r\n<p>Should you require transportation service outside the above schedule, it is available at charge through the bell captain.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Gallery Boutique:</strong></p>\r\n<p>Located at Lobby level, our Gallery Boutique offers an extensive  collections of beautiful reproductions and genuine antique handicrafts,  hand-loomed fabrics, home-wares, casual wear and souvenir items. Various  Muntigunung products such as cashew nut, rosella tea, rosella sweets  taste good and are great for souvenir. By purchasing Muntigunung  products you have support the Future for Children Foundation  http://www.zukunft-fuer-kinder.ch/en/home.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Maya Style</strong></p>\r\n<p>This elegant shop provides ranges of beautiful items for you to bring  home; dresses, exclusive jewelry, scarves, bags, sandals and much more.  The Maya Style is located below Lobby level.</p>', 'publish', 'about-us', 'allowed', '0', '0', 'shops', '10', '1', '2016-04-13 10:25:03', '1', '2016-12-20 17:14:38', '0');
INSERT INTO `lumonata_articles` VALUES ('57', 'Maya Experience Promotion', '', '<p>Book now and save until 40% on the prevailing Daily Rates for guest rooms and private Bali villas.</p>\r\n<ul>\r\n<li>Include Breakfast</li>\r\n<li>Valid for 1 January 2016 until 31 March 2017</li>\r\n<li>Non refundable</li>\r\n<li>Non combine able with other promotion</li>\r\n</ul>', 'publish', 'special-offer', 'allowed', '0', '0', 'maya-experience-promotion', '100', '1', '2016-04-13 12:12:01', '1', '2016-04-13 12:19:29', '0');
INSERT INTO `lumonata_articles` VALUES ('58', 'Maya Exclusive Early Bird', '', '<p>Book Minimum 60 days in advance and get up to 45% OFF discount from normal internet rate.</p>\r\n<ul>\r\n<li>Valid for 1 January 2016 &ndash; 31 March 2017</li>\r\n<li>Include breakfast</li>\r\n<li>Non-refundable</li>\r\n<li>Promotion cannot be combined with other promotion</li>\r\n</ul>', 'publish', 'special-offer', 'allowed', '0', '0', 'maya-exclusive-early-bird', '99', '1', '2016-04-13 12:13:13', '1', '2016-04-21 14:30:12', '0');
INSERT INTO `lumonata_articles` VALUES ('56', 'April Seasonal Offer', '', '<p>Enjoy 50% Discount from the normal internet rate for stay period 1 April &ndash; 14 July 2016.</p>\r\n<ul>\r\n<li>Booking period valid for : 9 April &ndash; 15 April 2016</li>\r\n<li>30% Discount for all treatment at Spa at Maya</li>\r\n<li>10% Discount at Mayasari Restaurant and River Cafe</li>\r\n<li>Include Breakfast</li>\r\n<li>Non-refundable</li>\r\n<li>Promotion cannot be combined with other promotion</li>\r\n</ul>', 'publish', 'special-offer', 'allowed', '0', '0', 'april-seasonal-offer', '101', '1', '2016-04-13 12:08:45', '1', '2016-11-21 14:23:02', '0');
INSERT INTO `lumonata_articles` VALUES ('59', 'Maya Flexible Promotion', '', '<p>Book now and save until 35% on the prevailing Daily Rates for guest rooms and private Bali villas.</p>\r\n<ul>\r\n<li>Valid for 1 January 2016 â€“ 31 March 2017</li>\r\n<li>Include Breakfast</li>\r\n<li>Flexible booking restriction</li>\r\n<li>Promotion cannot be combined with other promotion</li>\r\n</ul>', 'publish', 'special-offer', 'allowed', '0', '0', 'maya-flexible-promotion', '98', '1', '2016-04-13 12:13:45', '1', '2017-02-16 14:12:34', '0');
INSERT INTO `lumonata_articles` VALUES ('60', 'Maya Daily Rate', '', '<p>Welcoming you to a resort that bordered by the Petanu River valley to  the east and the verdant rice fields of Peliatan on the west. The  ten-hectare tract of land is a tranquil haven of tropical gardens and  108 architect-designed thatched private pool villas and luxury  guestrooms. Price include breakfast and complimentary afternoon tea. No  restriction applied.</p>', 'publish', 'special-offer', 'allowed', '0', '0', 'maya-daily-rate', '97', '1', '2016-04-13 12:14:11', '1', '2016-11-21 14:16:37', '0');
INSERT INTO `lumonata_articles` VALUES ('62', 'The Resort', '', '', 'publish', 'gallery', 'allowed', '0', '0', 'the-resort', '95', '1', '2016-04-13 16:57:51', '1', '2016-04-13 16:57:51', '0');
INSERT INTO `lumonata_articles` VALUES ('63', 'Restaurants &amp; Bar', '', '', 'publish', 'gallery', 'allowed', '0', '0', 'restaurants-bar', '94', '1', '2016-04-13 17:05:11', '1', '2016-04-13 17:05:11', '0');
INSERT INTO `lumonata_articles` VALUES ('64', 'Rooms &amp; Villas', '', '', 'publish', 'gallery', 'allowed', '0', '0', 'rooms-villas', '93', '1', '2016-04-13 17:11:55', '1', '2016-04-13 17:11:55', '0');
INSERT INTO `lumonata_articles` VALUES ('65', 'Spa', '', '', 'publish', 'gallery', 'allowed', '0', '0', 'spa', '92', '1', '2016-04-13 17:15:36', '1', '2016-04-13 17:15:36', '0');
INSERT INTO `lumonata_articles` VALUES ('66', 'Weddings', '', '', 'publish', 'gallery', 'allowed', '0', '0', 'weddings', '91', '1', '2016-04-13 17:17:48', '1', '2016-04-13 17:17:48', '0');
INSERT INTO `lumonata_articles` VALUES ('67', 'Meetings &amp; Events', '', '', 'publish', 'gallery', 'allowed', '0', '0', 'meetings-events', '90', '1', '2016-04-13 17:19:09', '1', '2016-04-13 17:19:09', '0');
INSERT INTO `lumonata_articles` VALUES ('68', 'Leisure Activities', '', '', 'publish', 'gallery', 'allowed', '0', '0', 'leisure-activities', '89', '1', '2016-04-13 17:21:59', '1', '2016-04-13 17:21:59', '0');
INSERT INTO `lumonata_articles` VALUES ('127', 'Coba Aja', '', '', 'publish', 'locations', '', '0', '0', 'coba-aja', '42', '1', '2016-06-06 12:24:09', '1', '2016-06-07 15:35:51', '0');
INSERT INTO `lumonata_articles` VALUES ('128', 'Best Rate Guarantee', '', '<h2><strong>Terms &amp; Conditions</strong></h2>\r\n<p>Maya Resorts ensures that you will find the best room rate for any of our hotels on mayaresorts.com. In order to validate your claim under this Best Rate Guarantee, it must meet the terms and conditions outlined below, and to be submitted in accordance to the claim process.</p>\r\n<ul>\r\n<li>The Best Rate Guarantee is only applicable for bookings made      through Maya Resorts official website <a href=\"http://www.mayaresorts.com\">www.mayaresorts.com</a> </li>\r\n<li>You must have found a lower publicly available rate for the same accommodation within 24 hours of      making your reservation. To be considered the same accommodation, the rate      must be available at the same hotel, same room category, same dates and      length of stay, same number of guests and same cancellations policies /      terms &amp; conditions.</li>\r\n<li>To make the claim, you will need      to submit the Claim Form within 24 hours of your confirmed reservation,      and at least seven (7) working days before your scheduled arrival at the      hotel.</li>\r\n<li>The same room type must be      advertised and available to the general public for booking at the same      time your claim is made. When the eligibility and availability of the      lower rate are verified, we will refund the difference as a credit to your      room account during your stay.</li>\r\n<li>We will not accept any screenshots or other purported evidence      of a lower rate that cannot be independently verified by the hotel nor      will we validate any request that we believe, is the result of a printing      or other error, or is made fraudulently or in bad faith.</li>\r\n<li>The lower rate must be in the same currency as what is      published on alilahotels.com, to alleviate disputes over currency      fluctuations.</li>\r\n<li>For a booking involving a multiple night split stay over      different dates, the Best Rate Guarantee will take into account the total      room cost of the stay as booked at the alilahotels.com rate, with the      total room cost of the stay available at the alternate lower rate, for the      same dates of stay only. This will be treated as one claim for one stay,      and the guest may not submit more than one claim for their split stays.</li>\r\n</ul>\r\n<h2><br /><strong>Exclusions</strong></h2>\r\n<p>The claim will not be applicable if the lower rate sourced involves any of the following;</p>\r\n<ul>\r\n<li>Reservations made less than seven (7) working days prior to      date of arrival.</li>\r\n<li>Websites selling &ldquo;undisclosed&rdquo; hotels on any &ldquo;opaque&rdquo; or      auction sites, where the hotel name is not made known until after the      booking is confirmed, or any private sale and flash sales sites.</li>\r\n<li>Negotiated corporate rates or rates offered on membership      program websites, rewards program, airline consolidator or airline staff      interline rates, or rates obtained via auction or similar process, or      rates available only by using a related promotion not offered to the      general public.</li>\r\n<li>Packaged rates that are sold as part of a travel package      including, but not limited to, accommodation plus airfare and other      destination services.</li>\r\n</ul>\r\n<ul>\r\n</ul>', 'publish', 'pages', 'allowed', '0', '0', 'best-rate-guarantee', '34', '1', '2016-06-06 15:52:03', '1', '2016-06-07 09:16:23', '0');
INSERT INTO `lumonata_articles` VALUES ('126', 'Coba2', '', '', 'publish', 'locations', '', '0', '0', 'coba-2', '43', '1', '2016-05-30 10:59:49', '1', '2016-06-07 15:35:51', '0');
INSERT INTO `lumonata_articles` VALUES ('122', 'asdasd', '', '<p>asdasasd</p>', 'publish', 'blogs', 'allowed', '0', '0', 'asdasd', '40', '1', '2016-05-23 15:02:48', '1', '2016-05-23 15:02:48', '0');
INSERT INTO `lumonata_articles` VALUES ('124', 'Bla', '', '', 'publish', 'locations', '', '0', '0', 'bla', '40', '1', '2016-05-30 10:58:49', '1', '2016-06-07 15:35:51', '0');
INSERT INTO `lumonata_articles` VALUES ('125', 'Coba', '', '', 'publish', 'locations', '', '0', '0', 'coba', '41', '1', '2016-05-30 10:59:07', '1', '2016-06-07 15:35:51', '0');
INSERT INTO `lumonata_articles` VALUES ('116', 'Mandala Wisata Wenara Wana', '', '', 'publish', 'locations', '', '0', '0', 'mandala-wisata-wenara-wana', '35', '1', '2016-05-18 10:07:33', '1', '2016-06-07 15:35:51', '0');
INSERT INTO `lumonata_articles` VALUES ('117', 'Dirty Duck Diner', '', '', 'publish', 'locations', '', '0', '0', 'dirty-duck-diner', '36', '1', '2016-05-18 10:08:35', '1', '2016-06-07 15:35:51', '0');
INSERT INTO `lumonata_articles` VALUES ('118', 'Goa Gajah', '', '', 'publish', 'locations', '', '0', '0', 'goa-gajah', '38', '1', '2016-05-18 10:09:25', '1', '2016-06-07 15:35:05', '0');
INSERT INTO `lumonata_articles` VALUES ('119', 'The Blanco Museum', '', '', 'publish', 'locations', '', '0', '0', 'the-blanco-museum', '38', '1', '2016-05-18 10:10:54', '1', '2016-06-07 15:35:51', '0');
INSERT INTO `lumonata_articles` VALUES ('120', 'Yoga Barn', '', '', 'publish', 'locations', '', '0', '0', 'yoga-barn', '38', '1', '2016-05-18 10:12:14', '1', '2016-06-07 15:26:57', '0');
INSERT INTO `lumonata_articles` VALUES ('121', 'Cafe Wayan', '', '', 'publish', 'locations', '', '0', '0', 'cafe-wayan', '39', '1', '2016-05-18 10:13:02', '1', '2016-06-07 15:35:51', '0');
INSERT INTO `lumonata_articles` VALUES ('129', 'Coba', '', '', 'publish', 'locations', '', '0', '0', 'coba-2', '33', '1', '2016-06-08 15:34:21', '1', '2016-06-08 15:34:21', '0');
INSERT INTO `lumonata_articles` VALUES ('130', 'Coba 2', '', '', 'publish', 'locations', '', '0', '0', 'coba-2', '32', '1', '2016-06-08 15:34:49', '1', '2016-06-08 15:34:49', '0');
INSERT INTO `lumonata_articles` VALUES ('131', 'Coba 3', '', '', 'publish', 'locations', '', '0', '0', 'coba-3', '31', '1', '2016-06-08 15:35:15', '1', '2016-06-08 15:35:15', '0');
INSERT INTO `lumonata_articles` VALUES ('132', 'Coba 4', '', '', 'publish', 'locations', '', '0', '0', 'coba-4', '30', '1', '2016-06-08 15:35:57', '1', '2016-06-08 15:35:57', '0');
INSERT INTO `lumonata_articles` VALUES ('133', 'Coba 5', '', '', 'publish', 'locations', '', '0', '0', 'coba-5', '29', '1', '2016-06-08 15:36:13', '1', '2016-06-08 15:36:13', '0');
INSERT INTO `lumonata_articles` VALUES ('134', 'Coba 6', '', '', 'publish', 'locations', '', '0', '0', 'coba-6', '28', '1', '2016-06-08 15:36:51', '1', '2016-06-08 15:36:51', '0');
INSERT INTO `lumonata_articles` VALUES ('135', 'Coba lah', '', '', 'publish', 'locations', '', '0', '0', 'coba-lah', '27', '1', '2016-06-08 15:42:16', '1', '2016-06-08 15:42:16', '0');
INSERT INTO `lumonata_articles` VALUES ('136', 'Coba DFonk', '', '', 'publish', 'locations', '', '0', '0', 'coba-dfonk', '26', '1', '2016-06-08 15:42:41', '1', '2016-06-08 15:42:41', '0');
INSERT INTO `lumonata_articles` VALUES ('137', 'VIP International Traveller, 2-2015', '', '', 'publish', 'articles', 'allowed', '0', '0', 'vip-international-traveller-2-2015', '17', '1', '2016-06-20 09:12:43', '1', '2016-06-20 05:43:00', '0');
INSERT INTO `lumonata_articles` VALUES ('138', 'Mice BTN, August-Oct 2015', '', '', 'publish', 'articles', 'allowed', '0', '0', 'mice-btn-august-oct-2015', '18', '1', '2016-06-20 09:58:12', '1', '2016-06-20 05:43:00', '0');
INSERT INTO `lumonata_articles` VALUES ('139', 'AirAsia, Travel 3 Sixty, December 2015', '', '', 'publish', 'articles', 'allowed', '0', '0', 'airasia-travel-3-sixty-december-2015', '19', '1', '2016-06-20 09:59:06', '1', '2016-06-20 05:43:00', '0');
INSERT INTO `lumonata_articles` VALUES ('140', 'Luxury Hotels &amp; Resorts, January 2015', '', '', 'publish', 'articles', 'allowed', '0', '0', 'luxury-hotels-resorts-january-2015', '20', '1', '2016-06-20 10:00:04', '1', '2016-06-20 05:43:00', '0');
INSERT INTO `lumonata_articles` VALUES ('141', 'Asia Dreams, Jan-Feb 2015', '', '', 'publish', 'articles', 'allowed', '0', '0', 'asia-dreams-jan-feb-2015', '21', '1', '2016-06-20 10:01:09', '1', '2016-06-20 05:43:00', '0');
INSERT INTO `lumonata_articles` VALUES ('142', 'Hello Bali, October 2014', '', '', 'publish', 'articles', 'allowed', '0', '0', 'hello-bali-october-2014', '22', '1', '2016-06-20 10:02:29', '1', '2016-06-20 05:43:00', '0');
INSERT INTO `lumonata_articles` VALUES ('143', 'Hello Bali, Sept 2014', '', '', 'publish', 'articles', 'allowed', '0', '0', 'hello-bali-sept-2014', '23', '1', '2016-06-20 10:03:54', '1', '2016-06-20 05:43:00', '0');
INSERT INTO `lumonata_articles` VALUES ('144', 'Hello Bali Guide Book, 2014', '', '', 'publish', 'articles', 'allowed', '0', '0', 'hello-bali-guide-book-2014', '24', '1', '2016-06-20 10:04:38', '1', '2016-06-20 05:43:00', '0');
INSERT INTO `lumonata_articles` VALUES ('146', 'Green Globe Certification 2013-2014 Effective of November 2013', '', '', 'publish', 'awards', 'allowed', '0', '0', 'green-globe-certification-2013-2014-effective-of-november-2013', '17', '1', '2016-06-23 13:32:25', '1', '2016-06-23 14:21:46', '0');
INSERT INTO `lumonata_articles` VALUES ('147', 'Awards 2008-2010 The Best in Southnest Asia HAPA Signature Resort Spa', '', '', 'publish', 'awards', 'allowed', '0', '0', 'awards-2008-2010-the-best-in-southnest-asia-hapa-signature-resort-spa', '18', '1', '2016-06-23 13:33:38', '1', '2016-06-23 14:22:08', '0');
INSERT INTO `lumonata_articles` VALUES ('148', 'Zoover Awards 2013 Voted The No 1 in Indonesia', '', '', 'publish', 'awards', 'allowed', '0', '0', 'zoover-awards-2013-voted-the-no-1-in-indonesia', '19', '1', '2016-06-23 13:34:45', '1', '2016-06-23 14:22:24', '0');
INSERT INTO `lumonata_articles` VALUES ('149', 'tripadvisor.com - Travelers Choice 2006 Best Hidden Gem Asia', '', '', 'publish', 'awards', 'allowed', '0', '0', 'tripadvisorcom-travelers-choice-2006-best-hidden-gem-asia', '20', '1', '2016-06-23 13:35:56', '1', '2016-06-23 14:22:45', '0');
INSERT INTO `lumonata_articles` VALUES ('150', 'Tri Hita Karana Award 2013 Emerald Supplementary  Award', '', '', 'publish', 'awards', 'allowed', '0', '0', 'tri-hita-karana-award-2013-emerald-supplementary-award', '21', '1', '2016-06-23 13:37:16', '1', '2016-06-23 14:23:10', '0');
INSERT INTO `lumonata_articles` VALUES ('151', 'Articles', '', '<p>[article_content]</p>', 'publish', 'about-us', 'allowed', '0', '0', 'articles', '17', '1', '2016-06-24 10:45:43', '1', '2016-07-27 03:28:16', '0');
INSERT INTO `lumonata_articles` VALUES ('152', 'Meeting Specifications', '', '<p style=\"text-align: center\">Maya Sanur\'s meeting facilities cater to a wide spectrum of requirements and needs;  be it a small scale company presentation to a full-scale international  conference.</p>\r\n<p style=\"text-align: center\">&nbsp;</p>\r\n<table class=\"membership\" style=\"width: 100%\" border=\"0\">\r\n<thead> \r\n<tr>\r\n<td>\r\n<p>VENUE</p>\r\n</td>\r\n<td>\r\n<p>Dimension (m)</p>\r\n</td>\r\n<td>\r\n<p>Size (m<sup>2</sup>)</p>\r\n</td>\r\n<td>\r\n<p>Height (m)</p>\r\n</td>\r\n<td>\r\n<p>Location</p>\r\n</td>\r\n</tr>\r\n</thead> \r\n<tbody>\r\n<tr>\r\n<td title=\"VENUE\">\r\n<p>Ballroom</p>\r\n</td>\r\n<td title=\"Dimension (m)\">\r\n<p>22.2 x 11</p>\r\n</td>\r\n<td title=\"Size (m2)\">\r\n<p>300</p>\r\n</td>\r\n<td title=\"Height (m)\">\r\n<p>4.4</p>\r\n</td>\r\n<td title=\"Location\">\r\n<p>Level 1</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td title=\"VENUE\">\r\n<p>Seminar Room</p>\r\n</td>\r\n<td title=\"Dimension (m)\">\r\n<p>8.5 x 9.6</p>\r\n</td>\r\n<td title=\"Size (m2)\">\r\n<p>81.5</p>\r\n</td>\r\n<td title=\"Height (m)\">\r\n<p>4</p>\r\n</td>\r\n<td title=\"Location\">\r\n<p>Level 3</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td title=\"VENUE\">\r\n<p>Board Room</p>\r\n</td>\r\n<td title=\"Dimension (m)\">\r\n<p>8 x 6</p>\r\n</td>\r\n<td title=\"Size (m2)\">\r\n<p>48</p>\r\n</td>\r\n<td title=\"Height (m)\">\r\n<p>3.2</p>\r\n</td>\r\n<td title=\"Location\">\r\n<p>Level 3</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td title=\"VENUE\">\r\n<p>The Pavilion Indoor</p>\r\n</td>\r\n<td title=\"Dimension (m)\">\r\n<p>20.5 x 9</p>\r\n</td>\r\n<td title=\"Size (m2)\">\r\n<p>184</p>\r\n</td>\r\n<td title=\"Height (m)\">\r\n<p>4.4</p>\r\n</td>\r\n<td title=\"Location\">\r\n<p>Level 2</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td title=\"VENUE\">\r\n<p>The Pavilion Outdoor</p>\r\n</td>\r\n<td title=\"Dimension (m)\">\r\n<p>21 x 8</p>\r\n</td>\r\n<td title=\"Size (m2)\">\r\n<p>164</p>\r\n</td>\r\n<td title=\"Height (m)\">\r\n<p>-</p>\r\n</td>\r\n<td title=\"Location\">\r\n<p>Level 2</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<table class=\"membership\" style=\"width: 100%\" border=\"0\">\r\n<thead> \r\n<tr>\r\n<td>\r\n<p>CAPACITY</p>\r\n</td>\r\n<td>\r\n<p>Cocktail</p>\r\n</td>\r\n<td>\r\n<p>Bqt Round</p>\r\n</td>\r\n<td>\r\n<p>U-Shape</p>\r\n</td>\r\n<td>\r\n<p>Class Room</p>\r\n</td>\r\n<td>\r\n<p>Board Room</p>\r\n</td>\r\n<td>\r\n<p>Theatre</p>\r\n</td>\r\n</tr>\r\n</thead> \r\n<tbody>\r\n<tr>\r\n<td title=\"CAPACITY\">\r\n<p>Ballroom</p>\r\n</td>\r\n<td title=\"Cocktail\">\r\n<p>200</p>\r\n</td>\r\n<td title=\"Bqt Round\">\r\n<p>120</p>\r\n</td>\r\n<td title=\"U-Shape\">\r\n<p>70</p>\r\n</td>\r\n<td title=\"Class Room\">\r\n<p>108</p>\r\n</td>\r\n<td title=\"Board Room\">\r\n<p>-</p>\r\n</td>\r\n<td title=\"Theatre\">\r\n<p>168</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td title=\"CAPACITY\">\r\n<p>Seminar Room</p>\r\n</td>\r\n<td title=\"Cocktail\">\r\n<p>60</p>\r\n</td>\r\n<td title=\"Bqt Round\">\r\n<p>40</p>\r\n</td>\r\n<td title=\"U-Shape\">\r\n<p>30</p>\r\n</td>\r\n<td title=\"Class Room\">\r\n<p>36</p>\r\n</td>\r\n<td title=\"Board Room\">\r\n<p>16</p>\r\n</td>\r\n<td title=\"Theatre\">\r\n<p>60</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td title=\"CAPACITY\">\r\n<p>Board Room</p>\r\n</td>\r\n<td title=\"Cocktail\">\r\n<p>-</p>\r\n</td>\r\n<td title=\"Bqt Round\">\r\n<p>16</p>\r\n</td>\r\n<td title=\"U-Shape\">\r\n<p>9</p>\r\n</td>\r\n<td title=\"Class Room\">\r\n<p>-</p>\r\n</td>\r\n<td title=\"Board Room\">\r\n<p>12</p>\r\n</td>\r\n<td title=\"Theatre\">\r\n<p>12</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td title=\"CAPACITY\">\r\n<p>The Pavilion Indoor</p>\r\n</td>\r\n<td title=\"Cocktail\">\r\n<p>90</p>\r\n</td>\r\n<td title=\"Bqt Round\">\r\n<p>48</p>\r\n</td>\r\n<td title=\"U-Shape\">\r\n<p>36</p>\r\n</td>\r\n<td title=\"Class Room\">\r\n<p>60</p>\r\n</td>\r\n<td title=\"Board Room\">\r\n<p>34</p>\r\n</td>\r\n<td title=\"Theatre\">\r\n<p>84</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td title=\"CAPACITY\">\r\n<p>The Pavilion Outdoor</p>\r\n</td>\r\n<td title=\"Cocktail\">\r\n<p>120</p>\r\n</td>\r\n<td title=\"Bqt Round\">\r\n<p>64</p>\r\n</td>\r\n<td title=\"U-Shape\">\r\n<p>-</p>\r\n</td>\r\n<td title=\"Class Room\">\r\n<p>-</p>\r\n</td>\r\n<td title=\"Board Room\">\r\n<p>-</p>\r\n</td>\r\n<td title=\"Theatre\">\r\n<p>-</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'publish', 'wedding-events', 'allowed', '0', '0', 'meeting-specifications', '15', '1', '2016-06-24 12:23:12', '1', '2016-06-24 13:41:32', '0');
INSERT INTO `lumonata_articles` VALUES ('155', 'Activities', '', '', 'publish', 'about-us', 'allowed', '0', '0', 'activities', '13', '1', '2016-07-26 17:02:05', '1', '2016-07-27 03:28:16', '0');
INSERT INTO `lumonata_articles` VALUES ('156', 'Cycling', '', '<p>A qualified resort guide will lead you off to discover amazing historical sites, visit ancient temples and simply enjoy the natural beauty of the rice terraces, distant mountains, turbulent rivers and natural springs.</p>\r\n<p><strong>Cycling tour</strong><br />Visit the ancient meditation place of Kebo Iwa (the most powerful warrior in his period) trough a beautiful scenery of Pejeng village and Sanding village including witnessing the activities of seller and buyer at Pejeng traditional market.<br /><br /><em>Duration approximately: 3 hours</em><br /><br /><strong>Ubud cycling</strong><br />This tour going to take you to explore the surrounding country side of ubud, pass villages, rice fields and opportunity to visit Balinese home compound, village temple and meet many Balinese artist along the trip.<br /><br /><em>Duration approximately: 2 hours</em></p>', 'publish', 'about-us', 'allowed', '0', '0', 'cycling', '14', '1', '2016-07-26 17:04:43', '1', '2016-07-27 03:28:16', '0');
INSERT INTO `lumonata_articles` VALUES ('157', 'Light Adventure', '', '<p><strong>White Water Rafting</strong><br />Your holiday in Ubud is not complete without experiencing the exciting white water rafting on the Ayung River. Amaze yourself with breathtaking view, feel the thrill and discover hidden waterfalls along the river. Round trip car transfers and lunch are included. We are working with 2 professional companies which offer world class facilities and safety standards.</p>\r\n<p><strong>Bali Safari &amp; Marine Park</strong><br />Explore the divided continents and islands on a tour at Bali Safari and Marine Park. From Sumatra to Kalimantan, Africa to India, name the animal as you meet them; orang-utans, white Indian tigers, leopards, camels, alligators, eagles and many more. Get entertained by the animals&rsquo; impressive tricks as you&rsquo;re being informed about jungle life.</p>\r\n<p><strong>Bali Agung Theatre featuring &lsquo;The Legend of a Balinese Goddess&rsquo;<br /></strong>The show features a cast of 150 people, 40 puppets and more than 30 animals, including ten elephants &ndash; all sharing a huge 60 by 40 meter stage. &ldquo;Bali Agung&rdquo; has commenced its long-term theatrical run at the new &ldquo;Bali Theatre in the Park&rdquo; at the Bali Safari and Marine Park with shows four times each week from 2.30 to 3.30 pm. A timeless journey through a magical theatrical performance.</p>\r\n<p><strong>Elephant Safari Park Taro<br /></strong>In the Bali Elephant Safari Park you can touch feed and ride an elephant through the tropical jungle of Bali. You will feel like royalty sprung from the Arabian Nights. See elephants painting and take home an elephant artwork helping to save this unique species. Wander through the elephant museum or tropical botanical gardens. It&rsquo;s known that elephant never forgets. Neither will you after mingling with these magnificent creatures.</p>', 'publish', 'about-us', 'allowed', '0', '0', 'light-adventure', '15', '1', '2016-07-26 17:06:55', '1', '2016-07-27 03:28:16', '0');
INSERT INTO `lumonata_articles` VALUES ('163', 'sadasdas', '', '', 'publish', 'restaurant', 'allowed', '0', '0', 'untitled', '4', '1', '2016-10-19 11:22:32', '1', '2016-10-19 11:22:41', '0');
INSERT INTO `lumonata_articles` VALUES ('164', 'asdasd', '', '<p><iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3AtDnEC4zak\" frameborder=\"0\" allowfullscreen></iframe></p>', 'publish', 'about-us', 'allowed', '0', '0', 'asdasd', '3', '1', '2016-12-28 10:47:05', '1', '2016-12-28 11:35:32', '0');
INSERT INTO `lumonata_articles` VALUES ('165', 'Video', '', '', 'publish', 'gallery', 'allowed', '0', '0', 'video', '2', '1', '2017-01-10 14:56:14', '1', '2017-01-10 15:54:42', '0');
INSERT INTO `lumonata_articles` VALUES ('166', 'Maya Sanur Special Offers', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium,  neque eu gravida tristique, orci nibh auctor nisl, ac placerat orci nisl  eu augue. Integer ac pretium urna. Ut sit amet orci in velit mattis  auctor sit amet sit amet lacus. Sed laoreet ex eget arcu fringilla, a  lobortis ligula dignissim. Pellentesque efficitur ligula ut magna  suscipit, nec luctus libero tincidunt. In eget metus at felis volutpat  interdum. Donec lacinia, diam id auctor sollicitudin, ligula justo  ornare nunc, vel ornare nulla arcu at nulla. Morbi semper fringilla  nulla sit amet lobortis.</p>\r\n<p>Suspendisse potenti. Nunc et molestie quam. In hac habitasse platea  dictumst. Donec orci lorem, rhoncus vitae tempus sit amet, rhoncus  vehicula ligula. In diam ante, finibus ut diam in, iaculis rutrum eros.  Nulla aliquam augue metus, non varius dolor placerat a. Curabitur  viverra eros sed dolor semper, sit amet imperdiet lorem lobortis. Morbi  quis eros a felis egestas finibus. Nulla urna nisl, mollis a nibh  lacinia, mattis pretium diam.</p>', 'publish', 'special-offer', 'allowed', '0', '0', 'maya-sanur-special-offers', '1', '1', '2017-02-16 12:09:14', '1', '2017-02-16 14:01:05', '0');

-- ----------------------------
-- Table structure for lumonata_attachment
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_attachment`;
CREATE TABLE `lumonata_attachment` (
  `lattach_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_id` bigint(20) NOT NULL,
  `lattach_loc` text CHARACTER SET utf8 NOT NULL,
  `lattach_loc_thumb` text CHARACTER SET utf8 NOT NULL,
  `lattach_loc_medium` text CHARACTER SET utf8 NOT NULL,
  `lattach_loc_large` text CHARACTER SET utf8 NOT NULL,
  `ltitle` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lalt_text` text CHARACTER SET utf8 NOT NULL,
  `lcaption` varchar(200) CHARACTER SET utf8 NOT NULL,
  `mime_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL DEFAULT '0',
  `upload_date` datetime NOT NULL,
  `date_last_update` datetime NOT NULL,
  PRIMARY KEY (`lattach_id`),
  KEY `article_id` (`larticle_id`),
  KEY `attachment_title` (`ltitle`)
) ENGINE=MyISAM AUTO_INCREMENT=280 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_attachment
-- ----------------------------
INSERT INTO `lumonata_attachment` VALUES ('97', '40', '/lumonata-content/files/201604/garden-villa-01-1460101861.jpg', '/lumonata-content/files/201604/garden-villa-01-1460101861-thumbnail.jpg', '/lumonata-content/files/201604/garden-villa-01-1460101861-medium.jpg', '/lumonata-content/files/201604/garden-villa-01-1460101861-large.jpg', 'garden-villa-01.jpg', '', '', 'image/jpeg', '183', '2016-04-08 15:51:02', '2016-04-08 15:51:02');
INSERT INTO `lumonata_attachment` VALUES ('93', '39', '/lumonata-content/files/201604/deluxe-room-02-1460101803.jpg', '/lumonata-content/files/201604/deluxe-room-02-1460101803-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-room-02-1460101803-medium.jpg', '/lumonata-content/files/201604/deluxe-room-02-1460101803-large.jpg', 'deluxe-room-02.jpg', '', '', 'image/jpeg', '187', '2016-04-08 15:50:04', '2016-04-08 15:50:04');
INSERT INTO `lumonata_attachment` VALUES ('94', '39', '/lumonata-content/files/201604/deluxe-room-03-1460101804.jpg', '/lumonata-content/files/201604/deluxe-room-03-1460101804-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-room-03-1460101804-medium.jpg', '/lumonata-content/files/201604/deluxe-room-03-1460101804-large.jpg', 'deluxe-room-03.jpg', '', '', 'image/jpeg', '186', '2016-04-08 15:50:05', '2016-04-08 15:50:05');
INSERT INTO `lumonata_attachment` VALUES ('95', '39', '/lumonata-content/files/201604/deluxe-room-04-1460101805.jpg', '/lumonata-content/files/201604/deluxe-room-04-1460101805-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-room-04-1460101805-medium.jpg', '/lumonata-content/files/201604/deluxe-room-04-1460101805-large.jpg', 'deluxe-room-04.jpg', '', '', 'image/jpeg', '185', '2016-04-08 15:50:07', '2016-04-08 15:50:07');
INSERT INTO `lumonata_attachment` VALUES ('96', '39', '/lumonata-content/files/201604/deluxe-room-05-1460101807.jpg', '/lumonata-content/files/201604/deluxe-room-05-1460101807-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-room-05-1460101807-medium.jpg', '/lumonata-content/files/201604/deluxe-room-05-1460101807-large.jpg', 'deluxe-room-05.jpg', '', '', 'image/jpeg', '184', '2016-04-08 15:50:08', '2016-04-08 15:50:08');
INSERT INTO `lumonata_attachment` VALUES ('102', '41', '/lumonata-content/files/201604/deluxe-pool-01-1460101923.jpg', '/lumonata-content/files/201604/deluxe-pool-01-1460101923-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-pool-01-1460101923-medium.jpg', '/lumonata-content/files/201604/deluxe-pool-01-1460101923-large.jpg', 'deluxe-pool-01.jpg', '', '', 'image/jpeg', '178', '2016-04-08 15:52:04', '2016-04-08 15:52:04');
INSERT INTO `lumonata_attachment` VALUES ('91', '38', '/lumonata-content/files/201604/superior-room-05-1460101701.jpg', '/lumonata-content/files/201604/superior-room-05-1460101701-thumbnail.jpg', '/lumonata-content/files/201604/superior-room-05-1460101701-medium.jpg', '/lumonata-content/files/201604/superior-room-05-1460101701-large.jpg', 'superior-room-05.jpg', '', '', 'image/jpeg', '189', '2016-04-08 15:48:23', '2016-04-08 15:48:23');
INSERT INTO `lumonata_attachment` VALUES ('90', '38', '/lumonata-content/files/201604/superior-room-04-1460101700.jpg', '/lumonata-content/files/201604/superior-room-04-1460101700-thumbnail.jpg', '/lumonata-content/files/201604/superior-room-04-1460101700-medium.jpg', '/lumonata-content/files/201604/superior-room-04-1460101700-large.jpg', 'superior-room-04.jpg', '', '', 'image/jpeg', '190', '2016-04-08 15:48:21', '2016-04-08 15:48:21');
INSERT INTO `lumonata_attachment` VALUES ('92', '39', '/lumonata-content/files/201604/deluxe-room-01-1460101802.jpg', '/lumonata-content/files/201604/deluxe-room-01-1460101802-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-room-01-1460101802-medium.jpg', '/lumonata-content/files/201604/deluxe-room-01-1460101802-large.jpg', 'deluxe-room-01.jpg', '', '', 'image/jpeg', '188', '2016-04-08 15:50:03', '2016-04-08 15:50:03');
INSERT INTO `lumonata_attachment` VALUES ('89', '38', '/lumonata-content/files/201604/superior-room-03-1460101699.jpg', '/lumonata-content/files/201604/superior-room-03-1460101699-thumbnail.jpg', '/lumonata-content/files/201604/superior-room-03-1460101699-medium.jpg', '/lumonata-content/files/201604/superior-room-03-1460101699-large.jpg', 'superior-room-03.jpg', '', '', 'image/jpeg', '191', '2016-04-08 15:48:20', '2016-04-08 15:48:20');
INSERT INTO `lumonata_attachment` VALUES ('88', '38', '/lumonata-content/files/201604/superior-room-02-1460101698.jpg', '/lumonata-content/files/201604/superior-room-02-1460101698-thumbnail.jpg', '/lumonata-content/files/201604/superior-room-02-1460101698-medium.jpg', '/lumonata-content/files/201604/superior-room-02-1460101698-large.jpg', 'superior-room-02.jpg', '', '', 'image/jpeg', '192', '2016-04-08 15:48:19', '2016-04-08 15:48:19');
INSERT INTO `lumonata_attachment` VALUES ('98', '40', '/lumonata-content/files/201604/garden-villa-02-1460101862.jpg', '/lumonata-content/files/201604/garden-villa-02-1460101862-thumbnail.jpg', '/lumonata-content/files/201604/garden-villa-02-1460101862-medium.jpg', '/lumonata-content/files/201604/garden-villa-02-1460101862-large.jpg', 'garden-villa-02.jpg', '', '', 'image/jpeg', '182', '2016-04-08 15:51:03', '2016-04-08 15:51:03');
INSERT INTO `lumonata_attachment` VALUES ('99', '40', '/lumonata-content/files/201604/garden-villa-03-1460101863.jpg', '/lumonata-content/files/201604/garden-villa-03-1460101863-thumbnail.jpg', '/lumonata-content/files/201604/garden-villa-03-1460101863-medium.jpg', '/lumonata-content/files/201604/garden-villa-03-1460101863-large.jpg', 'garden-villa-03.jpg', '', '', 'image/jpeg', '181', '2016-04-08 15:51:04', '2016-04-08 15:51:04');
INSERT INTO `lumonata_attachment` VALUES ('100', '40', '/lumonata-content/files/201604/garden-villa-04-1460101864.jpg', '/lumonata-content/files/201604/garden-villa-04-1460101864-thumbnail.jpg', '/lumonata-content/files/201604/garden-villa-04-1460101864-medium.jpg', '/lumonata-content/files/201604/garden-villa-04-1460101864-large.jpg', 'garden-villa-04.jpg', '', '', 'image/jpeg', '180', '2016-04-08 15:51:06', '2016-04-08 15:51:06');
INSERT INTO `lumonata_attachment` VALUES ('101', '40', '/lumonata-content/files/201604/garden-villa-05-1460101866.jpg', '/lumonata-content/files/201604/garden-villa-05-1460101866-thumbnail.jpg', '/lumonata-content/files/201604/garden-villa-05-1460101866-medium.jpg', '/lumonata-content/files/201604/garden-villa-05-1460101866-large.jpg', 'garden-villa-05.jpg', '', '', 'image/jpeg', '179', '2016-04-08 15:51:07', '2016-04-08 15:51:07');
INSERT INTO `lumonata_attachment` VALUES ('103', '41', '/lumonata-content/files/201604/deluxe-pool-02-1460101924.jpg', '/lumonata-content/files/201604/deluxe-pool-02-1460101924-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-pool-02-1460101924-medium.jpg', '/lumonata-content/files/201604/deluxe-pool-02-1460101924-large.jpg', 'deluxe-pool-02.jpg', '', '', 'image/jpeg', '177', '2016-04-08 15:52:06', '2016-04-08 15:52:06');
INSERT INTO `lumonata_attachment` VALUES ('104', '41', '/lumonata-content/files/201604/deluxe-pool-03-1460101926.jpg', '/lumonata-content/files/201604/deluxe-pool-03-1460101926-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-pool-03-1460101926-medium.jpg', '/lumonata-content/files/201604/deluxe-pool-03-1460101926-large.jpg', 'deluxe-pool-03.jpg', '', '', 'image/jpeg', '176', '2016-04-08 15:52:07', '2016-04-08 15:52:07');
INSERT INTO `lumonata_attachment` VALUES ('105', '41', '/lumonata-content/files/201604/deluxe-pool-04-1460101927.jpg', '/lumonata-content/files/201604/deluxe-pool-04-1460101927-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-pool-04-1460101927-medium.jpg', '/lumonata-content/files/201604/deluxe-pool-04-1460101927-large.jpg', 'deluxe-pool-04.jpg', '', '', 'image/jpeg', '175', '2016-04-08 15:52:09', '2016-04-08 15:52:09');
INSERT INTO `lumonata_attachment` VALUES ('106', '41', '/lumonata-content/files/201604/deluxe-pool-05-1460101929.jpg', '/lumonata-content/files/201604/deluxe-pool-05-1460101929-thumbnail.jpg', '/lumonata-content/files/201604/deluxe-pool-05-1460101929-medium.jpg', '/lumonata-content/files/201604/deluxe-pool-05-1460101929-large.jpg', 'deluxe-pool-05.jpg', '', '', 'image/jpeg', '174', '2016-04-08 15:52:10', '2016-04-08 15:52:10');
INSERT INTO `lumonata_attachment` VALUES ('116', '44', '/lumonata-content/files/201604/maya-sari-01-1460103025.jpg', '/lumonata-content/files/201604/maya-sari-01-1460103025-thumbnail.jpg', '/lumonata-content/files/201604/maya-sari-01-1460103025-medium.jpg', '/lumonata-content/files/201604/maya-sari-01-1460103025-large.jpg', 'maya-sari-01.jpg', '', '', 'image/jpeg', '164', '2016-04-08 16:10:26', '2016-04-08 16:10:26');
INSERT INTO `lumonata_attachment` VALUES ('110', '42', '/lumonata-content/files/201604/duplex-pool-04-1460101969.jpg', '/lumonata-content/files/201604/duplex-pool-04-1460101969-thumbnail.jpg', '/lumonata-content/files/201604/duplex-pool-04-1460101969-medium.jpg', '/lumonata-content/files/201604/duplex-pool-04-1460101969-large.jpg', 'duplex-pool-04.jpg', '', '', 'image/jpeg', '170', '2016-04-08 15:52:51', '2016-04-08 15:52:51');
INSERT INTO `lumonata_attachment` VALUES ('109', '42', '/lumonata-content/files/201604/duplex-pool-03-1460101968.jpg', '/lumonata-content/files/201604/duplex-pool-03-1460101968-thumbnail.jpg', '/lumonata-content/files/201604/duplex-pool-03-1460101968-medium.jpg', '/lumonata-content/files/201604/duplex-pool-03-1460101968-large.jpg', 'duplex-pool-03.jpg', '', '', 'image/jpeg', '171', '2016-04-08 15:52:49', '2016-04-08 15:52:49');
INSERT INTO `lumonata_attachment` VALUES ('108', '42', '/lumonata-content/files/201604/duplex-pool-02-1460101967.jpg', '/lumonata-content/files/201604/duplex-pool-02-1460101967-thumbnail.jpg', '/lumonata-content/files/201604/duplex-pool-02-1460101967-medium.jpg', '/lumonata-content/files/201604/duplex-pool-02-1460101967-large.jpg', 'duplex-pool-02.jpg', '', '', 'image/jpeg', '172', '2016-04-08 15:52:48', '2016-04-08 15:52:48');
INSERT INTO `lumonata_attachment` VALUES ('107', '42', '/lumonata-content/files/201604/duplex-pool-01-1460101966.jpg', '/lumonata-content/files/201604/duplex-pool-01-1460101966-thumbnail.jpg', '/lumonata-content/files/201604/duplex-pool-01-1460101966-medium.jpg', '/lumonata-content/files/201604/duplex-pool-01-1460101966-large.jpg', 'duplex-pool-01.jpg', '', '', 'image/jpeg', '173', '2016-04-08 15:52:47', '2016-04-08 15:52:47');
INSERT INTO `lumonata_attachment` VALUES ('274', '43', '/lumonata-content/files/201607/deluxe-room-01-1468208689.jpg', '/lumonata-content/files/201607/deluxe-room-01-1468208689-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-room-01-1468208689-medium.jpg', '/lumonata-content/files/201607/deluxe-room-01-1468208689-large.jpg', 'deluxe-room-01.jpg', '', '', 'image/jpeg', '6', '2016-07-11 11:44:50', '2016-07-11 11:44:50');
INSERT INTO `lumonata_attachment` VALUES ('275', '43', '/lumonata-content/files/201607/deluxe-room-02-1468208690.jpg', '/lumonata-content/files/201607/deluxe-room-02-1468208690-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-room-02-1468208690-medium.jpg', '/lumonata-content/files/201607/deluxe-room-02-1468208690-large.jpg', 'deluxe-room-02.jpg', '', '', 'image/jpeg', '5', '2016-07-11 11:44:51', '2016-07-11 11:44:51');
INSERT INTO `lumonata_attachment` VALUES ('276', '43', '/lumonata-content/files/201607/deluxe-room-03-1468208691.jpg', '/lumonata-content/files/201607/deluxe-room-03-1468208691-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-room-03-1468208691-medium.jpg', '/lumonata-content/files/201607/deluxe-room-03-1468208691-large.jpg', 'deluxe-room-03.jpg', '', '', 'image/jpeg', '4', '2016-07-11 11:44:51', '2016-07-11 11:44:51');
INSERT INTO `lumonata_attachment` VALUES ('277', '43', '/lumonata-content/files/201607/deluxe-room-04-1468208691.jpg', '/lumonata-content/files/201607/deluxe-room-04-1468208691-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-room-04-1468208691-medium.jpg', '/lumonata-content/files/201607/deluxe-room-04-1468208691-large.jpg', 'deluxe-room-04.jpg', '', '', 'image/jpeg', '3', '2016-07-11 11:44:52', '2016-07-11 11:44:52');
INSERT INTO `lumonata_attachment` VALUES ('87', '38', '/lumonata-content/files/201604/superior-room-01-1460101698.jpg', '/lumonata-content/files/201604/superior-room-01-1460101698-thumbnail.jpg', '/lumonata-content/files/201604/superior-room-01-1460101698-medium.jpg', '/lumonata-content/files/201604/superior-room-01-1460101698-large.jpg', 'superior-room-01.jpg', '', '', 'image/jpeg', '193', '2016-04-08 15:48:18', '2016-04-08 15:48:18');
INSERT INTO `lumonata_attachment` VALUES ('117', '44', '/lumonata-content/files/201604/maya-sari-02-1460103026.jpg', '/lumonata-content/files/201604/maya-sari-02-1460103026-thumbnail.jpg', '/lumonata-content/files/201604/maya-sari-02-1460103026-medium.jpg', '/lumonata-content/files/201604/maya-sari-02-1460103026-large.jpg', 'maya-sari-02.jpg', '', '', 'image/jpeg', '163', '2016-04-08 16:10:28', '2016-04-08 16:10:28');
INSERT INTO `lumonata_attachment` VALUES ('118', '44', '/lumonata-content/files/201604/maya-sari-03-1460103028.jpg', '/lumonata-content/files/201604/maya-sari-03-1460103028-thumbnail.jpg', '/lumonata-content/files/201604/maya-sari-03-1460103028-medium.jpg', '/lumonata-content/files/201604/maya-sari-03-1460103028-large.jpg', 'maya-sari-03.jpg', '', '', 'image/jpeg', '162', '2016-04-08 16:10:30', '2016-04-08 16:10:30');
INSERT INTO `lumonata_attachment` VALUES ('121', '46', '/lumonata-content/files/201604/river-cafe-01-1460103774.jpg', '/lumonata-content/files/201604/river-cafe-01-1460103774-thumbnail.jpg', '/lumonata-content/files/201604/river-cafe-01-1460103774-medium.jpg', '/lumonata-content/files/201604/river-cafe-01-1460103774-large.jpg', 'river-cafe-01.jpg', '', '', 'image/jpeg', '159', '2016-04-08 16:22:54', '2016-04-08 16:22:54');
INSERT INTO `lumonata_attachment` VALUES ('122', '47', '/lumonata-content/files/201604/bar-bedulu-01-1460103863.jpg', '/lumonata-content/files/201604/bar-bedulu-01-1460103863-thumbnail.jpg', '/lumonata-content/files/201604/bar-bedulu-01-1460103863-medium.jpg', '/lumonata-content/files/201604/bar-bedulu-01-1460103863-large.jpg', 'bar-bedulu-01.jpg', '', '', 'image/jpeg', '158', '2016-04-08 16:24:24', '2016-04-08 16:24:24');
INSERT INTO `lumonata_attachment` VALUES ('123', '48', '/lumonata-content/files/201604/romance-at-maya-01-1460103947.jpg', '/lumonata-content/files/201604/romance-at-maya-01-1460103947-thumbnail.jpg', '/lumonata-content/files/201604/romance-at-maya-01-1460103947-medium.jpg', '/lumonata-content/files/201604/romance-at-maya-01-1460103947-large.jpg', 'romance-at-maya-01.jpg', '', '', 'image/jpeg', '157', '2016-04-08 16:25:48', '2016-04-08 16:25:48');
INSERT INTO `lumonata_attachment` VALUES ('124', '48', '/lumonata-content/files/201604/romance-at-maya-02-1460103948.jpg', '/lumonata-content/files/201604/romance-at-maya-02-1460103948-thumbnail.jpg', '/lumonata-content/files/201604/romance-at-maya-02-1460103948-medium.jpg', '/lumonata-content/files/201604/romance-at-maya-02-1460103948-large.jpg', 'romance-at-maya-02.jpg', '', '', 'image/jpeg', '156', '2016-04-08 16:25:51', '2016-04-08 16:25:51');
INSERT INTO `lumonata_attachment` VALUES ('125', '49', '/lumonata-content/files/201604/candle-light-dinner-01-1460104016.jpg', '/lumonata-content/files/201604/candle-light-dinner-01-1460104016-thumbnail.jpg', '/lumonata-content/files/201604/candle-light-dinner-01-1460104016-medium.jpg', '/lumonata-content/files/201604/candle-light-dinner-01-1460104016-large.jpg', 'candle-light-dinner-01.jpg', '', '', 'image/jpeg', '155', '2016-04-08 16:26:57', '2016-04-08 16:26:57');
INSERT INTO `lumonata_attachment` VALUES ('126', '50', '/lumonata-content/files/201604/wedding-in-paradise-01-1460104308.jpg', '/lumonata-content/files/201604/wedding-in-paradise-01-1460104308-thumbnail.jpg', '/lumonata-content/files/201604/wedding-in-paradise-01-1460104308-medium.jpg', '/lumonata-content/files/201604/wedding-in-paradise-01-1460104308-large.jpg', 'wedding-in-paradise-01.jpg', '', '', 'image/jpeg', '154', '2016-04-08 16:31:49', '2016-04-08 16:31:49');
INSERT INTO `lumonata_attachment` VALUES ('127', '51', '/lumonata-content/files/201604/architecture-design-01-1460447805.jpg', '/lumonata-content/files/201604/architecture-design-01-1460447805-thumbnail.jpg', '/lumonata-content/files/201604/architecture-design-01-1460447805-medium.jpg', '/lumonata-content/files/201604/architecture-design-01-1460447805-large.jpg', 'architecture-design-01.jpg', '', '', 'image/jpeg', '153', '2016-04-12 15:56:47', '2016-04-12 15:56:47');
INSERT INTO `lumonata_attachment` VALUES ('128', '52', '/lumonata-content/files/201604/facilities-activities-01-1460513944.jpg', '/lumonata-content/files/201604/facilities-activities-01-1460513944-thumbnail.jpg', '/lumonata-content/files/201604/facilities-activities-01-1460513944-medium.jpg', '/lumonata-content/files/201604/facilities-activities-01-1460513944-large.jpg', 'facilities-activities-01.jpg', '', '', 'image/jpeg', '152', '2016-04-13 10:19:05', '2016-04-13 10:19:05');
INSERT INTO `lumonata_attachment` VALUES ('129', '53', '/lumonata-content/files/201604/awards-01-1460514189.jpg', '/lumonata-content/files/201604/awards-01-1460514189-thumbnail.jpg', '/lumonata-content/files/201604/awards-01-1460514189-medium.jpg', '/lumonata-content/files/201604/awards-01-1460514189-large.jpg', 'awards-01.jpg', '', '', 'image/jpeg', '151', '2016-04-13 10:23:10', '2016-04-13 10:23:10');
INSERT INTO `lumonata_attachment` VALUES ('130', '54', '/lumonata-content/files/201604/shops-01-1460514298.jpg', '/lumonata-content/files/201604/shops-01-1460514298-thumbnail.jpg', '/lumonata-content/files/201604/shops-01-1460514298-medium.jpg', '/lumonata-content/files/201604/shops-01-1460514298-large.jpg', 'shops-01.jpg', '', '', 'image/jpeg', '150', '2016-04-13 10:24:59', '2016-04-13 10:24:59');
INSERT INTO `lumonata_attachment` VALUES ('131', '54', '/lumonata-content/files/201604/shops-02-1460514299.jpg', '/lumonata-content/files/201604/shops-02-1460514299-thumbnail.jpg', '/lumonata-content/files/201604/shops-02-1460514299-medium.jpg', '/lumonata-content/files/201604/shops-02-1460514299-large.jpg', 'shops-02.jpg', '', '', 'image/jpeg', '149', '2016-04-13 10:25:00', '2016-04-13 10:25:00');
INSERT INTO `lumonata_attachment` VALUES ('132', '56', '/lumonata-content/files/201604/villa-in-ubud-maya-ubud-resort-spa-best-hotel-spa-pool-in-bali-1460520548.jpg', '/lumonata-content/files/201604/villa-in-ubud-maya-ubud-resort-spa-best-hotel-spa-pool-in-bali-1460520548-thumbnail.jpg', '/lumonata-content/files/201604/villa-in-ubud-maya-ubud-resort-spa-best-hotel-spa-pool-in-bali-1460520548-medium.jpg', '/lumonata-content/files/201604/villa-in-ubud-maya-ubud-resort-spa-best-hotel-spa-pool-in-bali-1460520548-large.jpg', 'villa-in-ubud-maya-ubud-resort-spa-best-hotel-spa-pool-in-bali.jpg', '', '', 'image/jpeg', '148', '2016-04-13 12:09:09', '2016-04-13 12:09:09');
INSERT INTO `lumonata_attachment` VALUES ('133', '57', '/lumonata-content/files/201604/114283791154354964581395440469842n-1460520716.jpg', '/lumonata-content/files/201604/114283791154354964581395440469842n-1460520716-thumbnail.jpg', '/lumonata-content/files/201604/114283791154354964581395440469842n-1460520716-medium.jpg', '/lumonata-content/files/201604/114283791154354964581395440469842n-1460520716-large.jpg', '11428379_1154354964581395_440469842_n.jpg', '', '', 'image/jpeg', '147', '2016-04-13 12:11:57', '2016-04-13 12:11:57');
INSERT INTO `lumonata_attachment` VALUES ('134', '58', '/lumonata-content/files/201604/main-pool-20092-e1453173647532-1460520780.jpg', '/lumonata-content/files/201604/main-pool-20092-e1453173647532-1460520780-thumbnail.jpg', '/lumonata-content/files/201604/main-pool-20092-e1453173647532-1460520780-medium.jpg', '/lumonata-content/files/201604/main-pool-20092-e1453173647532-1460520780-large.jpg', 'main-pool-2009_2-e1453173647532.jpg', '', '', 'image/jpeg', '146', '2016-04-13 12:13:00', '2016-04-13 12:13:00');
INSERT INTO `lumonata_attachment` VALUES ('137', '62', '/lumonata-content/files/201604/resort-01-1460537847.jpg', '/lumonata-content/files/201604/resort-01-1460537847-thumbnail.jpg', '/lumonata-content/files/201604/resort-01-1460537847-medium.jpg', '/lumonata-content/files/201604/resort-01-1460537847-large.jpg', 'resort-01.jpg', '', '', 'image/jpeg', '143', '2016-04-13 16:57:28', '2016-04-13 16:57:28');
INSERT INTO `lumonata_attachment` VALUES ('138', '62', '/lumonata-content/files/201604/resort-02-1460537848.jpg', '/lumonata-content/files/201604/resort-02-1460537848-thumbnail.jpg', '/lumonata-content/files/201604/resort-02-1460537848-medium.jpg', '/lumonata-content/files/201604/resort-02-1460537848-large.jpg', 'resort-02.jpg', '', '', 'image/jpeg', '142', '2016-04-13 16:57:28', '2016-04-13 16:57:28');
INSERT INTO `lumonata_attachment` VALUES ('139', '62', '/lumonata-content/files/201604/resort-03-1460537848.jpg', '/lumonata-content/files/201604/resort-03-1460537848-thumbnail.jpg', '/lumonata-content/files/201604/resort-03-1460537848-medium.jpg', '/lumonata-content/files/201604/resort-03-1460537848-large.jpg', 'resort-03.jpg', '', '', 'image/jpeg', '141', '2016-04-13 16:57:29', '2016-04-13 16:57:29');
INSERT INTO `lumonata_attachment` VALUES ('140', '62', '/lumonata-content/files/201604/resort-04-1460537849.jpg', '/lumonata-content/files/201604/resort-04-1460537849-thumbnail.jpg', '/lumonata-content/files/201604/resort-04-1460537849-medium.jpg', '/lumonata-content/files/201604/resort-04-1460537849-large.jpg', 'resort-04.jpg', '', '', 'image/jpeg', '140', '2016-04-13 16:57:29', '2016-04-13 16:57:29');
INSERT INTO `lumonata_attachment` VALUES ('141', '62', '/lumonata-content/files/201604/resort-05-1460537849.jpg', '/lumonata-content/files/201604/resort-05-1460537849-thumbnail.jpg', '/lumonata-content/files/201604/resort-05-1460537849-medium.jpg', '/lumonata-content/files/201604/resort-05-1460537849-large.jpg', 'resort-05.jpg', '', '', 'image/jpeg', '139', '2016-04-13 16:57:30', '2016-04-13 16:57:30');
INSERT INTO `lumonata_attachment` VALUES ('142', '62', '/lumonata-content/files/201604/resort-06-1460537850.jpg', '/lumonata-content/files/201604/resort-06-1460537850-thumbnail.jpg', '/lumonata-content/files/201604/resort-06-1460537850-medium.jpg', '/lumonata-content/files/201604/resort-06-1460537850-large.jpg', 'resort-06.jpg', '', '', 'image/jpeg', '138', '2016-04-13 16:57:31', '2016-04-13 16:57:31');
INSERT INTO `lumonata_attachment` VALUES ('143', '62', '/lumonata-content/files/201604/resort-07-1460537851.jpg', '/lumonata-content/files/201604/resort-07-1460537851-thumbnail.jpg', '/lumonata-content/files/201604/resort-07-1460537851-medium.jpg', '/lumonata-content/files/201604/resort-07-1460537851-large.jpg', 'resort-07.jpg', '', '', 'image/jpeg', '137', '2016-04-13 16:57:31', '2016-04-13 16:57:31');
INSERT INTO `lumonata_attachment` VALUES ('144', '62', '/lumonata-content/files/201604/resort-08-1460537851.jpg', '/lumonata-content/files/201604/resort-08-1460537851-thumbnail.jpg', '/lumonata-content/files/201604/resort-08-1460537851-medium.jpg', '/lumonata-content/files/201604/resort-08-1460537851-large.jpg', 'resort-08.jpg', '', '', 'image/jpeg', '136', '2016-04-13 16:57:32', '2016-04-13 16:57:32');
INSERT INTO `lumonata_attachment` VALUES ('145', '62', '/lumonata-content/files/201604/resort-09-1460537852.jpg', '/lumonata-content/files/201604/resort-09-1460537852-thumbnail.jpg', '/lumonata-content/files/201604/resort-09-1460537852-medium.jpg', '/lumonata-content/files/201604/resort-09-1460537852-large.jpg', 'resort-09.jpg', '', '', 'image/jpeg', '135', '2016-04-13 16:57:32', '2016-04-13 16:57:32');
INSERT INTO `lumonata_attachment` VALUES ('146', '62', '/lumonata-content/files/201604/resort-10-1460537852.jpg', '/lumonata-content/files/201604/resort-10-1460537852-thumbnail.jpg', '/lumonata-content/files/201604/resort-10-1460537852-medium.jpg', '/lumonata-content/files/201604/resort-10-1460537852-large.jpg', 'resort-10.jpg', '', '', 'image/jpeg', '134', '2016-04-13 16:57:33', '2016-04-13 16:57:33');
INSERT INTO `lumonata_attachment` VALUES ('147', '62', '/lumonata-content/files/201604/resort-11-1460537853.jpg', '/lumonata-content/files/201604/resort-11-1460537853-thumbnail.jpg', '/lumonata-content/files/201604/resort-11-1460537853-medium.jpg', '/lumonata-content/files/201604/resort-11-1460537853-large.jpg', 'resort-11.jpg', '', '', 'image/jpeg', '133', '2016-04-13 16:57:33', '2016-04-13 16:57:33');
INSERT INTO `lumonata_attachment` VALUES ('148', '62', '/lumonata-content/files/201604/resort-12-1460537853.jpg', '/lumonata-content/files/201604/resort-12-1460537853-thumbnail.jpg', '/lumonata-content/files/201604/resort-12-1460537853-medium.jpg', '/lumonata-content/files/201604/resort-12-1460537853-large.jpg', 'resort-12.jpg', '', '', 'image/jpeg', '132', '2016-04-13 16:57:34', '2016-04-13 16:57:34');
INSERT INTO `lumonata_attachment` VALUES ('149', '63', '/lumonata-content/files/201604/resto-01-1460538291.jpg', '/lumonata-content/files/201604/resto-01-1460538291-thumbnail.jpg', '/lumonata-content/files/201604/resto-01-1460538291-medium.jpg', '/lumonata-content/files/201604/resto-01-1460538291-large.jpg', 'resto-01.jpg', '', '', 'image/jpeg', '131', '2016-04-13 17:04:51', '2016-04-13 17:04:51');
INSERT INTO `lumonata_attachment` VALUES ('150', '63', '/lumonata-content/files/201604/resto-02-1460538291.jpg', '/lumonata-content/files/201604/resto-02-1460538291-thumbnail.jpg', '/lumonata-content/files/201604/resto-02-1460538291-medium.jpg', '/lumonata-content/files/201604/resto-02-1460538291-large.jpg', 'resto-02.jpg', '', '', 'image/jpeg', '130', '2016-04-13 17:04:52', '2016-04-13 17:04:52');
INSERT INTO `lumonata_attachment` VALUES ('151', '63', '/lumonata-content/files/201604/resto-03-1460538292.jpg', '/lumonata-content/files/201604/resto-03-1460538292-thumbnail.jpg', '/lumonata-content/files/201604/resto-03-1460538292-medium.jpg', '/lumonata-content/files/201604/resto-03-1460538292-large.jpg', 'resto-03.jpg', '', '', 'image/jpeg', '129', '2016-04-13 17:04:52', '2016-04-13 17:04:52');
INSERT INTO `lumonata_attachment` VALUES ('152', '63', '/lumonata-content/files/201604/resto-04-1460538292.jpg', '/lumonata-content/files/201604/resto-04-1460538292-thumbnail.jpg', '/lumonata-content/files/201604/resto-04-1460538292-medium.jpg', '/lumonata-content/files/201604/resto-04-1460538292-large.jpg', 'resto-04.jpg', '', '', 'image/jpeg', '128', '2016-04-13 17:04:53', '2016-04-13 17:04:53');
INSERT INTO `lumonata_attachment` VALUES ('153', '63', '/lumonata-content/files/201604/resto-05-1460538293.jpg', '/lumonata-content/files/201604/resto-05-1460538293-thumbnail.jpg', '/lumonata-content/files/201604/resto-05-1460538293-medium.jpg', '/lumonata-content/files/201604/resto-05-1460538293-large.jpg', 'resto-05.jpg', '', '', 'image/jpeg', '127', '2016-04-13 17:04:53', '2016-04-13 17:04:53');
INSERT INTO `lumonata_attachment` VALUES ('154', '63', '/lumonata-content/files/201604/resto-06-1460538293.jpg', '/lumonata-content/files/201604/resto-06-1460538293-thumbnail.jpg', '/lumonata-content/files/201604/resto-06-1460538293-medium.jpg', '/lumonata-content/files/201604/resto-06-1460538293-large.jpg', 'resto-06.jpg', '', '', 'image/jpeg', '126', '2016-04-13 17:04:54', '2016-04-13 17:04:54');
INSERT INTO `lumonata_attachment` VALUES ('155', '63', '/lumonata-content/files/201604/resto-07-1460538294.jpg', '/lumonata-content/files/201604/resto-07-1460538294-thumbnail.jpg', '/lumonata-content/files/201604/resto-07-1460538294-medium.jpg', '/lumonata-content/files/201604/resto-07-1460538294-large.jpg', 'resto-07.jpg', '', '', 'image/jpeg', '125', '2016-04-13 17:04:54', '2016-04-13 17:04:54');
INSERT INTO `lumonata_attachment` VALUES ('156', '63', '/lumonata-content/files/201604/resto-08-1460538294.jpg', '/lumonata-content/files/201604/resto-08-1460538294-thumbnail.jpg', '/lumonata-content/files/201604/resto-08-1460538294-medium.jpg', '/lumonata-content/files/201604/resto-08-1460538294-large.jpg', 'resto-08.jpg', '', '', 'image/jpeg', '124', '2016-04-13 17:04:55', '2016-04-13 17:04:55');
INSERT INTO `lumonata_attachment` VALUES ('157', '63', '/lumonata-content/files/201604/resto-09-1460538295.jpg', '/lumonata-content/files/201604/resto-09-1460538295-thumbnail.jpg', '/lumonata-content/files/201604/resto-09-1460538295-medium.jpg', '/lumonata-content/files/201604/resto-09-1460538295-large.jpg', 'resto-09.jpg', '', '', 'image/jpeg', '123', '2016-04-13 17:04:55', '2016-04-13 17:04:55');
INSERT INTO `lumonata_attachment` VALUES ('158', '63', '/lumonata-content/files/201604/resto-10-1460538295.jpg', '/lumonata-content/files/201604/resto-10-1460538295-thumbnail.jpg', '/lumonata-content/files/201604/resto-10-1460538295-medium.jpg', '/lumonata-content/files/201604/resto-10-1460538295-large.jpg', 'resto-10.jpg', '', '', 'image/jpeg', '122', '2016-04-13 17:04:56', '2016-04-13 17:04:56');
INSERT INTO `lumonata_attachment` VALUES ('159', '63', '/lumonata-content/files/201604/resto-11-1460538296.jpg', '/lumonata-content/files/201604/resto-11-1460538296-thumbnail.jpg', '/lumonata-content/files/201604/resto-11-1460538296-medium.jpg', '/lumonata-content/files/201604/resto-11-1460538296-large.jpg', 'resto-11.jpg', '', '', 'image/jpeg', '121', '2016-04-13 17:04:56', '2016-04-13 17:04:56');
INSERT INTO `lumonata_attachment` VALUES ('160', '63', '/lumonata-content/files/201604/resto-12-1460538296.jpg', '/lumonata-content/files/201604/resto-12-1460538296-thumbnail.jpg', '/lumonata-content/files/201604/resto-12-1460538296-medium.jpg', '/lumonata-content/files/201604/resto-12-1460538296-large.jpg', 'resto-12.jpg', '', '', 'image/jpeg', '120', '2016-04-13 17:04:57', '2016-04-13 17:04:57');
INSERT INTO `lumonata_attachment` VALUES ('161', '63', '/lumonata-content/files/201604/resto-13-1460538297.jpg', '/lumonata-content/files/201604/resto-13-1460538297-thumbnail.jpg', '/lumonata-content/files/201604/resto-13-1460538297-medium.jpg', '/lumonata-content/files/201604/resto-13-1460538297-large.jpg', 'resto-13.jpg', '', '', 'image/jpeg', '119', '2016-04-13 17:04:57', '2016-04-13 17:04:57');
INSERT INTO `lumonata_attachment` VALUES ('162', '63', '/lumonata-content/files/201604/resto-14-1460538297.jpg', '/lumonata-content/files/201604/resto-14-1460538297-thumbnail.jpg', '/lumonata-content/files/201604/resto-14-1460538297-medium.jpg', '/lumonata-content/files/201604/resto-14-1460538297-large.jpg', 'resto-14.jpg', '', '', 'image/jpeg', '118', '2016-04-13 17:04:58', '2016-04-13 17:04:58');
INSERT INTO `lumonata_attachment` VALUES ('163', '63', '/lumonata-content/files/201604/resto-15-1460538298.jpg', '/lumonata-content/files/201604/resto-15-1460538298-thumbnail.jpg', '/lumonata-content/files/201604/resto-15-1460538298-medium.jpg', '/lumonata-content/files/201604/resto-15-1460538298-large.jpg', 'resto-15.jpg', '', '', 'image/jpeg', '117', '2016-04-13 17:04:58', '2016-04-13 17:04:58');
INSERT INTO `lumonata_attachment` VALUES ('164', '63', '/lumonata-content/files/201604/resto-16-1460538298.jpg', '/lumonata-content/files/201604/resto-16-1460538298-thumbnail.jpg', '/lumonata-content/files/201604/resto-16-1460538298-medium.jpg', '/lumonata-content/files/201604/resto-16-1460538298-large.jpg', 'resto-16.jpg', '', '', 'image/jpeg', '116', '2016-04-13 17:04:59', '2016-04-13 17:04:59');
INSERT INTO `lumonata_attachment` VALUES ('165', '63', '/lumonata-content/files/201604/resto-17-1460538299.jpg', '/lumonata-content/files/201604/resto-17-1460538299-thumbnail.jpg', '/lumonata-content/files/201604/resto-17-1460538299-medium.jpg', '/lumonata-content/files/201604/resto-17-1460538299-large.jpg', 'resto-17.jpg', '', '', 'image/jpeg', '115', '2016-04-13 17:04:59', '2016-04-13 17:04:59');
INSERT INTO `lumonata_attachment` VALUES ('166', '63', '/lumonata-content/files/201604/resto-18-1460538299.jpg', '/lumonata-content/files/201604/resto-18-1460538299-thumbnail.jpg', '/lumonata-content/files/201604/resto-18-1460538299-medium.jpg', '/lumonata-content/files/201604/resto-18-1460538299-large.jpg', 'resto-18.jpg', '', '', 'image/jpeg', '114', '2016-04-13 17:05:00', '2016-04-13 17:05:00');
INSERT INTO `lumonata_attachment` VALUES ('167', '63', '/lumonata-content/files/201604/resto-19-1460538300.jpg', '/lumonata-content/files/201604/resto-19-1460538300-thumbnail.jpg', '/lumonata-content/files/201604/resto-19-1460538300-medium.jpg', '/lumonata-content/files/201604/resto-19-1460538300-large.jpg', 'resto-19.jpg', '', '', 'image/jpeg', '113', '2016-04-13 17:05:00', '2016-04-13 17:05:00');
INSERT INTO `lumonata_attachment` VALUES ('168', '63', '/lumonata-content/files/201604/resto-20-1460538300.jpg', '/lumonata-content/files/201604/resto-20-1460538300-thumbnail.jpg', '/lumonata-content/files/201604/resto-20-1460538300-medium.jpg', '/lumonata-content/files/201604/resto-20-1460538300-large.jpg', 'resto-20.jpg', '', '', 'image/jpeg', '112', '2016-04-13 17:05:00', '2016-04-13 17:05:00');
INSERT INTO `lumonata_attachment` VALUES ('169', '64', '/lumonata-content/files/201604/villa-01-1460538633.jpg', '/lumonata-content/files/201604/villa-01-1460538633-thumbnail.jpg', '/lumonata-content/files/201604/villa-01-1460538633-medium.jpg', '/lumonata-content/files/201604/villa-01-1460538633-large.jpg', 'villa-01.jpg', '', '', 'image/jpeg', '111', '2016-04-13 17:10:34', '2016-04-13 17:10:34');
INSERT INTO `lumonata_attachment` VALUES ('170', '64', '/lumonata-content/files/201604/villa-02-1460538634.jpg', '/lumonata-content/files/201604/villa-02-1460538634-thumbnail.jpg', '/lumonata-content/files/201604/villa-02-1460538634-medium.jpg', '/lumonata-content/files/201604/villa-02-1460538634-large.jpg', 'villa-02.jpg', '', '', 'image/jpeg', '110', '2016-04-13 17:10:34', '2016-04-13 17:10:34');
INSERT INTO `lumonata_attachment` VALUES ('171', '64', '/lumonata-content/files/201604/villa-03-1460538634.jpg', '/lumonata-content/files/201604/villa-03-1460538634-thumbnail.jpg', '/lumonata-content/files/201604/villa-03-1460538634-medium.jpg', '/lumonata-content/files/201604/villa-03-1460538634-large.jpg', 'villa-03.jpg', '', '', 'image/jpeg', '109', '2016-04-13 17:10:34', '2016-04-13 17:10:34');
INSERT INTO `lumonata_attachment` VALUES ('172', '64', '/lumonata-content/files/201604/villa-04-1460538634.jpg', '/lumonata-content/files/201604/villa-04-1460538634-thumbnail.jpg', '/lumonata-content/files/201604/villa-04-1460538634-medium.jpg', '/lumonata-content/files/201604/villa-04-1460538634-large.jpg', 'villa-04.jpg', '', '', 'image/jpeg', '108', '2016-04-13 17:10:35', '2016-04-13 17:10:35');
INSERT INTO `lumonata_attachment` VALUES ('173', '64', '/lumonata-content/files/201604/villa-05-1460538635.jpg', '/lumonata-content/files/201604/villa-05-1460538635-thumbnail.jpg', '/lumonata-content/files/201604/villa-05-1460538635-medium.jpg', '/lumonata-content/files/201604/villa-05-1460538635-large.jpg', 'villa-05.jpg', '', '', 'image/jpeg', '107', '2016-04-13 17:10:36', '2016-04-13 17:10:36');
INSERT INTO `lumonata_attachment` VALUES ('174', '64', '/lumonata-content/files/201604/villa-06-1460538636.jpg', '/lumonata-content/files/201604/villa-06-1460538636-thumbnail.jpg', '/lumonata-content/files/201604/villa-06-1460538636-medium.jpg', '/lumonata-content/files/201604/villa-06-1460538636-large.jpg', 'villa-06.jpg', '', '', 'image/jpeg', '106', '2016-04-13 17:10:36', '2016-04-13 17:10:36');
INSERT INTO `lumonata_attachment` VALUES ('175', '64', '/lumonata-content/files/201604/villa-07-1460538636.jpg', '/lumonata-content/files/201604/villa-07-1460538636-thumbnail.jpg', '/lumonata-content/files/201604/villa-07-1460538636-medium.jpg', '/lumonata-content/files/201604/villa-07-1460538636-large.jpg', 'villa-07.jpg', '', '', 'image/jpeg', '105', '2016-04-13 17:10:36', '2016-04-13 17:10:36');
INSERT INTO `lumonata_attachment` VALUES ('176', '64', '/lumonata-content/files/201604/villa-08-1460538636.jpg', '/lumonata-content/files/201604/villa-08-1460538636-thumbnail.jpg', '/lumonata-content/files/201604/villa-08-1460538636-medium.jpg', '/lumonata-content/files/201604/villa-08-1460538636-large.jpg', 'villa-08.jpg', '', '', 'image/jpeg', '104', '2016-04-13 17:10:37', '2016-04-13 17:10:37');
INSERT INTO `lumonata_attachment` VALUES ('177', '64', '/lumonata-content/files/201604/villa-09-1460538637.jpg', '/lumonata-content/files/201604/villa-09-1460538637-thumbnail.jpg', '/lumonata-content/files/201604/villa-09-1460538637-medium.jpg', '/lumonata-content/files/201604/villa-09-1460538637-large.jpg', 'villa-09.jpg', '', '', 'image/jpeg', '103', '2016-04-13 17:10:37', '2016-04-13 17:10:37');
INSERT INTO `lumonata_attachment` VALUES ('178', '64', '/lumonata-content/files/201604/villa-10-1460538637.jpg', '/lumonata-content/files/201604/villa-10-1460538637-thumbnail.jpg', '/lumonata-content/files/201604/villa-10-1460538637-medium.jpg', '/lumonata-content/files/201604/villa-10-1460538637-large.jpg', 'villa-10.jpg', '', '', 'image/jpeg', '102', '2016-04-13 17:10:38', '2016-04-13 17:10:38');
INSERT INTO `lumonata_attachment` VALUES ('179', '64', '/lumonata-content/files/201604/villa-11-1460538638.jpg', '/lumonata-content/files/201604/villa-11-1460538638-thumbnail.jpg', '/lumonata-content/files/201604/villa-11-1460538638-medium.jpg', '/lumonata-content/files/201604/villa-11-1460538638-large.jpg', 'villa-11.jpg', '', '', 'image/jpeg', '101', '2016-04-13 17:10:39', '2016-04-13 17:10:39');
INSERT INTO `lumonata_attachment` VALUES ('180', '64', '/lumonata-content/files/201604/villa-12-1460538639.jpg', '/lumonata-content/files/201604/villa-12-1460538639-thumbnail.jpg', '/lumonata-content/files/201604/villa-12-1460538639-medium.jpg', '/lumonata-content/files/201604/villa-12-1460538639-large.jpg', 'villa-12.jpg', '', '', 'image/jpeg', '100', '2016-04-13 17:10:39', '2016-04-13 17:10:39');
INSERT INTO `lumonata_attachment` VALUES ('181', '64', '/lumonata-content/files/201604/villa-13-1460538639.jpg', '/lumonata-content/files/201604/villa-13-1460538639-thumbnail.jpg', '/lumonata-content/files/201604/villa-13-1460538639-medium.jpg', '/lumonata-content/files/201604/villa-13-1460538639-large.jpg', 'villa-13.jpg', '', '', 'image/jpeg', '99', '2016-04-13 17:10:40', '2016-04-13 17:10:40');
INSERT INTO `lumonata_attachment` VALUES ('182', '64', '/lumonata-content/files/201604/villa-14-1460538640.jpg', '/lumonata-content/files/201604/villa-14-1460538640-thumbnail.jpg', '/lumonata-content/files/201604/villa-14-1460538640-medium.jpg', '/lumonata-content/files/201604/villa-14-1460538640-large.jpg', 'villa-14.jpg', '', '', 'image/jpeg', '98', '2016-04-13 17:10:40', '2016-04-13 17:10:40');
INSERT INTO `lumonata_attachment` VALUES ('183', '64', '/lumonata-content/files/201604/villa-15-1460538640.jpg', '/lumonata-content/files/201604/villa-15-1460538640-thumbnail.jpg', '/lumonata-content/files/201604/villa-15-1460538640-medium.jpg', '/lumonata-content/files/201604/villa-15-1460538640-large.jpg', 'villa-15.jpg', '', '', 'image/jpeg', '97', '2016-04-13 17:10:41', '2016-04-13 17:10:41');
INSERT INTO `lumonata_attachment` VALUES ('184', '64', '/lumonata-content/files/201604/villa-16-1460538641.jpg', '/lumonata-content/files/201604/villa-16-1460538641-thumbnail.jpg', '/lumonata-content/files/201604/villa-16-1460538641-medium.jpg', '/lumonata-content/files/201604/villa-16-1460538641-large.jpg', 'villa-16.jpg', '', '', 'image/jpeg', '96', '2016-04-13 17:10:41', '2016-04-13 17:10:41');
INSERT INTO `lumonata_attachment` VALUES ('185', '64', '/lumonata-content/files/201604/villa-17-1460538641.jpg', '/lumonata-content/files/201604/villa-17-1460538641-thumbnail.jpg', '/lumonata-content/files/201604/villa-17-1460538641-medium.jpg', '/lumonata-content/files/201604/villa-17-1460538641-large.jpg', 'villa-17.jpg', '', '', 'image/jpeg', '95', '2016-04-13 17:10:42', '2016-04-13 17:10:42');
INSERT INTO `lumonata_attachment` VALUES ('186', '64', '/lumonata-content/files/201604/villa-18-1460538642.jpg', '/lumonata-content/files/201604/villa-18-1460538642-thumbnail.jpg', '/lumonata-content/files/201604/villa-18-1460538642-medium.jpg', '/lumonata-content/files/201604/villa-18-1460538642-large.jpg', 'villa-18.jpg', '', '', 'image/jpeg', '94', '2016-04-13 17:10:43', '2016-04-13 17:10:43');
INSERT INTO `lumonata_attachment` VALUES ('187', '64', '/lumonata-content/files/201604/villa-19-1460538643.jpg', '/lumonata-content/files/201604/villa-19-1460538643-thumbnail.jpg', '/lumonata-content/files/201604/villa-19-1460538643-medium.jpg', '/lumonata-content/files/201604/villa-19-1460538643-large.jpg', 'villa-19.jpg', '', '', 'image/jpeg', '93', '2016-04-13 17:10:43', '2016-04-13 17:10:43');
INSERT INTO `lumonata_attachment` VALUES ('188', '64', '/lumonata-content/files/201604/villa-20-1460538643.jpg', '/lumonata-content/files/201604/villa-20-1460538643-thumbnail.jpg', '/lumonata-content/files/201604/villa-20-1460538643-medium.jpg', '/lumonata-content/files/201604/villa-20-1460538643-large.jpg', 'villa-20.jpg', '', '', 'image/jpeg', '92', '2016-04-13 17:10:44', '2016-04-13 17:10:44');
INSERT INTO `lumonata_attachment` VALUES ('189', '65', '/lumonata-content/files/201604/spa-01-1460538913.jpg', '/lumonata-content/files/201604/spa-01-1460538913-thumbnail.jpg', '/lumonata-content/files/201604/spa-01-1460538913-medium.jpg', '/lumonata-content/files/201604/spa-01-1460538913-large.jpg', 'spa-01.jpg', '', '', 'image/jpeg', '91', '2016-04-13 17:15:14', '2016-04-13 17:15:14');
INSERT INTO `lumonata_attachment` VALUES ('190', '65', '/lumonata-content/files/201604/spa-02-1460538914.jpg', '/lumonata-content/files/201604/spa-02-1460538914-thumbnail.jpg', '/lumonata-content/files/201604/spa-02-1460538914-medium.jpg', '/lumonata-content/files/201604/spa-02-1460538914-large.jpg', 'spa-02.jpg', '', '', 'image/jpeg', '90', '2016-04-13 17:15:14', '2016-04-13 17:15:14');
INSERT INTO `lumonata_attachment` VALUES ('191', '65', '/lumonata-content/files/201604/spa-03-1460538914.jpg', '/lumonata-content/files/201604/spa-03-1460538914-thumbnail.jpg', '/lumonata-content/files/201604/spa-03-1460538914-medium.jpg', '/lumonata-content/files/201604/spa-03-1460538914-large.jpg', 'spa-03.jpg', '', '', 'image/jpeg', '89', '2016-04-13 17:15:15', '2016-04-13 17:15:15');
INSERT INTO `lumonata_attachment` VALUES ('192', '65', '/lumonata-content/files/201604/spa-04-1460538915.jpg', '/lumonata-content/files/201604/spa-04-1460538915-thumbnail.jpg', '/lumonata-content/files/201604/spa-04-1460538915-medium.jpg', '/lumonata-content/files/201604/spa-04-1460538915-large.jpg', 'spa-04.jpg', '', '', 'image/jpeg', '88', '2016-04-13 17:15:15', '2016-04-13 17:15:15');
INSERT INTO `lumonata_attachment` VALUES ('193', '65', '/lumonata-content/files/201604/spa-05-1460538915.jpg', '/lumonata-content/files/201604/spa-05-1460538915-thumbnail.jpg', '/lumonata-content/files/201604/spa-05-1460538915-medium.jpg', '/lumonata-content/files/201604/spa-05-1460538915-large.jpg', 'spa-05.jpg', '', '', 'image/jpeg', '87', '2016-04-13 17:15:16', '2016-04-13 17:15:16');
INSERT INTO `lumonata_attachment` VALUES ('194', '65', '/lumonata-content/files/201604/spa-06-1460538916.jpg', '/lumonata-content/files/201604/spa-06-1460538916-thumbnail.jpg', '/lumonata-content/files/201604/spa-06-1460538916-medium.jpg', '/lumonata-content/files/201604/spa-06-1460538916-large.jpg', 'spa-06.jpg', '', '', 'image/jpeg', '86', '2016-04-13 17:15:16', '2016-04-13 17:15:16');
INSERT INTO `lumonata_attachment` VALUES ('195', '65', '/lumonata-content/files/201604/spa-07-1460538916.jpg', '/lumonata-content/files/201604/spa-07-1460538916-thumbnail.jpg', '/lumonata-content/files/201604/spa-07-1460538916-medium.jpg', '/lumonata-content/files/201604/spa-07-1460538916-large.jpg', 'spa-07.jpg', '', '', 'image/jpeg', '85', '2016-04-13 17:15:17', '2016-04-13 17:15:17');
INSERT INTO `lumonata_attachment` VALUES ('196', '65', '/lumonata-content/files/201604/spa-08-1460538917.jpg', '/lumonata-content/files/201604/spa-08-1460538917-thumbnail.jpg', '/lumonata-content/files/201604/spa-08-1460538917-medium.jpg', '/lumonata-content/files/201604/spa-08-1460538917-large.jpg', 'spa-08.jpg', '', '', 'image/jpeg', '84', '2016-04-13 17:15:17', '2016-04-13 17:15:17');
INSERT INTO `lumonata_attachment` VALUES ('197', '65', '/lumonata-content/files/201604/spa-09-1460538917.jpg', '/lumonata-content/files/201604/spa-09-1460538917-thumbnail.jpg', '/lumonata-content/files/201604/spa-09-1460538917-medium.jpg', '/lumonata-content/files/201604/spa-09-1460538917-large.jpg', 'spa-09.jpg', '', '', 'image/jpeg', '83', '2016-04-13 17:15:18', '2016-04-13 17:15:18');
INSERT INTO `lumonata_attachment` VALUES ('198', '65', '/lumonata-content/files/201604/spa-10-1460538918.jpg', '/lumonata-content/files/201604/spa-10-1460538918-thumbnail.jpg', '/lumonata-content/files/201604/spa-10-1460538918-medium.jpg', '/lumonata-content/files/201604/spa-10-1460538918-large.jpg', 'spa-10.jpg', '', '', 'image/jpeg', '82', '2016-04-13 17:15:18', '2016-04-13 17:15:18');
INSERT INTO `lumonata_attachment` VALUES ('199', '65', '/lumonata-content/files/201604/spa-11-1460538918.jpg', '/lumonata-content/files/201604/spa-11-1460538918-thumbnail.jpg', '/lumonata-content/files/201604/spa-11-1460538918-medium.jpg', '/lumonata-content/files/201604/spa-11-1460538918-large.jpg', 'spa-11.jpg', '', '', 'image/jpeg', '81', '2016-04-13 17:15:19', '2016-04-13 17:15:19');
INSERT INTO `lumonata_attachment` VALUES ('200', '65', '/lumonata-content/files/201604/spa-12-1460538919.jpg', '/lumonata-content/files/201604/spa-12-1460538919-thumbnail.jpg', '/lumonata-content/files/201604/spa-12-1460538919-medium.jpg', '/lumonata-content/files/201604/spa-12-1460538919-large.jpg', 'spa-12.jpg', '', '', 'image/jpeg', '80', '2016-04-13 17:15:19', '2016-04-13 17:15:19');
INSERT INTO `lumonata_attachment` VALUES ('201', '65', '/lumonata-content/files/201604/spa-13-1460538919.jpg', '/lumonata-content/files/201604/spa-13-1460538919-thumbnail.jpg', '/lumonata-content/files/201604/spa-13-1460538919-medium.jpg', '/lumonata-content/files/201604/spa-13-1460538919-large.jpg', 'spa-13.jpg', '', '', 'image/jpeg', '79', '2016-04-13 17:15:20', '2016-04-13 17:15:20');
INSERT INTO `lumonata_attachment` VALUES ('202', '65', '/lumonata-content/files/201604/spa-14-1460538920.jpg', '/lumonata-content/files/201604/spa-14-1460538920-thumbnail.jpg', '/lumonata-content/files/201604/spa-14-1460538920-medium.jpg', '/lumonata-content/files/201604/spa-14-1460538920-large.jpg', 'spa-14.jpg', '', '', 'image/jpeg', '78', '2016-04-13 17:15:20', '2016-04-13 17:15:20');
INSERT INTO `lumonata_attachment` VALUES ('203', '65', '/lumonata-content/files/201604/spa-15-1460538920.jpg', '/lumonata-content/files/201604/spa-15-1460538920-thumbnail.jpg', '/lumonata-content/files/201604/spa-15-1460538920-medium.jpg', '/lumonata-content/files/201604/spa-15-1460538920-large.jpg', 'spa-15.jpg', '', '', 'image/jpeg', '77', '2016-04-13 17:15:21', '2016-04-13 17:15:21');
INSERT INTO `lumonata_attachment` VALUES ('204', '65', '/lumonata-content/files/201604/spa-16-1460538921.jpg', '/lumonata-content/files/201604/spa-16-1460538921-thumbnail.jpg', '/lumonata-content/files/201604/spa-16-1460538921-medium.jpg', '/lumonata-content/files/201604/spa-16-1460538921-large.jpg', 'spa-16.jpg', '', '', 'image/jpeg', '76', '2016-04-13 17:15:21', '2016-04-13 17:15:21');
INSERT INTO `lumonata_attachment` VALUES ('205', '65', '/lumonata-content/files/201604/spa-17-1460538921.jpg', '/lumonata-content/files/201604/spa-17-1460538921-thumbnail.jpg', '/lumonata-content/files/201604/spa-17-1460538921-medium.jpg', '/lumonata-content/files/201604/spa-17-1460538921-large.jpg', 'spa-17.jpg', '', '', 'image/jpeg', '75', '2016-04-13 17:15:22', '2016-04-13 17:15:22');
INSERT INTO `lumonata_attachment` VALUES ('206', '65', '/lumonata-content/files/201604/spa-18-1460538922.jpg', '/lumonata-content/files/201604/spa-18-1460538922-thumbnail.jpg', '/lumonata-content/files/201604/spa-18-1460538922-medium.jpg', '/lumonata-content/files/201604/spa-18-1460538922-large.jpg', 'spa-18.jpg', '', '', 'image/jpeg', '74', '2016-04-13 17:15:22', '2016-04-13 17:15:22');
INSERT INTO `lumonata_attachment` VALUES ('207', '65', '/lumonata-content/files/201604/spa-19-1460538922.jpg', '/lumonata-content/files/201604/spa-19-1460538922-thumbnail.jpg', '/lumonata-content/files/201604/spa-19-1460538922-medium.jpg', '/lumonata-content/files/201604/spa-19-1460538922-large.jpg', 'spa-19.jpg', '', '', 'image/jpeg', '73', '2016-04-13 17:15:23', '2016-04-13 17:15:23');
INSERT INTO `lumonata_attachment` VALUES ('208', '65', '/lumonata-content/files/201604/spa-20-1460538923.jpg', '/lumonata-content/files/201604/spa-20-1460538923-thumbnail.jpg', '/lumonata-content/files/201604/spa-20-1460538923-medium.jpg', '/lumonata-content/files/201604/spa-20-1460538923-large.jpg', 'spa-20.jpg', '', '', 'image/jpeg', '72', '2016-04-13 17:15:24', '2016-04-13 17:15:24');
INSERT INTO `lumonata_attachment` VALUES ('209', '66', '/lumonata-content/files/201604/wedding-01-1460539060.jpg', '/lumonata-content/files/201604/wedding-01-1460539060-thumbnail.jpg', '/lumonata-content/files/201604/wedding-01-1460539060-medium.jpg', '/lumonata-content/files/201604/wedding-01-1460539060-large.jpg', 'wedding-01.jpg', '', '', 'image/jpeg', '71', '2016-04-13 17:17:40', '2016-04-13 17:17:40');
INSERT INTO `lumonata_attachment` VALUES ('210', '66', '/lumonata-content/files/201604/wedding-02-1460539060.jpg', '/lumonata-content/files/201604/wedding-02-1460539060-thumbnail.jpg', '/lumonata-content/files/201604/wedding-02-1460539060-medium.jpg', '/lumonata-content/files/201604/wedding-02-1460539060-large.jpg', 'wedding-02.jpg', '', '', 'image/jpeg', '70', '2016-04-13 17:17:40', '2016-04-13 17:17:40');
INSERT INTO `lumonata_attachment` VALUES ('211', '66', '/lumonata-content/files/201604/wedding-03-1460539060.jpg', '/lumonata-content/files/201604/wedding-03-1460539060-thumbnail.jpg', '/lumonata-content/files/201604/wedding-03-1460539060-medium.jpg', '/lumonata-content/files/201604/wedding-03-1460539060-large.jpg', 'wedding-03.jpg', '', '', 'image/jpeg', '69', '2016-04-13 17:17:41', '2016-04-13 17:17:41');
INSERT INTO `lumonata_attachment` VALUES ('212', '66', '/lumonata-content/files/201604/wedding-04-1460539061.jpg', '/lumonata-content/files/201604/wedding-04-1460539061-thumbnail.jpg', '/lumonata-content/files/201604/wedding-04-1460539061-medium.jpg', '/lumonata-content/files/201604/wedding-04-1460539061-large.jpg', 'wedding-04.jpg', '', '', 'image/jpeg', '68', '2016-04-13 17:17:41', '2016-04-13 17:17:41');
INSERT INTO `lumonata_attachment` VALUES ('213', '66', '/lumonata-content/files/201604/wedding-05-1460539061.jpg', '/lumonata-content/files/201604/wedding-05-1460539061-thumbnail.jpg', '/lumonata-content/files/201604/wedding-05-1460539061-medium.jpg', '/lumonata-content/files/201604/wedding-05-1460539061-large.jpg', 'wedding-05.jpg', '', '', 'image/jpeg', '67', '2016-04-13 17:17:42', '2016-04-13 17:17:42');
INSERT INTO `lumonata_attachment` VALUES ('214', '66', '/lumonata-content/files/201604/wedding-06-1460539062.jpg', '/lumonata-content/files/201604/wedding-06-1460539062-thumbnail.jpg', '/lumonata-content/files/201604/wedding-06-1460539062-medium.jpg', '/lumonata-content/files/201604/wedding-06-1460539062-large.jpg', 'wedding-06.jpg', '', '', 'image/jpeg', '66', '2016-04-13 17:17:42', '2016-04-13 17:17:42');
INSERT INTO `lumonata_attachment` VALUES ('215', '67', '/lumonata-content/files/201604/meeting-01-1460539142.jpg', '/lumonata-content/files/201604/meeting-01-1460539142-thumbnail.jpg', '/lumonata-content/files/201604/meeting-01-1460539142-medium.jpg', '/lumonata-content/files/201604/meeting-01-1460539142-large.jpg', 'meeting-01.jpg', '', '', 'image/jpeg', '65', '2016-04-13 17:19:03', '2016-04-13 17:19:03');
INSERT INTO `lumonata_attachment` VALUES ('216', '67', '/lumonata-content/files/201604/meeting-02-1460539143.jpg', '/lumonata-content/files/201604/meeting-02-1460539143-thumbnail.jpg', '/lumonata-content/files/201604/meeting-02-1460539143-medium.jpg', '/lumonata-content/files/201604/meeting-02-1460539143-large.jpg', 'meeting-02.jpg', '', '', 'image/jpeg', '64', '2016-04-13 17:19:03', '2016-04-13 17:19:03');
INSERT INTO `lumonata_attachment` VALUES ('217', '67', '/lumonata-content/files/201604/meeting-03-1460539143.jpg', '/lumonata-content/files/201604/meeting-03-1460539143-thumbnail.jpg', '/lumonata-content/files/201604/meeting-03-1460539143-medium.jpg', '/lumonata-content/files/201604/meeting-03-1460539143-large.jpg', 'meeting-03.jpg', '', '', 'image/jpeg', '63', '2016-04-13 17:19:03', '2016-04-13 17:19:03');
INSERT INTO `lumonata_attachment` VALUES ('218', '67', '/lumonata-content/files/201604/meeting-04-1460539143.jpg', '/lumonata-content/files/201604/meeting-04-1460539143-thumbnail.jpg', '/lumonata-content/files/201604/meeting-04-1460539143-medium.jpg', '/lumonata-content/files/201604/meeting-04-1460539143-large.jpg', 'meeting-04.jpg', '', '', 'image/jpeg', '62', '2016-04-13 17:19:04', '2016-04-13 17:19:04');
INSERT INTO `lumonata_attachment` VALUES ('219', '67', '/lumonata-content/files/201604/meeting-05-1460539144.jpg', '/lumonata-content/files/201604/meeting-05-1460539144-thumbnail.jpg', '/lumonata-content/files/201604/meeting-05-1460539144-medium.jpg', '/lumonata-content/files/201604/meeting-05-1460539144-large.jpg', 'meeting-05.jpg', '', '', 'image/jpeg', '61', '2016-04-13 17:19:04', '2016-04-13 17:19:04');
INSERT INTO `lumonata_attachment` VALUES ('220', '67', '/lumonata-content/files/201604/meeting-06-1460539144.jpg', '/lumonata-content/files/201604/meeting-06-1460539144-thumbnail.jpg', '/lumonata-content/files/201604/meeting-06-1460539144-medium.jpg', '/lumonata-content/files/201604/meeting-06-1460539144-large.jpg', 'meeting-06.jpg', '', '', 'image/jpeg', '60', '2016-04-13 17:19:04', '2016-04-13 17:19:04');
INSERT INTO `lumonata_attachment` VALUES ('221', '67', '/lumonata-content/files/201604/meeting-07-1460539144.jpg', '/lumonata-content/files/201604/meeting-07-1460539144-thumbnail.jpg', '/lumonata-content/files/201604/meeting-07-1460539144-medium.jpg', '/lumonata-content/files/201604/meeting-07-1460539144-large.jpg', 'meeting-07.jpg', '', '', 'image/jpeg', '59', '2016-04-13 17:19:04', '2016-04-13 17:19:04');
INSERT INTO `lumonata_attachment` VALUES ('222', '68', '/lumonata-content/files/201604/leisure-01-1460539309.jpg', '/lumonata-content/files/201604/leisure-01-1460539309-thumbnail.jpg', '/lumonata-content/files/201604/leisure-01-1460539309-medium.jpg', '/lumonata-content/files/201604/leisure-01-1460539309-large.jpg', 'leisure-01.jpg', '', '', 'image/jpeg', '58', '2016-04-13 17:21:50', '2016-04-13 17:21:50');
INSERT INTO `lumonata_attachment` VALUES ('223', '68', '/lumonata-content/files/201604/leisure-02-1460539310.jpg', '/lumonata-content/files/201604/leisure-02-1460539310-thumbnail.jpg', '/lumonata-content/files/201604/leisure-02-1460539310-medium.jpg', '/lumonata-content/files/201604/leisure-02-1460539310-large.jpg', 'leisure-02.jpg', '', '', 'image/jpeg', '57', '2016-04-13 17:21:50', '2016-04-13 17:21:50');
INSERT INTO `lumonata_attachment` VALUES ('224', '68', '/lumonata-content/files/201604/leisure-03-1460539310.jpg', '/lumonata-content/files/201604/leisure-03-1460539310-thumbnail.jpg', '/lumonata-content/files/201604/leisure-03-1460539310-medium.jpg', '/lumonata-content/files/201604/leisure-03-1460539310-large.jpg', 'leisure-03.jpg', '', '', 'image/jpeg', '56', '2016-04-13 17:21:51', '2016-04-13 17:21:51');
INSERT INTO `lumonata_attachment` VALUES ('225', '68', '/lumonata-content/files/201604/leisure-04-1460539311.jpg', '/lumonata-content/files/201604/leisure-04-1460539311-thumbnail.jpg', '/lumonata-content/files/201604/leisure-04-1460539311-medium.jpg', '/lumonata-content/files/201604/leisure-04-1460539311-large.jpg', 'leisure-04.jpg', '', '', 'image/jpeg', '55', '2016-04-13 17:21:51', '2016-04-13 17:21:51');
INSERT INTO `lumonata_attachment` VALUES ('226', '68', '/lumonata-content/files/201604/leisure-05-1460539311.jpg', '/lumonata-content/files/201604/leisure-05-1460539311-thumbnail.jpg', '/lumonata-content/files/201604/leisure-05-1460539311-medium.jpg', '/lumonata-content/files/201604/leisure-05-1460539311-large.jpg', 'leisure-05.jpg', '', '', 'image/jpeg', '54', '2016-04-13 17:21:52', '2016-04-13 17:21:52');
INSERT INTO `lumonata_attachment` VALUES ('227', '68', '/lumonata-content/files/201604/leisure-06-1460539312.jpg', '/lumonata-content/files/201604/leisure-06-1460539312-thumbnail.jpg', '/lumonata-content/files/201604/leisure-06-1460539312-medium.jpg', '/lumonata-content/files/201604/leisure-06-1460539312-large.jpg', 'leisure-06.jpg', '', '', 'image/jpeg', '53', '2016-04-13 17:21:53', '2016-04-13 17:21:53');
INSERT INTO `lumonata_attachment` VALUES ('228', '68', '/lumonata-content/files/201604/leisure-07-1460539313.jpg', '/lumonata-content/files/201604/leisure-07-1460539313-thumbnail.jpg', '/lumonata-content/files/201604/leisure-07-1460539313-medium.jpg', '/lumonata-content/files/201604/leisure-07-1460539313-large.jpg', 'leisure-07.jpg', '', '', 'image/jpeg', '52', '2016-04-13 17:21:53', '2016-04-13 17:21:53');
INSERT INTO `lumonata_attachment` VALUES ('229', '68', '/lumonata-content/files/201604/leisure-08-1460539313.jpg', '/lumonata-content/files/201604/leisure-08-1460539313-thumbnail.jpg', '/lumonata-content/files/201604/leisure-08-1460539313-medium.jpg', '/lumonata-content/files/201604/leisure-08-1460539313-large.jpg', 'leisure-08.jpg', '', '', 'image/jpeg', '51', '2016-04-13 17:21:54', '2016-04-13 17:21:54');
INSERT INTO `lumonata_attachment` VALUES ('230', '68', '/lumonata-content/files/201604/leisure-09-1460539314.jpg', '/lumonata-content/files/201604/leisure-09-1460539314-thumbnail.jpg', '/lumonata-content/files/201604/leisure-09-1460539314-medium.jpg', '/lumonata-content/files/201604/leisure-09-1460539314-large.jpg', 'leisure-09.jpg', '', '', 'image/jpeg', '50', '2016-04-13 17:21:55', '2016-04-13 17:21:55');
INSERT INTO `lumonata_attachment` VALUES ('231', '68', '/lumonata-content/files/201604/leisure-10-1460539315.jpg', '/lumonata-content/files/201604/leisure-10-1460539315-thumbnail.jpg', '/lumonata-content/files/201604/leisure-10-1460539315-medium.jpg', '/lumonata-content/files/201604/leisure-10-1460539315-large.jpg', 'leisure-10.jpg', '', '', 'image/jpeg', '49', '2016-04-13 17:21:55', '2016-04-13 17:21:55');
INSERT INTO `lumonata_attachment` VALUES ('232', '68', '/lumonata-content/files/201604/leisure-11-1460539315.jpg', '/lumonata-content/files/201604/leisure-11-1460539315-thumbnail.jpg', '/lumonata-content/files/201604/leisure-11-1460539315-medium.jpg', '/lumonata-content/files/201604/leisure-11-1460539315-large.jpg', 'leisure-11.jpg', '', '', 'image/jpeg', '48', '2016-04-13 17:21:56', '2016-04-13 17:21:56');
INSERT INTO `lumonata_attachment` VALUES ('236', '70', '/lumonata-content/files/201604/mayasym015-1460970600.jpg', '/lumonata-content/files/201604/mayasym015-1460970600-thumbnail.jpg', '/lumonata-content/files/201604/mayasym015-1460970600-medium.jpg', '/lumonata-content/files/201604/mayasym015-1460970600-large.jpg', 'Maya_S_YM_015.jpg', '', '', 'image/jpeg', '44', '2016-04-18 17:10:01', '2016-04-18 17:10:01');
INSERT INTO `lumonata_attachment` VALUES ('235', '70', '/lumonata-content/files/201604/breakfast-at-reef-1460970599.jpg', '/lumonata-content/files/201604/breakfast-at-reef-1460970599-thumbnail.jpg', '/lumonata-content/files/201604/breakfast-at-reef-1460970599-medium.jpg', '/lumonata-content/files/201604/breakfast-at-reef-1460970599-large.jpg', 'Breakfast-at-REEF.jpg', '', '', 'image/jpeg', '45', '2016-04-18 17:10:00', '2016-04-18 17:10:00');
INSERT INTO `lumonata_attachment` VALUES ('237', '70', '/lumonata-content/files/201604/mayasym017-1460970601.jpg', '/lumonata-content/files/201604/mayasym017-1460970601-thumbnail.jpg', '/lumonata-content/files/201604/mayasym017-1460970601-medium.jpg', '/lumonata-content/files/201604/mayasym017-1460970601-large.jpg', 'Maya_S_YM_017.jpg', '', '', 'image/jpeg', '43', '2016-04-18 17:10:02', '2016-04-18 17:10:02');
INSERT INTO `lumonata_attachment` VALUES ('262', '151', '/lumonata-content/files/201606/11453576b80355bb634-1466736458.13372091.jpg', '/lumonata-content/files/201606/11453576b80355bb634-1466736458-thumbnail.13372091.jpg', '/lumonata-content/files/201606/11453576b80355bb634-1466736458-medium.13372091.jpg', '/lumonata-content/files/201606/11453576b80355bb634-1466736458-large.13372091.jpg', '11453576b80355bb634.13372091.jpg', '', '', 'image/jpeg', '18', '2016-06-24 10:47:39', '2016-06-24 10:47:39');
INSERT INTO `lumonata_attachment` VALUES ('273', '43', '/lumonata-content/files/201607/deluxe-pool-05-1468208688.jpg', '/lumonata-content/files/201607/deluxe-pool-05-1468208688-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-pool-05-1468208688-medium.jpg', '/lumonata-content/files/201607/deluxe-pool-05-1468208688-large.jpg', 'deluxe-pool-05.jpg', '', '', 'image/jpeg', '7', '2016-07-11 11:44:49', '2016-07-11 11:44:49');
INSERT INTO `lumonata_attachment` VALUES ('272', '43', '/lumonata-content/files/201607/deluxe-pool-04-1468208688.jpg', '/lumonata-content/files/201607/deluxe-pool-04-1468208688-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-pool-04-1468208688-medium.jpg', '/lumonata-content/files/201607/deluxe-pool-04-1468208688-large.jpg', 'deluxe-pool-04.jpg', '', '', 'image/jpeg', '8', '2016-07-11 11:44:48', '2016-07-11 11:44:48');
INSERT INTO `lumonata_attachment` VALUES ('271', '43', '/lumonata-content/files/201607/deluxe-pool-03-1468208687.jpg', '/lumonata-content/files/201607/deluxe-pool-03-1468208687-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-pool-03-1468208687-medium.jpg', '/lumonata-content/files/201607/deluxe-pool-03-1468208687-large.jpg', 'deluxe-pool-03.jpg', '', '', 'image/jpeg', '9', '2016-07-11 11:44:48', '2016-07-11 11:44:48');
INSERT INTO `lumonata_attachment` VALUES ('270', '43', '/lumonata-content/files/201607/deluxe-pool-02-1468208686.jpg', '/lumonata-content/files/201607/deluxe-pool-02-1468208686-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-pool-02-1468208686-medium.jpg', '/lumonata-content/files/201607/deluxe-pool-02-1468208686-large.jpg', 'deluxe-pool-02.jpg', '', '', 'image/jpeg', '10', '2016-07-11 11:44:47', '2016-07-11 11:44:47');
INSERT INTO `lumonata_attachment` VALUES ('269', '43', '/lumonata-content/files/201607/deluxe-pool-01-1468208685.jpg', '/lumonata-content/files/201607/deluxe-pool-01-1468208685-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-pool-01-1468208685-medium.jpg', '/lumonata-content/files/201607/deluxe-pool-01-1468208685-large.jpg', 'deluxe-pool-01.jpg', '', '', 'image/jpeg', '11', '2016-07-11 11:44:46', '2016-07-11 11:44:46');
INSERT INTO `lumonata_attachment` VALUES ('268', '43', '/lumonata-content/files/201607/deluxe-room-01-1468200342.jpg', '/lumonata-content/files/201607/deluxe-room-01-1468200342-thumbnail.jpg', '/lumonata-content/files/201607/deluxe-room-01-1468200342-medium.jpg', '/lumonata-content/files/201607/deluxe-room-01-1468200342-large.jpg', 'deluxe-room-01.jpg', '', '', 'image/jpeg', '12', '2016-07-11 09:25:43', '2016-07-11 09:25:43');

-- ----------------------------
-- Table structure for lumonata_comments
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_comments`;
CREATE TABLE `lumonata_comments` (
  `lcomment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lcomment_parent` bigint(20) NOT NULL,
  `larticle_id` bigint(20) NOT NULL,
  `lcomentator_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lcomentator_email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lcomentator_url` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lcomentator_ip` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lcomment_date` datetime NOT NULL,
  `lcomment` text CHARACTER SET utf8 NOT NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lcomment_like` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `lcomment_type` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT 'like,comment,like_comment',
  PRIMARY KEY (`lcomment_id`),
  KEY `lcomment_status` (`lcomment_status`),
  KEY `lcomment_userid` (`luser_id`),
  KEY `lcomment_type` (`lcomment_type`),
  KEY `larticle_id` (`larticle_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_comments
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_friendship
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friendship`;
CREATE TABLE `lumonata_friendship` (
  `lfriendship_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `lfriend_id` bigint(20) NOT NULL,
  `lstatus` varchar(20) NOT NULL COMMENT 'connected, onrequest, pending, unfollow',
  PRIMARY KEY (`lfriendship_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lumonata_friendship
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_friends_list
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friends_list`;
CREATE TABLE `lumonata_friends_list` (
  `lfriends_list_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `llist_name` varchar(300) CHARACTER SET utf8 NOT NULL,
  `lorder` bigint(20) NOT NULL,
  PRIMARY KEY (`lfriends_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lumonata_friends_list
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_friends_list_rel
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friends_list_rel`;
CREATE TABLE `lumonata_friends_list_rel` (
  `lfriendship_id` bigint(20) NOT NULL,
  `lfriends_list_id` bigint(20) NOT NULL,
  PRIMARY KEY (`lfriendship_id`,`lfriends_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lumonata_friends_list_rel
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_meta_data
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_meta_data`;
CREATE TABLE `lumonata_meta_data` (
  `lmeta_id` int(11) NOT NULL AUTO_INCREMENT,
  `lmeta_name` varchar(200) NOT NULL,
  `lmeta_value` longtext NOT NULL,
  `lapp_name` varchar(200) NOT NULL,
  `lapp_id` int(11) NOT NULL,
  PRIMARY KEY (`lmeta_id`),
  KEY `meta_name` (`lmeta_name`),
  KEY `app_name` (`lapp_name`),
  KEY `app_id` (`lapp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lumonata_meta_data
-- ----------------------------
INSERT INTO `lumonata_meta_data` VALUES ('1', 'time_zone', 'Asia/Singapore', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('2', 'site_url', 'localhost/maya', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('3', 'web_title', 'Maya Resort  ', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('4', 'smtp_server', 'localhost', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('5', 'smtp', 'mail.lumonatalabs.com', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('6', 'email', 'ngurah@lumonata.com', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('7', 'web_tagline', '', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('8', 'invitation_limit', '10', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('9', 'date_format', 'F j, Y', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('10', 'time_format', 'H:i', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('11', 'post_viewed', '50', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('12', 'rss_viewed', '15', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('13', 'rss_view_format', 'full_text', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('14', 'list_viewed', '50', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('15', 'email_format', 'html', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('16', 'text_editor', 'tiny_mce', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('17', 'thumbnail_image_size', '300:300', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('18', 'large_image_size', '1024:1024', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('19', 'medium_image_size', '700:700', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('20', 'is_allow_comment', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('21', 'is_login_to_comment', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('22', 'is_auto_close_comment', '0', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('23', 'days_auto_close_comment', '15', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('24', 'is_break_comment', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('25', 'comment_page_displayed', 'last', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('26', 'comment_per_page', '3', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('27', 'save_changes', 'Save Changes', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('28', 'is_rewrite', 'yes', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('29', 'is_allow_post_like', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('30', 'is_allow_comment_like', '1', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('31', 'alert_on_register', '0', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('32', 'alert_on_comment', '0', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('33', 'alert_on_comment_reply', '0', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('34', 'alert_on_liked_post', '0', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('35', 'alert_on_liked_comment', '0', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('36', 'web_name', 'Maya Resort  ', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('37', 'meta_description', '', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('38', 'meta_keywords', 'Maya Resort', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('39', 'meta_title', 'Maya Resort', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('40', 'status_viewed', '50', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('41', 'update', 'true', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('42', 'the_date_format', 'F j, Y', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('43', 'the_time_format', 'H:i', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('44', 'thumbnail_image_width', '300', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('45', 'thumbnail_image_height', '300', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('46', 'medium_image_width', '700', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('47', 'medium_image_height', '700', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('48', 'large_image_width', '1024', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('49', 'large_image_height', '1024', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('50', 'email_per_send', '5', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('51', 'newsletter_notif_email', 'ngurah@lumonata.com', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('52', 'newsletter_smtp', 'mail.lumonatalabs.com', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('53', 'front_theme', 'custom', 'themes', '0');
INSERT INTO `lumonata_meta_data` VALUES ('54', 'admin_theme', 'default', 'themes', '0');
INSERT INTO `lumonata_meta_data` VALUES ('55', 'custome_bg_color', 'FFF', 'themes', '0');
INSERT INTO `lumonata_meta_data` VALUES ('56', 'active_plugins', '{\"lumonata-additional-data\":\"\\/additional\\/additional.php\",\"lumonata-meta-data\":\"\\/metadata\\/metadata.php\",\"lumonata-destination-application\":\"\\/destinations\\/destinations.php\",\"lumonata-custom-application\":\"\\/custom-post\\/custom-post.php\",\"post-gallery\":\"\\/post-gallery\\/post-gallery.php\",\"lumonata-locations-application\":\"\\/locations\\/locations.php\",\"global-setting\":\"\\/global\\/global.php\",\"post-video\":\"\\/post-video\\/post-video.php\"}', 'plugins', '0');
INSERT INTO `lumonata_meta_data` VALUES ('69', 'post_type_setting', '{\"title\":\"Accommodation\",\"description\":\"Praesent efficitur lacus sit amet venenatis semper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\",\"background\":\"1815057186932af69e3.27862094.jpg\"}', 'accommodation', '6');
INSERT INTO `lumonata_meta_data` VALUES ('59', 'menu_set', '{\"ubud-header\":\"Ubud Header\",\"footer\":\"Footer\",\"sanur-header\":\"Sanur Header\"}', 'menus', '0');
INSERT INTO `lumonata_meta_data` VALUES ('60', 'menu_items_ubud-header', '[{\"id\":0,\"label\":\"About Us\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/ubud\\/about-us\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/ubud\\/about-us\\/\"},{\"id\":1,\"label\":\" Accommodations\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/ubud\\/accommodation\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/ubud\\/accommodation\\/\"},{\"id\":2,\"label\":\"Dining\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/ubud\\/restaurant\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/ubud\\/restaurant\\/\"},{\"id\":3,\"label\":\"Spa & Wellness\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/ubud\\/spa-and-wellness\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/ubud\\/spa-and-wellness\\/\"},{\"id\":4,\"label\":\"Wedding & Events\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/ubud\\/wedding-events\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/ubud\\/wedding-events\\/\"},{\"id\":5,\"label\":\"Gallery\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/ubud\\/gallery\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/ubud\\/gallery\\/\"},{\"id\":6,\"label\":\"Location\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/ubud\\/location\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/ubud\\/location\\/\"}]', 'menus', '0');
INSERT INTO `lumonata_meta_data` VALUES ('61', 'menu_order_ubud-header', '[{\"id\":0},{\"id\":1},{\"id\":2},{\"id\":3},{\"id\":4},{\"id\":5},{\"id\":6}]', 'menus', '0');
INSERT INTO `lumonata_meta_data` VALUES ('62', 'menu_items_footer', '[{\"id\":0,\"label\":\"Vision\",\"target\":\"_self\",\"link\":\"\\/?page_id=1\",\"permalink\":\"vision\\/\"},{\"id\":1,\"label\":\"People\",\"target\":\"_self\",\"link\":\"\\/?page_id=2\",\"permalink\":\"people\\/\"},{\"id\":2,\"label\":\"Sustainability\",\"target\":\"_self\",\"link\":\"\\/?page_id=3\",\"permalink\":\"sustainability\\/\"},{\"id\":3,\"label\":\"Affiliates\",\"target\":\"_self\",\"link\":\"\\/?page_id=4\",\"permalink\":\"affiliates\\/\"},{\"id\":4,\"label\":\"Contact\",\"target\":\"_self\",\"link\":\"\\/?page_id=5\",\"permalink\":\"contact\\/\"}]', 'menus', '0');
INSERT INTO `lumonata_meta_data` VALUES ('63', 'menu_order_footer', '[{\"id\":0},{\"id\":1},{\"id\":2},{\"id\":3},{\"id\":4}]', 'menus', '0');
INSERT INTO `lumonata_meta_data` VALUES ('64', 'post_type_setting', '{\"title\":\"Dining\",\"description\":\"\",\"background\":\"327145784b5b55441f7.74286022.jpg\"}', 'restaurant', '6');
INSERT INTO `lumonata_meta_data` VALUES ('127', 'post_type_setting', '{\"title\":\"\",\"description\":\"\",\"background\":\"\"}', 'restaurant', '7');
INSERT INTO `lumonata_meta_data` VALUES ('65', 'post_type_setting', '{\"title\":\"Wedding & Events\",\"description\":\"<p>Donec sed odio dui. Maecenas faucibus mollis interdum. Praesent commodo  cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus  eget urna mollis ornare vel eu leo.<\\/p>\",\"background\":\"1239657076bfa982a91.64954098.jpg\"}', 'wedding-events', '6');
INSERT INTO `lumonata_meta_data` VALUES ('66', 'post_type_setting', '{\"title\":\"About Us\",\"description\":\"Donec sed odio dui. Maecenas faucibus mollis interdum. Praesent commodo  cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus  eget urna mollis ornare vel eu leo.\",\"background\":\"989571730c86bf261.32152905.jpg\"}', 'about-us', '6');
INSERT INTO `lumonata_meta_data` VALUES ('67', 'post_type_setting', '{\"title\":\"Special Offers\",\"description\":\"\",\"background\":\"19066571dafa9697941.47262131.jpg\"}', 'special-offer', '6');
INSERT INTO `lumonata_meta_data` VALUES ('68', 'post_type_setting', '{\"title\":\"Gallery\",\"description\":\"<p>nothing to say<\\/p>\",\"background\":\"\"}', 'gallery', '6');
INSERT INTO `lumonata_meta_data` VALUES ('70', 'post_type_setting', '{\"title\":\"Accommodation\",\"description\":\"Quisque ut vehicula elit. Suspendisse tristique, nunc a pretium commodo, ex mi tempor tortor, vel consequat ipsum dui malesuada tellus.\",\"background\":\"2370057171e212df3e2.16815940.jpg\"}', 'accommodation', '7');
INSERT INTO `lumonata_meta_data` VALUES ('77', 'post_type_setting', '{\"title\":\"Special Offers\",\"description\":\"\",\"background\":\"\"}', 'special-offer', '7');
INSERT INTO `lumonata_meta_data` VALUES ('75', 'menu_items_sanur-header', '[{\"id\":0,\"label\":\"About Us\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/sanur\\/about-us\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/sanur\\/about-us\"},{\"id\":1,\"label\":\"Accommodations\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/sanur\\/accommodation\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/sanur\\/accommodation\\/\"},{\"id\":2,\"label\":\"Dining\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/sanur\\/restaurant\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/sanur\\/restaurant\\/\"},{\"id\":3,\"label\":\"Spa & Wellness\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/sanur\\/spa-and-wellness\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/sanur\\/spa-and-wellness\"},{\"id\":4,\"label\":\"Wedding & Events\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/sanur\\/wedding-events\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/sanur\\/wedding-events\\/\"},{\"id\":5,\"label\":\"Gallery\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/sanur\\/gallery\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/sanur\\/gallery\\/\"},{\"id\":6,\"label\":\"Location\",\"target\":\"_self\",\"link\":\"http:\\/\\/localhost\\/maya\\/sanur\\/location\\/\",\"permalink\":\"http:\\/\\/localhost\\/maya\\/sanur\\/location\\/\"}]', 'menus', '0');
INSERT INTO `lumonata_meta_data` VALUES ('76', 'menu_order_sanur-header', '[{\"id\":0},{\"id\":1},{\"id\":2},{\"id\":3},{\"id\":4},{\"id\":5},{\"id\":6}]', 'menus', '0');
INSERT INTO `lumonata_meta_data` VALUES ('78', 'post_type_setting', '{\"title\":\"Spa & Wellness\",\"description\":\"\",\"background\":\"22929571ebff3547fc9.21561392.jpg\"}', 'spa-and-wellness', '6');
INSERT INTO `lumonata_meta_data` VALUES ('79', 'post_type_setting', '{\"title\":\"Spa & Wellness\",\"description\":\"\",\"background\":\"246571ebff3a5dd28.35727217.jpg\"}', 'spa-and-wellness', '7');
INSERT INTO `lumonata_meta_data` VALUES ('73', 'post_type_setting', '{\"title\":\"dfasfas\",\"description\":\"gfhfghgf\",\"background\":\"949357173210dd2c59.92797097.jpg\"}', 'about-us', '7');
INSERT INTO `lumonata_meta_data` VALUES ('93', 'smtp_email_port', '2525', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('94', 'smtp_email_address', 'ngurah@lumonatalabs.com', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('95', 'smtp_email_pwd', 'caberawit', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('96', 'r_public_key', '6LfGqfsSAAAAAD364TDpt6VuCwJfNDYd-JfR7odF', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('97', 'r_secret_key', '6LfGqfsSAAAAAEDsjyYMF81QxwwB_zTrgOX0Gvdv', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('98', 'ig_client_id', '656af9140a8f40fd9b28868ea373dab9', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('99', 'ig_client_secret', '8d62f6f7bcd947749a3d24fc6a0d5c36', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('100', 'ig_token', '', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('102', 'post_type_setting', '{\"title\":\"Articles\",\"description\":\"\",\"background\":\"\"}', 'articles', '7');
INSERT INTO `lumonata_meta_data` VALUES ('101', 'post_type_setting', '{\"title\":\"Articles\",\"description\":\"Donec sed odio dui. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo.\",\"background\":\"2753657675cc7532826.49496395.jpg\"}', 'articles', '6');
INSERT INTO `lumonata_meta_data` VALUES ('105', 'analytic_view_id', '117024539', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('107', 'g_analytic', '', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('108', 'post_type_setting', '{\"title\":\"\",\"description\":\"\",\"background\":\"\"}', 'gallery', '7');
INSERT INTO `lumonata_meta_data` VALUES ('104', 'analytic_token', '{\"access_token\":\"ya29.CjMPAwUNUA2h0Vt0l9y9GQmzF6eZAa2kn362Vx8vxTDDJfTiqnvNta2eBVz2Fho_Mu_r7e8\",\"token_type\":\"Bearer\",\"expires_in\":3597,\"created\":1467076740}', 'global_setting', '0');
INSERT INTO `lumonata_meta_data` VALUES ('113', 'large_gallery_width', '1024', 'gallery', '0');
INSERT INTO `lumonata_meta_data` VALUES ('114', 'large_gallery_height', '1024', 'gallery', '0');
INSERT INTO `lumonata_meta_data` VALUES ('115', 'medium_gallery_width', '700', 'gallery', '0');
INSERT INTO `lumonata_meta_data` VALUES ('116', 'medium_gallery_height', '700', 'gallery', '0');
INSERT INTO `lumonata_meta_data` VALUES ('117', 'thumb_gallery_width', '350', 'gallery', '0');
INSERT INTO `lumonata_meta_data` VALUES ('118', 'thumb_gallery_height', '350', 'gallery', '0');
INSERT INTO `lumonata_meta_data` VALUES ('119', 'crop_gallery_thumb', '0', 'gallery', '0');
INSERT INTO `lumonata_meta_data` VALUES ('120', 'large_gallery_width', '0', 'restaurant', '0');
INSERT INTO `lumonata_meta_data` VALUES ('121', 'large_gallery_height', '0', 'restaurant', '0');
INSERT INTO `lumonata_meta_data` VALUES ('122', 'medium_gallery_width', '0', 'restaurant', '0');
INSERT INTO `lumonata_meta_data` VALUES ('123', 'medium_gallery_height', '0', 'restaurant', '0');
INSERT INTO `lumonata_meta_data` VALUES ('124', 'thumb_gallery_width', '0', 'restaurant', '0');
INSERT INTO `lumonata_meta_data` VALUES ('125', 'thumb_gallery_height', '0', 'restaurant', '0');
INSERT INTO `lumonata_meta_data` VALUES ('126', 'crop_gallery_thumb', '0', 'restaurant', '0');
INSERT INTO `lumonata_meta_data` VALUES ('128', 'large_gallery_width', '0', 'special-offer', '0');
INSERT INTO `lumonata_meta_data` VALUES ('129', 'large_gallery_height', '0', 'special-offer', '0');
INSERT INTO `lumonata_meta_data` VALUES ('130', 'medium_gallery_width', '0', 'special-offer', '0');
INSERT INTO `lumonata_meta_data` VALUES ('131', 'medium_gallery_height', '0', 'special-offer', '0');
INSERT INTO `lumonata_meta_data` VALUES ('132', 'thumb_gallery_width', '0', 'special-offer', '0');
INSERT INTO `lumonata_meta_data` VALUES ('133', 'thumb_gallery_height', '0', 'special-offer', '0');
INSERT INTO `lumonata_meta_data` VALUES ('134', 'crop_gallery_thumb', '0', 'special-offer', '0');

-- ----------------------------
-- Table structure for lumonata_notifications
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_notifications`;
CREATE TABLE `lumonata_notifications` (
  `lnotification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lpost_id` bigint(20) NOT NULL,
  `lpost_owner` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `laffected_user` bigint(20) NOT NULL,
  `laction_name` varchar(50) NOT NULL,
  `laction_date` date NOT NULL,
  `lstatus` varchar(10) NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY (`lnotification_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lumonata_notifications
-- ----------------------------

-- ----------------------------
-- Table structure for lumonata_rules
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_rules`;
CREATE TABLE `lumonata_rules` (
  `lrule_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lparent` bigint(20) NOT NULL,
  `lname` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lsef` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ldescription` text CHARACTER SET utf8 NOT NULL,
  `lrule` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lgroup` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lcount` bigint(20) NOT NULL DEFAULT '0',
  `lorder` bigint(20) NOT NULL DEFAULT '1',
  `lsubsite` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT 'arunna',
  PRIMARY KEY (`lrule_id`),
  KEY `rules_name` (`lname`),
  KEY `sef` (`lsef`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_rules
-- ----------------------------
INSERT INTO `lumonata_rules` VALUES ('1', '0', 'Uncategorized', 'uncategorized', '', 'category', 'default', '73', '4', 'arunna');

-- ----------------------------
-- Table structure for lumonata_rule_relationship
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_rule_relationship`;
CREATE TABLE `lumonata_rule_relationship` (
  `lapp_id` bigint(20) NOT NULL,
  `lrule_id` bigint(20) NOT NULL,
  `lorder_id` bigint(20) NOT NULL,
  PRIMARY KEY (`lapp_id`,`lrule_id`),
  KEY `taxonomy_id` (`lrule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_rule_relationship
-- ----------------------------
INSERT INTO `lumonata_rule_relationship` VALUES ('13', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('14', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('15', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('16', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('17', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('18', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('19', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('20', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('21', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('22', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('23', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('24', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('25', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('26', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('27', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('28', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('29', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('38', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('0', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('39', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('40', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('41', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('42', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('43', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('44', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('45', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('46', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('47', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('48', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('49', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('50', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('51', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('52', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('53', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('54', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('57', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('56', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('58', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('59', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('60', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('62', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('63', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('64', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('65', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('66', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('67', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('68', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('70', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('100', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('101', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('122', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('137', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('138', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('139', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('140', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('141', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('142', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('143', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('144', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('146', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('147', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('148', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('149', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('150', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('151', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('152', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('155', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('156', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('157', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('163', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('164', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('165', '1', '0');
INSERT INTO `lumonata_rule_relationship` VALUES ('166', '1', '0');

-- ----------------------------
-- Table structure for lumonata_users
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_users`;
CREATE TABLE `lumonata_users` (
  `luser_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lusername` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ldisplay_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lpassword` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lemail` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lregistration_date` datetime NOT NULL,
  `luser_type` varchar(50) CHARACTER SET utf8 NOT NULL,
  `lactivation_key` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lavatar` varchar(200) CHARACTER SET utf8 NOT NULL,
  `lsex` int(11) NOT NULL COMMENT '1=male,2=female',
  `lbirthday` date NOT NULL,
  `lstatus` int(11) NOT NULL COMMENT '0=pendding activation, 1=active,2=blocked',
  `ldlu` datetime NOT NULL,
  PRIMARY KEY (`luser_id`),
  KEY `username` (`lusername`),
  KEY `display_name` (`ldisplay_name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of lumonata_users
-- ----------------------------
INSERT INTO `lumonata_users` VALUES ('1', 'admin', 'Administrator', 'fcea920f7412b5da7be0cf42b8c93759', 'support@lumonata.com', '0000-00-00 00:00:00', 'administrator', '', 'man-1.jpg|man-2.jpg|man-3.jpg', '1', '2011-03-19', '1', '2015-03-09 15:52:57');
