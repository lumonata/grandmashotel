<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>

    <head>

        <meta name="generator" content="HTML Tidy, see www.w3.org">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <link type="text/css" href="themes/default/css/menu-list.css" rel="stylesheet" media="screen" />

        <script type="text/javascript" src="javascript/jquery-1.1.4.js"></script>
        <script type="text/javascript" src="javascript/interface-1.2.js"></script>
        <script type="text/javascript" src="javascript/inestedsortable.js"></script>

        <title>Menus</title>
    </head>



    <body>
    <?php
        require_once('../lumonata_config.php');         
        require_once('../lumonata_settings.php');         
        require_once('../lumonata-functions/settings.php');         
        require_once('../lumonata-classes/actions.php');         
        require_once('../lumonata-functions/menus.php');         
        require_once('../lumonata-functions/user.php');

        if(is_user_logged()): ?>

        <div id="procces_alert" class="process-alert" style="">Saving...</div>
        <input type="hidden" value="<?php echo $_GET['active_tab']; ?>" id="activetab" />
        <div class="wrap">

            <?php 
                $menu_items=json_decode(get_meta_data('menu_items_'.$_GET['active_tab'],'menus'),true);            
                $menu_order=json_decode(get_meta_data('menu_order_'.$_GET['active_tab'],'menus'),true);

                ?>
                <ul id="theorder" class="page-list">           
                    <?php echo get_menu_items($menu_items,$menu_order,'theorder'); ?>
                </ul>
                <?php

                echo looping_js_nav($menu_items);            
            ?>
        </div>
        <br/>
        <script type="text/javascript">
            jQuery(document).ready(function(){
                jQuery('input.urlNum1').keyup(function(){
                    var val = jQuery(this).val();
                    jQuery(this).parent().find('input.urlNum2').val(val);
                });
         
                jQuery('#theorder').NestedSortable({         
                    accept:'page-item1',         
                    noNestingClass:'no-nesting',         
                    currentNestingClass:'current-nesting',         
                    opacity:0.8,         
                    helperclass:'helper',         
                    onChange:function(serialized){
                        jQuery.ajax({
                            type:'POST',         
                            url:'../lumonata-functions/menus.php',         
                            data:'active_tab=<?php echo $_GET['active_tab'];?>&'+serialized[0].hash         
                        });         
                    },
                    autoScroll:true,         
                    handle:'.sort-handle'         
                });
            });         
        </script>

        <?php
        
        endif; 
    ?>
   </body>
</html>



